export interface UserInfo {
  createdUsers: CreatedUsers[]
  userCreationErrors: UserCreationErrors[]
}

export interface CreatedUsers {
  username: string
  firstName: string
  lastName: string
  email: string
  emailVerified: boolean
  enabled: boolean
  credentials: Credentials[]
  realmRoles: RealmRoles[]
  clientRoles: ClientRoles[]
  attributes: Attributes[]
  id: string
}

export interface Credentials {
  temporary: boolean
  type: string
  value: string
}

export enum RealmRoles {
  User = 'user',
  Admin = 'admin',
}

export enum ClientRoles {
  HeartbeatApp = 'heartbeat-app',
}

export enum HeartbeatApp {
  Project_viewers = 'project-viewers',
  Project_creators = 'project-creators',
  Project_editors = 'project-editors',
  Project_delete = 'project-delete',
}

export interface Attributes {
  organization_id: string
}

export interface UserCreationErrors {
  errorMessage: string
}
