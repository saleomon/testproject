/* eslint-disable no-unused-vars */
// ***********************************************************
// This example support/e2e.js is processed and
// loaded automatically before your test files.
//
// This is a great place to put global configuration and
// behavior that modifies Cypress.
//
// You can change the location of this file or turn off
// automatically serving support files with the
// 'supportFile' configuration option.
//
// You can read more here:
// https://on.cypress.io/configuration
// ***********************************************************
// Import commands.js using ES2015 syntax:

import 'cypress-xpath'
import 'cypress-wait-until'
import './commands.js'
import './commands.ts'
import './outcomeCommands'
import './nodeCommands'
import 'cypress-mochawesome-reporter/register'
import 'cypress-ag-grid'
import '@4tw/cypress-drag-drop'
import registerCypressGrep from '@cypress/grep'
/* Cypress.env('grepTags', '@ui')
Cypress.env('grepTags', '@smoke')
Cypress.env('grepTags', '@validation') */
registerCypressGrep()

require('cypress-commands')

const fs = require('fs') // for file
const path = require('path') // for file path

// eslint-disable-next-line import/no-extraneous-dependencies
// const registerCypressGrep = require('cypress-grep')

// Alternatively you can use CommonJS syntax:
// require('./commands')
Cypress.on('uncaught:exception', (err, runnable) => {
  // returning false here prevents Cypress from failing the test
  return false
})
