import DiagramToolbar from '../classes/DiagramToolbar'
import DialogModal, {
  AddNewClosedQNodeDialog,
  AddNewGotoNodeDialog,
  AddNewOpenQNodeDialog,
  AddNewOutcomeNodeDialog,
  AddNewPromptNodeDialog,
  AddNewSkillRouterNodeDialog,
  AddNewSubflowNodeDialog,
  AddNewTagNodeDialog,
} from '../classes/DialogModal'

Cypress.Commands.add('waitForDiagram', (id) => {
  cy.window().then(($win) => {
    cy.wrap($win.go.Diagram.fromDiv(id)).should('not.be.null')
    cy.get('#' + id).should('be.visible')
  })
})

Cypress.Commands.add('addNodeIntoDiagram', (srcNodeSelector, targetNodeLabel) => {
  let dataTransfer = new DataTransfer()
  let targetNode_loc = null
  let targetNode_locTransformed = null
  let targetLocationX = null
  let targetLocationY = null
  let myDiagram = null

  cy.window().then(($win) => {
    cy.wait(2000)
    myDiagram = $win.go.Diagram.fromDiv('goDiagram')
    // cy.wait(500)

    if (targetNodeLabel === '+') {
      // need to remove this block
      targetNode_loc = myDiagram.findNodeForKey('Start').actualBounds.center
      targetNode_locTransformed = myDiagram.transformDocToView(targetNode_loc)
      targetLocationX = Math.round(targetNode_locTransformed.x)
      targetLocationY = Math.round(targetNode_locTransformed.y) + 120 // + scale formula  ; //related to Start node
    } else {
      targetNode_loc = myDiagram.findNodeForKey(targetNodeLabel).actualBounds.center
      targetNode_locTransformed = myDiagram.transformDocToView(targetNode_loc)
      targetLocationX = Math.round(targetNode_locTransformed.x)
      targetLocationY = Math.round(targetNode_locTransformed.y)
    }
  })

  cy.get(srcNodeSelector).then(($node) => {
    cy.wrap($node).trigger('dragstart', {
      DragEvent: {
        dataTransfer,
      },
    })
  })

  cy.get('[id="goDiagram"]')
    .find('canvas')
    .first()
    .then(($canvas) => {
      cy.wrap($canvas).trigger('dragover', {
        x: targetLocationX,
        y: targetLocationY,
        force: true,
        DragEvent: {
          force: true,
          dataTransfer,
        },
      })
    })

  cy.get(srcNodeSelector).then(($node) => {
    cy.wrap($node).trigger('dragend', {
      DragEvent: {
        force: true,
        dataTransfer,
      },
    })
  })
})

Cypress.Commands.add('addNode', (srcNodeSelector, targetNodeLabel, nodeLabel, linkLabel ) => {
    let dataTransfer = new DataTransfer();
    let targetNode_loc = null
    let targetNode_locTransformed = null
    let targetLocationX = null
    let targetLocationY = null
    let myDiagram = null

    cy.window().then($win => {

        cy.wait(2000)
        myDiagram = $win.go.Diagram.fromDiv("goDiagram")
        //cy.wait(500)

        if (targetNodeLabel === "+") { //need to remove this block
            targetNode_loc = myDiagram.findNodeForKey("Start").actualBounds.center
            targetNode_locTransformed = myDiagram.transformDocToView(targetNode_loc)
            targetLocationX = Math.round(targetNode_locTransformed.x);
            targetLocationY = Math.round(targetNode_locTransformed.y) + 120 // + scale formula  ; //related to Start node
        } else {
            targetNode_loc = myDiagram.findNodeForKey(targetNodeLabel).actualBounds.center
            targetNode_locTransformed = myDiagram.transformDocToView(targetNode_loc)
            targetLocationX = Math.round(targetNode_locTransformed.x);
            targetLocationY = Math.round(targetNode_locTransformed.y);
        }
    })

    cy.get(srcNodeSelector).then($node => {
        cy.wrap($node)
            .trigger('dragstart', {
                DragEvent: {
                    dataTransfer
                }
            })
    })

    cy.get('[id="goDiagram"]').find('canvas').first().then($canvas => {
        cy.wrap($canvas)
            .trigger('dragover', {
                x: targetLocationX,
                y: targetLocationY,
                force: true,
                DragEvent: {
                    force: true,
                    dataTransfer
                }
            })
    })

    cy.get(srcNodeSelector).then($node => {
        cy.wrap($node)
            .trigger('dragend', {
                DragEvent: {
                    force: true,
                    dataTransfer
                }
            })
    })

    cy.get('[placeholder="Enter unique Node Label."]').clear().type(nodeLabel)
    cy.get('[placeholder="Enter unique Link Label."]').clear().type(linkLabel)
    cy.get('.ant-modal-content .ant-btn-primary').click() 
})

//selects (clicks) node by label on diagram
Cypress.Commands.add('clickNode', (targetNodeLabel) => {
  let targetNode_loc = null
  let targetNode_locTransformed = null
  let targetLocationX = null
  let targetLocationY = null
  // let myDiagram = null

  cy.window().then(($win) => {
    cy.wait(1000)
    cy.wrap($win.go.Diagram.fromDiv('goDiagram')).should('exist').should('not.be.null')
    // cy.wait(2000)

    targetNode_loc = $win.go.Diagram.fromDiv('goDiagram').findNodeForKey(targetNodeLabel).actualBounds.center
    targetNode_locTransformed = $win.go.Diagram.fromDiv('goDiagram').transformDocToView(targetNode_loc)
    targetLocationX = Math.round(targetNode_locTransformed.x)
    targetLocationY = Math.round(targetNode_locTransformed.y)
  })

  cy.get('[id="goDiagram"]')
    .find('canvas')
    .first()
    .then(($canvas) => {
      cy.wrap($canvas).click(targetLocationX, targetLocationY)
    })
})

Cypress.Commands.add('dblClickNode', (targetNodeLabel) => {
  let targetNode_loc = null
  let targetNode_locTransformed = null
  let targetLocationX = null
  let targetLocationY = null
  let myDiagram = null

  cy.window().then(($win) => {
    cy.wrap($win.go.Diagram.fromDiv('goDiagram')).should('exist')
    myDiagram = $win.go.Diagram.fromDiv('goDiagram')
    cy.wait(1000)

    targetNode_loc = myDiagram.findNodeForKey(targetNodeLabel).actualBounds.center
    targetNode_locTransformed = myDiagram.transformDocToView(targetNode_loc)
    targetLocationX = Math.round(targetNode_locTransformed.x)
    targetLocationY = Math.round(targetNode_locTransformed.y)
  })

  cy.get('[id="goDiagram"]')
    .find('canvas')
    .first()
    .then(($canvas) => {
      cy.wrap($canvas).dblclick(targetLocationX, targetLocationY)
    })
})

Cypress.Commands.add('moveNode', (nodeKey, changeX, changeY) => {
  let myRobot = null
  let myDiagram = null
  cy.window().then((win) => {
    const scr = win.document.createElement('script')
    scr.src = 'https://unpkg.com/gojs/extensions/Robot.js'
    win.document.body.appendChild(scr)
  })

  // make sure it's loaded
  cy.window().should('have.property', 'Robot')

  cy.window().then((win) => {
    myDiagram = win.go.Diagram.fromDiv(win.document.getElementById('goDiagram'))
    myRobot = new win.Robot(myDiagram)
    let node = myDiagram.findNodeForKey(nodeKey)
    let loc = node.actualBounds.center
    myRobot.mouseDown(loc.x, loc.y, 0)
    myRobot.mouseMove(loc.x + changeX, loc.y + changeY, 150)
    myRobot.mouseUp(loc.x + changeX, loc.y + changeY, 150)
  })
})

Cypress.Commands.add('linkNodes', (srcNodeLabel, targetNodeLabel) => {
  let myRobot = null
  let myDiagram = null
  cy.window().then((win) => {
    const scr = win.document.createElement('script')
    scr.src = 'https://unpkg.com/gojs/extensions/Robot.js'
    win.document.body.appendChild(scr)
  })
  // make sure it's loaded
  cy.window().should('have.property', 'Robot')
  cy.window().then((win) => {
    myDiagram = win.go.Diagram.fromDiv(win.document.getElementById('goDiagram'))
    // find add node location out of src node
    let collection = myDiagram.findNodeForKey(srcNodeLabel).findNodesOutOf()
    let it = collection.iterator
    it.next()
    expect(it.value.data.type).equals('addPoint')
    let addSrcNodeLoc = it.value.actualBounds.center
    // find location of target node
    let targetNode = myDiagram.findNodeForKey(targetNodeLabel)
    let targetNodeLoc = targetNode.actualBounds.center
    // link nodes
    myRobot = new win.Robot(myDiagram)
    myRobot.mouseDown(addSrcNodeLoc.x, addSrcNodeLoc.y, 0)
    myRobot.mouseMove(targetNodeLoc.x, targetNodeLoc.y, 1000)
    myRobot.mouseUp(targetNodeLoc.x, targetNodeLoc.y, 0)
  })
})

Cypress.Commands.add('getNodeLocation', (nodeKey) => {
  let myDiagram = null
  let loc
  cy.window().should('have.property', 'go')
  cy.window().then((win) => {
    myDiagram = win.go.Diagram.fromDiv(win.document.getElementById('goDiagram'))
    loc = myDiagram.findNodeForKey(nodeKey).actualBounds.center
    return loc
  })
})

Cypress.Commands.add('findNodesOutOf', (nodeKey) => {
  // :data array
  cy.window().then(($win) => {
    let array = []
    cy.wrap($win.go.Diagram.fromDiv('goDiagram'))
      .should('exist')
      .then(($diagram) => {
        let collection = $diagram.findNodeForKey(nodeKey).findNodesOutOf()
        let it = collection.iterator
        while (it.next()) {
          array.push(it.value.data)
        }
        cy.log('NODES-OUT-OF COUNT' + array.length)
      })
    return cy.wrap(array)
  })
})

Cypress.Commands.add('findNodesInto', (nodeKey) => {
  // :data array
  cy.window().then(($win) => {
    let array = []
    cy.wrap($win.go.Diagram.fromDiv('goDiagram'))
      .should('exist')
      .then(($diagram) => {
        let collection = $diagram.findNodeForKey(nodeKey).findNodesInto()
        let it = collection.iterator
        while (it.next()) {
          array.push(it.value.data)
        }
        cy.log('NODES-INTO COUNT' + array.length)
      })
    return cy.wrap(array)
  })
})

Cypress.Commands.add('findNodeForData', (nodeData) => {
  // :object
  cy.window().then(($win) => {
    cy.wrap($win.go.Diagram.fromDiv('goDiagram'))
      .should('exist')
      .then(($diagram) => {
        let node = $diagram.findNodeForData(nodeData)
        return cy.wrap(node)
      })
  })
})

Cypress.Commands.add('findNodeForKey', (nodeKey) => {
  // :object
  cy.window().then(($win) => {
    cy.wrap($win.go.Diagram.fromDiv('goDiagram'))
      .should('exist')
      .then(($diagram) => {
        let node = $diagram.findNodeForKey(nodeKey)
        return cy.wrap(node)
      })
  })
})

Cypress.Commands.add('findLinksOutOf', (nodeKey) => {
  // :data array
  cy.window().then(($win) => {
    let array = []
    cy.wrap($win.go.Diagram.fromDiv('goDiagram'))
      .should('exist')
      .then(($diagram) => {
        let collection = $diagram.findNodeForKey(nodeKey).findLinksOutOf()
        let it = collection.iterator
        while (it.next()) {
          array.push(it.value.data)
        }
        cy.log('LINKS-OUT-OF COUNT' + array.length)
      })
    return cy.wrap(array)
  })
})

Cypress.Commands.add('findLinksInto', (nodeKey) => {
  // :data array
  cy.window().then(($win) => {
    let array = []
    cy.wrap($win.go.Diagram.fromDiv('goDiagram'))
      .should('exist')
      .then(($diagram) => {
        let collection = $diagram.findNodeForKey(nodeKey).findLinksInto()
        let it = collection.iterator
        while (it.next()) {
          array.push(it.value.data)
        }
        cy.log('LINKS-INTO COUNT' + array.length)
      })
    return cy.wrap(array)
  })
})

Cypress.Commands.add('getChildNodeByLinkText', (parentKey, linkText) => {
  cy.window().then(($win) => {
    cy.wrap($win.go.Diagram.fromDiv('goDiagram'))
      .should('exist')
      .then(($diagram) => {
        let collection = $diagram.findNodeForKey(parentKey).findLinksOutOf()
        let it = collection.iterator
        while (it.next()) {
          if (it.value.data.text === linkText) {
            let linkTo = it.value.data.to
            return $diagram.findNodeForKey(linkTo)
          }
        }
      })
  })
})

Cypress.Commands.add('fillSubflowNode', (obj) => {
  let subflow = new AddNewSubflowNodeDialog()
  subflow.labelInput().type(obj.nodeLabel)
  subflow.descriptionInput().clear().type(obj.nodeDescription)
  subflow.linkLabelInput().clear().type(obj.linkLabel)
  subflow.linkDescriptionInput().clear().type(obj.linkDescription)
  if (obj.channels.length !== 0) {
    cy.get('.ant-select-selection__choice__remove').then(($remove) => {
      for (let i = 0; i < $remove.length; i++) {
        cy.get('.ant-select-selection__choice__remove').then(($remove) => {
          $remove.click()
          cy.wait(500)
        })
      }
    })

    for (let i = 0; i < obj.channels.length; i++) {
      subflow.channelComboBox().type(obj.channels[i])
      cy.get('li.ant-select-dropdown-menu-item.ant-select-dropdown-menu-item-active')
        .contains(obj.channels[i])
        .should('be.visible')
        .click()
    }
  }
})

Cypress.Commands.add('fillSkillsRouterNode', (obj) => {
  let skillrouter = new AddNewSkillRouterNodeDialog()
  skillrouter.labelInput().type(obj.nodeLabel)
  skillrouter.descriptionInput().clear().type(obj.nodeDescription)
  skillrouter.linkLabelInput().clear().type(obj.linkLabel)
  skillrouter.linkDescriptionInput().clear().type(obj.linkDescription)
  if (obj.skills.length !== 0) {
    // if skills are defined in json
    for (let i = 0; i < obj.skills.length; i++) {
      skillrouter.skillsComboBox().type(obj.skills[i])
      cy.get('li.ant-select-dropdown-menu-item.ant-select-dropdown-menu-item-active')
        .contains(obj.skills[i])
        .should('be.visible')
        .click()
    }
  }
})

Cypress.Commands.add('fillTagNode', (obj) => {
  let tag = new AddNewTagNodeDialog()
  tag.labelInput().type(obj.nodeLabel)
  tag.descriptionInput().clear().type(obj.nodeDescription)
  tag.linkLabelInput().clear().type(obj.linkLabel)
  tag.linkDescriptionInput().clear().type(obj.linkDescription)
})

Cypress.Commands.add('fillOutcomeNode', (obj) => {
  let outcome = new AddNewOutcomeNodeDialog()
  outcome.endpointLinkLabelInput().type(obj.endpointLabel)
  outcome.linkTypeCombobox().click()
  cy.get('li').contains(obj.linkType).should('be.visible').click()
  outcome.reasonInput().type(obj.reason)
})

Cypress.Commands.add('fillGotoNode', (obj) => {
  let goto = new AddNewGotoNodeDialog()
  goto.linkLabelInput().type(obj.linkLabel)
  goto.linkDescriptionInput().clear().type(obj.linkDescription)
  goto.linkTypeCombobox().click()
  cy.get('li').contains(obj.linkType).should('be.visible').click()
  goto.linkToNodeCombobox().click()
  cy.get('li').contains(obj.linkToNode).should('be.visible').click()
})

Cypress.Commands.add('fillPromptNode', (obj) => {
  let prompt = new AddNewPromptNodeDialog()
  prompt.labelInput().type(obj.nodeLabel)
  prompt.descriptionInput().clear().type(obj.nodeDescription)
  if (obj.template !== '') {
    prompt.templateCombobox().click()
    cy.get('li').contains(obj.template).should('be.visible').click()
  }
  prompt.defaultPromptInput().clear().type(obj.defaultPrompt)
  if (obj.linkLabel !== '') {
    prompt.linkLabelInput().clear().type(obj.linkLabel)
  }
  prompt.linkDescriptionInput().clear().type(obj.linkDescription)
})

Cypress.Commands.add('fillOpenQuestionNode', (obj) => {
  let openq = new AddNewOpenQNodeDialog()
  openq.labelInput().type(obj.nodeLabel)
  openq.descriptionInput().type(obj.nodeDescription)
  if (obj.template !== '') {
    openq.templateCombobox().click()
    cy.get('li').contains(obj.template).should('be.visible').click()
  }
  openq.defaultOpenQuestionInput().clear().type(obj.defaultOpenQuestion)
  if (obj.linkLabel !== '') {
    openq.linkLabelInput().clear().type(obj.linkLabel)
  }
  openq.linkDescriptionInput().clear().type(obj.linkDescription)
})

Cypress.Commands.add('fillClosedQuestionNode', (obj) => {
  let closedq = new AddNewClosedQNodeDialog()
  closedq.labelInput().type(obj.nodeLabel)
  closedq.descriptionInput().type(obj.nodeDescription)
  if (obj.template !== '') {
    closedq.templateCombobox().click()
    cy.get('li').contains(obj.template).should('be.visible').click()
  }
  closedq.defaultClosedQuestionInput().clear().type(obj.defaultClosedQuestion)
  if (obj.linkLabel !== '') {
    closedq.linkLabelInput().clear().type(obj.linkLabel)
  }
  closedq.linkDescriptionInput().clear().type(obj.linkDescription)
})

Cypress.Commands.add('deleteSelectedNode', () => {
  let toolbar = new DiagramToolbar()
  toolbar.deleteNodeButton().click()
  cy.get('.ant-popover-placement-top')
    .should('be.visible')
    .then(() => {
      cy.get('span')
        .contains('Yes')
        .then(($yesButton) => {
          $yesButton.click()
          cy.get('.ant-message').contains('Delete successful')
          cy.wait(3000)
        })
    })
})

Cypress.Commands.add('linkOffSelectedNode', () => {
  let toolbar = new DiagramToolbar()
  toolbar.cutLinkToParentButton().click()
  cy.wait(3000)
})

// gets outcomes and endpoints by text on PalleteBox
// ---------usage: cy.getOutcomeNodeByText('Escalate')

Cypress.Commands.add('getOutcomeNodeByText', (text) => {
  if (text === 'Fallback' || text === 'Escalate') {
    cy.contains(text)
      .parent()
      .parent()
      .parent()
      .then(($outcomeNode) => {
        return $outcomeNode
      })
  } else {
    cy.contains(text)
      .parent()
      .parent()
      .then(($endpointNode) => {
        return $endpointNode
      })
  }
})
