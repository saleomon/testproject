import { TagPopUpInput } from '../../types/input/TagPopUpInput'
import AddNewTagNodePopUp from '../../classes/popups/AddNewTagNodePopUp'

declare global {
  namespace Cypress {
    interface Chainable {
      fillTagPopUp: typeof fillTagPopUp
    }
  }
}

export default function fillTagPopUp(input: TagPopUpInput) {
  AddNewTagNodePopUp.isOpen()
    .setLabel(input.nodeLabel)
    .setDescription(input.nodeDescription)
    .setLink(input.linkLabel)
    .setLinkDescription(input.linkDescription)
}
