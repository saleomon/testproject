import { ProjectInfo } from '../../types/response/ProjectInfo'

export default function createProjectFail(projectJson: ProjectInfo) {
  let project = projectJson
  project.label = project.label + '_' + Date.now()
  cy.loginAPI()
  cy.request({
    method: 'POST',
    url: Cypress.env('baseUrlAPI') + '/api/projects/import',
    failOnStatusCode: false,
    headers: {
      Authorization: 'Bearer ' + Cypress.env('token'),
    },
    body: project,
  })
}
