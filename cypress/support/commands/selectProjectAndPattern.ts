import FilterBar from '../../classes/bars/FilterBar'
import NavigationBar from '../../classes/navigation/NavigationBar'

declare global {
  namespace Cypress {
    interface Chainable {
      selectProjectAndPattern: typeof selectProjectAndPattern
    }
  }
}

export default function selectProjectAndPattern(project: string, pattern: string): void {
  NavigationBar.selectProject(project)
  cy.get('div').contains(project).should('be.visible')
  cy.get('.topLink').contains('Patterns').should('be.visible').click()
  cy.url().should('contain', '/patterns')
  FilterBar.selectPattern(pattern)
}
