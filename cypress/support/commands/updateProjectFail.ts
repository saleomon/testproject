import { ProjectInfo } from '../../types/response/ProjectInfo'

export default function updateProjectFail(projectJson: ProjectInfo) {
  let project = projectJson
  project.label = project.label + '_' + Date.now()
  cy.loginAPI()
  cy.request({
    method: 'PUT',
    url: Cypress.env('baseUrlAPI') + '/api/projects/import' + Cypress.env('projectId'),
    failOnStatusCode: false,
    headers: {
      Authorization: 'Bearer ' + Cypress.env('token'),
    },
    body: project,
  })
}
