import FilterBar from '../../classes/bars/FilterBar'
import NavigationBar from '../../classes/navigation/NavigationBar'

declare global {
  namespace Cypress {
    interface Chainable {
      selectNewProject: typeof selectNewProject
    }
  }
}

export default function selectNewProject(project: string): void {
  NavigationBar.selectProject(project)
  cy.get('div').contains(project).should('be.visible')
  cy.get('.topLink').contains('Patterns').should('be.visible').click()
}
