import { ProjectInfo } from '../../types/response/ProjectInfo'

declare global {
  namespace Cypress {
    interface Chainable {
      precondition: typeof precondition
    }
  }
}

export default function precondition(preconditionId: string, pattern: string = preconditionId) {
  cy.fixture('projects-templates/' + preconditionId + '.json').as('project')

  cy.get<ProjectInfo>('@project').then((json: ProjectInfo) => {
    cy.loginAPI()
    cy.createNewProject(json).as('current-project')
    cy.loginUI()
  })

  cy.get<ProjectInfo>('@current-project').then((projectInfo: ProjectInfo) => {
    cy.selectProjectAndPattern(projectInfo.label, pattern)
    Cypress.env('projectId', projectInfo.id)
  })
}
