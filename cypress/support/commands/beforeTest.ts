declare global {
  namespace Cypress {
    interface Chainable {
      beforeTest: typeof beforeTest
    }
  }
}

export default function beforeTest(projectPath: string, pattern: string): void {
  cy.loginAPI()
  cy.fixture(projectPath).then(($json) => {
    cy.createNewProject($json).then(($projectInfo) => {
      cy.loginUI(Cypress.env('username'), Cypress.env('password'))
      cy.selectProjectAndPattern($projectInfo.label, pattern)
      Cypress.env('projectName', $projectInfo.label)
      Cypress.env('projectId', $projectInfo.id)
    })
  })
}
