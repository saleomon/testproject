declare global {
  namespace Cypress {
    interface Chainable {
      sendKeys(text: string, opt?: Partial<Cypress.TypeOptions>): Cypress.Chainable
    }
  }
}

export default function sendKeys(
  subject: JQuery<HTMLInputElement>,
  text: string,
  opt?: Partial<Cypress.TypeOptions>,
): Cypress.Chainable {
  return cy
    .wrap(subject)
    .filter(':visible')
    .clear()
    .then(($el) => text === '' || cy.wrap($el).type(text, opt))
}
