declare global {
  namespace Cypress {
    interface Chainable {
      selectCustom: typeof selectCustom
    }
  }
}

export default function selectCustom(selectName: string, optionValue: string): void {
  cy.get(selectName).parent().next().find('[role="combobox"]').filter(':visible').click()
  cy.get(selectName)
    .filter(':visible')
    .parent()
    .next()
    .find('[role="combobox"]')
    .then(($combobox) => {
      cy.wrap($combobox)
        .attribute('aria-controls')
        .then(($id) => {
          cy.get('#' + $id)
            .find('ul>li')
            .should('be.visible')
            .contains(optionValue)
            .click()
        })
    })
}
