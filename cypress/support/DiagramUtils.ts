import { Diagram, Key, Node, Point } from 'gojs'

class DiagramUtils {
  // @ts-ignore
  private diagram: Diagram

  findDiagram(id: string = 'goDiagram'): Cypress.Chainable<Diagram> {
   cy.wait(1000)
    return cy.window().then(($win) => {
      return cy
        .waitUntil(() => !Cypress._.isEmpty($win.go.Diagram.fromDiv(id)), {
          interval: 100,
          timeout: 4000,
          errorMsg: 'Diagram is not initialize',
          // verbose: true,
        })
        .then(() => {
          this.diagram = $win.go.Diagram.fromDiv(id) as Diagram

          return this.diagram
        })
    })
  }

  findNodeByKey(nodeKey: Key): Cypress.Chainable<Node> {
    return cy
      .waitUntil(() => !Cypress._.isEmpty(this.diagram.findNodeForKey(nodeKey)), {
        interval: 200,
        timeout: 4000,
        errorMsg: 'Node is not initialize',
        // verbose: true,
      })
      .then(() => cy.wrap(this.diagram.findNodeForKey(nodeKey) as Node))
  }

  findNodeWithLocByKey(
    nodeKey: Key,
  ): Cypress.Chainable<{ $node: Node; $nodeLoc: Point; $nodeLocTf: Point; $locX: number; $locY: number }> {
    return this.findNodeByKey(nodeKey).then(($node) => {
      this.diagram.zoomToFit()

      let nodeLoc = $node.actualBounds.center
      let nodeLocTf = this.diagram.transformDocToView(nodeLoc)

      let locX = Math.round(nodeLocTf.x)
      let locY = Math.round(nodeLocTf.y)

      return cy.wrap({ $node, $nodeLoc: nodeLoc, $nodeLocTf: nodeLocTf, $locX: locX, $locY: locY })
    })
  }

  addNodeInDiagram(nodeSelector: string, nodeTarget: string = 'Start', plus: boolean = false) {
    let dataTransfer = new DataTransfer()
    let locX: number
    let locY: number

    this.findNodeWithLocByKey(nodeTarget).then(({ $locX, $locY }) => {
      locX = $locX
      locY = plus ? $locY + 120 : $locY
    })

    cy.get(nodeSelector).then(($node) => {
      cy.wrap($node).trigger('dragstart', {
        DragEvent: {
          dataTransfer,
        },
      })
    })

    cy.get('[id="goDiagram"]')
      .find('canvas')
      .first()
      .then(($canvas) => {
        cy.wrap($canvas).trigger('dragover', {
          x: locX,
          y: locY,
          force: true,
          DragEvent: {
            force: true,
            dataTransfer,
          },
        })
      })

    cy.get(nodeSelector).then(($node) => {
      cy.wrap($node).trigger('dragend', {
        DragEvent: {
          force: true,
          dataTransfer,
        },
      })
    })
  }

  findChildNodeByLinkText(parentKey: string, linkText: string): Cypress.Chainable<Node> {
    return this.findNodeByKey(parentKey).then(($node) => {
      cy.waitUntil(
        () => {
          let collection = $node.findLinksOutOf()
          let it = collection.iterator
          let list = []
          while (it.next()) {
            cy.log('Link of node', it.value)
            if (it.value.data.text === linkText) {
              list.push(true)
            } else {
              list.push(false)
            }
          }
          return list.find((value) => value === true)
        },
        {
          interval: 100,
          timeout: 4000,
          errorMsg: 'Child node is not found',
          // verbose: true,
        },
      )
      let node
      let collection = $node.findLinksOutOf()
      let it = collection.iterator
      while (it.next()) {
        if (it.value.data.text === linkText) {
          let linkTo = it.value.data.to
          this.findNodeByKey(linkTo).then(($it) => {
            node = $it
          })
        }
      }
      return node
    })
  }

  click(nodeLabel: string, opt: Partial<Cypress.ClickOptions> = {}) {
    let locX: number
    let locY: number

    opt.force = true

    this.findNodeWithLocByKey(nodeLabel).then(({ $locX, $locY }) => {
      locX = $locX
      locY = $locY
    })

    cy.get('[id="goDiagram"]')
      .find('canvas')
      .first()
      .then(($canvas) => {
        cy.wrap($canvas).click(locX, locY, opt)
      })
  }

  doubleClick(nodeLabel: string, opt: Partial<Cypress.ClickOptions> = {}) {
    let locX: number
    let locY: number

    opt.force = true

    this.findNodeWithLocByKey(nodeLabel).then(({ $locX, $locY }) => {
      locX = $locX
      locY = $locY
    })

    cy.get('[id="goDiagram"]')
      .find('canvas')
      .first()
      .then(($canvas) => {
        cy.wrap($canvas).dblclick(locX, locY, opt)
      })
  }

  checkBackgroundHeaderColor(r: any, g: any, b: any, a: any) {
    cy.get('.channelOverlay').should('have.attr', 'style', `background-color: rgba(${r}, ${g}, ${b}, ${a});`)
  }

  checkTextHeaderColor(text: string, r: any, g: any, b: any, a: any) {
    cy.get('.channelText').should('have.text', text).and('have.css', 'color', `rgba(${r}, ${g}, ${b}, ${a})`)
  }

  checkChannelText(text: string) {
    cy.get('.channelText').should('have.text', text)
  }

  checkAlertMessage(text: string) {
    cy.get('.ant-message-notice-content').should('contain', text)
  }

  isDiagramEmpty() {
    return cy.waitUntil(() => this.diagram.findNodesByExample({}).count === 0, {
      interval: 100,
      timeout: 4000,
      errorMsg: 'Diagram is not initialized',
    })
  }

  checkNodesId(nodeLabel: string): Cypress.Chainable <boolean> {
    return this.findNodeByKey(nodeLabel).then(($node) => $node.data.data.id)
  }

  checkNodesFlowId(nodeLabel: string, flowNumber: string): Cypress.Chainable <boolean> {
    return this.findNodeByKey(nodeLabel).then(($node) => $node.data.data.flow[flowNumber].id)
  }

  checkNodesInputId(nodeLabel: string, inputnumber: string): Cypress.Chainable <boolean> {
    return this.findNodeByKey(nodeLabel).then(($node) => $node.data.data.input[inputnumber].id)
  }
  checkNodesOutputId(nodeLabel: string, outputnumber: string): Cypress.Chainable <boolean> {
    return this.findNodeByKey(nodeLabel).then(($node) => $node.data.data.output[outputnumber].id)
  }
}

export default new DiagramUtils()
