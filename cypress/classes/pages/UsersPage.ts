export class UsersPage {
  constructor(
    private projectsTabSel = () => cy.xpath('//a[@href="/control-panel/projects"" and contains(text(), "Projects")]'),
    private activeProjectsTabSel = () =>
      cy.xpath('//a[@class="topLink router-link-exact-active router-link-active" and contains(text(), "Projects")]'),
    private usersTabSel = () => cy.xpath('//a[@href="/control-panel/users" and contains(text(), "Users")]'),
    private activeUsersTabSel = () =>
      cy.xpath('//a[@class="topLink router-link-exact-active router-link-active" and contains(text(), "Users")]'),
    private addUserBtnSel = () => cy.xpath('//button[@id="createNewUserButton"]'),
    private userSel = () => cy.xpath('//div[@role="row"]'),
    private userNameLabelSel = () => cy.xpath('//span[@class="ag-header-cell-text" and contains(text(), "User Name")]'),
    private createNewProjectSel = 'button[id="createNewProjectButton"]',
    private selectProject = (text: string) => cy.xpath(`//div[text()='${text}']`),
  ) {}

  openProjects() {
    this.projectsTabSel().should('be.visible').click()

    return this
  }

  isProjectsTabOpened() {
    this.activeProjectsTabSel().should('be.visible')

    return this
  }

  openUsers() {
    this.usersTabSel().should('be.visible').click()

    return this
  }

  isUsersTabOpened() {
    this.activeUsersTabSel().should('be.visible')

    return this
  }

  clickOnUsernameLabel() {
    this.userNameLabelSel().should('be.visible').click()

    return this
  }

  clickOnAddUserBtn() {
    this.addUserBtnSel().should('be.visible').click()

    return this
  }

  isUserDisplayed(text: string) {
    this.userSel().should('contain.text', text)

    return this
  }

  isUserNotDisplayed(text: string) {
    this.userSel().should('not.contain.text', text)

    return this
  }

  clickCreateNewProject(opt?: Partial<Cypress.TypeOptions>) {
    cy.get(this.createNewProjectSel).should('be.visible').click(opt)

    return this
  }

  clickNameProject(text: string, opt?: Partial<Cypress.TypeOptions>) {
    this.selectProject(text).should('be.visible').click(opt)

    return this
  }

  isNotExitsProject(text: string) {
    this.selectProject(text).should('not.exist')

    return this
  }

  checkNameProject(text: string) {
    this.selectProject(text).should('be.visible')

    return this
  }

  checkDescriptionProject(text: string) {
    this.selectProject(text).should('exist')

    return this
  }
}

export default new UsersPage()
