class DesignerPage {
  constructor(private outcomesSel = (text: string) => cy.xpath(`//div[@class="boxDiv"]//b[text()="${text}"]`)) {}

  checkOutcomes(outcomes: string) {
    this.outcomesSel(outcomes).should('exist')

    return this
  }
}

export default new DesignerPage()
