class LoginPage {
  constructor(
    private usernameSel = () => cy.get('[autocomplete="username"]'),
    private passwordSel = () => cy.get('[autocomplete="password"]'),
    private loginBtnSel = () => cy.get('.ant-btn'),
    private errAlertSel = () => cy.get('.ant-message-error'),
  ) {}

  open(): LoginPage {
    cy.visit('/')

    this.loginBtnSel().should('be.visible')

    return this
  }

  setUsername(username: string, opt?: Partial<Cypress.TypeOptions>): LoginPage {
    this.usernameSel().sendKeys(username, opt)

    return this
  }

  isUsernamePlaceholderDisplayed(text: string) {
    this.usernameSel().invoke('attr', 'placeholder').should('eql', text)

    return this
  }

  isUsernameSet(text: string) {
    this.usernameSel().should('have.value', text)

    return this
  }

  setPassword(password: string, opt?: Partial<Cypress.TypeOptions>): LoginPage {
    this.passwordSel().sendKeys(password, opt)

    return this
  }

  clickOnLoginButton(opt?: Partial<Cypress.ClickOptions>): LoginPage {
    this.loginBtnSel().click(opt)

    return this
  }

  isClickableLoginButton(): LoginPage {
    this.loginBtnSel().should('not.be.disabled')

    return this
  }

  isNotClickableLoginButton(): LoginPage {
    this.loginBtnSel().should('be.disabled')

    return this
  }

  getErrAlert(): Cypress.Chainable<JQuery<HTMLElement>> {
    return this.errAlertSel()
  }

  isErrAlert(text: string): LoginPage {
    this.errAlertSel().then(($el) => {
      expect($el.text()).equals(text)
    })

    return this
  }

  isErrAlertCount(count: number): LoginPage {
    this.errAlertSel().should('have.length', count)

    return this
  }
}

export default new LoginPage()
