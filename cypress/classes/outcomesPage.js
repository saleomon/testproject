const contentsOutcomes = require('../fixtures/data/outcomes_data/outcomesTestData')

const OutcomesPageElements = {
  // methods
  getRow() {
    return cy
      .get(OutcomesPageElements.leftContainer)
      .contains(contentsOutcomes.projectedOutcomeLbl)
      .parent()
      .parent()
      .parent()
      .attribute('row-id')
  },

  getLastRow() {
    return cy.get(OutcomesPageElements.outcomeLeftContainer).children().last().find('b')
  },

  checkOutcomeLabelUnique() {
    return cy.get(OutcomesPageElements.outcomeNameValidationMsg).should('have.text', 'Label is not unique!')
  },

  checkNoRowsMsg(table, message) {
    cy.get(table).find(OutcomesPageElements.noRowsMsg).should('exist').and('have.text', message)
  },

  checkDefaultValue(select, value) {
    return cy.get(select).parent().next().find('.ant-select-selection-selected-value').should('have.text', value)
  },

  getDeleteBtn(table, name) {
    return cy.get(table).find(OutcomesPageElements.outcomeRow).contains(name).next(OutcomesPageElements.deleteBtnTable)
  },

  getEditBtn(table, name) {
    // cy.get(table).find('[ref="eBodyViewport"]').scrollTo('bottom')
    return cy
      .get(table)
      .find('[ref="eLeftContainer"]')
      .find('[role="row"]')
      .contains(name)
      .parent()
      .children(OutcomesPageElements.editBtnTable)
  },

  checkValidationError(label, message, field, input, flag) {
    if (input.length > 0) {
      cy.get(field).filter(':visible').clear({ force: true }).type(input)
      cy.get('.ant-popover-title').filter(':visible').click()
    } else {
      cy.get(field).should('be.visible').clear()
      cy.get('.ant-popover-title').filter(':visible').click()
    }
    if (flag == true) {
      cy.get(label)
        .filter(':visible')
        .parent()
        .siblings()
        .find(this.validationMsg)
        .should('exist')
        .and('have.text', message)
      cy.get(label).filter(':visible').parent().siblings().find(this.redHighlighted).should('exist')
    } else {
      cy.get(label).filter(':visible').parent().siblings().find(this.validationMsg).should('not.exist')
      cy.get(label).filter(':visible').parent().siblings().find(this.redHighlighted).should('not.exist')
    }
  },

  checkValidationErrorSelect(label, message) {
    cy.get(label)
      .filter(':visible')
      .parent()
      .siblings()
      .find(this.validationMsg)
      .should('exist')
      .and('have.text', message)
    cy.get(label).filter(':visible').parent().siblings().find(this.redHighlighted).should('exist')
  },

  checkSkillOutcomesNames(skillOutcomesNames) {
    cy.get(OutcomesPageElements.skillPatternTable)
      .find(OutcomesPageElements.leftContainer)
      .contains(contentsOutcomes.skillPatternName)
      .parent()
      .parent()
      .parent()
      .attribute('row-id')
      .then(($attr) => {
        const rowNumber = $attr
        cy.get(OutcomesPageElements.skillPatternTable)
          .find('[ref="eCenterContainer"]')
          .find('[role="row"]')
          .eq(rowNumber)
          .then(($trow) => {
            expect($trow.find('[col-id="outcomes"]').text()).to.contain(skillOutcomesNames)
          })
      })
  },

  checkNodeCount(table, name, count) {
    cy.get(table)
      .find(OutcomesPageElements.leftContainer)
      .contains(name)
      .parent()
      .parent()
      .parent()
      .attribute('row-id')
      .then(($attr) => {
        const rowNumber = $attr
        cy.get(table)
          .find(OutcomesPageElements.centerContainer)
          .find('[role="row"]')
          .eq(rowNumber)
          .then(($trow) => {
            expect($trow.find(OutcomesPageElements.patternNodesRow).text()).to.equal(count)
          })
      })
  },

  fillGlobalPattern(patternName, patternType, patternDescription, patternChannels, patternOutcomes) {
    cy.get(OutcomesPageElements.globalPatternNameField)
      .should('be.visible')
      .filter(':visible')
      .clear()
      .type(patternName)
    cy.selectCustom(OutcomesPageElements.patternTypeLbl, patternType)
    cy.get(OutcomesPageElements.globalPatternDescriptionField).filter(':visible').clear().type(patternDescription)
    cy.multiselect(OutcomesPageElements.patternChannelsLbl, patternChannels)
    cy.multiselect(OutcomesPageElements.patternOutcomesLbl, patternOutcomes)
  },

  fillGlobalPatternObj(patternObj) {
    cy.get(OutcomesPageElements.globalPatternNameField).last().clear({ force: true }).type(patternObj.patternName)
    cy.selectCustom(OutcomesPageElements.patternTypeLbl, patternObj.patternType)
    cy.get(OutcomesPageElements.globalPatternDescriptionField).clear().type(patternObj.patternDescription)
    cy.multiselect(OutcomesPageElements.patternChannelsLbl, patternObj.patternChannels)
    cy.multiselect(OutcomesPageElements.patternOutcomesLbl, patternObj.patternOutcomes)
  },

  checkSearchMultiselect(multiselect, input, searchResult) {
    cy.get(multiselect).filter(':visible').type(input)
    cy.get('.ant-select-dropdown-placement-bottomLeft')
      .filter(':visible')
      .find('li.ant-select-dropdown-menu-item')
      .should(($els) => {
        // map elements to array of their innerText
        let elsText = $els.toArray().map((el) => el.innerText)
        expect(elsText).to.deep.eq(searchResult)
      })
    cy.get('.ant-popover-title').click()
    /*  cy.get(multiselect).parent().parent().find('[data-cy="patternChannelField"]').then($elem => {
      cy.wrap($elem).find('[role="presentation"]').should('not.exist')
    }) */
  },

  checksearchNoData(multiselect, input) {
    cy.get(multiselect).filter(':visible').type(input)
    cy.get('.ant-empty-image').should('exist')
    cy.get('.ant-popover-title').click()
    cy.get('.ant-empty-image').should('not.be.visible')
  },

  // global selectors
  leftContainer: '[ref="eLeftContainer"]',
  centerContainer: '[ref="eCenterContainer"]',
  outcomeRow: '[role="row"]',
  editBtnTable: '.editButton',
  deleteBtnTable: '*[class^="deleteButton"]',
  toastMsg: '.ant-message-success',
  validationMsg: '.ant-form-explain',
  patternTable: '#useCasesView',
  projectedOutcomesTable: '#metricsProjected',
  skillPatternTable: '#skillsGridWrapper',
  skillOutcomeTable: '#skillsOutcomeGridWrapper',
  checkIcon: '[aria-label="icon: check"]',
  redHighlighted: '.ant-form-item-control.has-error',
  requiredField: '.ant-form-item-required',

  // modal create/edit outcome selectors
  openCreateNewProjectedOutcomeBtn: '#addProjectedButton',
  outcomeNameLabel: '[title="Outcome Name"]',
  outcomePercentageLabel: '[title="Percentage"]',
  outcomeNameField: '[data-cy="outcomeLabelField"]',
  outcomeDescriptionField: '[placeholder="Describe this Outcome."]',
  outcomePercentage: '[role="spinbutton"]',
  outcomeValueSel: '[title="Value"]',
  outcomeColorSel: '[title="Color"]',
  addProjectedOutcomeBtn: '[data-cy="outcomeFormSubmit"]',
  addProjectedOutcomeBtnDataCy: 'outcomeFormSubmit',
  increaseOutcomePercentage: '[aria-label="Increase Value"]',
  decreaseOutcomePercentage: '[aria-label="Decrease Value"]',
  outcomeNameValidationMsg: '.ant-form-explain',

  // table Projected Outcomes selectors
  outcomeTargetCell: '[col-id="percentage"]',
  outcomeValueCell: '[col-id="outcomeValue"]',
  outcomeColorCell: '[col-id="color"]',
  outcomeDescriptionCell: '[col-id="description"]',

  // modal create/edit global pattern selectors
  openCreateNewGlobalPatternBtn: '#addUseCaseButton',
  globalPatternNameField: '[data-cy="patternNameField"]',
  typeSelect: '[title="Type"]',
  globalPatternChannelsSelect: '[data-cy="patternChannelField"]',
  globalPatternOutcomesSelect: '[data-cy="patternOutcomesField"]',
  globalPatternDescriptionField: '[data-cy="patternDescriptionField"]',
  globalPatternNameLabel: '[title="Pattern Name"]',
  patternChannelsLbl: '[title="Channels"]',
  patternOutcomesLbl: '[title="Outcomes"]',
  patternTypeLbl: '[title="Type"]',
  addGlobalPatternBtn: '[data-cy="patternSubmitButton"]',

  // table Global Patterns selectors
  patternOutcomesRow: '[col-id="outcomes"]',
  patternTypeRow: '[col-id="patternType"]',
  patternChannelsRow: '[col-id="channels"]',
  patternNodesRow: '[col-id="nodeCount"]',
  patternDescriptionRow: '[col-id="description"]',

  // modal create/edit skill pattern
  openCreateSkillPatternBtn: '#addSkillButton',
  skillPatternNameField: '[data-cy="patternNameField"]',
  skillPatternDescriptionField: '[data-cy="patternDescriptionField"]',
  skillPatternAddBtn: '[data-cy="patternSubmitButton"]',
  skillPatternNameLabel: '[title="Skill Name"]',
  slillPatternchannelsLbl: '[title="Channels"]',

  openCreateSkillOutcomeBtn: '#addSkillOutcomeButton',
  skillOutcomeAddBtn: '[data-cy="outcomeFormSubmit"]',

  // table Skill Outcomes
  skillOutcomeName: '.skillLabel',
  skillDescription: '.skillDescription',
}

export { OutcomesPageElements }
