import NavigationBar from '../navigation/NavigationBar'
import { DialogPopOver } from './DialogPopOver'

export class ProjectAction extends DialogPopOver {
  constructor(
    public editProjectProperties = () => cy.xpath('//*[@id="actionsTab"]//*[contains(@class,"anticon-edit")]'),
    public actionsTabExpCollapse = (action: string) =>
      cy.xpath("//*[@id='actionsTab']//*[text()='" + action + "']//i[contains(@class,'ant-collapse-arrow')]"),
    public deleteProjectBtn = () => cy.xpath('//span[text()="Delete project"]/..'),
    public exportProjectBtn = () => cy.xpath('//span[text()="Export project as json"]/..'),
    public yesDeleteProjectBtn = () => cy.xpath('//span[text()="Yes"]/..'),
    public actionTabPopover = "//*[@id='actionsTab']",
    public deletedSuccessMsg = () =>
      cy.xpath("//*[contains(@class,'ant-message-custom-content')]//*[text()='Deleted']"),
    public projectActionHeaderlbl = () => cy.xpath('//*[@id="views"]//*[@class="header"]//span'),
    public projectLabelInput = () => cy.xpath('//input[@placeholder="Enter your label"]'),
    public projectTypeDpd = () => cy.xpath('//div[@class="ui-select__display"]'),
    public projectTypeSelectedDpd = () => cy.xpath('//div[@class="ui-select__display-value"]'),
    public projectDscriptionInput = () => cy.xpath('//*[@placeholder="Enter the description for this project"]'),
    public saveProjectActionBtn = () => cy.xpath('//span[text()="Save"]/..'),
    public resetProjectActionBtn = () => cy.xpath('//span[text()="Reset"]/..'),
    public updatedMsg = () => cy.xpath('//*[@class="anticon anticon-check-circle"]/..//span[text()="Updated"]'),
  ) {
    super()
  }

  expandProjectControl(action: string) {
    this.actionsTabExpCollapse(action).click()
    return this
  }

  deleteProject(projectName: string) {
    this.deleteProjectBtn().should('be.visible').click({ force: true })
    this.yesDeleteProjectBtn().should('be.visible').click()    
    this.isDeleted()
    NavigationBar.projectNotVisible(projectName)
    return this
  }

  exportProject() {
    this.exportProjectBtn().should('be.visible').click({ force: true })
    return this
  }

  actionTabIsClose() {
    cy.xpath(this.actionTabPopover).should('not.be.visible')
    return this
  }

  isDeleted() {
    this.deletedSuccessMsg().should('be.visible')
    return this
  }

  actionTabHeaderLbl(projectName: string) {
    this.projectActionHeaderlbl().contains(projectName)
    return this
  }

  clickEditProjectProperty() {
    this.editProjectProperties().click()
    return this
  }

  updateProjectLabel(updateLabel: string) {
    this.projectLabelInput().type(updateLabel)
    return this
  }

  updateProjectType(type: string) {
    this.projectTypeDpd().should('be.visible').click()
    cy.get('li').contains(type).should('be.visible').click()
    return this
  }

  updateProjectDescription(description: string) {
    this.projectDscriptionInput().type(description)
    return this
  }

  saveActions() {
    this.saveProjectActionBtn().click()
    this.updatedMsg().should('be.visible')
    return this
  }

  resetActions() {
    this.resetProjectActionBtn().click()
    return this
  }

  checkLabelUpdated(labelText: string) {
    this.projectLabelInput().should('have.value', labelText)
  }

  checkdescriptionUpdated(descriptionText: string) {
    this.projectDscriptionInput().should('have.value', descriptionText)
  }

  checkProjectTypeUpdated(projectType: string) {
    this.projectTypeSelectedDpd().contains(projectType)
  }
}

export default new ProjectAction()
