import { DialogPopOver } from './DialogPopOver'

export class FlowConditionPopOver extends DialogPopOver {
  constructor(
    protected labelSel = () => cy.get('[placeholder="Enter unique condition label."]'),
    protected descriptionSel = () => cy.get('[placeholder="Describe this for developers."]'),
    protected popoverAddFlowCondition = () => cy.get(this.popoverTitleSel).contains('Add New Flow Condition'),
    protected popoverEditFlowCondition = () => cy.get(this.popoverTitleSel).contains('Edit Existing Flow Condition'),
  ) {
    super()
  }

  setLabel(text: string, opt?: Partial<Cypress.TypeOptions>) {
    this.labelSel().should('be.visible').sendKeys(text, opt)

    return this
  }

  setContent(text: string, opt?: Partial<Cypress.TypeOptions>) {
    this.descriptionSel().should('be.visible').sendKeys(text, opt)

    return this
  }

  fillAddFlowCondition(flowName: string, flowType: string, flowDescription: string) {
    // this.addBtnSel().click()
    this.popoverAddFlowCondition().contains('Add New Flow Condition').should('be.visible')
    this.popoverAddFlowCondition().next().find('input').type(flowName)
    this.popoverAddFlowCondition()
      .next()
      .find('[role="combobox"]')
      .click()
      .then(($combobox) => {
        cy.wrap($combobox)
          .attribute('aria-controls')
          .then(($id) => {
            cy.get('#' + $id)
              .find('li')
              .contains(flowType)
              .click()
          })
      })
    this.popoverAddFlowCondition().next().find('textarea').type(flowDescription)
  }

  fillEditFlowCondition(flowName: string, flowDescription: string, flowType?: string) {
    // this.addBtnSel().click()
    this.popoverEditFlowCondition().contains('Edit Existing Flow Condition').should('be.visible')
    this.popoverEditFlowCondition().next().find('input').clear().type(flowName)
    if (flowType) {
      this.popoverEditFlowCondition()
      .next()
      .find('[role="combobox"]')
      .click()
      .then(($combobox) => {
        cy.wrap($combobox)
          .attribute('aria-controls')
          .then(($id) => {
            cy.get('#' + $id)
              .find('li')
              .contains(flowType)
              .click()
          })
      })
    }
   
    this.popoverEditFlowCondition().next().find('textarea').type(flowDescription)
  }

  flowLabelValidation(item: object) {
    if (!item.error) {
      this.labelSel().clear().type(item.input).blur().parent().parent().find('.ant-form-explain').should('not.exist')
    } else {
      this.labelSel()
        .clear()
        .then((field) => {
          if (item.input !== '') cy.wrap(field).type(item.input)
        })
        .blur()
        .parent()
        .parent()
        .find('.ant-form-explain')
        .should('contain', item.message)
    }
  }
}

export default new FlowConditionPopOver()
