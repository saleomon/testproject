import { DialogPopOver } from './DialogPopOver'

export class AddEntityPopOver extends DialogPopOver {
  constructor(
    protected entityLabelField = '[placeholder="Enter unique entity label."]',
    protected entityDescriptionField = '[placeholder="Describe this for developers."]',
    protected mockValueField = '[placeholder="Provide an example value for this entity."]',
    protected PIISwitch = '[title="PII"]',
    protected type = '[title="Type"]',
    protected formatType = '[title="Format Type"]',
    public entityGroup = '[title="Group"]',
    protected writableSwitch = '[title="Writable"]',
    protected entityGroupModal = 'Add New Entity Group',
    protected entityLabel = '[title="Entity Label"]',
    protected toggleElement = (element: string) =>
      cy.get(element, { timeout: 1000 }).should('be.visible').filter(':visible').parent().next().find('button').last(),
  ) {
    super()
  }

  toggle(element: string) {
    return this.toggleElement(element)
  }

  fillEntity(entity: object, PII?: boolean, Writable?: boolean) {
    cy.get(this.entityLabelField).last().clear().type(entity.entityLabel)
    cy.selectCustom(this.type, entity.type)
    cy.selectCustom(this.formatType, entity.formatType)
    cy.get(this.entityDescriptionField).last().clear().type(entity.enitityDescription)
    cy.get(this.mockValueField).last().clear().type(entity.mockValue)

    this.toggleElement(this.PIISwitch)
      .attribute('class')
      .then((attr) => {
        cy.log(attr)
        if (attr === 'ant-switch' && PII) {
          this.toggleElement(this.PIISwitch).click().should('have.class', 'ant-switch-checked')
        } else {
          this.toggleElement(this.PIISwitch).click().should('not.have.class', 'ant-switch-checked')
        }
      })

    this.toggleElement(this.writableSwitch)
      .attribute('class')
      .then(($attr) => {
        if ($attr === 'ant-switch' && Writable) {
          this.toggleElement(this.writableSwitch).click().should('have.class', 'ant-switch-checked')
        } else {
          this.toggleElement(this.writableSwitch).click().should('not.have.class', 'ant-switch-checked')
        }
      })
  }
}

export default new AddEntityPopOver()
