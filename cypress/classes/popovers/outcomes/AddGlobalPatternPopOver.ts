import { DialogPopOver } from '../DialogPopOver'

export class AddGlobalPatternPopOver extends DialogPopOver {
  constructor(
    protected patternChannelsLabel = '[title="Channels"]',
    protected patternOutcomesLabel = '[title="Outcomes"]',
    protected redHighlightedSel = '.ant-form-item-control.has-error',
    protected requiredFieldSel = '.ant-form-item-required',
    protected validationMsgSel = '.ant-form-explain',
    protected patternTypeLbl = '[title="Type"]',
    protected patternTypeLabel = () => cy.get('[title="Type"]'),
    protected globalPatternNameField = () => cy.get('[data-cy="patternNameField"]').filter(':visible'),
    protected globalPatternChannelsSelect = () => cy.get('[data-cy="patternChannelField"]'),
    protected globalPatternOutcomesSelect = () => cy.get('[data-cy="patternOutcomesField"]'),
    protected globalPatternDescriptionField = () => cy.get('[data-cy="patternDescriptionField"]'),
    protected globalPatternNameLabel = () => cy.get('[title="Pattern Name"]'),
  ) {
    super()
  }

  setType(type: string) {
    cy.selectCustom(this.patternTypeLbl, type)

    return this
  }

  fillGlobalPattern(
    patternName: string,
    patternType: string,
    patternDescription: string,
    patternChannels: Array<string>,
    patternOutcomes: Array<string>,
  ) {
    this.globalPatternNameField().should('be.visible').filter(':visible').clear().type(patternName)
    cy.selectCustom(this.patternTypeLbl, patternType)
    this.globalPatternDescriptionField().clear().type(patternDescription)
    cy.multiselect(this.patternChannelsLabel, patternChannels)
    cy.multiselect(this.patternOutcomesLabel, patternOutcomes)
  }

  fillGlobalPatternObj(patternObj: object) {
    this.globalPatternNameField().should('be.visible').filter(':visible').clear().type(patternObj.name)
    cy.selectCustom(this.patternTypeLbl, patternObj.type)
    this.globalPatternDescriptionField().filter(':visible').clear().type(patternObj.description)
    cy.multiselect(this.patternChannelsLabel, patternObj.channels)
    cy.wait(2000)
    cy.multiselect(this.patternOutcomesLabel, patternObj.outcomes)
  }

  checkSearchMultiselect(multiselect: string, input, searchResult) {
    cy.get(multiselect).filter(':visible').type(input)
    cy.get('.ant-select-dropdown-placement-bottomLeft')
      .filter(':visible')
      .find('li.ant-select-dropdown-menu-item')
      .should(($els) => {
        // map elements to array of their innerText
        let elsText = $els.toArray().map((el) => el.innerText)
        expect(elsText).to.deep.eq(searchResult)
      })
    this.popoverTitle().click()
    /*  cy.get(multiselect).parent().parent().find('[data-cy="patternChannelField"]').then($elem => {
      cy.wrap($elem).find('[role="presentation"]').should('not.exist')
    }) */
  }

  checksearchNoData(multiselect: string, input: string) {
    cy.get(multiselect).filter(':visible').type(input)
    cy.get('.ant-empty-image').should('exist')
    this.popoverTitle().click()
    cy.get('.ant-empty-image').should('not.be.visible')
  }

  checkValidationError(label: string, message: string, field: string, input: string, flag: boolean) {
    if (input.length > 0) {
      cy.get(field).filter(':visible').clear({ force: true }).type(input)
      this.popoverTitle().filter(':visible').click()
    } else {
      cy.get(field).should('be.visible').clear()
      this.popoverTitle().filter(':visible').click()
    }
    if (flag === true) {
      cy.get(label)
        .filter(':visible')
        .parent()
        .siblings()
        .find(this.validationMsgSel)
        .should('exist')
        .and('have.text', message)
      cy.get(label).filter(':visible').parent().siblings().find(this.redHighlightedSel).should('exist')
    } else {
      cy.get(label).filter(':visible').parent().siblings().find(this.validationMsgSel).should('not.exist')
      cy.get(label).filter(':visible').parent().siblings().find(this.redHighlightedSel).should('not.exist')
    }
  }

  checkValidationErrorSelect(label: string, message: string) {
    cy.get(label)
      .filter(':visible')
      .parent()
      .siblings()
      .find(this.validationMsgSel)
      .should('exist')
      .and('have.text', message)
    cy.get(label).filter(':visible').parent().siblings().find(this.redHighlightedSel).should('exist')
  }
}

export default new AddGlobalPatternPopOver()
