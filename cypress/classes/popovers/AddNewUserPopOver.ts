import { DialogPopOver } from './DialogPopOver'

export class AddNewUserPopOver extends DialogPopOver {
  constructor(
    protected usernameSel = () => cy.get('[placeholder="Enter username."]'),
    protected emailSel = () => cy.get('[placeholder="Enter email."]'),
    protected firstNameSel = () => cy.get('[placeholder="Nomen"]'),
    protected lastNameSel = () => cy.get('[placeholder="Nescio"]'),
    protected passwordSel = () => cy.get('[placeholder="Password"]'),
    protected repeatPasswordSel = () => cy.get('[placeholder="Repeat Password"]'),
    protected usernameWarningMessage = () =>
      cy.xpath(
        '//input[@placeholder="Enter username."]/ancestor::div[@class="ant-form-item-control has-error"]/div[@class="ant-form-explain"]',
      ),
    protected emailWarningMessage = () =>
      cy.xpath(
        '//input[@placeholder="Enter email."]/ancestor::div[@class="ant-form-item-control has-error"]/div[@class="ant-form-explain"]',
      ),
    protected firstNameWarningMessage = () =>
      cy.xpath(
        '//input[@placeholder="Nomen"]/ancestor::div[@class="ant-form-item-control has-error"]/div[@class="ant-form-explain"]',
      ),
    protected lastNameWarningMessage = () =>
      cy.xpath(
        '//input[@placeholder="Nescio"]/ancestor::div[@class="ant-form-item-control has-error"]/div[@class="ant-form-explain"]',
      ),
    protected passwordWarningMessage = () =>
      cy.xpath(
        '//input[@placeholder="Password"]/ancestor::div[@class="ant-form-item-control has-error"]/div[@class="ant-form-explain"]',
      ),
    protected repeatPasswordWarningMessage = () =>
      cy.xpath(
        '//input[@placeholder="Repeat Password"]/ancestor::div[@class="ant-form-item-control has-error"]/div[@class="ant-form-explain"]',
      ),
    protected toastMessageError = () => cy.xpath('//div[@class="ant-message-custom-content ant-message-error"]'),
    protected toastMessageSuccess = (text: string) => cy.xpath(`//*[contains(text(), "${text}")]`),
  ) {
    super()
  }

  setUsername(text: string, opt?: Partial<Cypress.TypeOptions>) {
    this.usernameSel().should('be.visible').sendKeys(text, opt)

    return this
  }

  isUsernameSet(text: string) {
    this.usernameSel().should('contain.value', text)

    return this
  }

  setEmail(text: string, opt?: Partial<Cypress.TypeOptions>) {
    this.emailSel().should('be.visible').sendKeys(text, opt)

    return this
  }

  isEmailSet(text: string) {
    this.emailSel().should('contain.value', text)

    return this
  }

  setFirstName(text: string, opt?: Partial<Cypress.TypeOptions>) {
    this.firstNameSel().should('be.visible').sendKeys(text, opt)

    return this
  }

  isFirstNameSet(text: string) {
    this.firstNameSel().should('contain.value', text)

    return this
  }

  setLastName(text: string, opt?: Partial<Cypress.TypeOptions>) {
    this.lastNameSel().should('be.visible').sendKeys(text, opt)

    return this
  }

  isLastNameSet(text: string) {
    this.lastNameSel().should('contain.value', text)

    return this
  }

  setPassword(text: string, opt?: Partial<Cypress.TypeOptions>) {
    this.passwordSel().should('be.visible').sendKeys(text, opt)

    return this
  }

  isPasswordSet(text: string) {
    this.passwordSel().should('contain.value', text)

    return this
  }

  setRepeatPassword(text: string, opt?: Partial<Cypress.TypeOptions>) {
    this.repeatPasswordSel().should('be.visible').sendKeys(text, opt)

    return this
  }

  isRepeatPasswordSet(text: string) {
    this.repeatPasswordSel().should('contain.value', text)

    return this
  }

  isUsernameWarningMessageDisplayed(text: string) {
    this.usernameWarningMessage().should('be.visible').contains(text)

    return this
  }

  isEmailWarningMessageDisplayed(text: string) {
    this.emailWarningMessage().should('be.visible').contains(text)

    return this
  }

  isFirstNameWarningMessageDisplayed(text: string) {
    this.firstNameWarningMessage().should('be.visible').contains(text)

    return this
  }

  isLastNameWarningMessageDisplayed(text: string) {
    this.lastNameWarningMessage().should('be.visible').contains(text)

    return this
  }

  isPasswordWarningMessageDisplayed(text: string) {
    this.passwordWarningMessage().should('be.visible').contains(text)

    return this
  }

  isRepeatPasswordWarningMessageDisplayed(text: string) {
    this.repeatPasswordWarningMessage().should('be.visible').contains(text)

    return this
  }

  isErrorToastMessageDisplayed(text: string) {
    this.toastMessageError().should('be.visible').contains(text)

    return this
  }

  isSuccessToastMessageDisplayed(text: string) {
    this.toastMessageSuccess(text).should('be.visible').contains(text)

    return this
  }
}

export default new AddNewUserPopOver()
