import { DialogPopOver } from './DialogPopOver'

export class AddNewOutputCondition extends DialogPopOver {
  constructor(
    protected labelSel = () => cy.get('[placeholder="Enter a condition label."]'),
    protected contentSel = () => cy.get('[placeholder="Enter Voice or Chat Prompt for User."]'),
  ) {
    super()
  }

  setLabel(text: string, opt?: Partial<Cypress.TypeOptions>) {
    this.labelSel().should('be.visible').sendKeys(text, opt)

    return this
  }

  setContent(text: string, opt?: Partial<Cypress.TypeOptions>) {
    this.contentSel().should('be.visible').sendKeys(text, opt)

    return this
  }

  outputLabelValidation(item: object) {
    if (!item.error) {
      this.labelSel().clear().type(item.input).blur().parent().parent().find('.ant-form-explain').should('not.exist')
    } else {
      this.labelSel()
        .clear()
        .then((field) => {
          if (item.input !== '') cy.wrap(field).type(item.input)
        })
        .blur()
        .parent()
        .parent()
        .find('.ant-form-explain')
        .should('contain', item.message)
    }
  }
}

export default new AddNewOutputCondition()
