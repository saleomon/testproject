import { OutcomesTable } from './OutcomesTable'

export class ProjectedOutcomesTable extends OutcomesTable {
  constructor(
    protected openCreateNewProjectedOutcomeBtn = () => cy.get('#addProjectedButton'),
    protected outcomeTargetCellSel = '[col-id="percentage"]',
    protected outcomeValueCellSel = '[col-id="outcomeValue"]',
    protected outcomeColorCellSel = '[col-id="color"]',
    protected outcomeDescriptionCellSel = '[col-id="description"]',
  ) {
    super()
  }

  openCreateNewProjectedOutcome(opt?: Partial<Cypress.ClickOptions>) {
    this.openCreateNewProjectedOutcomeBtn().should('be.visible').click(opt)

    return this
  }

  deleteRow(name: string, opt?: Partial<Cypress.ClickOptions>) {
    this.projectedOutcomesTable().find(this.outcomeRow).contains(name).next(this.deleteBtnTableSel).click(opt)

    return this
  }

  checkAbsenceProjectedOutcome(outcome: string) {
    this.projectedOutcomesTable().find(this.outcomeRow).contains(outcome).should('not.exist') 

  }

  checkPatternAbsence(name: string) {
    this.globalPatternTable().find(this.outcomeRow).contains(name).should('not.exist')

    return this
  }

  editRow(name: string, opt?: Partial<Cypress.ClickOptions>) {
    this.projectedOutcomesTable()
      .find(this.leftContainerSel)
      .find(this.outcomeRow)
      .contains(name)
      .parent()
      .children(this.editBtnTableSel).click(opt)

    return this
  }

  checkOutcomeFields(outcome: object) {
    this.projectOutcomesTable()
      .find(this.leftContainerSel)
      .contains(outcome.projectedOutcomeLabel)
      .parent()
      .parent()
      .parent()
      .attribute('row-id')
      .then(($attr) => {
        const rowNumber = $attr
        this.centerContainer()
          .find(this.outcomeRow)
          .eq(rowNumber)
          .then(($trow) => {
            expect($trow.find(this.outcomeTargetCellSel).text()).to.equal(outcome.outcomePercentage)
            expect($trow.find(this.outcomeValueCellSel).text()).to.equal(outcome.outcomeValue)
            expect($trow.find(this.outcomeColorCellSel).text()).to.equal(outcome.outcomeColor)
            expect($trow.find(this.outcomeDescriptionCellSel).text()).to.equal(outcome.outcomeDescription)
          })
      })

    return this
  }
}
export default new ProjectedOutcomesTable()
