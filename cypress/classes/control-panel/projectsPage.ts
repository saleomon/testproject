import { Table } from '../tables/Table'

export class ProjectsPage extends Table {
  constructor(
    public tables = () => cy.get('#views'),
    public leftContainer = () => cy.get('.ag-pinned-left-cols-container'),
    public centerContainer = () => cy.get('.ag-center-cols-viewport'),
    public importBtProjectSel = '//div[@class="actionButtonsRightDiv"]/button/span[contains(text(),"Import Project from Json")]',
    public projectJsonSel = 'textarea[class="ant-input"]',
    public projectLabelSel = '//div[@class="ag-header-cell-label"]//span[text() = "Project Label"]',
  ) {
    super()
  }

  getRowProjectInfo(Name: string, Type: string, Description: any) {
    cy.get('.closeButton').should('be.visible').click()
    cy.get('.ag-body-viewport').scrollTo('bottom')
    this.leftContainer()
      .should('be.visible')
      .contains(Name)
      .parent()
      .attribute('row-id')
      .then(($attr) => {
        const rowNumber = $attr
        this.tables().contains(Name).parent().attribute('row-id').should('contain', rowNumber)
      })

    this.centerContainer()
      .should('be.visible')
      .contains(Type)
      .parent()
      .attribute('row-id')
      .then(($attr) => {
        const rowNumber = $attr
        this.tables().contains(Type).parent().attribute('row-id').should('contain', rowNumber)
      })
    this.centerContainer()
      .should('be.visible')
      .contains(Description)
      .parent()
      .attribute('row-id')
      .then(($attr) => {
        const rowNumber = $attr
        this.tables().contains(Description).parent().attribute('row-id').should('contain', rowNumber)
      })
  }

  getRowProjectLabel(Name: string, Type: string) {
    cy.get('.closeButton').should('be.visible').click()
    cy.get('.ag-body-viewport').scrollTo('bottom')
    this.leftContainer()
      .should('be.visible')
      .contains(Name)
      .parent()
      .attribute('row-id')
      .then(($attr) => {
        const rowNumber = $attr
        this.tables().contains(Name).parent().attribute('row-id').should('contain', rowNumber)
      })
  }

  clickImportBtProjectS(opt: Partial<Cypress.ClickOptions> = {}) {
    opt.force = true
    cy.xpath(this.importBtProjectSel).should('be.visible').click(opt)

    return this
  }

  setProjectJson(projectJSON: string, opt: Partial<Cypress.TypeOptions> = {}) {
    opt.force = true

    cy.get(this.projectJsonSel).should('be.visible').sendKeys(projectJSON, {
      parseSpecialCharSequences: false,
      delay: 0,
    })

    return this
  }

  checkProjectType(projectName: string, projectType: string) {
    cy.get('div[role="row"]').contains(projectName).should('exist').click()
    cy.xpath('//div[@aria-selected="true"]/div[@col-id="projectType"]').contains(projectType).should('exist')

    return this
  }

  clickProjectLabel(opt: Partial<Cypress.TypeOptions> = {}) {
    opt.force = true
    cy.xpath(this.projectLabelSel).should('be.visible').click(opt)

    return this
  }

  checkProjectExist(projectName: string, opt: Partial<Cypress.TypeOptions> = {}) {
    opt.force = true
    cy.xpath('//div[@ref="eLabel"]/span[contains(text(), "Project Label")]').should('be.visible').click(opt)

    cy.get('div[role="row"]').contains(projectName).should('exist')

    return this
  }
}
export default new ProjectsPage()
