import { GeneralTabSideBar } from './GeneralTabSideBar'

class TabOutputSideBar extends GeneralTabSideBar {
  constructor(
    private propertiesSel = () => cy.get('li[aria-controls="propertyTab"]'),
    private nodeDescSel = () => cy.get('textarea[placeholder]'),
    private nodeDescEditBtnSel = () =>
      cy.xpath('//textarea[@placeholder]/following-sibling::button[contains(@class, "contentEditButton")]'),
    private editAcceptBtnSel = () => cy.xpath('(//button[contains(@class, "saveItEditButton")])[1]'),
    private bothBtnSel = () =>
      cy.xpath('//label[contains(@class, "ant-radio-button-wrapper")]//span[contains(text(), "Both")]'),
    private outputBtnSel = () =>
      cy.xpath('//label[contains(@class, "ant-radio-button-wrapper")]//span[contains(text(), " Output ")]'),
    private descriptionBtnSel = () =>
      cy.xpath('//label[contains(@class, "ant-radio-button-wrapper")]//span[contains(text(), "Description")]'),
    private outputSel = () => cy.get('li[aria-controls="templateOutputTab"]'),
    private contentVisibleSel = () => cy.xpath('(//div[@class="contentView"])[2]'),
    private contentEmptySel = () => cy.xpath('(//div[@class="contentEmpty"])[2]'),
    private addNewOutputConditionBtnSel = () => cy.get('#templateOutputTab button.addButton'),
    private channelsSel = () => cy.get('li[aria-controls="channelTab"]'),
    private currentChannelSel = () => cy.xpath('(//div[@class="itemHeader"])[2]'),
    private sideNodeSel = () => cy.get('div.itemHeader'),
    private nameNodeSel = (linkNode: string) =>
      cy.xpath('//div[@class="itemHeader" and contains(text(), "' + linkNode + '")]'),
    private sideNodeNameSel = (linkNode: string) =>
      cy.xpath(
        `//div[@class="itemHeader" and contains(text(), "${linkNode}")]/following-sibling::div[@class="contentBody"]`,
      ),
    private addBthSel = () => cy.xpath('//div[@class="ant-collapse-extra"]//button'),
    private reasonNodeSel = () => cy.xpath('//div[@class="descriptionSpan"]'),
    private openQuestionSection = () => cy.get('.openColor[role="tablist"] '),
    private editableTextArea = () => cy.get('.contentEdit'),
    private outputDescription = (name: string) => cy.xpath('//div[contains(text(),"' + name + '")]').closest('.editorWrapper'),
    private sectionInsideOpenQuestion = (name: string) => cy.xpath('//div[contains(text(),"' + name + '")]').closest('.editorWrapper').eq(1), 

  ) {
    super()
  }

  deleteOutput(question: string) {
    this.sectionInsideOpenQuestion(question).within(() => {
      this.moreButton().click()
    })
    this.clickMenuItem('Delete Item')
  }

  editOutput(question: string, newValue: string) {
    this.sectionInsideOpenQuestion(question).within(() => {
      cy.get('.contentEditButton').should('be.visible').click()
      cy.get('.contentBodyEditor').type(newValue)
      this.clickOnSaveBtn()
    })
  }

  openOutput(opt?: Partial<Cypress.ClickOptions>) {
    this.outputSel().should('be.visible').click(opt)

    return this
  }

  isContentSet(text: string) {
    this.contentVisibleSel().should('have.text', ' ' + text + ' ')

    return this
  }

  clickOnAddNewOutputConditionBtn(opt?: Partial<Cypress.ClickOptions>) {
    this.addNewOutputConditionBtnSel().should('be.visible').click(opt)

    return this
  }

  clickOnOutputBtn(opt?: Partial<Cypress.ClickOptions>) {
    this.outputBtnSel().should('be.visible').click(opt)

    return this
  }

  isOutputBtnFocused() {
    this.outputBtnSel().parent().should('have.class', 'ant-radio-button-wrapper-checked')

    return this
  }

  clickOnDescriptionBtn(opt?: Partial<Cypress.ClickOptions>) {
    this.descriptionBtnSel().should('be.visible').click(opt)

    return this
  }

  isDescriptionBtnFocused() {
    this.descriptionBtnSel().parent().should('have.class', 'ant-radio-button-wrapper-checked')

    return this
  }

  clickNodeLabelEditBtn() {
    cy.get('.contentFooter > .contentEditButton').should('be.visible').click()
    return this
  }

  isContent(question: string , description :string) {
    this.outputDescription(question).within(() => {
      cy.get('.contentBody').should('contain', description )
        })
  }

}

export default new TabOutputSideBar()
