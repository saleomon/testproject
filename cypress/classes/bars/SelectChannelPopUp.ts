export class SelectChannelPopUp {
  constructor(
    private selectChannelSel = (text: string) => cy.xpath(`//div[text()='${text}']`),

  ) {}

  checkDuplicateChannel(text: string, opt?: Partial<Cypress.ClickOptions>) {
    this.selectChannelSel(text).should('be.visible', text)

    return this
  }

  isNotExistkDuplicateChannel(text: string) {
    this.selectChannelSel(text).should('not.exist', text)

    return this
  }
}

export default new SelectChannelPopUp()
