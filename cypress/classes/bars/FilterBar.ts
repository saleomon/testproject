import {text} from "stream/consumers";

export class FilterBar {
  constructor(
    public exportExcelBtn = () => cy.contains('Export Excel'),
    public exportCSVBtn = () => cy.contains('Export CSV'),
    public patternDropdownSection = () => cy.get('.ant-select-tree-treenode-switcher-open'),
    public escalateFallbackSectionSel = '[title="Escalate-Fallback"]',

    public filterDropdownByName = (name: string) => `.filterbar .filterDropDownsDiv #${name}Filter .filterButton`,
    public flowSearchField = '.ant-select-search__field',
    public flowSearchSelectTree = '.ant-select-tree',
    public checkListFlows = (flow: string) => '//span[@title="' + flow + '"]',
    public dropdownMenuData = '.ant-dropdown-menu-item',
    public generateFlow = 'button.toolbarRightSideItem:nth-of-type(4)',
    private emptyProjectToastMessageSel = '//span[contains(text(), "Could not find any selected flow.")]',
    public projectDropdownSel = '//div[@class="projectDropDown ant-dropdown-trigger"]',
    private deletedChannelSel = (pattern: string) => cy.xpath(`//li[text()='${pattern}']`),
  ) {}

  checkSelectedPattern(patternName: string) {
    cy.get(this.filterDropdownByName('pattern')).should('have.text', patternName)

    return this
  }

  checkSelectedFlow(flowName: string) {
    cy.get(this.filterDropdownByName('flow')).should('have.text', flowName)

    return this
  }

  checkSelectedChannel(channelName: string) {
    cy.get(this.filterDropdownByName('channel')).should('have.text', channelName)

    return this
  }

  checkListFlow(flow: string) {
    cy.get(this.filterDropdownByName('flow')).should('be.visible').click()
    cy.get(this.checkListFlows(flow)).should('have.text', flow)
    cy.get(this.checkListFlows(flow)).should('have.text', flow)

    return this
  }

  selectPattern(patternName: string) {
    cy.get(this.filterDropdownByName('pattern')).should('be.visible').click()
    cy.get('li')
      .find('span[title="' + patternName + '"]')
      .click()
    cy.get(this.filterDropdownByName('pattern')).find('span').contains(patternName)

    return this
  }

  checkPatternDropdownSection(section: string, patternName: string) {
    cy.get(this.filterDropdownByName('pattern')).should('be.visible').click()
    this.patternDropdownSection().find('span').contains(section).parent().parent().find('span').contains(patternName)

    return this
  }

  selectFlow(flowName: string) {
    cy.get(this.filterDropdownByName('flow')).should('be.visible').click()
    cy.get('li').find('span').contains(flowName).click()
    cy.get(this.filterDropdownByName('flow')).find('span').contains(flowName)

    return this
  }

  selectChannel(channelName: string) {
    cy.get(this.filterDropdownByName('channel')).click()
    cy.get(this.dropdownMenuData).contains(channelName).click()

    return this
  }

  isNotExistChannel(channelName: string) {
    cy.get(this.filterDropdownByName('channel')).click()
    cy.get(this.dropdownMenuData).should('not.exist', channelName).click()

    return this
  }

  checkDeletedChannel(channelName: string) {
    cy.get(this.filterDropdownByName('channel')).should('be.visible').click()
    this.deletedChannelSel(channelName).should('not.exist', channelName)

    return this
  }

  searchFlow(flowName: string) {
    cy.get(this.filterDropdownByName('flow')).should('be.visible').click()
    cy.get(this.flowSearchField).should('be.visible').type(flowName)

    return this
  }

  checkFlowSearchResult(flowName: string) {
    cy.get(this.filterDropdownByName('flow')).should('be.visible').click()
    cy.get(this.flowSearchSelectTree).contains(flowName).should('be.visible')

    return this
  }

  getFlowFiltersContainer() {
    cy.get(this.filterDropdownByName('flow')).should('be.visible').click()
  }

  openChannelDropdown(opt?: Partial<Cypress.ClickOptions>) {
    cy.get(this.filterDropdownByName('channel')).should('be.visible').click(opt)

    return this
  }

  exportExcel(opt?: Partial<Cypress.ClickOptions>) {
    this.exportExcelBtn().should('be.visible').click()

    return this
  }

  exportCSV(opt?: Partial<Cypress.ClickOptions>) {
    this.exportCSVBtn().should('be.visible').click()
  }

  clickGenerateFlow(opt?: Partial<Cypress.ClickOptions>) {
    cy.get(this.generateFlow).should('be.visible').click(opt)
  }

  checkSelectedProject(text: string) {
    cy.xpath(this.projectDropdownSel).should('contain.text', text)

    return this
  }

  isEmptyProjectToastMessageDisplayed(text: string) {
    cy.xpath(this.emptyProjectToastMessageSel).should('contain.text', text)

    return this
  }
}

export default new FilterBar()
