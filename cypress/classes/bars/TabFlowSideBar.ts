import { GeneralTabSideBar } from './GeneralTabSideBar'

class TabFlowSideBar extends GeneralTabSideBar {
  constructor(
    // private flowSel = () => cy.get('li[aria-controls="flowTab"]'),
    public moreButtonSel = '.modButton',
    public flowConditionLabel = () => cy.get('.itemHeader'),
    private channelsSel = () => cy.get('li[aria-controls="channelTab"]'),
    private currentChannelSel = () => cy.xpath('(//div[@class="itemHeader"])[2]'),
    private sideNodeSel = () => cy.get('div.itemHeader'),
    private nameNodeSel = (linkNode: string) =>
      cy.xpath('//div[@class="itemHeader" and contains(text(), "' + linkNode + '")]'),
    private sideNodeNameSel = (linkNode: string) =>
      cy.xpath(
        `//div[@class="itemHeader" and contains(text(), "${linkNode}")]/following-sibling::div[@class="contentBody"]`,
      ),
    private addBthSel = () => cy.xpath('//div[@class="ant-collapse-extra"]//button'),
    private reasonNodeSel = () => cy.xpath('//div[@class="descriptionSpan"]'),
    private editBthSel = () => cy.xpath('//li[@name="47f32d91-2280-45c2-8b1f-80b6844dc2d7"]//button'),
    private deleteFlowBtn = () => cy.xpath('//li[@class="ant-dropdown-menu-item ant-dropdown-menu-item-active"]'),
  ) {
    super()
  }

  clickDeleteFlowBtn(opt?: Partial<Cypress.ClickOptions>) {
    this.deleteFlowBtn().click(opt)
  }

  clickOnEditBtn(opt?: Partial<Cypress.ClickOptions>) {
    this.editBthSel().click(opt)
  }

  isSideNode(linkNode: string, text: string) {
    this.nameNodeSel(linkNode).should('contain.text', text)

    return this
  }

  isSideNodeConditionSel(linkNode: string, text: string) {
    this.sideNodeNameSel(linkNode).should('contain.text', text)

    return this
  }

  isSideNodeReasonSel(text: string) {
    this.reasonNodeSel().should('contain.text', text)

    return this
  }

  clickOnPlusBtn(opt?: Partial<Cypress.ClickOptions>) {
    this.addBthSel().should('be.visible').click(opt)

    return this
  }

  flowActions(flowName: string) {
    this.flowConditionLabel().contains(flowName).parent().find(this.moreButtonSel).should('be.visible').click()

    return this
  }

  selectActionEditProperties() {
    this.menuItem('Edit Properties').should('be.visible').click()

    return this
  }

  selectActionDeleteFlow(){
    this.menuItem('Delete Flow').should('be.visible').click()

    return this
  }

  openChannels() {
    this.channelsSel().should('be.visible').click()

    return this
  }

  isChannelChosen(text: string) {
    this.currentChannelSel().should('have.text', text)

    return this
  }

  openFlow() {
    this.flowSel().should('be.visible').click()

    return this
  }

  isSideNodeDescription(linkNode: string, text: string) {
    this.sideNodeSel().contains(linkNode).should('contain.text', text)

    return this
  }
}

export default new TabFlowSideBar()
