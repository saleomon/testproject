export class GeneralTabSideBar {
  constructor(
    protected sideBarSel = () => cy.get('div[selectednode="[object Object]"]'),
    protected closeBtnSel = () => cy.xpath('//button[contains(@class, "closeButton")]'),
    protected titleSel = () => cy.get('.header'),
    protected tabByName = (tabName: string) => cy.get('.ui-tab-header-item__text').contains(tabName),
    protected flowSel = () => cy.get('li[aria-controls="flowTab"]'),
    protected saveButton = (field: string) => cy.contains('div', field).find('.saveItEditButton'),
    protected menuItem = (name: string) => cy.get('li[role="menuitem"]').contains(name),
    protected moreButton = () => cy.get('.itemDropDown'),
    protected elementSelector = 'div[selectednode="[object Object]"]',
    protected headerSelector = '.header ',
    protected closeButtonSelector = '.header' + '.closeButton',
  ) {}

  isTitleSet(text: string) {
    this.titleSel().should('contain.text', text)

    return this
  }

  selectTabByName(tabName: string) {
    this.tabByName(tabName).click({ force: true })

    return this
  }

  openFlowTab() {
    this.flowSel().should('be.visible').click()

    return this
  }

  closeButton() {
    return cy
      .get(this.elementSelector)
      .find(this.closeButtonSelector)
      .then(($obj) => {
        cy.wrap($obj).then(($wrapped) => {
          $wrapped
        })
      })
  }

  clickMenuItem(item: string) {
    this.menuItem(item).click()
  }

  clickOnCloseBtn(opt?: Partial<Cypress.ClickOptions>) {
    this.closeBtnSel().should('be.visible').click(opt)

    return this
  }

  clickOnSaveBtn(field : string , opt?: Partial<Cypress.ClickOptions>) {
    this.saveButton(field).should('be.visible').click(opt)

    return this
  }
}

export default new GeneralTabSideBar()
