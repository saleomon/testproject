import { GeneralTabSideBar } from './GeneralTabSideBar'

class TabPropertiesSideBar extends GeneralTabSideBar {
  constructor(
    private propertiesSel = () => cy.get('li[aria-controls="propertyTab"]'),
    private templateSection = () => cy.xpath('//div[@class="ant-collapse-header" and contains(text(), "Template")]'),
    private templateLabel = () => cy.get('input[placeholder="Enter unique Template Label"]'),
    private templateDescription = () => cy.get('textarea[placeholder="Enter Template Description"]'),
    private nodePropertiesListSel = () =>
      cy.xpath('//div[@class="ant-collapse-header" and contains(text(), "Node Properties")]'),
    private nodeLabelSel = () => cy.get('input[placeholder="Enter Unique Node Label"]'),
    private externalLink = () => cy.get('input[placeholder="Provide Link to External Documentation"]'),
    private activeNodeLabelSel = () => cy.get('textarea[placeholder="Enter Unique Node Label"]'),
    private nodeLabelEditBtnSel = () =>
      cy.xpath('//div/following-sibling::button[contains(@class, "contentEditButton")]'),
    private nodeDescSel = () => cy.get('textarea[placeholder]'),
    private nodeDescEditBtnSel = () =>
      cy.xpath('//textarea[@placeholder]/following-sibling::button[contains(@class, "contentEditButton")]'),
    private editLabelAcceptBtnSel = () => cy.xpath('(//button[contains(@class, "saveItEditButton")])[1]'),
    //private editDescAcceptBtnSel = () => cy.xpath('(//button[contains(@class, "saveItEditButton")])[2]'),
    private editDescAcceptBtnSel = () => cy.contains('div', 'Node Description').find('.saveItEditButton'),
    private descriptionBtnSel = () =>
      cy.xpath('//label[contains(@class, "ant-radio-button-wrapper")]//span[contains(text(), "Description")]'),
    private outputBtnSel = () =>
      cy.xpath('(//label[contains(@class, "ant-radio-button-wrapper")]//span[contains(text(), " Output ")])[1]'),
    private bothBtnSel = () =>
      cy.xpath('//label[contains(@class, "ant-radio-button-wrapper")]//span[contains(text(), "Both")]'),
    private duplicateBtnSel = () =>
      cy.xpath('//button[contains(@class, "newTemplateButton ant-btn")]//span[contains(text(), "Duplicate Node")]'),
    private cancelEdit = () => cy.get(':nth-child(3) > .contentEdit > .editControls > .cancelEditButton') ,
    private toastMessageSel = () => cy.xpath('//div[@class="ant-message-custom-content ant-message-warning"]'),
    private datamodelListSel = () => cy.xpath('//div[@class="ant-collapse-header" and contains(text(), "Datamodel")]'),
    private nodeTypeSel = () => cy.get('input[placeholder="This shouldn\'t be a text box it should be a dropdown"]'),
    private datamodelHumanParameterSel = () => cy.get('.datamodelWindow p'),
    private externalLinkEditBtn = () => cy.contains('div', 'External Documentation Link').find('.contentEditButton'),
    private externalLinkDescriptionInput = () => cy.contains('div', 'External Documentation Link').find('.ant-input'),
  ) {
    super()
  }
  openProperties() {
    this.propertiesSel().should('be.visible').click()

    return this
  }

  openTemplateSection() {
    this.templateSection().should('be.visible').click()

    return this
  }

  isTemplateLabelSet (text: string) {
    this.templateLabel().should('have.value', text)

    return this
  }

  isTemplateDescriptionSet (text: string) {
    this.templateDescription().should('have.value', text)

    return this
  }

  clickOnNodePropertiesListBtn(opt?: Partial<Cypress.ClickOptions>) {
    this.nodePropertiesListSel().should('be.visible').click(opt)

    return this
  }

  setNodeLabel(text: string, opt?: Partial<Cypress.TypeOptions>) {
    this.nodeLabelEditBtnSel().should('be.visible').click()
    this.activeNodeLabelSel().sendKeys(text, opt)
    this.editLabelAcceptBtnSel().should('be.visible').click()

    return this
  }

  isNodeLabelEditBtnDisplayed() {
    this.nodeLabelEditBtnSel().should('be.visible')

    return this
  }

  isNodeLabelSet(text: string) {
    this.nodeLabelSel().should('have.value', text)

    return this
  }

  setDescription(text: string, opt?: Partial<Cypress.TypeOptions>) {
    this.nodeDescEditBtnSel().should('be.visible').click()
    this.nodeDescSel().sendKeys(text, opt)
    this.editDescAcceptBtnSel().should('be.visible').click()

    return this
  }

  isDescriptionSet(text: string) {
    this.nodeDescSel().should('have.value', text)

    return this
  }

  clickOnDescriptionBtn(opt?: Partial<Cypress.ClickOptions>) {
    this.descriptionBtnSel().should('be.visible').click(opt)

    return this
  }

  isDescriptionBtnFocused() {
    this.descriptionBtnSel().parent().should('have.class', 'ant-radio-button-wrapper-checked')

    return this
  }

  clickOnOutputBtn(opt?: Partial<Cypress.ClickOptions>) {
    this.outputBtnSel().should('be.visible').click(opt)
    return this
  }
  editBtnExternalDocumentationLink(text : string) {
    this.externalLinkEditBtn().should('be.visible').click()
    this.externalLinkDescriptionInput().clear().type(text)

    return this
  }

  isExternalDocumentationLink() {
    this.externalLink().should('be.visible')

    return this
  }

  clickOnCancelEdit() {
    this.cancelEdit().should('be.visible').click()
  }

  isExternalDocumentationLinkSet(text: string) {
    this.externalLink().should('contain', text)

    return this
  }

  checkLinkBtn (url: string) {
    this.linkBtn().should('be.visible').should('have.attr', 'href').and('include', url )
    return this
  }


  isOutputBtnFocused() {
    this.outputBtnSel().parent().should('have.class', 'ant-radio-button-wrapper-checked')
    return this
  }

  clickOnBothBtn(opt?: Partial<Cypress.ClickOptions>) {
    this.bothBtnSel().should('be.visible').click(opt)

    return this
  }

  isBothBtnFocused() {
    this.bothBtnSel().parent().should('have.class', 'ant-radio-button-wrapper-checked')

    return this
  }

  clickOnDuplicateBtn(opt?: Partial<Cypress.ClickOptions>) {
    this.duplicateBtnSel().should('be.visible').click(opt)

    return this
  }

  isToastMessageDisplayed(text: string) {
    this.toastMessageSel().should('contain.text', text)

    return this
  }

  clickOnDatamodelListBtn(opt?: Partial<Cypress.ClickOptions>) {
    this.datamodelListSel().should('be.visible').click(opt)

    return this
  }

  isDatamodelHumanParameterSet(text: string) {
    this.datamodelHumanParameterSel().contains(text)

    return this
  }

  nodeLabelValidation(item: object) {
    if (!item.error) {
      this.nodeLabelSel().clear().type(item.input).blur().parent().parent().find('.ant-form-explain').should('not.exist')
    } else {
      this.nodeLabelSel()
        .clear()
        .then((field) => {
          if (item.input !== '') cy.wrap(field).type(item.input)
        })
        .blur()
        .parent()
        .parent()
        .find('.ant-form-explain')
        .should('contain', item.message)
    }
  }

  isNodeTypeSet(text: string) {
    this.nodeTypeSel().should('have.value', text)

    return this
  }
}

export default new TabPropertiesSideBar()
