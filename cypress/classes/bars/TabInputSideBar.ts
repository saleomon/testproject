import { GeneralTabSideBar } from './GeneralTabSideBar'

class TabInputSideBar extends GeneralTabSideBar {
  constructor(
    public inputParsersOptionsDropdown = () => cy.get('.optionsButton'),
    public optionsDropdownItem = (itemName: string) => cy.get('.optionsDropdownItem').contains(itemName),
    public addOptionButton = (optionName: string) =>
      cy.get('.ant-collapse-header').contains(optionName).find('.addButton'),
    public closeQuestionModalForm = (formName: string) =>
      cy.get('.ant-popover-title').contains(formName).parent().find('.closedQuestionForm'),
    public addClosedInputField = (formName: string, fieldName: string, fieldType: string) =>
      this.closeQuestionModalForm(formName)
        .find('.ant-form-item-label')
        .contains(fieldName)
        .parents('.ant-row')
        .find(fieldType),
    public formButtonByText = (buttonText: string) => cy.get('form button').contains(buttonText),
    public createdOptionField = (label: string, fieldClass: string) =>
      cy.get('.itemHeader').contains(label).parent().parent().find(`[class="${fieldClass}"]`),
    public inputParsersCounter = (count: string) =>
      cy.get('.paneHeader').contains('Input Parsers').find(`[title="${count}"]`),
    public applyToAllFlowCheckbox = () => cy.get('.applyToFlowCheckbox'),
    public editNodeModalItem = (itemName: string) => cy.get('.itemHeader').contains(itemName),
    public moreIcon = () => cy.get('.inputList .anticon-more'),
    public menuItemByName = (itemName: string) =>
      cy.get('[role="menu"]:not(.optionsDropdown) [role="menuitem"]').contains(itemName),
    public editContentButton = () => cy.get('.inputList .contentEditButton'),
    public contentEditorTextarea = () => cy.get('.inputList .contentBodyEditor'),
    public saveEditContentButton = () => cy.get('.inputList .saveItEditButton'),
    public addClosedInputFieldError = (formName: string, fieldName: string) =>
      this.closeQuestionModalForm(formName)
        .find('.ant-form-item-label')
        .contains(fieldName)
        .parents('.ant-row')
        .find('.ant-form-explain'),
    public nodeDetailsPanel = () => cy.get('[defaultactivekey="propertyTab"]'),
    public nodeDetailsInput = (input: string) => this.nodeDetailsPanel().contains(`${input}`).find('input'),
    public nodeDetailsTextarea = (textarea: string) => this.nodeDetailsPanel().contains(`${textarea}`).find('textarea'),
  ) {
    super()
  }

  // tab Input - will be moved to TabInputSideBar.ts
  selectInputParserOption(optionName: string) {
    this.inputParsersOptionsDropdown().click()
    this.optionsDropdownItem(optionName).click()

    return this
  }

  clickAddOptionButton(optionName: string) {
    this.addOptionButton(optionName).click()

    return this
  }

  fillAddClosedInputField(formName: string, data: object) {
    // @ts-ignore
    this.addClosedInputField(formName, data.fieldName, data.fieldType).clear().type(data.value)

    return this
  }

  clickFormButtonByText(buttonText: string) {
    this.formButtonByText(buttonText).click({ force: true })

    return this
  }

  checkCreatedOption(label: string, grammarSet: string, description: string) {
    this.createdOptionField(label, 'itemLabel').should('have.text', label)
    this.createdOptionField(label, 'contentBody').should('have.text', grammarSet)
    this.createdOptionField(label, 'descriptionSpan').should('have.text', description)

    return this
  }

  checkInputParsersCount(count: string) {
    this.inputParsersCounter(count).should('be.visible')

    return this
  }

  addClosedOption(label: string, grammarSet: string, description: string) {
    this.clickAddOptionButton('Closed Options')
      .fillAddClosedInputField('Add Closed Input', {
        fieldName: 'Selection Label',
        fieldType: 'input',
        value: label,
      })
      .fillAddClosedInputField('Add Closed Input', {
        fieldName: 'Grammar Set',
        fieldType: 'textarea',
        value: grammarSet,
      })
      .fillAddClosedInputField('Add Closed Input', {
        fieldName: 'Description',
        fieldType: 'textarea',
        value: description,
      })
      .clickFormButtonByText('Add')
  }

  clickApplyAllToFlowCheckbox() {
    this.applyToAllFlowCheckbox().click()

    return this
  }

  checkEditNodeModalItemState(itemName: string, state: string) {
    this.editNodeModalItem(itemName).should(state)

    return this
  }

  clickMoreIcon() {
    this.moreIcon().click({ force: true })

    return this
  }

  selectMenuItemByName(itemName: string) {
    this.menuItemByName(itemName).click({ force: true })

    return this
  }

  clickEditContentButton() {
    this.editContentButton().click({ force: true })

    return this
  }

  fillContentEditorTextarea(value: string) {
    this.contentEditorTextarea().clear().type(value)

    return this
  }

  clickSaveEditContentButton() {
    this.saveEditContentButton().click({ force: true })

    return this
  }

  checkAddClosedInputFieldError(formName: string, fieldName: string, error: string) {
    this.addClosedInputFieldError(formName, fieldName).should('have.text', error)

    return this
  }

  checkAddClosedInputFieldErrorState(formName: string, fieldName: string, state: string) {
    this.addClosedInputFieldError(formName, fieldName).should(state)

    return this
  }
}

export default new TabInputSideBar()
