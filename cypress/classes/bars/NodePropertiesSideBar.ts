export class NodePropertiesSideBar {
  constructor(
    private sideBarSel = () => cy.get('div[selectednode="[object Object]"]'),
    private propertiesSel = () => cy.get('li[aria-controls="propertyTab"]'),
    private nodeDescSel = () => cy.get('textarea[placeholder]'),
    private nodeDescEditBtnSel = () =>
      cy.xpath('//textarea[@placeholder]/following-sibling::button[contains(@class, "contentEditButton")]'),
    // private editAcceptBtnSel = () => cy.xpath('(//button[contains(@class, "saveItEditButton")])[1]'),
    private editAcceptBtnSel = () => cy.contains('div', 'Node Description').find('.saveItEditButton '),
    private bothBtnSel = () =>
      cy.xpath('//label[contains(@class, "ant-radio-button-wrapper")]//span[contains(text(), "Both")]'),
    private outputBtnSel = () =>
      cy.xpath('//label[contains(@class, "ant-radio-button-wrapper")]//span[contains(text(), " Output ")]'),
    private descriptionBtnSel = () =>
      cy.xpath('//label[contains(@class, "ant-radio-button-wrapper")]//span[contains(text(), "Description")]'),
    private outputSel = () => cy.get('li[aria-controls="OutputTab"]'),
    private contentVisibleSel = () => cy.xpath('(//div[@class="contentView"])[2]'),
    private contentEmptySel = () => cy.xpath('(//div[@class="contentEmpty"])[2]'),
    private addNewOutputConditionBtnSel = () => cy.get('#OutputTab button.addButton'),
    private flowSel = () => cy.get('li[aria-controls="flowTab"]'),
    private channelsSel = () => cy.get('li[aria-controls="channelTab"]'),
    private currentChannelSel = () => cy.xpath('(//div[@class="itemHeader"])[2]'),
    private sideNodeSel = () => cy.get('div.itemHeader'),
    private nameNodeSel = (linkNode: string) =>
      cy.xpath('//div[@class="itemHeader" and contains(text(), "' + linkNode + '")]'),
    private sideNodeNameSel = (linkNode: string) =>
      cy.xpath(
        `//div[@class="itemHeader" and contains(text(), "${linkNode}")]/following-sibling::div[@class="contentBody"]`,
      ),
    private addBthSel = () => cy.xpath('//div[@class="ant-collapse-extra"]//button'),
    private reasonNodeSel = () => cy.xpath('//div[@class="descriptionSpan"]'),
    private propetiesPanel = () => cy.get('.is-active > .ui-ripple-ink'),
  ) {}

  openProperties() {
    this.propertiesSel().should('be.visible').click()

    return this
  }

  selectTabByName(tabName: string) {
    this.tabByName(tabName).click({ force: true })

    return this
  }

  // tab Properties - will be moved to TabPropertiesSideBar.ts
  setDescription(text: string, opt?: Partial<Cypress.TypeOptions>) {
    this.nodeDescEditBtnSel().should('be.visible').click()
    this.nodeDescSel().sendKeys(text, opt)
    this.editAcceptBtnSel().should('be.visible').click()

    return this
  }

  isDescriptionSet(text: string) {
    this.nodeDescSel().should('have.value', text)

    return this
  }

  clickOnBothBtn(opt?: Partial<Cypress.ClickOptions>) {
    this.bothBtnSel().should('be.visible').click(opt)

    return this
  }

  isBothBtnFocused() {
    this.bothBtnSel().parent().should('have.class', 'ant-radio-button-wrapper-checked')

    return this
  }

  isSideNode(linkNode: string, text: string) {
    this.nameNodeSel(linkNode).should('contain.text', text)

    return this
  }

  isSideNodeConditionSel(linkNode: string, text: string) {
    this.sideNodeNameSel(linkNode).should('contain.text', text)

    return this
  }

  isNotExistSideNodeCondition(linkNode: string, text: string) {
    this.sideNodeNameSel(linkNode).should('not.exist', text)

    return this
  }

  isSideNodeReasonSel(text: string) {
    this.reasonNodeSel().should('contain.text', text)

    return this
  }

  // tab Output - will be moved to TabOutputSideBar.ts
  clickOnOutputBtn(opt?: Partial<Cypress.ClickOptions>) {
    this.outputBtnSel().should('be.visible').click(opt)

    return this
  }

  isOutputBtnFocused() {
    this.outputBtnSel().parent().should('have.class', 'ant-radio-button-wrapper-checked')

    return this
  }

  clickOnDescriptionBtn(opt?: Partial<Cypress.ClickOptions>) {
    this.descriptionBtnSel().should('be.visible').click(opt)

    return this
  }

  isDescriptionBtnFocused() {
    this.descriptionBtnSel().parent().should('have.class', 'ant-radio-button-wrapper-checked')

    return this
  }

  openOutput(opt?: Partial<Cypress.ClickOptions>) {
    this.outputSel().should('be.visible').click(opt)

    return this
  }

  isContentSet(text: string) {
    this.contentVisibleSel().should('have.text', ' ' + text + ' ')

    return this
  }

  clickOnAddNewOutputConditionBtn(opt?: Partial<Cypress.ClickOptions>) {
    this.addNewOutputConditionBtnSel().should('be.visible').click(opt)

    return this
  }

  // tab Flow - will be moved to TabFlowSideBar.ts

  openFlow() {
    this.flowSel().should('be.visible').click()

    return this
  }

  openChannels() {
    this.channelsSel().should('be.visible').click()

    return this
  }

  isChannelChosen(text: string) {
    this.currentChannelSel().should('have.text', text)
  }

  // from NodeDetails.ts

  checkNodeDetailsInputValue(field: string, value: string) {
    this.nodeDetailsInput(field).should('have.value', value)

    return this
  }

  checkNodeDetailsTextareaValue(field: string, value: string) {
    this.nodeDetailsTextarea(field).should('have.value', value)

    return this
  }

  checkPropertiesPanelVisibility(assertion: any) {
    this.propetiesPanel().should(assertion)
  }

  isSideNodeDescription(linkNode: string, text: string) {
    this.sideNodeSel().contains(linkNode).should('contain.text', text)

    return this
  }
}

export default new NodePropertiesSideBar()
