import { dataMapper } from '../../fixtures/data/deploy-mapper/mapperData'
// const jq = require('node-jq')

export class Mapper {
  constructor(
    private description = () => cy.get('[col-id="description"]'),
    public conditionalLabel = () => cy.get('[placeholder="Enter unique condition label."]'),
    private tabContainer = () => cy.get('.gridMod'),
    private headerContainer = () => cy.get('[ref="eHeaderContainer"]'),
    private centerContainer = () => cy.get('[ref="eCenterContainer"]'),
    private leftContainer = () => cy.get('[ref="eLeftContainer"]'),
    private headerTagID = () => cy.get('[col-id="tag"] .ag-header-cell-text'),
    private headerMappingValues = () => cy.get('[col-id="0_0"] .ag-header-group-text'),
    private headerNodeDetails = () => cy.get('[col-id="2_0"] .ag-header-group-text'),
    private centerViewport = () => cy.get('[ref="eCenterViewport"]'),
    /*  private rowIdLeftContainer = () => cy.get('[ref="eLeftContainer"]')
    .contains(tagId)
    .parent()
    .parent()
    .parent()
    .attribute('row-id'), */
    public columnGroupText = '.ag-header-group-text',
    private headerCellText = '.ag-header-cell-text',
    private row = '[role="row"]',
    private cellValue = '.ag-cell-value',
    private hasMappingColumn = '[col-id="1"]',
    private nodeLabelColumn = '[col-id="nodeLabel"]',
    private payloadlColumn = '[col-id="payload"]',
    private descriptionColumn = '[col-id="description"]',
    private elementTypeColumn = '[col-id="elementType"]',
    private nodeTypeColumn = '[col-id="nodeType"]',
    private channelColumn = '[col-id="channel"]',
    private templateLabelColumn = '[col-id="templateLabel"]',
    private conditionColumn = '[col-id="condition"]',
    private conditionTypeColumn = '[col-id="conditionType"]',
    private gotoColumn = '[col-id="goto"]',
    private piiColumn = '[col-id="pii"]',
    private parentColumn = '[col-id="parent"]',
    private rowId = 'row-id',
    private centerColumnClipper = '[ref="eCenterColsClipper"]',
    private outputSel = '//label[@class="ant-radio-button-wrapper"][1]',
  ) {}

  chooseTab(name: string, opt?: Partial<Cypress.ClickOptions>) {
    this.tabContainer().contains('span', name).should('be.visible').click(opt)
    return this
  }

  checkTableAG(entity: object) {
    cy.get('#grid')
      .getAgGridData()
      .then((actualTableData) => {
        cy.agGridValidateRowsSubset(actualTableData, entity)
      })
  }

  // check particular header cell value
  checkHeaderValue(columnId: string, value: string) {
    this.headerContainer()
      .find(columnId)
      .scrollIntoView()
      .find(this.headerCellText)
      .should('have.text', value)
      .and('be.visible')

    return this
  }

  // check particular cell value
  checkCellValue(row: string, column: string, value: string) {
    this.centerContainer().find(row).find(column).scrollIntoView().should('have.text', value)

    return this
  }

  // check header cell values
  checkHeadersValues() {
    // tagId, mappingValues, nodeDetails, hasMapping, node, payload, description
    this.headerTagID().should('have.text', 'Tag ID')
    this.headerMappingValues().should('have.text', 'Mapping Values')
    this.headerNodeDetails().should('have.text', 'Node Details')
    this.checkHeaderValue(this.hasMappingColumn, 'Has Mapping')
    this.checkHeaderValue(this.nodeLabelColumn, 'Node')
    this.checkHeaderValue(this.payloadlColumn, 'Payload')
    this.checkHeaderValue(this.descriptionColumn, 'Description')

    return this
  }

  // tagId, mappingValues, nodeDetails, hasMapping, node, payload, description +
  checkHeadersWithNodeDetails() {
    this.headerTagID().should('have.text', 'Tag ID')
    this.headerMappingValues().should('have.text', 'Mapping Values')
    this.headerNodeDetails().should('have.text', 'Node Details')
    this.checkHeaderValue(this.hasMappingColumn, 'Has Mapping')
    this.checkHeaderValue(this.nodeLabelColumn, 'Node')
    this.checkHeaderValue(this.elementTypeColumn, 'Element Type')
    this.checkHeaderValue(this.nodeTypeColumn, 'Node Type')
    this.checkHeaderValue(this.channelColumn, 'Channel')
    this.checkHeaderValue(this.templateLabelColumn, 'Template Label')
    this.checkHeaderValue(this.conditionColumn, 'Condition')
    this.checkHeaderValue(this.conditionTypeColumn, 'Condition Type')
    this.checkHeaderValue(this.gotoColumn, 'Goto')
    this.centerViewport().scrollTo('right')
    this.checkHeaderValue(this.piiColumn, 'PII')
    this.checkHeaderValue(this.parentColumn, 'Parent')
    this.checkHeaderValue(this.payloadlColumn, 'Payload')
    this.checkHeaderValue(this.descriptionColumn, 'Description')
    this.centerViewport().scrollTo('left')

    return this
  }

  // check particular cell value in column Tag ID
  checkCellValueTagID(row: string, value: string) {
    this.leftContainer().find(row).find(this.cellValue).should('have.text', value)
  }

  // check particular row values
  checkRowValues(row: string, value: string, hasMapping: string, node: string, payload: string, description: string) {
    this.checkCellValueTagID(row, value)
    this.checkCellValue(row, this.hasMappingColumn, hasMapping)
    this.checkCellValue(row, this.nodeLabelColumn, node)
    this.checkCellValue(row, this.payloadlColumn, payload)
    this.checkCellValue(row, this.descriptionColumn, description)

    return this
  }

  // check particular row values
  checkRowValuesObject(row: string, json) {
    this.checkCellValueTagID(row, json.tagID)
    this.checkCellValue(row, this.hasMappingColumn, json.hasMapping)
    this.checkCellValue(row, this.nodeLabelColumn, json.node)
    this.checkCellValue(row, this.payloadlColumn, json.payload)
    this.checkCellValue(row, this.descriptionColumn, json.description)

    return this
  }

  // check particular row values with object as a value
  checkFullRowValues(row: string, json: object, project: string, flowStart: string) {
    this.checkCellValueTagID(row, json.tagID)
    this.checkCellValue(row, '[col-id="1"]', json.hasMapping)
    this.checkCellValue(row, '[col-id="nodeLabel"]', json.node)
    this.checkCellValue(row, '[col-id="elementType"]', json.elementType)
    this.checkCellValue(row, '[col-id="nodeType"]', json.nodeType)
    this.checkCellValue(row, '[col-id="channel"]', json.channel)
    this.checkCellValue(row, '[col-id="templateLabel"]', json.templateLabel)
    this.checkCellValue(row, '[col-id="condition"]', json.condition)
    this.checkCellValue(row, '[col-id="conditionType"]', json.conditionType)
    this.checkCellValue(row, '[col-id="goto"]', json.goto)
    this.checkCellValue(row, '[col-id="pii"]', json.pii)
    if (flowStart === 'flowPattern') {
      this.checkCellValue(row, '[col-id="parent"]', project + '/' + json.pattern)
    } else {
      this.checkCellValue(row, '[col-id="parent"]', project + '/' + json.pattern + '/' + dataMapper.subFlow)
    }

    if (flowStart === 'flowStartFlow') {
      this.checkCellValue(
        row,
        '[col-id="payload"]',
        'Start Flow: ' + project + '/' + json.pattern + '/' + dataMapper.subFlow + ' | GOTO: ' + json.nodeGoTo,
      )
    } else {
      this.checkCellValue(row, '[col-id="payload"]', json.payload)
    }

    // this.checkCellValue(row, '[col-id="description"]', "Start Flow: " + pattern + project + "/" + pattern )
    if (flowStart === 'flowStartFlow') {
      this.checkCellValue(
        row,
        '[col-id="description"]',
        'Start Flow: ' + json.pattern + project + '/' + json.pattern + '/' + dataMapper.subFlow,
      )
    } else if (flowStart === 'flowStartPattern') {
      this.checkCellValue(row, this.descriptionColumn, project + '/' + json.pattern)
    } else {
      this.checkCellValue(row, this.descriptionColumn, json.description)
    }
    cy.get('[ref="eCenterViewport"]').scrollTo('left')

    return this
  }

  checkRowValuesNew(row: string, json: object, project?: string) {
    this.checkCellValueTagID(row, json.tagID)
    this.checkCellValue(row, this.hasMappingColumn, json.hasMapping)
    this.checkCellValue(row, this.nodeLabelColumn, json.node)
    this.checkCellValue(row, this.payloadlColumn, json.payload)
    if (project) {
      this.checkCellValue(row, this.descriptionColumn, 'Start Flow: ' + json.pattern + project + '/' + json.pattern)
    } else {
      this.checkCellValue(row, this.descriptionColumn, json.description, project)
    }

    return this
  }

  checkRowByTagId(tagId: string, json) {
    cy.get('[ref="eLeftContainer"]')
      .contains(tagId)
      .parent()
      .parent()
      .parent()
      .attribute(this.rowId)
      .then(($attr) => {
        const rowNumber = $attr
        cy.get(this.centerColumnClipper)
          .find(this.row)
          .eq(rowNumber)
          .then(($row) => {
            expect($row.find(this.hasMappingColumn).text()).to.equal(json.hasMapping)
            expect($row.find(this.nodeLabelColumn).text()).to.equal(json.node)
            expect($row.find(this.payloadlColumn).text()).to.equal(json.payload)
            expect($row.find(this.descriptionColumn).text()).to.equal(json.description)
          })
      })

    return this
  }

  expandNodeDetails() {
    cy.contains(this.columnGroupText, 'Node Details').should('be.visible').dblclick()

    return this
  }

  checkExportToExcel(compareFile) {
    cy.task('parseXlsx', 'cypress/downloads/export.xlsx').then((XLSXObject) => {
      let sheet1 = XLSXObject[0]
      let headers = sheet1.data[0]
      let result = []
      sheet1.data.slice(1).forEach((row, index) => {
        let object = {}
        headers.forEach((header, index) => {
          object[header] = row[index]
        })
        result.push(object)
      })
      cy.readFile(compareFile).should('deep.equal', result)
    })

    return this
  }

  checkExportToExcelNodeDetails(compareFile) {
    cy.task('parseXlsx', 'cypress/downloads/export.xlsx').then((XLSXObject) => {
      let sheet1 = XLSXObject[0]
      let headers = sheet1.data[0]
      let result = []
      sheet1.data.slice(1).forEach((row, index) => {
        let object = {}
        headers.forEach((header, index) => {
          object[header] = row[index]
        })
        result.push(object)
      })
      cy.log(result[0].Parent)
      let parent = Cypress.env('projectName') + '/' + 'PatternEndPoint/SubFlow'

      cy.readFile(compareFile).then((file) => {
        file[0].Parent = parent
        // write the merged array
        cy.writeFile(compareFile, JSON.stringify(file))
      })

      cy.readFile(compareFile).should('deep.equal', result)
    })

    return this
  }

  // WIP - works only for payload
  checkCellByTagId(tagId: string, column: string, value: string) {
    this.leftContainer()
      .contains(tagId)
      .parent()
      .parent()
      .parent()
      .attribute(this.rowId)
      .then(($attr) => {
        const rowNumber = $attr
        cy.get(this.centerColumnClipper)
          .find(this.row)
          .eq(rowNumber)
          .then(($row) => {
            expect($row.find(column).text()).to.equal(value)
          })
      })
  }

  clickOnOutput(opt?: Partial<Cypress.ClickOptions>) {
    cy.xpath(this.outputSel).click(opt)
  }
}
export default new Mapper()
