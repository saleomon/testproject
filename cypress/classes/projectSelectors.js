export default class ProjectsPageElements {
    static createProjectBtn = '#createNewProjectButton'
    static projectNameField = '[placeholder="Enter unique project label."]'
    static projectDescriptionField = '[placeholder="Describe this Project."]'
    static projectTypeSel = '[title="Type"]' 
    static exportProjectBtn = '[data-cy="ExportProjectButton"] button' 
}

