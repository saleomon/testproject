export default class NodePropertiesPanel {
    constructor(){
        this.elementSelector = 'div[selectednode="[object Object]"]' //any other ideas? ;)
        this.closePropertiesButtonClassSelector = '.closeButton.ant-btn.ant-btn-link.ant-btn-circle.ant-btn-sm.ant-btn-icon-only'
    }

    element(){
        return cy.get(this.elementSelector).then($obj => {
            cy.wrap($obj).then($wrapped => {
                $wrapped
            })
        })
    }

    closePropertiesButton(){
        return cy.get(this.closePropertiesButtonClassSelector).then($obj => {
            cy.wrap($obj).then($wrapped => {
                $wrapped
            })
        })
    }

    
}