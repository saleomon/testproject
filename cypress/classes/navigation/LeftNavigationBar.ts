export class LeftNavigationBar {
  constructor(
    public deployIconButton = '.navButton:nth-child(4)',
    public controlPanelBtn = '//div[@class="navButton"]/a[@href="/control-panel/projects"]',
    public outcomecTab = 'div.navButton a[href="/designer/metrics"]',
  ) {}

  clickOnDeploytBtn(opt?: Partial<Cypress.ClickOptions>) {
    cy.get(this.deployIconButton).should('be.visible').click(opt)

    return this
  }

  clickOnSettingBtn(opt?: Partial<Cypress.ClickOptions>) {
    cy.xpath(this.controlPanelBtn).should('be.visible').click(opt)

    return this
  }

  clickOutcomecTab(opt?: Partial<Cypress.ClickOptions>) {
    cy.get(this.outcomecTab).should('be.visible').click(opt)

    return this
  }
}

export default new LeftNavigationBar()
