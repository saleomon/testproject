import { DeployTopBarHrefs } from '../../fixtures/navigation-data/deploy-top-bar-hrefs'
import { DesignerTopBarHrefs } from '../../fixtures/navigation-data/designer-top-bar-hrefs'

export class NavigationBar {
  constructor(
    public leftSidebarButton = (page: string) => cy.get(`.navButton a[href="${page}"]`),
    public userAvatarButton = () => cy.get('#header #user'),
    public changePassword = () => cy.xpath('//ul[@role="menu"]/li[contains(text(), "Change Password")]'),
    public horizontalNavBar = () => cy.get('#app #nav'),
    public topBarButton = (tab: string) => cy.get(`.topLink[href="${tab}"]`),
    public projectsDropDown = () => cy.get('#header .projectDropDown'),
    public userIconButton = () => cy.get('ant-avatar ant-avatar-circle ant-avatar-icon'),
    public flowIconButton = () => cy.get('a[href="/designer/nodes"]'),
    public patternIconButton = () => cy.get('a[href="/designer/patterns"]'),
    public contentIconButton = () => cy.get('a[href="/designer/content"]'),
    public outcomesIconButton = () => cy.xpath('//a[@href="/designer/metrics" and contains(text(), "Outcomes")]'),
    public projectOption = (projectName: string) => cy.xpath("//li[normalize-space()='" + projectName + "']"),
    public rocketIconButton = () => cy.get('.navButton:nth-child(4)'),
  ) {}

  clickLeftSidebarButton(page: string, opt?: Partial<Cypress.ClickOptions>) {
    this.leftSidebarButton(page).click(opt)
    this.horizontalNavBar().should('be.visible')

    return this
  }

  clickUserAvatarButton(opt?: Partial<Cypress.ClickOptions>) {
    this.userAvatarButton().click(opt)

    return this
  }

  clickOnChangePassword(opt?: Partial<Cypress.ClickOptions>) {
    this.changePassword().click(opt)

    return this
  }

  clickTopBarButton(tab: string, opt?: Partial<Cypress.ClickOptions>) {
    this.topBarButton(tab).first().click(opt)
    this.horizontalNavBar().should('be.visible')

    return this
  }

  selectProject(projectName: string) {
    this.projectsDropDown().should('be.visible').click()
    cy.get('li').contains(projectName).should('be.visible').click()
    this.projectsDropDown().should('be.visible')
    // eslint-disable-next-line @typescript-eslint/no-unsafe-call
    this.projectsDropDown()
      // @ts-ignore
      .text()
      .then(($text: string) => {
        expect($text).include(projectName)
      })
  }

  clickUserButton(opt?: Partial<Cypress.ClickOptions>) {
    this.userIconButton().click(opt)

    return this
  }

  clickFlowBtn(opt?: Partial<Cypress.ClickOptions>) {
    this.flowIconButton().click(opt)

    return this
  }

  clickPatternBtn(opt?: Partial<Cypress.ClickOptions>) {
    this.patternIconButton().click(opt)

    return this
  }

  clickContentBtn(opt?: Partial<Cypress.ClickOptions>) {
    this.contentIconButton().click(opt)

    return this
  }

  clickOutcomeBtn(opt?: Partial<Cypress.ClickOptions>) {
    this.outcomesIconButton().click(opt)

    return this
  }

  openMapperPage() {
    cy.visit(DeployTopBarHrefs.mapper)

    return this
  }

  openPatternsPage() {
    cy.visit(DesignerTopBarHrefs.patterns)

    return this
  }

  openFlowsPage() {
    cy.visit(DesignerTopBarHrefs.flows)

    return this
  }

  openOutcomesPage() {
    cy.visit(DesignerTopBarHrefs.outcomes)

    return this
  }

  projectNotVisible(projectName: string) {
    this.projectsDropDown().should('be.visible').click({ force: true })
    this.projectOption(projectName).should('not.exist')
  }

  projectIsVisible(projectName: string) {
    this.projectsDropDown().should('be.visible').click({ force: true })
    this.projectOption(projectName).should('exist')
  }

  clickOnRocketBtn(opt?: Partial<Cypress.ClickOptions>) {
    this.rocketIconButton().should('be.visible').click(opt)
    return this
  }
}

export default new NavigationBar()
