class PropertiesPanelSel {
  constructor(
    public addButtonSel = () => '.addButton', 
    public moreButtonSel = () => '.anticon-more'
   ) {}
}

export default new PropertiesPanelSel()
