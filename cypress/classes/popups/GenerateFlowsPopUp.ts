import { AddNewNodePopUp } from './AddNewNodePopUp'

export class GenerateFlowsPopUp extends AddNewNodePopUp {
  constructor(protected settingsIconButton = 'button.settingsButton') {
    super()
  }

  clickOnSettingsIconBtn(opt?: Partial<Cypress.TypeOptions>) {
    cy.get(this.settingsIconButton).should('be.visible').click(opt)

    return this
  }
}

export default new GenerateFlowsPopUp()
