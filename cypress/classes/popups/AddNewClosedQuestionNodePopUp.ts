import { AddNewNodePopUp } from './AddNewNodePopUp'

export class AddNewClosedQuestionNodePopUp extends AddNewNodePopUp {
  constructor(
    protected defaultClosedQuestionSel = () => cy.xpath('(//input[@placeholder="Enter default content"])'),
    protected templateSel = () => cy.get('div[class="ant-select ant-select-enabled ant-select-sm"]'),
    protected templateOption = () => cy.get('//li[contains(@class,"ant-select-dropdown-menu-item")]'),
  ) {
    super()
  }

  isTemplateSelected(text: string) {
    this.templateSel().should('contain.text', text)

    return this
  }

  setDefaultClosedQuestion(text: string) {
    this.defaultClosedQuestionSel().sendKeys(text)

    return this
  }

  isDefaultClosedQuestionSet(text: string) {
    this.defaultClosedQuestionSel().should('contain.value', text)

    return this
  }

  setTemplate(templateOption: string){
    this.templateSel().click()
    cy.get('.ant-select-dropdown-menu-item').contains(templateOption).click();
    return this
  }
}

export default new AddNewClosedQuestionNodePopUp()
