import { DialogPopUp } from './DialogPopUp'

class EditExistingFlowConditionPopUp extends DialogPopUp {
  constructor(
    protected conditionLabel = () => cy.get('input[placeholder="Enter unique condition label."]'),
    protected saveBtnSel = () => cy.get('span.ant-form-item-children button.ant-btn.ant-btn-primary'),
  ) {
    super()
  }

  setConditionLabel(text: string, opt?: Partial<Cypress.TypeOptions>) {
    this.conditionLabel().sendKeys(text, opt)

    return this
  }

  clickOnSaveBtn(opt?: Partial<Cypress.ClickOptions>) {
    this.saveBtnSel().click(opt)

    return this
  }
}

export default new EditExistingFlowConditionPopUp()
