export class CreateNewProjectPopUp {
  constructor(
    protected projectNameFieldSel = 'input[placeholder="Enter unique project label."]',
    protected typeProjectSel = 'div[class="ant-select-selection__rendered"]',
    protected listTypeProjectSel = 'li[class="ant-select-dropdown-menu-item"]',
    protected descriptionSel = 'textarea[placeholder="Describe this Project."]',
    protected addButtonSel = 'button[class="ant-btn ant-btn-primary"]',
    protected testTypeProjectSel = 'li[class="ant-select-dropdown-menu-item ant-select-dropdown-menu-item-active"]',
  ) {
  }

  setProjectNameField(text: string, opt?: Partial<Cypress.TypeOptions>) {
    cy.get(this.projectNameFieldSel).should('be.visible').sendKeys(text)

    return this
  }

  selectTypeProject(text: string, opt?: Partial<Cypress.TypeOptions>) {
    cy.get(this.typeProjectSel).should('be.visible').click(opt)
    cy.get(this.listTypeProjectSel)
      .filter(':visible')
      .filter(':contains("' + text + '")')
      .click(opt)

    return this

    return this
  }

  selectTestProject(opt?: Partial<Cypress.TypeOptions>) {
    cy.get(this.typeProjectSel).should('be.visible').click(opt)
    cy.get(this.testTypeProjectSel).should('be.visible').click({ multiple: true })

    return this

    return this
  }

  setDescription(text: string, opt?: Partial<Cypress.TypeOptions>) {
    cy.get(this.descriptionSel).should('be.visible').sendKeys(text)

    return this
  }

  clickAddButton(opt?: Partial<Cypress.TypeOptions>) {
    cy.get(this.addButtonSel).should('be.visible').click(opt)

    return this
  }
}

export default new CreateNewProjectPopUp()
