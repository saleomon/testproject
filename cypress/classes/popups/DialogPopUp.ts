
export class DialogPopUp {
  constructor(
    protected popupSel = () => cy.get('.ant-modal-content'),
    protected popupCloseBtnSel = () => cy.get('.ant-modal-close'),
    protected addBtnSel = () => cy.xpath('//button[@class="ant-btn ant-btn-primary" and span[contains(text(), "Add")]]'),
    protected resetBtnSel = () => cy.xpath('//button[./span[contains(text(), "Reset")]]'),
    protected cancelBtnSel = () => cy.xpath('//button[./span[contains(text(), "Cancel")]]'),
    protected errorText = () => cy.get('.ant-form-explain') 
  ) {}

  isOpen() {
    this.popupSel().should('be.visible')

    return this
  }

  clickOnCloseBtn(opt?: Partial<Cypress.ClickOptions>) {
    this.popupCloseBtnSel().filter(':visible').click(opt)

    return this
  }

  clickOnAddBtn(opt?: Partial<Cypress.ClickOptions>) {
    this.addBtnSel().filter(':visible').click(opt)

    return this
  }

  clickOnResetBtn(opt?: Partial<Cypress.ClickOptions>) {
    this.resetBtnSel().filter(':visible').click(opt)

    return this
  }
}

export default new DialogPopUp()
