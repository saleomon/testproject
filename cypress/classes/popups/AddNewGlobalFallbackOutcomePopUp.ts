import { AddNewNodePopUp } from './AddNewNodePopUp'

export class AddNewGlobalFallbackOutcomePopUp extends AddNewNodePopUp {
  constructor(
    protected endpointLinkSel = () => cy.get('[placeholder="Enter unique Endpoint Label."]'),
    protected outcomeLinkType = () =>
      cy.get('span.ant-form-item-children div.ant-select-selection.ant-select-selection--single'),
    protected listOutcomeLinkType = () => cy.get(' ul[role="listbox"] li.ant-select-dropdown-menu-item'),
    protected reasonSel = () => cy.get('textarea[placeholder="Describe the reason."]'),
    protected errorMessage = () => cy.get('div.ant-form-explain'),
  ) {
    super()
  }

  clickOnAddBtn(opt: Partial<Cypress.ClickOptions> = {}) {
    opt.force = true

    this.addBtnSel().filter(':visible').click(opt)

    return this
  }

  setEndpointLink(text: string, opt?: Partial<Cypress.TypeOptions>) {
    this.endpointLinkSel().sendKeys(text, opt)

    return this
  }

  isEndpointLinkSet(text: string) {
    this.endpointLinkSel().should('contain.value', text)

    return this
  }

  selectLinkType(text: string, opt?: Partial<Cypress.ClickOptions>) {
    this.outcomeLinkType().should('be.visible').click(opt)
    this.listOutcomeLinkType().contains(text).click(opt)

    return this
  }

  isLinkTypeSet(text: string) {
    this.outcomeLinkType().should('contain.text', text)

    return this
  }

  setReason(text: string, opt?: Partial<Cypress.TypeOptions>) {
    this.reasonSel().sendKeys(text, opt)

    return this
  }

  isReasonEmptySet(text: string) {
    this.reasonSel().should('contain.value', text)

    return this
  }

  checkErrorMessage(text: string) {
    this.errorMessage().should('contain.text', text)

    return this
  }
}

export default new AddNewGlobalFallbackOutcomePopUp()
