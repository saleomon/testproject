import { DialogPopUp } from './DialogPopUp'

export class AddNewNodePopUp extends DialogPopUp {
  constructor(
    protected labelSel = () => cy.get('[placeholder="Enter unique Node Label."]'),
    protected descriptionSel = () => cy.get('[placeholder="Describe this Conversational Node."]'),
    protected linkSel = () => cy.get('[placeholder="Enter unique Link Label."]'),
    protected linkDescriptionSel = () => cy.get('[placeholder="Describe this Link."]'),
    protected promptSel = () => cy.get('[placeholder="Describe this Link."]'),
    protected multiComboboxSel = () => cy.get('.ant-select-selection.ant-select-selection--multiple'),
    protected singleComboboxSel = () => cy.get('.ant-select-selection.ant-select-selection--single'),
    protected errorMessageLabelSel = () =>
      cy.xpath('//div[@class="ant-form-explain" and contains(text(), "Each Node Label must be uniquely named.")]'),
    protected errorMessageLinkSel = () =>
      cy.xpath('//div[@class="ant-form-explain" and contains(text(), "Each Link Label must be uniquely named.")]'),
  ) {
    super()
  }

  setLabel(text: string, opt?: Partial<Cypress.TypeOptions>) {
    this.labelSel().sendKeys(text, opt)

    return this
  }

  isLabelSet(text: string) {
    this.labelSel().should('contain.value', text)

    return this
  }

  isNodeLabelErrorMessageNotDisplayed() {
    this.errorMessageLabelSel().should('not.exist')

    return this
  }

  setDescription(text: string, opt?: Partial<Cypress.TypeOptions>) {
    this.descriptionSel().sendKeys(text, opt)

    return this
  }

  isDescriptionSet(text: string) {
    this.descriptionSel().should('contain.value', text)

    return this
  }

  setLink(text: string, opt?: Partial<Cypress.TypeOptions>) {
    this.linkSel().sendKeys(text, opt)

    return this
  }

  isLinkSet(text: string) {
    this.linkSel().should('contain.value', text)

    return this
  }

  isLinkLabelErrorMessageNotDisplayed() {
    this.errorMessageLinkSel().should('not.exist')

    return this
  }

  setLinkDescription(text: string, opt?: Partial<Cypress.TypeOptions>) {
    this.linkDescriptionSel().sendKeys(text, opt)

    return this
  }

  isLinkDescriptionSet(text: string) {
    this.linkDescriptionSel().should('contain.value', text)

    return this
  }

  setPrompt(text: string, opt?: Partial<Cypress.TypeOptions>) {
    this.promptSel().sendKeys(text, opt)

    return this
  }

  errorTextCheck(text: string) {
    this.errorText().should('contain', text)
    return this
  }
  
}

export default new AddNewNodePopUp()
