import { AddNewNodePopUp } from './AddNewNodePopUp'

export class AddNewTagNodePopUp extends AddNewNodePopUp {
  errorTextCheck(text: string) {
    this.errorText().should('contain', text)
    return this
  }
}

export default new AddNewTagNodePopUp()
