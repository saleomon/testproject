class ChangePassword {
  constructor(
    protected oldBtnSel = () => cy.xpath('//input[@placeholder="Enter Old Password"]'),
    protected newBtnSel = () => cy.xpath('//input[@placeholder="Enter New Password"]'),
    protected repeatBtnSel = () => cy.xpath('//input[@placeholder="Enter New Password Again"]'),
    protected submitBtnSel = () => cy.xpath('//button[@class="ant-btn ant-btn-primary"]'),
  ) {}

  setOldPassword(password: string, opt?: Partial<Cypress.TypeOptions>) {
    this.oldBtnSel().sendKeys(password, opt)

    return this
  }

  setNewPassword(password: string, opt?: Partial<Cypress.TypeOptions>) {
    this.newBtnSel().sendKeys(password, opt)

    return this
  }

  setRepeatPassword(password: string, opt?: Partial<Cypress.TypeOptions>) {
    this.repeatBtnSel().sendKeys(password, opt)

    return this
  }

  clickOnSubmitBtn(opt?: Partial<Cypress.ClickOptions>) {
    this.submitBtnSel().click(opt)

    return this
  }

  isOldPassSet(text: string): ChangePassword {
    this.oldBtnSel().should('have.text', text)

    return this
  }

  isNewPassSet(text: string): ChangePassword {
    this.newBtnSel().should('have.text', text)

    return this
  }

  isRepeatPassSet(text: string): ChangePassword {
    this.repeatBtnSel().should('have.text', text)

    return this
  }
}

export default new ChangePassword()
