export class ReEnablePopUp {
  constructor(
    protected yesButtonSel = (username: string) =>
      cy.xpath(
        `//div[@class="ant-popover-message-title" and contains(text(), "Re-enable the user account ${username}")]/following::button[@class="ant-btn ant-btn-primary ant-btn-sm"]`,
      ),
  ) {}

  clickYesBtn(username: string, opt?: Partial<Cypress.TypeOptions>) {
    this.yesButtonSel(username).click(opt)

    return this
  }
}

export default new ReEnablePopUp()
