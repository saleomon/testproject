export class EntityTable {
  constructor(
    protected editBtnTableSel = '.editButton',
    protected labelColumn = '[col-id="label"]',
    protected descriptionColumn = '[col-id="description"]',
    protected valueTypeColumn = '[col-id="valueType"]',
    protected formatTypeColumn = '[col-id="formatType"]',
    protected writableColumn = '[col-id="writable"]',
    protected piiColumn = '[col-id="containsPii"]',
    protected mockValueColumn = '[col-id="mockValue"]',
    protected entityGroupColumn = '[col-id="entityGroupID"]',

    protected entityList = '[col-id="entityList"]',
    protected primaryKey = '[col-id="primaryKey"]',
    protected dependsOn = '[col-id="dependsOn"]',
    protected sharedEndPoint = '[col-id="sharedEndPoint"]',
    protected namespace = '[col-id="label_1"]',
    // Transit API is not implemented yet

    protected leftContainerSel = '[ref="eLeftContainer"]',
    protected centerContainerSel = '[ref="eCenterContainer"]',
    protected row = '[role="row"]',
    protected openModalBtn = () => cy.get('.addButton').filter(':visible'),
    protected closeIcon = '.eject-icon',
    protected tab = () => cy.get('.selectTokens'),
    protected entityTable = () => cy.get('#grid-entities'),
    protected entityGroupTable = () => cy.get('#grid-entity-groups'),
    protected leftContainer = () => cy.get('[ref="eLeftContainer"]'),
    protected centerContainer = () => cy.get('[ref="eCenterContainer"]'),
    protected body = () => cy.get('[ref="eBodyViewport"]'),
    protected editEntityButton = (name: string) =>
      cy.get('#grid-entities').find(this.leftContainerSel).find(this.row).contains(name)
        .parent().children(this.editBtnTableSel),
    protected editEntityGroupButton = (name: string) =>
      cy.get('#grid-entity-groups').find('b').contains(name).parent().children(this.editBtnTableSel),
  ) {}

  openEditEnityGroupPopOver(label: string) {
    this.entityGroupTable()
      .find('b')
      .filter(':visible')
      .contains(label)
      .parent()
      .children('.editButton')
      .should('be.visible')
      .click()
  }

  checkEntityRowData(entity: object) {
    this.entityTable()
      .find(this.leftContainerSel).filter(':visible').find('b').contains(entity.entityLabel)
      .parent().parent().parent().parent().parent().attribute('row-id').then(($attr) => {
        const rowNumber = $attr
        this.entityTable().find(this.centerContainerSel).filter(':visible')
          .find(this.row).eq(rowNumber).scrollIntoView().then(($trow) => {
            expect($trow.find(this.valueTypeColumn).text()).to.equal(entity.type)
            expect($trow.find(this.formatTypeColumn).text()).to.equal(entity.formatType)
            expect($trow.find(this.descriptionColumn).text().trim()).to.equal(entity.description)
            expect($trow.find(this.entityGroupColumn).text()).to.equal(entity.entityGroup)
            expect($trow.find(this.piiColumn).text()).to.equal(entity.PII)
            expect($trow.find(this.writableColumn).text()).to.equal(entity.Writable)
            expect($trow.find(this.mockValueColumn).text()).to.equal(entity.mockValue)
          })
      })

    return this
  }

  checkEntityGroupRowData(entityGroup: object) {
    this.entityGroupTable().find('b').contains(entityGroup.entityGroupLabel)
      .parent().parent().parent().attribute('row-id').then(($attr) => {
        const rowNumber = $attr
        this.entityGroupTable()
          .find(this.centerContainerSel).filter(':visible').find(this.row)
          // .filter(':visible')
          .eq(rowNumber).scrollIntoView().then(($trow) => {
            expect($trow.find(this.entityList).text()).to.equal(entityGroup.entityList)
            expect($trow.find(this.primaryKey).text()).to.equal(entityGroup.primaryKey)
            expect($trow.find(this.dependsOn).text().trim()).to.equal(entityGroup.dependsOn)
            expect($trow.find(this.sharedEndPoint).text().trim()).to.equal(entityGroup.sharedEndpoint)
            expect($trow.find(this.descriptionColumn).text().trim()).to.equal(entityGroup.enitityGroupDescription)
            expect($trow.find(this.namespace).text().trim()).to.equal(
              Cypress.env('projectNameForEntity') +
                '/' +
                'TestPattern EntityEditor' +
                '/' +
                entityGroup.entityGroupLabel,
            )
          })
      })

    return this
  }

  checkTableAG(entity: object) {
    cy.get('#grid')
      .getAgGridData()
      .then((actualTableData) => {
        cy.agGridValidateRowsSubset(actualTableData, entity)
      })
  }

  closePopOver() {
    this.body().first().click({ force: true })
  }

  getEditEntityBtn(name: string) {
    return this.editEntityButton(name)
  }

  getEditEntityGroupBtn(name: string) {
    return this.editEntityGroupButton(name)
  }

  getDeleteEntityButton(name: string) {
    return this.entityTable().find(this.row).contains(name).next('.deleteButton')
  }

  getDeleteEntityGroupButton(name: string) {
    return this.entityGroupTable().find(this.row).contains(name).next('.deleteButton')
  }

  confirmDeletingEntityGroup() {
    cy.contains('Are you sure delete this Group?').should('be.visible')
    cy.contains('Yes').click()
  }

  confirmDeletingEntity() {
    cy.contains('Are you sure delete this entity?').should('be.visible')
    cy.contains('Yes').click()
  }

  openAddEntityPopOver() {
    this.openModalBtn().should('be.visible').click()
  }

  chooseTab(tab: string) {
    this.tab().contains('span', tab).should('be.visible').click({ force: true })
  }

  checkEntityAbsence(entity: string) {
    cy.contains('b', entity).should('not.exist')
  }

  checkEntityPresence(entity: string) {
    cy.contains('b', entity).should('exist')
  }
}
export default new EntityTable()
