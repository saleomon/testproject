export default class DiagramToolbar {
    constructor(){
        this.toolBarSelector = ".toolbarWrapper"
        this.deleteNodeButtonSelector = '[aria-label="Delete Outline icon"]'
        this.cutLinkToParentButtonSelector = '[aria-label="Link Off icon"]'
        this.revertToAutoLayoutButtonSelector = '[aria-label="Content Save Off Outline icon"]'
        this.saveLayoutButtonSelector = '[aria-label="Content Save Outline icon"]'
        this.entitiesViewerButtonSelector ='[aria-label="Database Plus Outline icon"]'
        this.closeButtonSelector = this.toolBarSelector + " .closeButton"
        this.openButtonSelector = this.toolBarSelector + " .openButton"
    }

    element(){
        return cy.get(this.toolBarSelector)
    }

    closeToolbarButton(){
        return cy.get(this.closeButtonSelector)
    }

    openToolbarButton(){
        return cy.get(this.openButtonSelector)
    }

    deleteNodeButton(){
        return cy.get(this.toolBarSelector).find(this.deleteNodeButtonSelector).then($obj => {
            cy.wrap($obj).then($wrapped => {
                $wrapped
            })
        })
    }

    cutLinkToParentButton(){
        return cy.get(this.toolBarSelector).find(this.cutLinkToParentButtonSelector).then($obj => {
            cy.wrap($obj).then($wrapped => {
                $wrapped
            })
        })
    }

    revertToAutoLayoutButton(){
        return cy.get(this.toolBarSelector).find(this.revertToAutoLayoutButtonSelector).then($obj => {
            cy.wrap($obj).then($wrapped => {
                $wrapped
            })
        })
    }

    saveLayoutButton(){
        return cy.get(this.toolBarSelector).find(this.saveLayoutButtonSelector).then($obj => {
            cy.wrap($obj).then($wrapped => {
                $wrapped
            })
        })
    }

    entitiesViewerButton(){
        return cy.get(this.toolBarSelector).find(this.entitiesViewerButtonSelector).then($obj => {
            cy.wrap($obj).then($wrapped => {
                $wrapped
            })
        })
    }    
}