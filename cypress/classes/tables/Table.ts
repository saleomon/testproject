export class Table {
  constructor(
    private expandRowArrow = () => '.ag-group-contracted:not(.ag-hidden) .ag-icon-tree-closed',

    private collapseRowArrow = () => '.ag-icon-tree-open',

    private selectedFlowTableRow = () => '.ag-center-cols-container [role="row"]',

    private tableRow = () => cy.get('[role="row"] [role="gridcell"]'),
    public columnHeaderByName = (name: string) => cy.get('.ag-header-container .ag-header-cell-label').contains(name),
  ) {}

  expandTableRowByText(rowText: string, opt?: Partial<Cypress.ClickOptions>) {
    this.tableRow().contains(rowText).parent('span').find(this.expandRowArrow()).click(opt)

    return this
  }

  expandAllTableRows(opt?: Partial<Cypress.ClickOptions>) {
    return cy.get(this.expandRowArrow()).each(($row) => {
      cy.wrap($row).click(opt)
    })
  }

  collapseTableRowByText(rowText: string, opt?: Partial<Cypress.ClickOptions>) {
    this.tableRow().contains(rowText).parent('span').find(this.collapseRowArrow()).click(opt)

    return this
  }

  checkTableInfo(tableSelector: string, expectedRow: [object]) {
    // eslint-disable-next-line @typescript-eslint/no-unsafe-call
    cy.get(tableSelector)
      // @ts-ignore
      .getAgGridData()
      .then((fullTable: [object]) => {
        fullTable.forEach(function (row: object, index: number) {
          // @ts-ignore
          Object.keys(row).forEach((key: string) => expect(expectedRow[index][key]).equals(row[key]))
        })
      })

    return this
  }

  checkTableLength(tableSelector: string, expectedNumber: number) {
    cy.get(tableSelector).find(this.selectedFlowTableRow()).should('have.length', expectedNumber)

    return this
  }
}
export default new Table()
