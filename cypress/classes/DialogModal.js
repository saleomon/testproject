export default class DialogModal {
  constructor() {
    this.dialogSelector = '.ant-modal-content' // dialog div
    this.closeButtonSelector = '.ant-modal-close' // close button
    this.addButtonSelector = 'Add'
    this.resetButtonSelector = 'Reset'
    this.cancelButtonSelector = 'Cancel'
  }

  dialogContainer() {
    return cy.get(this.dialogSelector).then(($obj) => {
      cy.wrap($obj).then(($wrapped) => {
        $wrapped
      })
    })
  }

  addButton() {
    return cy
      .get('span')
      .contains(this.addButtonSelector)
      .then(($obj) => {
        cy.wrap($obj).then(($wrapped) => {
          $wrapped
        })
      })
  }

  cancelButton() {
    return cy
      .get('span')
      .contains(this.cancelButtonSelector)
      .then(($obj) => {
        cy.wrap($obj).then(($wrapped) => {
          $wrapped
        })
      })
  }

  resetButton() {
    return cy
      .get('span')
      .contains(this.resetButtonSelector)
      .then(($obj) => {
        cy.wrap($obj).then(($wrapped) => {
          $wrapped
        })
      })
  }

  closeButton() {
    return cy.get(this.closeButtonSelector).then(($obj) => {
      cy.wrap($obj).then(($wrapped) => {
        $wrapped
      })
    })
  }
}

export class AddNewNodeDialog extends DialogModal {
  constructor() {
    super()
    this.labelSelector = '[placeholder="Enter unique Node Label."]'
    this.descriptionSelector = '[placeholder="Describe this Conversational Node."]'
    this.linkSelector = '[placeholder="Enter unique Link Label."]'
    this.linkDescriptionSelector = '[placeholder="Describe this Link."]'
    this.promptSelector = '[placeholder="Enter default content"]'
    this.multiComboboxSelector = '.ant-select-selection.ant-select-selection--multiple'
    this.singleComboboxSelector = '.ant-select-selection.ant-select-selection--single'
  }

  labelInput() {
    return cy
      .get(this.labelSelector)
      .should('be.visible')
      .then(($obj) => {
        cy.wrap($obj).then(($wrapped) => {
          $wrapped
        })
      })
  }

  descriptionInput() {
    return cy.get(this.descriptionSelector).then(($obj) => {
      cy.wrap($obj).then(($wrapped) => {
        $wrapped
      })
    })
  }

  linkLabelInput() {
    return cy.get(this.linkSelector).then(($obj) => {
      cy.wrap($obj).then(($wrapped) => {
        $wrapped
      })
    })
  }

  linkDescriptionInput() {
    return cy.get(this.linkDescriptionSelector).then(($obj) => {
      cy.wrap($obj).then(($wrapped) => {
        $wrapped
      })
    })
  }

  promptInput() {
    return cy.get(this.promptSelector).then(($obj) => {
      cy.wrap($obj).then(($wrapped) => {
        $wrapped
      })
    })
  }

  multiSelectCombobox() {
    return cy
      .get(this.dialogSelector)
      .find(this.multiComboboxSelector)
      .then(($obj) => {
        cy.wrap($obj).then(($wrapped) => {
          $wrapped
        })
      })
  }

  singleSelectCombobox() {
    return cy
      .get(this.dialogSelector)
      .find(this.singleComboboxSelector)
      .then(($obj) => {
        cy.wrap($obj).then(($wrapped) => {
          $wrapped
        })
      })
  }
}

// subflow node modal dialog (Patterns, Flows)
export class AddNewSubflowNodeDialog extends AddNewNodeDialog {
  constructor() {
    super()
  }

  channelComboBox() {
    return super.multiSelectCombobox()
  }
}

// skill router node modal dialog (Patterns)
export class AddNewSkillRouterNodeDialog extends AddNewNodeDialog {
  constructor() {
    super()
  }

  skillsComboBox() {
    return super.multiSelectCombobox()
  }
}

// tag node modal dialog (Patterns, Flows)
export class AddNewTagNodeDialog extends AddNewNodeDialog {
  constructor() {
    super()
  }

  checkResetAllFields() {
    this.labelInput.s
  }
}

// prompt node modal dialog (Flows)
export class AddNewPromptNodeDialog extends AddNewNodeDialog {
  constructor() {
    super()
  }

  templateCombobox() {
    return super.singleSelectCombobox()
  }

  defaultPromptInput() {
    return super.promptInput()
  }
}

// ClosedQ node modal dialog (Flows)
export class AddNewClosedQNodeDialog extends AddNewNodeDialog {
  constructor() {
    super()
  }

  templateCombobox() {
    return super.singleSelectCombobox()
  }

  defaultClosedQuestionInput() {
    return super.promptInput()
  }
}

// OpenQ node modal dialog (Flows)
export class AddNewOpenQNodeDialog extends AddNewNodeDialog {
  constructor() {
    super()
  }

  templateCombobox() {
    return super.singleSelectCombobox()
  }

  defaultOpenQuestionInput() {
    return super.promptInput()
  }
}

// an outcome node modal dialog: global outcomes, pattern outcomes, exit (from Patterns, Flows)
export class AddNewOutcomeNodeDialog extends AddNewNodeDialog {
  constructor() {
    super()
    this.endPointLinkLabelSelector = '[placeholder="Enter unique Endpoint Label."]'
    this.reasonSelector = '[placeholder="Describe the reason."]'
  }

  endpointLinkLabelInput() {
    return cy.get(this.endPointLinkLabelSelector).then(($obj) => {
      cy.wrap($obj).then(($wrapped) => {
        $wrapped
      })
    })
  }

  reasonInput() {
    return cy.get(this.reasonSelector).then(($obj) => {
      cy.wrap($obj).then(($wrapped) => {
        $wrapped
      })
    })
  }

  linkTypeCombobox() {
    return super.singleSelectCombobox()
  }
}

// goto node modal dialog (Patterns, Flows)
export class AddNewGotoNodeDialog extends AddNewNodeDialog {
  constructor() {
    super()
  }

  linkTypeCombobox() {
    return cy
      .get(this.dialogSelector)
      .find('div')
      .contains('Link Type')
      .parent()
      .next()
      .find(this.singleComboboxSelector)
      .then(($obj) => {
        cy.wrap($obj).then(($wrapped) => {
          $wrapped
        })
      })
  }

  linkToNodeCombobox() {
    return cy
      .get(this.dialogSelector)
      .find('div')
      .contains('Link to Node')
      .parent()
      .next()
      .find(this.singleComboboxSelector)
      .then(($obj) => {
        cy.wrap($obj).then(($wrapped) => {
          $wrapped
        })
      })
  }
}
