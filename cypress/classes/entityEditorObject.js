// old object - can be used in a few specs - should delete
const entityEditorObj = {
  openEntitiesViewer: '[aria-label="Database Plus Outline icon"]',
  openModalBtn: '.addButton ',
  entityLabelField: '[placeholder="Enter unique entity label."]',
  entityDescriptionField: '[placeholder="Describe this for developers."]',
  mockValueField: '[placeholder="Provide an example value for this entity."]',
  addBtn: '.ant-btn.ant-btn-primary',
  PIISwitch: '[title="PII"]',
  writableSwitch: '[title="Writable"]',
  closeIcon: '.eject-icon',
  openToolbarIcon: '#toolbar',
  sharedEndpoint: '[title="Shared Endpoint"]',
  entityGroupModal: 'Add New Entity Group',
  primaryKey: '[title="Primary Key"]',
  dependsOn: '[title="Depends On"]',
  entityGroupTable: '#grid-entity-groups',
  entityTable: '#grid-entity-groups',
  entityLabel: '[title="Entity Label"]',

  lengthLimitErrMsg: 'Length should be 2 to 32 chars',
  emptyFieldErrMsg: 'Please provide a unique entity label',
  notUniqueErrMsg: 'Label must be unique',
  containSpacesErrMsg: 'Label must be not use spaces or underscores',
  slashesPresentErrMsg: "Please don't use slashes.",
  noErrMsg: '',

  // methods
  chooseTab(tab){
    cy.get('.selectTokens').contains('span', tab).click()
  },

  checkFormatData(select, array) {
    cy.selectCustom('[title="Type"]', select)
    cy.get('[title="Format Type"]').parent().next().find('[role="combobox"]').click()
    cy.get('[role="listbox"]')
      .last()
      .find('li')
      .each(($item, i) => {
        expect($item.text()).to.equal(array[i])
      })
    cy.get('[title="Format Type"]').parent().next().find('[role="combobox"]').click()
  },

  toogle(element) {
    return cy
      .get(element, { timeout: 1000 })
      .should('be.visible')
      .filter(':visible')
      .parent()
      .next()
      .find('button')
      .last()
  },

  checkEntityGroupTabel(entityGroupLabel, entityList, primaryKey, dependsOn, sharedEndPoint, label_1, description) {
    cy.get('#entityViewer #grid')
      .last()
      .find('[role="rowgroup"]')
      .contains(entityGroupLabel)
      .parent()
      .parent()
      .parent()
      .attribute('row-id')
      .then(($attr) => {
        const rowNumber = $attr
        const dependsOnStr = dependsOn.toString()
        cy.get('#entityViewer #grid')
          .last()
          .find('[ref="eCenterContainer"]')
          .find('[role="row"]')
          .eq(rowNumber)
          .then(($trow) => {
            expect($trow.find('[col-id="entityList"]').text()).to.equal(entityList)
            expect($trow.find('[col-id="primaryKey"]').text()).to.equal(primaryKey)
            expect($trow.find('[col-id="dependsOn"]').text()).to.equal(dependsOnStr)
            expect($trow.find('[col-id="sharedEndPoint"]').text()).to.equal(sharedEndPoint)
            expect($trow.find('[col-id="label_1"]').text()).to.equal(label_1)
            expect($trow.find('[col-id="description"]').text()).to.equal(description)
          })
      })
  },

  fillEntity(entityLabel, enitityDescription, type, formatType, mockValue, PII, Writable) {
    cy.get(entityEditorObj.entityLabelField).last().clear().type(entityLabel)
    cy.selectCustom('[title="Type"]', type)
    cy.selectCustom('[title="Format Type"]', formatType)
    cy.get(entityEditorObj.entityDescriptionField).last().clear().type(enitityDescription)
    cy.get(entityEditorObj.mockValueField).last().clear().type(mockValue)

    this.toogle(entityEditorObj.PIISwitch)
      .attribute('class')
      .then((attr) => {
        cy.log(attr)
        if (attr === 'ant-switch' && PII) {
          entityEditorObj.toogle(entityEditorObj.PIISwitch).click().should('have.class', 'ant-switch-checked')
        } else {
          entityEditorObj.toogle(entityEditorObj.PIISwitch).click().should('not.have.class', 'ant-switch-checked')
        }
      })

    this.toogle(entityEditorObj.writableSwitch)
      .attribute('class')
      .then(($attr) => {
        if ($attr == 'ant-switch' && Writable) {
          entityEditorObj.toogle(entityEditorObj.writableSwitch).click().should('have.class', 'ant-switch-checked')
        } else {
          entityEditorObj.toogle(entityEditorObj.writableSwitch).click().should('not.have.class', 'ant-switch-checked')
        }
      })
  },

  fillPII(value, tooltip) {
    const rawTypeString = (str) => str.replace(/{/g, '{{}')
    cy.contains('.itemConditionType', '- preBridge')
      .parent()
      .next()
      .within(($item) => {
        cy.wrap($item).find('.contentEditButton').click()
        cy.wrap($item).find('.contentBodyEditor').clear().type(rawTypeString(value))
        if (tooltip && tooltip !== 'notFound') {
          cy.document().its('body').find('.highlight').should('have.text', tooltip)
        } else if (tooltip === 'notFound') {
          cy.document().its('body').find('.tribute-container li').should('have.text', 'No Match Found!')
        }
        cy.wrap($item).find('.saveItEditButton').click()
      })
  },
}

export { entityEditorObj }
