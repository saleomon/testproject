import ChangePassword from '../../classes/popups/ChangePassword'
import LoginPage from '../../classes/pages/LoginPage'
import NavigationBar from '../../classes/navigation/NavigationBar'
import UserValidCreds from '../../fixtures/provider/UserValidCreds'
import UserInvalidCreds from '../../fixtures/provider/UserInvalidCreds'
import UserEmptyCreds from '../../fixtures/provider/UserEmptyCreds'

describe('check login [API]', () => {
  beforeEach(() => {
    cy.restoreLocalStorage()
    cy.loginAPI()
  })

  UserValidCreds.forEach((input) => {
    it(input.title, () => {
      cy.request({
        method: 'POST',
        url: Cypress.env('baseUrlAPI') + '/auth/login',
        body: {
          username: input.username,
          password: input.password,
        },
      }).then((resp) => {
        expect(resp.status).eql(200)
        expect(resp.body.code).eql(200)
        expect(resp.body.message).eql('successfully retrieved token')
        expect(resp.body.payload.id).a('string')
        expect(resp.body.payload.userName).eql(Cypress.env<string>('username'))
        expect(resp.body.payload.email).eql(null)
        expect(resp.body.payload.isAdmin).eql(true)
        expect(resp.body.payload.accessToken).a('string')
        expect(resp.body.payload.expiresIn).eql(600)
        expect(resp.body.payload.refreshToken).a('string')
        expect(resp.body.payload.refreshExpiresIn).eql(1800)
      })
    })
  })

  UserInvalidCreds.forEach((input) => {
    it(input.title, () => {
      cy.request({
        method: 'POST',
        url: Cypress.env('baseUrlAPI') + '/auth/login',
        body: {
          username: input.username,
          password: input.password,
        },
        failOnStatusCode: false,
      }).then((resp) => {
        expect(resp.body.status).eql(input.errors.status)
        expect(resp.body.message).eql(input.errors.message)
      })
    })
  })

  UserEmptyCreds.forEach((input) => {
    it(input.title, () => {
      cy.request({
        method: 'POST',
        url: Cypress.env('baseUrlAPI') + '/auth/login',
        body: {
          username: input.username,
          password: input.password,
        },
        failOnStatusCode: false,
      }).then((resp) => {
        expect(resp.body.name).eql('ValidationError')
        expect(resp.body.message).eql('Validation Failed')
        expect(resp.body.statusCode).eql(400)
        expect(resp.body.error).eql('Bad Request')

        expect(resp.body.details.body[0].message).eql(input.errors.message)
        expect(resp.body.details.body[0].path).eql(input.errors.path)
        expect(resp.body.details.body[0].type).eql(input.errors.type)
      })
    })
  })

  it.only('[CMTS-11012] Login page. Check login with credentials for newly created user (API)', () => {
    let username = 'username_' + Date.now()
    cy.request({
      method: 'POST',
      url: Cypress.env('baseUrlAPI') + '/auth/login',
      body: {
        username: Cypress.env('username'),
        password: Cypress.env('password'),
      },
    }).then((resp1) => {
      expect(resp1.body.status).eql('success')
      expect(resp1.body.code).eql(200)
      expect(resp1.body.message).eql('successfully retrieved token')
      expect(resp1.body.payload.id).a('string')
      expect(resp1.body.payload.userName).eql('dhb-admin')
      expect(resp1.body.payload.email).eql(null)
      // expect(resp1.body.payload.isAdmin).eql(true)
      expect(resp1.body.payload.accessToken).a('string')
      expect(resp1.body.payload.expiresIn).eql(600)
      expect(resp1.body.payload.refreshToken).a('string')
      expect(resp1.body.payload.refreshExpiresIn).eql(1800)

      cy.request({
        method: 'POST',
        url: Cypress.env('baseUrlAPI') + '/admin/users',
        headers: {
          Authorization: 'Bearer ' + resp1.body.payload.accessToken,
        },
        body: [
          {
            username,
            firstName: 'testuser',
            lastName: 'testuser',
            email: username + '@deloitte.ua',
            emailVerified: false,
            enabled: true,
            credentials: [
              {
                temporary: false,
                type: 'password',
                value: '12345678',
              },
            ],
            realmRoles: ['admin'],
            clientRoles: {
              'heartbeat-app': [
                'project-viewers',
                'project-creators',
                'project-editors',
                'project-delete',
                'heartreach-viewers',
              ],
            },
            attributes: {
              organization_id: 'a58dcd34-45fd-4798-b2e7-493955bd08ff',
            },
          },
        ],
      }).then((resp2) => {
        expect(resp2.body.createdUsers[0].username).eql(username)
        expect(resp2.body.createdUsers[0].firstName).eql('testuser')
        expect(resp2.body.createdUsers[0].lastName).eql('testuser')
        expect(resp2.body.createdUsers[0].email).eql(username + '@deloitte.ua')
        // expect(resp2.body.createdUsers[0].emailVerified).eql(false)
        expect(resp2.body.createdUsers[0].enabled).eql(true)
        // expect(resp2.body.createdUsers[0].credentials[0].temporary).eql(false)
        expect(resp2.body.createdUsers[0].credentials[0].type).eql('password')
        expect(resp2.body.createdUsers[0].credentials[0].value).eql('12345678')
        expect(resp2.body.createdUsers[0].realmRoles[0]).eql('admin')
        expect(resp2.body.createdUsers[0].clientRoles['heartbeat-app'][0]).eql('project-viewers')
        expect(resp2.body.createdUsers[0].clientRoles['heartbeat-app'][1]).eql('project-creators')
        expect(resp2.body.createdUsers[0].clientRoles['heartbeat-app'][2]).eql('project-editors')
        expect(resp2.body.createdUsers[0].clientRoles['heartbeat-app'][3]).eql('project-delete')
        expect(resp2.body.createdUsers[0].clientRoles['heartbeat-app'][4]).eql('heartreach-viewers')
        expect(resp2.body.createdUsers[0].attributes.organization_id).a('string')

        cy.request({
          method: 'POST',
          url: Cypress.env('baseUrlAPI') + '/auth/login',
          body: {
            username,
            password: '12345678',
          },
        }).then((resp3) => {
          expect(resp3.body.status).eql('success')
          expect(resp3.body.code).eql(200)
          expect(resp3.body.message).eql('successfully retrieved token')
          expect(resp3.body.payload.id).a('string')
          expect(resp3.body.payload.userName).eql(username)
          expect(resp3.body.payload.email).eql(username + '@deloitte.ua')
         //  expect(resp3.body.payload.isAdmin).eql(true)
          expect(resp3.body.payload.accessToken).a('string')
          expect(resp3.body.payload.expiresIn).eql(600)
          expect(resp3.body.payload.refreshToken).a('string')
          expect(resp3.body.payload.refreshExpiresIn).eql(1800)
        })
      })
    })
  })

  it('[CMTS-11013] Login page. Check login with updated password (API)', () => {
    let username = 'username_' + Date.now()
    cy.request({
      method: 'POST',
      url: Cypress.env('baseUrlAPI') + '/auth/login',
      body: {
        username: 'dhb-admin',
        password: 'dhb@dm!n123',
      },
    }).then((resp1) => {
      expect(resp1.body.status).eql('success')
      expect(resp1.body.code).eql(200)
      expect(resp1.body.message).eql('successfully retrieved token')
      expect(resp1.body.payload.id).a('string')
      expect(resp1.body.payload.userName).eql('dhb-admin')
      expect(resp1.body.payload.email).eql(null)
      expect(resp1.body.payload.isAdmin).eql(true)
      expect(resp1.body.payload.accessToken).a('string')
      expect(resp1.body.payload.expiresIn).eql(600)
      expect(resp1.body.payload.refreshToken).a('string')
      expect(resp1.body.payload.refreshExpiresIn).eql(1800)

      cy.request({
        method: 'POST',
        url: Cypress.env('baseUrlAPI') + '/admin/users',
        headers: {
          Authorization: 'Bearer ' + resp1.body.payload.accessToken,
        },
        body: [
          {
            username,
            firstName: 'testuser',
            lastName: 'testuser',
            email: username + '@deloitte.ua',
            emailVerified: false,
            enabled: true,
            credentials: [
              {
                temporary: false,
                type: 'password',
                value: '12345678',
              },
            ],
            realmRoles: ['user'],
            clientRoles: {
              'heartbeat-app': [
                'project-viewers',
                'project-creators',
                'project-editors',
                'project-delete',
                'heartreach-viewers',
              ],
            },
            attributes: {
              organization_id: 'a58dcd34-45fd-4798-b2e7-493955bd08ff',
            },
          },
        ],
      }).then((resp2) => {
        expect(resp2.body.createdUsers[0].username).eql(username)
        expect(resp2.body.createdUsers[0].firstName).eql('testuser')
        expect(resp2.body.createdUsers[0].lastName).eql('testuser')
        expect(resp2.body.createdUsers[0].email).eql(username + '@deloitte.ua')
        expect(resp2.body.createdUsers[0].emailVerified).eql(false)
        expect(resp2.body.createdUsers[0].enabled).eql(true)
        expect(resp2.body.createdUsers[0].credentials[0].temporary).eql(false)
        expect(resp2.body.createdUsers[0].credentials[0].type).eql('password')
        expect(resp2.body.createdUsers[0].credentials[0].value).eql('12345678')
        expect(resp2.body.createdUsers[0].realmRoles[0]).eql('user')
        expect(resp2.body.createdUsers[0].clientRoles['heartbeat-app'][0]).eql('project-viewers')
        expect(resp2.body.createdUsers[0].clientRoles['heartbeat-app'][1]).eql('project-creators')
        expect(resp2.body.createdUsers[0].clientRoles['heartbeat-app'][2]).eql('project-editors')
        expect(resp2.body.createdUsers[0].clientRoles['heartbeat-app'][3]).eql('project-delete')
        expect(resp2.body.createdUsers[0].clientRoles['heartbeat-app'][4]).eql('heartreach-viewers')
        expect(resp2.body.createdUsers[0].attributes.organization_id).a('string')

        cy.request({
          method: 'PATCH',
          url: Cypress.env('baseUrlAPI') + '/auth/password/' + username + '@deloitte.ua',
          headers: {
            Authorization: 'Bearer ' + resp1.body.payload.accessToken,
          },
          body: {
            oldPassword: '12345678',
            newPassword: '12345678910',
          },
        }).then(() => {
          LoginPage.open().setUsername(username).setPassword('12345678910').clickOnLoginButton()
          NavigationBar.clickUserAvatarButton().clickOnChangePassword()
          ChangePassword.setOldPassword('12345678')
            .setNewPassword('12345678910')
            .setRepeatPassword('12345678910')
            .clickOnSubmitBtn()

          cy.request({
            method: 'POST',
            url: Cypress.env('baseUrlAPI') + '/auth/login',
            body: {
              username,
              password: '12345678910',
            },
          }).then((resp1) => {
            expect(resp1.body.status).eql('success')
            expect(resp1.body.code).eql(200)
            expect(resp1.body.message).eql('successfully retrieved token')
            expect(resp1.body.payload.id).a('string')
            expect(resp1.body.payload.accessToken).a('string')
            expect(resp1.body.payload.expiresIn).eql(600)
            expect(resp1.body.payload.refreshToken).a('string')
            expect(resp1.body.payload.refreshExpiresIn).eql(1800)
          })
        })
      })
    })

    it('[CMTS-11014] Login page. Check login with credentials of deleted user (API)', () => {
      let username = 'username_' + Date.now()

      cy.request({
        method: 'POST',
        url: Cypress.env('baseUrlAPI') + '/auth/login',
        body: {
          username: Cypress.env('username'),
          password: Cypress.env('password'),
        },
      }).then((resp1) => {
        expect(resp1.body.status).eql('success')
        expect(resp1.body.code).eql(200)
        expect(resp1.body.message).eql('successfully retrieved token')
        expect(resp1.body.payload.id).a('string')
        expect(resp1.body.payload.userName).eql('dhb-admin')
        expect(resp1.body.payload.email).eql(null)
        expect(resp1.body.payload.isAdmin).eql(true)
        expect(resp1.body.payload.accessToken).a('string')
        expect(resp1.body.payload.expiresIn).eql(600)
        expect(resp1.body.payload.refreshToken).a('string')
        expect(resp1.body.payload.refreshExpiresIn).eql(1800)

        cy.request({
          method: 'POST',
          url: Cypress.env('baseUrlAPI') + '/admin/users',
          headers: {
            Authorization: 'Bearer ' + resp1.body.payload.accessToken,
          },
          body: [
            {
              username,
              firstName: 'testuser',
              lastName: 'testuser',
              email: username + '@deloitte.ua',
              emailVerified: false,
              enabled: true,
              credentials: [
                {
                  temporary: false,
                  type: 'password',
                  value: '12345678',
                },
              ],
              realmRoles: ['user'],
              clientRoles: {
                'heartbeat-app': [
                  'project-viewers',
                  'project-creators',
                  'project-editors',
                  'project-delete',
                  'heartreach-viewers',
                ],
              },
              attributes: {
                organization_id: 'a58dcd34-45fd-4798-b2e7-493955bd08ff',
              },
            },
          ],
        }).then((resp2) => {
          expect(resp2.body.createdUsers[0].username).eql(username)
          expect(resp2.body.createdUsers[0].firstName).eql('testuser')
          expect(resp2.body.createdUsers[0].lastName).eql('testuser')
          expect(resp2.body.createdUsers[0].email).eql(username + '@deloitte.ua')
          expect(resp2.body.createdUsers[0].emailVerified).eql(false)
          expect(resp2.body.createdUsers[0].enabled).eql(true)
          expect(resp2.body.createdUsers[0].credentials[0].temporary).eql(false)
          expect(resp2.body.createdUsers[0].credentials[0].type).eql('password')
          expect(resp2.body.createdUsers[0].credentials[0].value).eql('12345678')
          expect(resp2.body.createdUsers[0].realmRoles[0]).eql('user')
          expect(resp2.body.createdUsers[0].clientRoles['heartbeat-app'][0]).eql('project-viewers')
          expect(resp2.body.createdUsers[0].clientRoles['heartbeat-app'][1]).eql('project-creators')
          expect(resp2.body.createdUsers[0].clientRoles['heartbeat-app'][2]).eql('project-editors')
          expect(resp2.body.createdUsers[0].clientRoles['heartbeat-app'][3]).eql('project-delete')
          expect(resp2.body.createdUsers[0].clientRoles['heartbeat-app'][4]).eql('heartreach-viewers')
          expect(resp2.body.createdUsers[0].attributes.organization_id).a('string')

          cy.request({
            method: 'DELETE',
            url: Cypress.env('baseUrlAPI') + '/admin/users/' + username,
            headers: {
              Authorization: 'Bearer ' + resp1.body.payload.accessToken,
            },
          }).then((resp3) => {
            expect(resp3.body.status).eql('success')
            expect(resp3.body.code).eql(200)
            expect(resp3.body.message).eql('User successfully deleted')

            cy.request({
              method: 'POST',
              url: Cypress.env('baseUrlAPI') + '/auth/login',
              body: {
                username,
                password: '12345678',
              },
              failOnStatusCode: false,
            }).then((resp4) => {
              expect(resp4.body.status).eql('failed')
              expect(resp4.body.message).eql(
                'unable to login user ' + username + ': Error: Request failed with status code 401',
              )
            })
          })
        })
      })
    })
  })

  it('Login page. Check login with credentials of disabled user (API)', () => {
    cy.fixture('users_templates/userDisabled.json').as('user')

    cy.get('@user').then(($json) => {
      cy.createUser($json)
    })
    cy.get('@user').then((user) => {
      const password = user[0].credentials[0].value
      const { username } = user[0]
      cy.getTokenFail(username, password)
    })
  })
})
