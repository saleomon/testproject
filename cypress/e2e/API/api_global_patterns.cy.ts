import { ProjectInfo } from '../../types/response/ProjectInfo'

describe('CMTS-140-test-pack-for-global-interaction-patterns-validations', { tags: ['@api', '@regression'] }, () => {
  beforeEach(() => {
    cy.loginAPI()
  })

  afterEach(() => {
    cy.get<ProjectInfo>('@projects').then((json: ProjectInfo) => {
      cy.removeProject(json.id)
    })
  })
  
  describe('positive scenarios', () => {
    it('check creating global pattern with all outcomes and channels values', () => {
      cy.fixture('metrics-templates/global-patterns.json').as('projects')
      cy.get<ProjectInfo>('@projects').then((json: ProjectInfo) => {
        cy.createNewProject(json).then((resp) => {
          expect(resp.patterns[0].label).to.deep.equal(json.patterns[0].label)
          expect(resp.patterns[0].patternType).to.deep.equal(json.patterns[0].patternType)

          for (let i = 0; i < resp.patterns[0].channels.length - 1; i++) {
            expect(resp.patterns[0].channels[i].label).to.deep.equal(json.patterns[0].channels[i].label)
            expect(resp.patterns[0].channels[i].description).to.deep.equal(json.patterns[0].channels[i].description)
          }

          for (let i = 0; i < resp.patterns[0].outcomes.length - 1; i++) {
            expect(resp.patterns[0].outcomes[i].label).to.deep.equal(json.patterns[0].outcomes[i].label)
          }
        })
      })
    })
    it('CMTS-8440-to-validate-the-name-with-2-characters-is-a-valid-pattern-s-name', () => {
      cy.fixture('metrics-templates/global-patterns.json').as('projects')
      cy.get<ProjectInfo>('@projects').then((json: ProjectInfo) => {
        cy.createNewProject(json).then((resp) => {
          expect(resp.patterns[1].label).to.deep.equal(json.patterns[1].label)
          expect(resp.patterns[1].patternType).to.deep.equal(json.patterns[1].patternType)

          for (let i = 0; i < resp.patterns[1].channels.length - 1; i++) {
            expect(resp.patterns[1].channels[i].label).to.deep.equal(json.patterns[1].channels[i].label)
            expect(resp.patterns[1].channels[i].description).to.deep.equal(json.patterns[1].channels[i].description)
          }
        })
      })
    })

    it('CMTS-8452-to-validate-it-is-possible-to-create-agent-assist-type-pattern', () => {
      cy.fixture('metrics-templates/global-patterns.json').as('projects')
      cy.get<ProjectInfo>('@projects').then((json: ProjectInfo) => {
        cy.createNewProject(json).then((resp) => {
          expect(resp.patterns[2].label).to.deep.equal(json.patterns[2].label)
          expect(resp.patterns[2].patternType).to.deep.equal(json.patterns[2].patternType)

          for (let i = 0; i < resp.patterns[2].channels.length - 1; i++) {
            expect(resp.patterns[2].channels[i].label).to.deep.equal(json.patterns[2].channels[i].label)
            expect(resp.patterns[2].channels[i].description).to.deep.equal(json.patterns[2].channels[i].description)
          }
        })
      })
    })

    it('CMTS-8453-to-validate-it-is-possible-to-create-mobile-app-type-pattern', () => {
      cy.fixture('metrics-templates/global-patterns.json').as('projects')
      cy.get<ProjectInfo>('@projects').then((json: ProjectInfo) => {
        cy.createNewProject(json).then((resp) => {
          expect(resp.patterns[3].label).to.deep.equal(json.patterns[3].label)
          expect(resp.patterns[3].patternType).to.deep.equal(json.patterns[3].patternType)

          for (let i = 0; i < resp.patterns[3].channels.length - 1; i++) {
            expect(resp.patterns[3].channels[i].label).to.deep.equal(json.patterns[3].channels[i].label)
            expect(resp.patterns[3].channels[i].description).to.deep.equal(json.patterns[3].channels[i].description)
          }
        })
      })
    })

    it('CMTS-8454-to-validate-it-is-possible-to-create-skill-pattern-type-pattern', () => {
      cy.fixture('metrics-templates/global-patterns.json').as('projects')
      cy.get<ProjectInfo>('@projects').then((json: ProjectInfo) => {
        cy.createNewProject(json).then((resp) => {
          expect(resp.patterns[4].label).to.deep.equal(json.patterns[4].label)
          expect(resp.patterns[4].patternType).to.deep.equal(json.patterns[4].patternType)

          for (let i = 0; i < resp.patterns[4].channels.length - 1; i++) {
            expect(resp.patterns[4].channels[i].label).to.deep.equal(json.patterns[4].channels[i].label)
            expect(resp.patterns[4].channels[i].description).to.deep.equal(json.patterns[4].channels[i].description)
          }
        })
      })
    })

    it('CMTS-8442-to-validate-the-name-with-32-uppercase-and-lowercase-characters-is-a-valid-pattern-s-name', () => {
      cy.fixture('metrics-templates/global-patterns.json').as('projects')
      cy.get<ProjectInfo>('@projects').then((json: ProjectInfo) => {
        cy.createNewProject(json).then((resp) => {
          expect(resp.patterns[5].label).to.deep.equal(json.patterns[5].label)
          expect(resp.patterns[5].patternType).to.deep.equal(json.patterns[5].patternType)

          for (let i = 0; i < resp.patterns[5].channels.length - 1; i++) {
            expect(resp.patterns[5].channels[i].label).to.deep.equal(json.patterns[5].channels[i].label)
            expect(resp.patterns[5].channels[i].description).to.deep.equal(json.patterns[5].channels[i].description)
          }
        })
      })
    })

    it('CMTS-8444-to-validate-the-name-with-32-special-characters-is-a-valid-pattern-s-name', () => {
      cy.fixture('metrics-templates/global-patterns.json').as('projects')
      cy.get<ProjectInfo>('@projects').then((json: ProjectInfo) => {
        cy.createNewProject(json).then((resp) => {
          expect(resp.patterns[6].label).to.deep.equal(json.patterns[6].label)
          expect(resp.patterns[6].patternType).to.deep.equal(json.patterns[6].patternType)

          for (let i = 0; i < resp.patterns[6].channels.length - 1; i++) {
            expect(resp.patterns[6].channels[i].label).to.deep.equal(json.patterns[6].channels[i].label)
            expect(resp.patterns[6].channels[i].description).to.deep.equal(json.patterns[6].channels[i].description)
          }
        })
      })
    })
  })
})
