import { ProjectInfo } from '../../types/response/ProjectInfo'

describe('CMTS-8414-test-pack-for-projected-outcomes-validations', { tags: ['@api', '@regression'] }, () => {
  beforeEach(() => {
    cy.loginAPI()
  })
  afterEach(() => {
    cy.removeProject(Cypress.env('projectId'))
  })

  describe('positive tests', () => {
    it('CMTS-8819-to-validate-it-is-possible-to-create-the-outcome-with-empty-description', () => {
      cy.fixture('metrics-templates/projected-outcomes.json').as('projects')
      cy.get<ProjectInfo>('@projects').then((json: ProjectInfo) => {
        cy.createNewProject(json).then((resp) => {
          expect(resp.outcomes[12].label).to.deep.equal(json.outcomes[12].label)
          expect(resp.outcomes[12].type).to.deep.equal(json.outcomes[12].type)
          expect(resp.outcomes[12].color).to.deep.equal(json.outcomes[12].color)
          expect(resp.outcomes[12].percentage).to.deep.equal(json.outcomes[12].percentage)
          expect(resp.outcomes[12].outcomeType).to.deep.equal(json.outcomes[12].outcomeType)
          expect(resp.outcomes[12].outcomeValue).to.deep.equal(json.outcomes[12].outcomeValue)
        })
      })
    })

    it('CMTS-8421-to-validate-the-name-with-32-characters-including-special-characters-inside-is-a-valid', () => {
      cy.fixture('metrics-templates/projected-outcomes.json').as('projects')
      cy.get<ProjectInfo>('@projects').then((json: ProjectInfo) => {
        cy.createNewProject(json).then((resp) => {
          expect(resp.outcomes[3].label).to.deep.equal(json.outcomes[3].label)
          expect(resp.outcomes[3].type).to.deep.equal(json.outcomes[3].type)
          expect(resp.outcomes[3].color).to.deep.equal(json.outcomes[3].color)
          expect(resp.outcomes[3].percentage).to.deep.equal(json.outcomes[3].percentage)
          expect(resp.outcomes[3].outcomeType).to.deep.equal(json.outcomes[3].outcomeType)
          expect(resp.outcomes[3].outcomeValue).to.deep.equal(json.outcomes[3].outcomeValue)
        })
      })
    })

    it('CMTS-8415-to-validate-the-name-with-2-characters-is-a-valid-outcome-s-name', () => {
      cy.fixture('metrics-templates/projected-outcomes.json').as('projects')
      cy.get<ProjectInfo>('@projects').then((json: ProjectInfo) => {
        cy.createNewProject(json).then((resp) => {
          expect(resp.outcomes[4].label).to.deep.equal(json.outcomes[4].label)
          expect(resp.outcomes[4].type).to.deep.equal(json.outcomes[4].type)
          expect(resp.outcomes[4].color).to.deep.equal(json.outcomes[4].color)
          expect(resp.outcomes[4].percentage).to.deep.equal(json.outcomes[4].percentage)
          expect(resp.outcomes[4].outcomeType).to.deep.equal(json.outcomes[4].outcomeType)
          expect(resp.outcomes[4].outcomeValue).to.deep.equal(json.outcomes[4].outcomeValue)
        })
      })
    })

    it('CMTS-8417-to-validate-the-name-with-32-uppercase-and-lowercase-is-a-valid-outcome-s-name', () => {
      cy.fixture('metrics-templates/projected-outcomes.json').as('projects')
      cy.get<ProjectInfo>('@projects').then((json: ProjectInfo) => {
        cy.createNewProject(json).then((resp) => {
          expect(resp.outcomes[5].label).to.deep.equal(json.outcomes[5].label)
          expect(resp.outcomes[5].type).to.deep.equal(json.outcomes[5].type)
          expect(resp.outcomes[5].color).to.deep.equal(json.outcomes[5].color)
          expect(resp.outcomes[5].percentage).to.deep.equal(json.outcomes[5].percentage)
          expect(resp.outcomes[5].outcomeType).to.deep.equal(json.outcomes[5].outcomeType)
          expect(resp.outcomes[5].outcomeValue).to.deep.equal(json.outcomes[5].outcomeValue)
        })
      })
    })

    it('CMTS-8418-to-validate-the-name-with-32-characters-including-the-space-inside-is-a-valid-outcome-s', () => {
      cy.fixture('metrics-templates/projected-outcomes.json').as('projects')
      cy.get<ProjectInfo>('@projects').then((json: ProjectInfo) => {
        cy.createNewProject(json).then((resp) => {
          expect(resp.outcomes[6].label).to.deep.equal(json.outcomes[6].label)
          expect(resp.outcomes[6].type).to.deep.equal(json.outcomes[6].type)
          expect(resp.outcomes[6].color).to.deep.equal(json.outcomes[6].color)
          expect(resp.outcomes[6].percentage).to.deep.equal(json.outcomes[6].percentage)
          expect(resp.outcomes[6].outcomeType).to.deep.equal(json.outcomes[6].outcomeType)
          expect(resp.outcomes[6].outcomeValue).to.deep.equal(json.outcomes[6].outcomeValue)
        })
      })
    })

    it('CMTS-8431-to-validate-it-is-possible-to-create-the-outcome-with-positive-value', () => {
      cy.fixture('metrics-templates/projected-outcomes.json').as('projects')
      cy.get<ProjectInfo>('@projects').then((json: ProjectInfo) => {
        cy.createNewProject(json).then((resp) => {
          expect(resp.outcomes[8].label).to.deep.equal(json.outcomes[8].label)
          expect(resp.outcomes[8].type).to.deep.equal(json.outcomes[8].type)
          expect(resp.outcomes[8].color).to.deep.equal(json.outcomes[8].color)
          expect(resp.outcomes[8].percentage).to.deep.equal(json.outcomes[8].percentage)
          expect(resp.outcomes[8].outcomeType).to.deep.equal(json.outcomes[8].outcomeType)
          expect(resp.outcomes[8].outcomeValue).to.deep.equal(json.outcomes[8].outcomeValue)
        })
      })
    })

    it('CMTS-8430-to-validate-it-is-possible-to-create-the-outcome-with-neutral-value', () => {
      cy.fixture('metrics-templates/projected-outcomes.json').as('projects')
      cy.get<ProjectInfo>('@projects').then((json: ProjectInfo) => {
        cy.createNewProject(json).then((resp) => {
          expect(resp.outcomes[9].label).to.deep.equal(json.outcomes[9].label)
          expect(resp.outcomes[9].type).to.deep.equal(json.outcomes[9].type)
          expect(resp.outcomes[9].color).to.deep.equal(json.outcomes[9].color)
          expect(resp.outcomes[9].percentage).to.deep.equal(json.outcomes[9].percentage)
          expect(resp.outcomes[9].outcomeType).to.deep.equal(json.outcomes[9].outcomeType)
          expect(resp.outcomes[9].outcomeValue).to.deep.equal(json.outcomes[9].outcomeValue)
        })
      })
    })

    it('CMTS-8432-to-validate-it-is-possible-to-create-the-outcome-with-negative-value', () => {
      cy.fixture('metrics-templates/projected-outcomes.json').as('projects')
      cy.get<ProjectInfo>('@projects').then((json: ProjectInfo) => {
        cy.createNewProject(json).then((resp) => {
          expect(resp.outcomes[12].label).to.deep.equal(json.outcomes[12].label)
          expect(resp.outcomes[12].type).to.deep.equal(json.outcomes[12].type)
          expect(resp.outcomes[12].color).to.deep.equal(json.outcomes[12].color)
          expect(resp.outcomes[12].percentage).to.deep.equal(json.outcomes[12].percentage)
          expect(resp.outcomes[12].outcomeType).to.deep.equal(json.outcomes[12].outcomeType)
          expect(resp.outcomes[12].outcomeValue).to.deep.equal(json.outcomes[12].outcomeValue)
        })
      })
    })
  })

  describe('negative tests', () => {
    it('CMTS-8428-to-validate-it-is-not-possible-for-user-to-create-the-fallback-outcome-cause-it-is-default', () => {
      cy.fixture('metrics-templates/projected_outcomes_negative_validation/projected_outcomes_fallback.json').as('projects')
      cy.get<ProjectInfo>('@projects').then((json: ProjectInfo) => {
        cy.createProjectFail(json).then((resp) => {
          Cypress.env('projectId', resp.body.projectObject.id)
          expect(resp.status).equal(400)
        })
      })
    })
    it('CMTS-8423-to-validate-the-name-with-1-character-is-not-valid-outcome-s-name', () => {
      cy.fixture('metrics-templates/projected_outcomes_negative_validation/projected_outcomes_one_character.json').as('projects')
      cy.get<ProjectInfo>('@projects').then((json: ProjectInfo) => {
        cy.createProjectFail(json).then((resp) => {
          Cypress.env('projectId', resp.body.projectObject.id)
          expect(resp.status).equal(400)
        })
      })
    })

    it('CMTS-8427-to-validate-it-is-not-possible-to-create-the-outcome-with-the-default-outcomes-name-which', () => {
      cy.fixture('metrics-templates/projected_outcomes_negative_validation/projected_outcomes_negative_hitl.json').as('projects')
      cy.get<ProjectInfo>('@projects').then((json: ProjectInfo) => {
        cy.createProjectFail(json).then((resp) => {
          Cypress.env('projectId', resp.body.projectObject.id)
          expect(resp.status).equal(400)
        })
      })
    })

    it('to-validate-it-is-not-possible-to-create-the-outcome-with-33-symbols', () => {
      cy.fixture('metrics-templates/projected_outcomes_negative_validation/projected_outcomes_33_symbols.json').as('projects')
      cy.get<ProjectInfo>('@projects').then((json: ProjectInfo) => {
        cy.createProjectFail(json).then((resp) => {
          Cypress.env('projectId', resp.body.projectObject.id)
          expect(resp.status).equal(400)
        })
      })
    })

    it('to-validate-it-is-not-possible-to-create-the-outcome-with-33-symbols', () => {
      cy.fixture('metrics-templates/projected_outcomes_negative_validation/projected_outcomes_empty.json').as('projects')
      cy.get<ProjectInfo>('@projects').then((json: ProjectInfo) => {
        cy.createProjectFail(json).then((resp) => {
          expect(resp.status).equal(400)
          expect(resp.body.name).equal('ValidationError')
        })
      })
    })
  })
})