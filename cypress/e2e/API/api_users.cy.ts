describe('CMTS-9586-automate-user-access-and-user-management-api', { tags: ['@api', '@regression'] }, () => {
  beforeEach(() => {
    cy.loginAPI()
  })
  afterEach(() => {
    // cy.task('cleanTableProjects') // skip to updating test2 - this task can deleted required projects
    cy.task('deleteUser', Cypress.env('testUser'))
  })

  it('[CMTS-11494] Add user to project', { tags: '@api' }, () => {
    cy.loginAPI()
    cy.fixture('projects-templates/project_one_pattern.json').then(($json) => {
      cy.createNewProject($json)
    })
    cy.fixture('users_templates/userAllRights.json').then(($json) => {
      cy.createUser($json)
    })
    cy.fixture('users_templates/projectsAccess.json').then(($json) => {
      $json.projects[0].id = Cypress.env('projectId')
      cy.request({
        method: 'POST',
        url: Cypress.env('baseUrlAPI') + '/admin/users/' + Cypress.env('testUser') + '/projects',
        headers: {
          Authorization: 'Bearer ' + Cypress.env('token'),
        },
        body: $json,
      }).then((resp) => {
        expect(resp.status).equals(200)
      })
    })
  })

  it('check if viewer can not create project - api', { tags: '@api' }, () => {
    cy.fixture('users_templates/userViewRole.json').as('user')

    cy.get('@user').then(($json) => {
      cy.createUser($json)
    })
    cy.get('@user').then((user) => {
      const password = user[0].credentials[0].value
      const { username } = user[0]
      cy.getToken(username, password)
      cy.fixture('projects-templates/project_one_pattern.json').then(($json) => {
        cy.createNewProject($json, 403)
      })
    })
  })

  it('check if viewer dont see the button Create New Project', { tags: '@api' }, () => {
    cy.fixture('users_templates/userViewRole.json').as('user')

    cy.get('@user').then(($json) => {
      cy.createUser($json)
    })
    cy.get('@user').then((user) => {
      const password = user[0].credentials[0].value
      const { username } = user[0]
      cy.loginUI(username, password)
      cy.get('#createNewProjectButton').should('not.be.visible')
    })
  })
})
