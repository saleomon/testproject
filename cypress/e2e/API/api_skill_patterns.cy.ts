import { ProjectInfo } from '../../types/response/ProjectInfo'

describe('CMTS-141-test-pack-for-skill-library-validation', { tags: ['@api', '@regression'] }, () => {
  before(() => {
    cy.loginAPI()
    cy.fixture('metrics-templates/skill_patterns_api_negative_validation/skill_patterns_unique.json').as('projects')
    cy.get<ProjectInfo>('@projects').then((json: ProjectInfo) => {
      cy.createNewProject(json)
    })
  })

  beforeEach(() => {
    cy.loginAPI()
  })

  afterEach(() => {
    cy.get<ProjectInfo>('@projects').then((json: ProjectInfo) => {
      cy.removeProject(json.id)
    })
  })

  describe('positive tests', () => {
    it('CMTS-8421-to-validate-the-name-with-32-characters-including-special-characters-inside-is-a-valid', () => {
      cy.fixture('metrics-templates/skill_patterns_API.json').as('projects')
      cy.get<ProjectInfo>('@projects').then((json: ProjectInfo) => {
        cy.createNewProject(json).then((resp) => {
          expect(resp.patterns[0].label).to.deep.equal(json.patterns[0].label)
          expect(resp.patterns[0].patternType).to.deep.equal(json.patterns[0].patternType)
          expect(resp.patterns[0].description).to.deep.equal(json.patterns[0].description)
          for (let i = 0; i < resp.patterns[0].channels.length; i++) {
            expect(resp.patterns[0].channels[i].label).to.deep.equal(json.patterns[0].channels[i].label)
            expect(resp.patterns[0].channels[i].description).to.deep.equal(json.patterns[0].channels[i].description)
          }
        })
      })
    })

    it('CMTS-9682-to-validate-the-name-with-2-characters-is-a-valid-skill-pattern-name', () => {
      cy.fixture('metrics-templates/skill_patterns_API.json').as('projects')
      cy.get<ProjectInfo>('@projects').then((json: ProjectInfo) => {
        cy.createNewProject(json).then((resp) => {
          expect(resp.patterns[7].label).to.deep.equal(json.patterns[7].label)
          expect(resp.patterns[7].patternType).to.deep.equal(json.patterns[7].patternType)
          expect(resp.patterns[7].description).to.deep.equal(json.patterns[7].description)
          for (let i = 0; i < resp.patterns[7].channels.length; i++) {
            expect(resp.patterns[7].channels[i].label).to.deep.equal(json.patterns[7].channels[i].label)
            expect(resp.patterns[7].channels[i].description).to.deep.equal(json.patterns[7].channels[i].description)
          }
        })
      })
    })

    it('CMTS-8438-to-validate-it-is-possible-to-create-the-skill-pattern-without-description', () => {
      cy.fixture('metrics-templates/skill_patterns_API.json').as('projects')
      cy.get<ProjectInfo>('@projects').then((json: ProjectInfo) => {
        cy.createNewProject(json).then((resp) => {
          expect(resp.patterns[4].label).to.deep.equal(json.patterns[4].label)
          expect(resp.patterns[4].patternType).to.deep.equal(json.patterns[4].patternType)
          expect(resp.patterns[4].description).to.deep.equal(json.patterns[4].description)
          for (let i = 0; i < resp.patterns[4].channels.length; i++) {
            expect(resp.patterns[4].channels[i].label).to.deep.equal(json.patterns[4].channels[i].label)
            expect(resp.patterns[4].channels[i].description).to.deep.equal(json.patterns[4].channels[i].description)
          }
        })
      })
    })
  })

  describe('negative tests', () => {
    it('CMTS-8435-to-validate-the-name-with-33-characters-including-the-space-in-the-end-is-not-valid-skill', () => {
      cy.fixture(
        'metrics-templates/skill_patterns_api_negative_validation/skill_patterns_33_symbols_space_at_end.json',
      ).as('projects')
      cy.get<ProjectInfo>('@projects').then((json: ProjectInfo) => {
        cy.createProjectFail(json).then((resp) => {
          Cypress.env('projectId', resp.body.projectObject.id)
          expect(resp.status).equal(400)
        })
      })
    })

    it('CMTS-8436-to-validate-the-name-with-33-characters-including-the-space-in-the-beginning-is-not-valid', () => {
      cy.fixture(
        'metrics-templates/skill_patterns_api_negative_validation/skill_patterns_33_symbols_space_at_start.json',
      ).as('projects')
      cy.get<ProjectInfo>('@projects').then((json: ProjectInfo) => {
        cy.createProjectFail(json).then((resp) => {
          Cypress.env('projectId', resp.body.projectObject.id)
          expect(resp.status).equal(400)
        })
      })
    })

    it('CMTS-8777-to-validate-the-name-with-32-special-characters-including-slashes-is-not-valid-skill', () => {
      cy.fixture('metrics-templates/skill_patterns_api_negative_validation/skill_patterns_slash.json').as('projects')
      cy.get<ProjectInfo>('@projects').then((json: ProjectInfo) => {
        cy.createProjectFail(json).then((resp) => {
          Cypress.env('projectId', resp.body.projectObject.id)
          expect(resp.status).equal(400)
        })
      })
    })

    it('CMTS-8437-to-validate-it-is-not-possible-to-create-the-skill-pattern-without-channel', () => {
      cy.fixture('metrics-templates/skill_patterns_api_negative_validation/skill_patterns_no_channels.json').as(
        'projects',
      )
      cy.get<ProjectInfo>('@projects').then((json: ProjectInfo) => {
        cy.createProjectFail(json).then((resp) => {
          Cypress.env('projectId', resp.body.projectObject.id)
          expect(resp.status).equal(400)
        })
      })
    })

    it('CMTS-9502-to-validate-it-is-not-possible-to-create-the-skill-pattern-with-empty-skill-name', () => {
      cy.fixture('metrics-templates/skill_patterns_api_negative_validation/skill_patterns_empty.json').as('projects')
      cy.get<ProjectInfo>('@projects').then((json: ProjectInfo) => {
        cy.createProjectFail(json).then((resp) => {
          expect(resp.status).equal(400)
          expect(resp.body.name).equal('ValidationError')
        })
      })
    })

    it('CMTS-9683-to-validate-it-is-not-possible-to-create-the-skill-pattern-with-name-already-exist', () => {
      cy.fixture('metrics-templates/skill_patterns_api_negative_validation/skill_patterns_add_unique_pattern.json').as(
        'projects',
      )
      cy.get<ProjectInfo>('@projects').then((json: ProjectInfo) => {
        cy.updateProjectFail(json).then((resp) => {
          expect(resp.status).equal(400)
          expect(resp.body.name).equal('ValidationError')
        })
      })
    })
  })
})
