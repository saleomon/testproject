import { entityEditorObj } from '../../classes/entityEditorObject'
import DiagramUtils from '../../support/DiagramUtils'
import NavigationBar from '../../classes/navigation/NavigationBar'
import FilterBar from '../../classes/bars/FilterBar'

describe('integration scenarios', () => {
  before(() => {
    cy.getToken()
    cy.fixture('entity-templates/entity.json').then(($json) => {
      cy.createNewProject($json).then(($projectInfo) => {
        cy.loginUI()
        NavigationBar.selectProject($projectInfo.label)
        Cypress.env('projectId', $projectInfo.id)
      })
    })

    NavigationBar.openPatternsPage()
  })

  afterEach(() => {
    cy.removeProject(Cypress.env('projectId'))
  })

  context('CMTS-9520-check-appearance-of-label-pii-on-content-panel', { tags: ['@smoke', '@integration'] }, () => {
    before(() => {
      FilterBar.selectPattern('Pattern for PII')
      NavigationBar.openFlowsPage()
      DiagramUtils.findDiagram()
      DiagramUtils.click('ClosedQuestion')
      // open tab Output
      cy.get('[aria-controls="OutputTab"]').should('be.visible').click() // version 1.1.36
   })

    it('add PII', () => {
      // add PII
      entityEditorObj.fillPII('{{Username}}')
      // check if PII label exists
      cy.get('.PiiLabel').should('exist').and('have.text', ' PII ')
    })

    it('delete PII', () => {
      // delete PII
      entityEditorObj.fillPII(' ')
      // check if PII label does not exist
      cy.get('.PiiLabel').should('not.exist')
    })

    it('check if PII label exists if toogle PII for entity', () => {
      // add PII
      entityEditorObj.fillPII('{{Birthdate}}')
      // check if PII label does not exist
      cy.get('.PiiLabel').should('not.exist')
      // open entity editor
      cy.get(entityEditorObj.openEntitiesViewer).click()
      cy.get('.selectTokens').contains('span', 'Entity').click()
      // toogle PII for entity
      cy.getEditButtonOnTable('.dataTable', 'Birthdate').click()
      entityEditorObj.toogle(entityEditorObj.PIISwitch).click().should('have.class', 'ant-switch-checked')
      cy.get('.ant-popover-title').last().parent().contains('Save').should('be.visible').click()
      cy.get(entityEditorObj.closeIcon).click()
      // check if PII label exists
      cy.get('.PiiLabel').should('exist').and('have.text', ' PII ')
    })

    it('check if PII label does not exist if untoogle PII for entity', () => {
      // add PII
      entityEditorObj.fillPII('{{Gender}}')
      // check if PII label exists
      cy.get('.PiiLabel').should('exist')
      // open entity editor
      cy.get('.toolbarWrapper .openButton')
        .attribute('style')
        .then(($attr) => {
          const style = $attr
          if (style !== 'display: none;') {
            cy.get('[aria-label="Play icon"]').filter(':visible').click()
          }
        })

      // cy.get('[aria-label="Play icon"]').filter(':visible').click()
      cy.get(entityEditorObj.openEntitiesViewer).click()
      cy.get('.selectTokens').contains('span', 'Entity').click()
      // toogle PII for entity
      cy.getEditButtonOnTable('.dataTable', 'Gender').click()
      entityEditorObj.toogle(entityEditorObj.PIISwitch).click().should('not.have.class', 'ant-switch-checked')
      cy.get('.ant-popover-title').last().parent().contains('Save').should('be.visible').click()
      cy.get(entityEditorObj.closeIcon).click()

      // check if PII label exists
      cy.get('.PiiLabel').should('not.exist')
    })

    it('check appearance of  PII tooltip with PII value', () => {
      entityEditorObj.fillPII('{{', 'Username')
    })

    it('check appearance of Match not Found if no entites with PII', () => {
      /* cy.get('.saveItEditButton').filter(':visible').then(($save) => {
        if ($save)
        {
          cy.wrap($save).click()
        }
      }) */
      // check appearance of  PII tooltip No Match Found
      entityEditorObj.fillPII('{{Age}}', 'notFound')
    })
  })
})
