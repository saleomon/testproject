import { dataObject } from '../../../fixtures/data/data-spec-node-details'
import FilterBar from '../../../classes/bars/FilterBar.ts'
import NodeDetailsPanel from '../../../classes/NodeDetailsPanel'
import { DesignerTopBarHrefs } from '../../../fixtures/navigation-data/designer-top-bar-hrefs'
import NavigationBar from '../../../classes/navigation/NavigationBar.ts'

describe('Node Details test', () => {
  let createdProjectInfo = {}

  before(function () {
    cy.fixture('projects-templates/node-details-spec-project.json').then(($json) => {
      cy.createProject(Cypress.env('baseUrlAPI'), Cypress.env('username'), Cypress.env('password'), $json).then(
        ($projectInfo) => {
          createdProjectInfo = $projectInfo
        },
      )
    })

    cy.UILogin(Cypress.env('username'), Cypress.env('password'))
    Cypress.LocalStorage.clear = function (keys, ls, rs) {
      if (keys) {
      }
    }
  })

  beforeEach(function () {
     // restore localstorage
    cy.restoreLocalStorage()
  })


  it('Select project', () => {
    cy.selectProject(createdProjectInfo.label)
    cy.get('div').contains(createdProjectInfo.label).should('be.visible')
  })

  it('Select a pattern', () => {
    cy.get('.topLink').contains('Patterns').should('be.visible').click()
    cy.url().should('contain', '/patterns')
    FilterBar.selectPattern('TestPatternName')
  })

  it('Verify Nodes Details on Patterns Page', () => {
    cy.zoomOut(10)

    let nodeDetails = new NodeDetailsPanel()
    let { subflow } = dataObject.pattern_diagram
    let skillrouter = dataObject.pattern_diagram.skillsrouter
    let { tag } = dataObject.pattern_diagram

    // assertions on Start node
    cy.clickNode('Start').then(() => {
      nodeDetails.headerText().then(($text) => {
        expect($text).to.contain.text('Start')
      })
      nodeDetails.nodeLabel().then(($nodeLabel) => {
        expect($nodeLabel).to.have.value('Start')
      })
      nodeDetails.nodeType().then(($nodeType) => {
        expect($nodeType).to.have.value('startPoint')
      })
      nodeDetails.nodeDescription().then(($nodeDescription) => {
        expect($nodeDescription).to.have.value('Pattern Flow Start')
      })
    })

    // First node assertions
    cy.clickNode(subflow.nodeLabel).then(() => {
      nodeDetails.element().should('be.visible')
      nodeDetails.selectTab('Flow')
      nodeDetails.selectTab('Properties')
      nodeDetails.collapse('Node Properties')
      nodeDetails.expand('Node Properties')
      nodeDetails.expand('Datamodel')

      nodeDetails.headerText().then(($text) => {
        expect($text).to.contain.text(subflow.nodeLabel)
      })
      nodeDetails.nodeLabel().then(($nodeLabel) => {
        expect($nodeLabel).to.have.value(subflow.nodeLabel)
      })
      nodeDetails.nodeType().then(($nodeType) => {
        expect($nodeType).to.have.value(subflow.nodeType)
      })
      nodeDetails.nodeDescription().then(($nodeDescription) => {
        expect($nodeDescription).to.have.value(subflow.nodeDescription)
      })
    })

    // Skillrouter node assertions
    cy.clickNode(skillrouter.nodeLabel).then(() => {
      nodeDetails.headerText().then(($text) => {
        expect($text).to.contain.text(skillrouter.nodeLabel)
      })
      nodeDetails.nodeLabel().then(($nodeLabel) => {
        expect($nodeLabel).to.have.value(skillrouter.nodeLabel)
      })
      nodeDetails.nodeType().then(($nodeType) => {
        expect($nodeType).to.have.value(skillrouter.nodeType)
      })
      nodeDetails.nodeDescription().then(($nodeDescription) => {
        expect($nodeDescription).to.have.value(skillrouter.nodeDescription)
      })
    })

    // Tag node assertions
    cy.clickNode(tag.nodeLabel).then(() => {
      nodeDetails.headerText().then(($text) => {
        expect($text).to.contain.text(tag.nodeLabel)
      })
      nodeDetails.nodeLabel().then(($nodeLabel) => {
        expect($nodeLabel).to.have.value(tag.nodeLabel)
      })
      nodeDetails.nodeType().then(($nodeType) => {
        expect($nodeType).to.have.value(tag.nodeType)
      })
      nodeDetails.nodeDescription().then(($nodeDescription) => {
        expect($nodeDescription).to.have.value(tag.nodeDescription)
      })
    })

    // Tag node assertions
    cy.clickNode('gotoNode1').then(() => {
      nodeDetails.headerText().then(($text) => {
        expect($text).to.contain.text(subflow.nodeLabel)
      })
      nodeDetails.nodeLabel().then(($nodeLabel) => {
        expect($nodeLabel).to.have.value(subflow.nodeLabel)
      })
      nodeDetails.nodeType().then(($nodeType) => {
        expect($nodeType).to.have.value(subflow.nodeType)
      })
      nodeDetails.nodeDescription().then(($nodeDescription) => {
        expect($nodeDescription).to.have.value(subflow.nodeDescription)
      })
    })
  })

  it('Select Flow', () => {
    // Build diagram on Flow page
    NavigationBar.clickTopBarButton(DesignerTopBarHrefs.flows)
    NavigationBar.topBarButton(DesignerTopBarHrefs.flows).parent().should('be.visible')
    cy.url().should('include', DesignerTopBarHrefs.flows)
  })

  it('Verify Nodes Details on Flows Page', () => {
    let nodeDetails = new NodeDetailsPanel()
    let { subflow } = dataObject.flow_diagram
    let { tag } = dataObject.flow_diagram
    let { prompt } = dataObject.flow_diagram
    let { openq } = dataObject.flow_diagram
    let { closedq } = dataObject.flow_diagram

    cy.zoomOut(10)

    // assertions on Start node
    cy.clickNode('Start').then(() => {
      nodeDetails.headerText().then(($text) => {
        expect($text).to.contain.text('Start')
      })
      nodeDetails.nodeLabel().then(($nodeLabel) => {
        expect($nodeLabel).to.have.value('Start')
      })
      nodeDetails.nodeType().then(($nodeType) => {
        expect($nodeType).to.have.value('startPoint')
      })
      nodeDetails.nodeDescription().then(($nodeDescription) => {
        expect($nodeDescription).to.have.value('Pattern Flow Start')
      })
    })

    // First node assertions
    cy.clickNode(subflow.nodeLabel).then(() => {
      nodeDetails.element().should('be.visible')
      nodeDetails.selectTab('Properties')

      nodeDetails.headerText().then(($text) => {
        expect($text).to.contain.text(subflow.nodeLabel)
      })
      nodeDetails.nodeLabel().then(($nodeLabel) => {
        expect($nodeLabel).to.have.value(subflow.nodeLabel)
      })
      nodeDetails.nodeType().then(($nodeType) => {
        expect($nodeType).to.have.value(subflow.nodeType)
      })
      nodeDetails.nodeDescription().then(($nodeDescription) => {
        expect($nodeDescription).to.have.value(subflow.nodeDescription)
      })
    })

    // Tag node assertions
    cy.clickNode(tag.nodeLabel).then(() => {
      nodeDetails.headerText().then(($text) => {
        expect($text).to.contain.text(tag.nodeLabel)
      })
      nodeDetails.nodeLabel().then(($nodeLabel) => {
        expect($nodeLabel).to.have.value(tag.nodeLabel)
      })
      nodeDetails.nodeType().then(($nodeType) => {
        expect($nodeType).to.have.value(tag.nodeType)
      })
      nodeDetails.nodeDescription().then(($nodeDescription) => {
        expect($nodeDescription).to.have.value(tag.nodeDescription)
      })
    })

    // Prompt node assertions
    cy.clickNode(prompt.nodeLabel).then(() => {
      nodeDetails.headerText().then(($text) => {
        expect($text).to.contain.text(prompt.nodeLabel)
      })
      nodeDetails.nodeLabel().then(($nodeLabel) => {
        expect($nodeLabel).to.have.value(prompt.nodeLabel)
      })
      nodeDetails.nodeType().then(($nodeType) => {
        expect($nodeType).to.have.value(prompt.nodeType)
      })
      nodeDetails.nodeDescription().then(($nodeDescription) => {
        expect($nodeDescription).to.have.value(prompt.nodeDescription)
      })
    })

    // Openq node assertions
    cy.clickNode(openq.nodeLabel).then(() => {
      nodeDetails.headerText().then(($text) => {
        expect($text).to.contain.text(openq.nodeLabel)
      })
      nodeDetails.nodeLabel().then(($nodeLabel) => {
        expect($nodeLabel).to.have.value(openq.nodeLabel)
      })
      nodeDetails.nodeType().then(($nodeType) => {
        expect($nodeType).to.have.value(openq.nodeType)
      })
      nodeDetails.nodeDescription().then(($nodeDescription) => {
        expect($nodeDescription).to.have.value(openq.nodeDescription)
      })
    })

    // Closedq node assertions
    cy.clickNode(closedq.nodeLabel).then(() => {
      nodeDetails.headerText().then(($text) => {
        expect($text).to.contain.text(closedq.nodeLabel)
      })
      nodeDetails.nodeLabel().then(($nodeLabel) => {
        expect($nodeLabel).to.have.value(closedq.nodeLabel)
      })
      nodeDetails.nodeType().then(($nodeType) => {
        expect($nodeType).to.have.value(closedq.nodeType)
      })
      nodeDetails.nodeDescription().then(($nodeDescription) => {
        expect($nodeDescription).to.have.value(closedq.nodeDescription)
      })
    })

    nodeDetails.close()
  })

  afterEach(function () {
    // preserve local storage after each hook
    cy.saveLocalStorage()
  })

  after(function () {
    cy.deleteProject(Cypress.env('baseUrlAPI'), Cypress.env('username'), Cypress.env('password'), createdProjectInfo.id)
  })
})
