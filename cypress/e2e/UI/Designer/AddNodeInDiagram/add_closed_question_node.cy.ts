import NavigationBar from '../../../../classes/navigation/NavigationBar'
import PalleteBox from '../../../../classes/PaletteBox'
import DiagramUtils from '../../../../support/DiagramUtils'
import { ProjectInfo } from '../../../../types/response/ProjectInfo'
import AddNewClosedQuestionNodePopUp from '../../../../classes/popups/AddNewClosedQuestionNodePopUp'
import TabPropertiesSideBar from '../../../../classes/bars/TabPropertiesSideBar'
import AddNewNodePopUp from '../../../../classes/popups/AddNewNodePopUp'

describe('Add New Node - Closed Question', { tags: '@validation' }, () => {
  beforeEach(() => {
    cy.precondition('add-node-project', 'general')
    DiagramUtils.findDiagram()
    DiagramUtils.doubleClick('nodeLabel')
    NavigationBar.openFlowsPage()
    cy.wait(1000)
    DiagramUtils.findDiagram()
    DiagramUtils.addNodeInDiagram(PalleteBox.subflowNodeIdSelector, 'Start', true)
    AddNewNodePopUp.setLabel('Testing').clickOnAddBtn({ force: true })
  })

  after(() => {
    cy.get<ProjectInfo>('@current-project').then((json: ProjectInfo) => {
      cy.removeProject(json.id)
    })
  })

  it('[ CMTS-15850 ] Verify if the user is able to create a closed question node without a Default closed question', () => {
    DiagramUtils.findDiagram()
    DiagramUtils.addNodeInDiagram(PalleteBox.closedQNodeIdSelector, 'Testing')
    AddNewClosedQuestionNodePopUp.setLink('default').setLabel('TestingCloseQuestion').clickOnAddBtn({ force: true })
    DiagramUtils.findChildNodeByLinkText('Testing', 'default').then(($el) => {
      expect($el.data.key).equals('TestingCloseQuestion')
    })
  })

  it('[ CMTS-16358 ] Verify if the user is able to create a closed question node with a Default closed question', () => {
    DiagramUtils.findDiagram()
    DiagramUtils.addNodeInDiagram(PalleteBox.closedQNodeIdSelector, 'Testing')
    AddNewClosedQuestionNodePopUp.setLink('Testingwithdefaultclosed')
      .setDefaultClosedQuestion('Testingwithdefaultclosed')
      .setLabel('Testingwithdefaultclosed')
      .clickOnAddBtn({ force: true })
    DiagramUtils.findChildNodeByLinkText('Testing', 'Testingwithdefaultclosed').then(($el) => {
      expect($el.data.key).equals('Testingwithdefaultclosed')
    })
  })

  it('[ CMTS-16473 ] Verify if we are able to create closed question node with a custom template', () => {
    DiagramUtils.findDiagram()
    DiagramUtils.addNodeInDiagram(PalleteBox.closedQNodeIdSelector, 'Testing')
    AddNewClosedQuestionNodePopUp.setLink('Default2')
      .setDefaultClosedQuestion('How are you ?')
      .setLabel('Hello')
      .setLinkDescription('Ok')
      .setDescription('welcome')
      .clickOnAddBtn({ force: true })
    DiagramUtils.findChildNodeByLinkText('Testing', 'Default2').then(($el) => {
      expect($el.data.key).equals('Hello')
    })
    DiagramUtils.click('Hello')
    TabPropertiesSideBar.isTitleSet('Hello').isTitleSet('DefaultClosedTemplate').isNodeLabelSet('Hello')
    TabPropertiesSideBar.openTemplateSection()
      .isTemplateLabelSet('DefaultClosedTemplate')
      .isTemplateDescriptionSet('This is the default closed template.')
  })
})
