import FilterBar from '../../../../classes/bars/FilterBar'
import NavigationBar from '../../../../classes/navigation/NavigationBar'
import PalleteBox from '../../../../classes/PaletteBox'
import NodeValidation from '../../../../classes/validation/NodeLabel'
import DiagramUtils from '../../../../support/DiagramUtils'
import NodeLabel from '../../../../fixtures/data/validation/validationNodeLabel'
import LinkLabel from '../../../../fixtures/data/validation/validationLinkLabel'
import { ProjectInfo } from '../../../../types/response/ProjectInfo'

describe.skip('Add OpenQ Node - Validation', { tags: '@validation' }, () => {
  before(() => {
    cy.fixture('projects-templates/add-node-project.json').as('projects')

    cy.get<ProjectInfo>('@projects').then((json: ProjectInfo) => {
      cy.loginAPI()
      cy.createNewProject(json).then((projectInfo: ProjectInfo) => {
        Cypress.env('projectId', projectInfo.id)
        cy.loginUI()
        NavigationBar.selectProject(projectInfo.label)
      })
      NavigationBar.openFlowsPage()
      FilterBar.selectPattern('flowNodes')
      DiagramUtils.findDiagram()
      DiagramUtils.addNodeInDiagram(PalleteBox.openQNodeIdSelector, 'nodeLabel')
    })
  })

  after(() => {
    cy.removeProject(Cypress.env('projectId'))
  })

  NodeLabel.forEach((input) => {
    it(input.title, () => {
      NodeValidation.nodeLabel(input)
    })
  })

  LinkLabel.forEach((input) => {
    it(input.title, () => {
      NodeValidation.linkLabel(input)
    })
  })
})
