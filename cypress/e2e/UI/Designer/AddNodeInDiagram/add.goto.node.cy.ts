import PalleteBox from '../../../../classes/PaletteBox'
import NodeValidation from '../../../../classes/validation/NodeLabel'
import DiagramUtils from '../../../../support/DiagramUtils'
import LinkLabel from '../../../../fixtures/data/validation/validationLinkLabel'
import { ProjectInfo } from '../../../../types/response/ProjectInfo'
import AddNewGoLinkNodePopUp from '../../../../classes/popups/AddNewGoLinkNodePopUp'

describe('Goto Link node adding validations', () => {
  afterEach(() => {
    cy.get<ProjectInfo>('@current-project').then((json: ProjectInfo) => {
      cy.removeProject(json.id)
    })
  })

  it(
    '[CMTS-10650] [CMTS-10688] GoTo link node can be added with the default link label',
    { tags: '@validation' },
    () => {
      cy.precondition('goto', 'general')
      DiagramUtils.findDiagram()
      DiagramUtils.addNodeInDiagram(PalleteBox.goToNodeIdSelector, 'nodeLabel')
      AddNewGoLinkNodePopUp.setLink('default').selectLinkToNode('subflow12').clickOnAddBtn({ force: true })
      DiagramUtils.findChildNodeByLinkText('nodeLabel', 'default').then(($el) => {
        expect($el.data.key).equals('gotoNode1')
      })
    },
  )

  it(
    '[CMTS-10685] [CMTS-10682] GoTo link node can be added with Normal (down) link type and without link description',
    { tags: '@validation' },
    () => {
      cy.precondition('goto', 'general')
      DiagramUtils.findDiagram()
      DiagramUtils.addNodeInDiagram(PalleteBox.goToNodeIdSelector, 'nodeLabel')
      AddNewGoLinkNodePopUp.setLink('Testing')
        .selectLinkType('Normal (down)')
        .selectLinkToNode('subflow12')
        .clickOnAddBtn({ force: true })
      DiagramUtils.findChildNodeByLinkText('nodeLabel', 'Testing').then(($el) => {
        expect($el.data.key).equals('gotoNode1')
      })
    },
  )

  it(
    '[CMTS-10686] GoTo link node can be added with Escalate to HitL (right) link type',
    { tags: '@validation' },
    () => {
      cy.precondition('goto', 'general')
      DiagramUtils.findDiagram()
      DiagramUtils.addNodeInDiagram(PalleteBox.goToNodeIdSelector, 'nodeLabel')
      AddNewGoLinkNodePopUp.setLink('Testing')
        .selectLinkType('Escalate to HitL (right)')
        .selectLinkToNode('subflow12')
        .clickOnAddBtn({ force: true })
      DiagramUtils.findChildNodeByLinkText('nodeLabel', 'Testing').then(($el) => {
        expect($el.data.key).equals('gotoNode1')
      })
    },
  )

  it('[CMTS-10687] GoTo link node can be added with Abandonment (left) link type', { tags: '@validation' }, () => {
    cy.precondition('goto', 'general')
    DiagramUtils.findDiagram()
    DiagramUtils.addNodeInDiagram(PalleteBox.goToNodeIdSelector, 'nodeLabel')
    AddNewGoLinkNodePopUp.setLink('Testing')
      .selectLinkType('Abandonment (left)')
      .selectLinkToNode('subflow12')
      .clickOnAddBtn({ force: true })
    DiagramUtils.findChildNodeByLinkText('nodeLabel', 'Testing').then(($el) => {
      expect($el.data.key).equals('gotoNode1')
    })
  })

  it('[CMTS-10689] GoTo link node cannot be added without a link to node', { tags: '@validation' }, () => {
    cy.precondition('goto', 'general')
    DiagramUtils.findDiagram()
    DiagramUtils.addNodeInDiagram(PalleteBox.goToNodeIdSelector, 'nodeLabel')
    AddNewGoLinkNodePopUp.setLink('Testing')
      .selectLinkType('Abandonment (left)')
      .clickOnAddBtn({ force: true })
      .isLinkToNodeErrorMessageDisplayed()
  })

  it(
    '[CMTS-10691] A new GoTo link node is added after clicking the Add button in the Add Goto Link pop-up',
    { tags: '@validation' },
    () => {
      cy.precondition('goto', 'general')
      DiagramUtils.findDiagram()
      DiagramUtils.addNodeInDiagram(PalleteBox.goToNodeIdSelector, 'nodeLabel')
      AddNewGoLinkNodePopUp.setLink('Testing')
        .selectLinkType('Abandonment (left)')
        .selectLinkToNode('subflow12')
        .clickOnAddBtn({ force: true })
      DiagramUtils.findDiagram()
      DiagramUtils.findChildNodeByLinkText('nodeLabel', 'Testing').then(($el) => {
        expect($el.data.key).equals('gotoNode1')
      })
    },
  )

  it(
    '[CMTS-10702] A new GoTo link node is added after clicking the Add button in the Add Goto Link pop-up',
    { tags: '@validation' },
    () => {
      cy.precondition('gotoNodesValidation', 'general')
      DiagramUtils.findDiagram()
      DiagramUtils.addNodeInDiagram(PalleteBox.subflowNodeIdSelector, 'nodeLabel')
      DiagramUtils.doubleClick('nodeLabel')
      // Add Goto node 1
      DiagramUtils.findDiagram()
      DiagramUtils.addNodeInDiagram(PalleteBox.goToNodeIdSelector, 'Open2')
      AddNewGoLinkNodePopUp.selectLinkType('Normal (down)')
        .selectLinkToNode('Prompt1')
        .setLink('default')
        .clickOnAddBtn({ force: true })
      DiagramUtils.findDiagram()
      DiagramUtils.findChildNodeByLinkText('Open2', 'default').then(($el) => {
        expect($el.data.key).equals('gotoNode1')
      })
      // Add Goto node 2
      cy.wait(500)
      DiagramUtils.findDiagram()
      DiagramUtils.addNodeInDiagram(PalleteBox.goToNodeIdSelector, 'Open2', false)
      AddNewGoLinkNodePopUp.setLink('No')
        .selectLinkType('Escalate to HitL (right)')
        .selectLinkToNode('Open1')
        .clickOnAddBtn({ force: true })
      DiagramUtils.findDiagram()
      DiagramUtils.findChildNodeByLinkText('Open2', 'No').then(($el) => {
        expect($el.data.key).equals('gotoNode2')
      })
      // Add Goto node 3
      cy.wait(500)
      DiagramUtils.findDiagram()
      DiagramUtils.addNodeInDiagram(PalleteBox.goToNodeIdSelector, 'Open2', false)
      AddNewGoLinkNodePopUp.setLink('Yes')
        .selectLinkType('Abandonment (left)')
        .selectLinkToNode('Close1')
        .clickOnAddBtn({ force: true })
      DiagramUtils.findDiagram()
      DiagramUtils.findChildNodeByLinkText('Open2', 'Yes').then(($el) => {
        expect($el.data.key).equals('gotoNode1')
      })
    },
  )

  LinkLabel.forEach((input) => {
    it(input.title, () => {
      cy.precondition('add-node-project', 'general')
      DiagramUtils.findDiagram()
      DiagramUtils.addNodeInDiagram(PalleteBox.goToNodeIdSelector, 'nodeLabel')
      NodeValidation.linkLabel(input)
    })
  })
})
