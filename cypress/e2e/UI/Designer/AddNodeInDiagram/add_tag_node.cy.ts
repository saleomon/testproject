import { ProjectInfo } from '../../../../types/response/ProjectInfo'
import DiagramUtils from '../../../../support/DiagramUtils'
import NavigationBar from '../../../../classes/navigation/NavigationBar'
import { DesignerTopBarHrefs } from '../../../../fixtures/navigation-data/designer-top-bar-hrefs'
import PalleteBox from '../../../../classes/PaletteBox'
import NodeDetails from '../../../../classes/NodeDetails'
import NodeDetailsPanel from '../../../../classes/NodeDetailsPanel'
import '../../../../fixtures/data/messagesErrorText.json'
import DialogModal from '../../../../classes/popups/DialogPopUp'
import AddNewNodePopUp from '../../../../classes/popups/AddNewNodePopUp'
import AddNewTagNodePopUp from '../../../../classes/popups/AddNewTagNodePopUp'

describe('Adding Tag node validation', () => {
  let validationMessages: any
  beforeEach(() => {
    cy.fixture('projects-templates/empty.json').as('projects')
    cy.get<ProjectInfo>('@projects').then((json: ProjectInfo) => {
      cy.loginAPI()
      cy.createNewProject(json).then((projectInfo: ProjectInfo) => {
        cy.loginUI()
        NavigationBar.selectProject(projectInfo.label)
      })
    })
    cy.fixture('data/messagesErrorText.json').then((data) => {
      validationMessages = data
    })
    NavigationBar.clickTopBarButton(DesignerTopBarHrefs.patterns)
    DiagramUtils.findDiagram()
    DiagramUtils.addNodeInDiagram(PalleteBox.tagNodeIdSelector, 'Start', true)
  })

  afterEach(() => {
    cy.get<ProjectInfo>('@projects').then((json: ProjectInfo) => {
      cy.removeProject(json.id)
    })
  })

  it('[ CMTS-10225 ] Tag node cannot be added if the node label contains 1 character', { tags: '@validation' }, () => {
    AddNewNodePopUp.setLabel('a')
    NodeDetails.saveButton().click()
    AddNewTagNodePopUp.errorTextCheck(validationMessages.characterLength)
    DialogModal.clickOnCloseBtn()
  })

  it('[ CMTS-10226 ] Tag node cannot be added if the node label contains 33 characters', { tags: '@validation' },() => {
    AddNewNodePopUp.setLabel('morethan32morethan32morethan32mor')
    NodeDetails.saveButton().click()
    AddNewTagNodePopUp.errorTextCheck(validationMessages.characterLength)
    DialogModal.clickOnCloseBtn()
  })

  it('[ CMTS-10231 ] Tag node cannot be added if the node label contains a space at the end', { tags: '@validation' }, () => {
    AddNewNodePopUp.setLabel('morethan32morethan32moret   han')
    NodeDetails.saveButton().click()
    AddNewTagNodePopUp.errorTextCheck(validationMessages.spacesAndunderscores)
    DialogModal.clickOnCloseBtn()
  })

  it('[ CMTS-10240 ] Tag node cannot be added if the node label contains an underscore at the end', { tags: '@validation' }, () => {
    AddNewNodePopUp.setLabel('morethan32moreth_a')
    NodeDetails.saveButton().click()
    AddNewTagNodePopUp.errorTextCheck(validationMessages.spacesAndunderscores)
    DialogModal.clickOnCloseBtn()
  })

  it('[ CMTS-10243 ] [ CMTS-10273] Tag node cannot be added if the node label contains special characters ', { tags: '@validation' }, () => {
    AddNewNodePopUp.setLabel('morethan32moreth&&a')
    NodeDetails.saveButton().click()
    AddNewTagNodePopUp.errorTextCheck(validationMessages.startWithLetter)
    DialogModal.clickOnCloseBtn()
  })

  it('[ CMTS-10244 ] Tag node cannot be added if the node label contains symbols ', { tags: '@validation' }, () => {
    AddNewNodePopUp.setLabel('ab€£¥©™')
    NodeDetails.saveButton().click()
    AddNewTagNodePopUp.errorTextCheck(validationMessages.startWithLetter)
    DialogModal.clickOnCloseBtn()
  })

  it('[ CMTS-10245 ] Tag node cannot be added if the node label starts with a digit ', { tags: '@validation' }, () => {
    AddNewNodePopUp.setLabel('2morethan32moreth')
    NodeDetails.saveButton().click()
    AddNewTagNodePopUp.errorTextCheck(validationMessages.startWithLetter)
    DialogModal.clickOnCloseBtn()
  })

  it('[ CMTS-10246 ] Tag node cannot be added without the node label ', { tags: '@validation' }, () => {
    NodeDetails.saveButton().click()
    AddNewTagNodePopUp.errorTextCheck(validationMessages.uniqueName)
    DialogModal.clickOnCloseBtn()
  })

  it('[ CMTS-10252 ] Tag node cannot be added without a link label ', { tags: '@validation' }, () => {
    NodeDetails.linkLabel().eq(2).clear()
    AddNewNodePopUp.setLabel('morethan32more')
    NodeDetails.saveButton().click()
    AddNewTagNodePopUp.errorTextCheck(validationMessages.uniqueLinkLabel)
    DialogModal.clickOnCloseBtn()
  })

  it('[ CMTS-10375 ] Tag node cannot be added if the link label contains a space in the middle ', { tags: '@validation' }, () => {
    NodeDetails.linkLabel().eq(2).clear().type('moretha   n32more')
    AddNewNodePopUp.setLabel('morethan32more')
    NodeDetails.saveButton().click()
    AddNewTagNodePopUp.errorTextCheck(validationMessages.spacesAndunderscores)
    DialogModal.clickOnCloseBtn()
  })

  it('[ CMTS-10376 ] Tag node cannot be added if the link label contains an underscore in the middle ', { tags: '@validation' }, () => {
    NodeDetails.linkLabel().eq(2).clear().type('moetha_n32more')
    AddNewNodePopUp.setLabel('morethan32more')
    NodeDetails.saveButton().click()
    AddNewTagNodePopUp.errorTextCheck(validationMessages.spacesAndunderscores)
    DialogModal.clickOnCloseBtn()
  })

  it('[ CMTS-10445 ] Tag node cannot be added if the link label contains symbols ', { tags: '@validation' }, () => {
    NodeDetails.linkLabel().eq(2).clear().type('moretha   n32more')
    AddNewNodePopUp.setLabel('ab€£¥©™')
    NodeDetails.saveButton().click()
    AddNewTagNodePopUp.errorTextCheck(validationMessages.startWithLetter)
    DialogModal.clickOnCloseBtn()
  })

  it('[ CMTS-10376 ] Tag node cannot be added if the link label contains an underscore in the middle ', { tags: '@validation' }, () => {
    NodeDetails.linkLabel().eq(2).clear().type('a')
    AddNewNodePopUp.setLabel('morethan32more')
    NodeDetails.saveButton().click()
    AddNewTagNodePopUp.errorTextCheck(validationMessages.characterLength)
    DialogModal.clickOnCloseBtn()
  })

  it('[ CMTS-10261 ] Tag node cannot be added if the link label contains 1 character ', { tags: '@validation' }, () => {
    NodeDetails.linkLabel().eq(2).clear().type('a')
    AddNewNodePopUp.setLabel('morethan32more')
    NodeDetails.saveButton().click()
    AddNewTagNodePopUp.errorTextCheck(validationMessages.characterLength)
    DialogModal.clickOnCloseBtn()
  })

  it('[ CMTS-10262 ] Tag node cannot be added if the link label contains 33 characters ', { tags: '@validation' }, () => {
    NodeDetails.linkLabel().eq(2).clear().type('a')
    AddNewNodePopUp.setLabel('morethan32moremoremoremore')
    NodeDetails.saveButton().click()
    AddNewTagNodePopUp.errorTextCheck(validationMessages.characterLength)
    DialogModal.clickOnCloseBtn()
  })

  it('[ CMTS-10264 ] Tag node cannot be added if the link label contains an underscore at the end ', { tags: '@validation' }, () => {
    NodeDetails.linkLabel().eq(2).clear().type('a')
    AddNewNodePopUp.setLabel('morethan32more__123')
    NodeDetails.saveButton().click()
    AddNewTagNodePopUp.errorTextCheck(validationMessages.spacesAndunderscores)
    DialogModal.clickOnCloseBtn()
  })

  // Failing due to issue
  it('[ CMTS-10265 ] Tag node cannot be added if the link label contains special characters ', { tags: '@validation' }, () => {
    NodeDetails.linkLabel().eq(2).clear().type('ab_@#')
    AddNewNodePopUp.setLabel('Greeting')
    NodeDetails.saveButton().click()
    AddNewTagNodePopUp.errorTextCheck(validationMessages.startWithLetter)
    DialogModal.clickOnCloseBtn()
  })

  it('[ CMTS-10230 ] Tag node with the already existing name cannot be added ', { tags: '@validation' }, () => {
    NodeDetails.linkLabel().eq(2).clear().type('Testing')
    AddNewNodePopUp.setLabel('Testing')
    NodeDetails.saveButton().click()
    DiagramUtils.findDiagram()
    DiagramUtils.addNodeInDiagram(PalleteBox.tagNodeIdSelector, 'Testing', true)
    NodeDetails.linkLabel().eq(2).clear().type('Testing')
    AddNewNodePopUp.setLabel('Testing')
    NodeDetails.saveButton().click()
    AddNewTagNodePopUp.errorTextCheck(validationMessages.existingLabel)
    DialogModal.clickOnCloseBtn()
  })
  
  it('[ CMTS-10221 ] Tag node can be added if the node label contains 2 characters ', { tags: '@validation' }, () => {
    NodeDetails.linkLabel().eq(2).clear().type('Testing')
    AddNewNodePopUp.setLabel('aa')
    NodeDetails.saveButton().click()
    DiagramUtils.findDiagram()
    DiagramUtils.findNodeByKey('aa')
  })

  it('[ CMTS-10222 ] Tag node can be added if the node label contains 32 characters and capital letters ', { tags: '@validation' },  () => {
    NodeDetails.linkLabel().eq(2).clear().type('Testing')
    AddNewNodePopUp.setLabel('TestingTagNodeTestingTagNodeABCD')
    NodeDetails.saveButton().click()
    DiagramUtils.findDiagram()
    DiagramUtils.findNodeByKey('TestingTagNodeTestingTagNodeABCD')
  })

  it('[ CMTS-10223 ] [ CMTS-10268 ] Tag node can be added if the node label contains a letter , digit and does not contain Link description', { tags: '@validation' }, () => {
    NodeDetails.linkLabel().eq(2).clear().type('Testing')
    AddNewNodePopUp.setLabel('TestingTagNodeTestingTagNode2023')
    NodeDetails.saveButton().click()
    DiagramUtils.findDiagram()
    DiagramUtils.findNodeByKey('TestingTagNodeTestingTagNode2023')
  })

  it('[ CMTS-10253 ] Tag node can be added if the link label contains 2 characters ', { tags: '@validation' }, () => {
    NodeDetails.linkLabel().eq(2).clear().type('aa')
    AddNewNodePopUp.setLabel('Testing')
    NodeDetails.saveButton().click()
    DiagramUtils.findDiagram()
    DiagramUtils.findNodeByKey('Testing')
  })

  it('[ CMTS-10254 ] Tag node can be added if the link label contains 32 characters and capital letters ', { tags: '@validation' }, () => {
    NodeDetails.linkLabel().eq(2).clear().type('TestingTagNodeTestingTagNodeABCD')
    AddNewNodePopUp.setLabel('Testing')
    NodeDetails.saveButton().click()
    DiagramUtils.findDiagram()
    DiagramUtils.findNodeByKey('Testing')
  })

  let detailsPanel = new NodeDetailsPanel()
  it('[ CMTS-10272 ] A new tag node is added after clicking the Add button in the Add New Node pop-up ', { tags: '@smoke' }, () => {
    NodeDetails.linkLabel().eq(2).clear().type('TestingTagNodeTestingTagNodeABCD')
    AddNewNodePopUp.setLabel('Testing')
    NodeDetails.saveButton().click()
    DiagramUtils.findDiagram()
    DiagramUtils.findNodeByKey('Testing')
    DiagramUtils.click('Testing')
    cy.get(detailsPanel.headerSelector).should('contain', 'Testing')
    cy.get(detailsPanel.nodeEdit).find('.ant-input').should('have.value', 'Testing')
    NodeDetails.nodeTypeField().should('have.value', 'tagNode')
    DiagramUtils.findChildNodeByLinkText('Start', 'TestingTagNodeTestingTagNodeABCD').then(($el) => {
      expect($el.data.key).equals('Testing')
    })
  })
})
