import { ProjectInfo } from '../../../../types/response/ProjectInfo'
import DiagramUtils from '../../../../support/DiagramUtils'
import NavigationBar from '../../../../classes/navigation/NavigationBar'
import { DesignerTopBarHrefs } from '../../../../fixtures/navigation-data/designer-top-bar-hrefs'
import PalleteBox from '../../../../classes/PaletteBox.js'
import  { messages } from '../../../../fixtures/data/messagesErrorText.js'

import FilterBar from '../../../../classes/bars/FilterBar'
import TabPropertiesSideBar from '../../../../classes/bars/TabPropertiesSideBar'
import TabOutputSideBar from '../../../../classes/bars/TabOutputSideBar'
import AddNewPromptNodePopUp from '../../../../classes/popups/AddNewPromptNodePopUp'

describe('Adding Prompt node validation', () => {
  beforeEach(()=> {
      cy.fixture('entity-templates/entity.json').as('projects')
      cy.get<ProjectInfo>('@projects').then((json: ProjectInfo) => {
        cy.loginAPI()
        cy.createNewProject(json).then((projectInfo: ProjectInfo) => {
          cy.loginUI()
          NavigationBar.selectProject(projectInfo.label)
        })
      })
     NavigationBar.clickTopBarButton(DesignerTopBarHrefs.flows)
     FilterBar.selectChannel('RWC')
      DiagramUtils.findDiagram()
      DiagramUtils.addNodeInDiagram(PalleteBox.promptNodeIdSelector, 'Start', true)
    })

    afterEach(() => {
      cy.get<ProjectInfo>('@projects').then((json: ProjectInfo) => {
        cy.removeProject(json.id)
      })
    })
  
    it("[ CMTS-10411 ] Prompt node with the already existing name cannot be added", () => {
      AddNewPromptNodePopUp.setLabel('Prompt1')
      .clickOnAddBtn({force:true})
      DiagramUtils.addNodeInDiagram(PalleteBox.promptNodeIdSelector, 'Prompt1', false)
      AddNewPromptNodePopUp.setLabel('Prompt1')
      .clickOnAddBtn()
      .errorTextCheck(messages.existingLabel)
      .clickOnCloseBtn()
      })

    it("[ CMTS-10422 ] Prompt node cannot be added without the node label", () => {
      AddNewPromptNodePopUp.clickOnAddBtn({force:true})
      .errorTextCheck(messages.uniqueName)
      .clickOnCloseBtn()
        })

    it("[ CMTS-10424 ] [ CMTS-10453 ] Prompt node can be added without a node description and Link description", () => {
      AddNewPromptNodePopUp.setLabel('Testing')
          .clickOnAddBtn({force:true})
          DiagramUtils.findDiagram()
          DiagramUtils.findNodeByKey('Testing')
          DiagramUtils.click('Testing')
          TabPropertiesSideBar.isTitleSet('Testing')
          .isNodeLabelSet('Testing')
          DiagramUtils.findChildNodeByLinkText('Start', 'default').then(($el) => {
            expect($el.data.key).equals('Testing')
          })
        })
    
    it("[ CMTS-10432 ] [ CMTS-10483 ] Prompt node cannot be added without a link label ", () => {
      AddNewPromptNodePopUp.setLink('')
      .setLabel('Testing')
      .clickOnAddBtn({force:true})
      .errorTextCheck(messages.uniqueLinkLabel)
      .clickOnCloseBtn()
              cy.log('Validate if the prompt node is not added')
              DiagramUtils.findDiagram()
              DiagramUtils.findChildNodeByLinkText('Start', 'default').then(($el) => {
                expect($el.data.key).not.equals('Testing')
              })
        })
        
    
    it("[ CMTS-10446 ] Prompt node cannot be added after clicking the Add button if a warning message is displayed in the Add New Node pop-up", () => {
      AddNewPromptNodePopUp.setLabel('morethan32moreth_a')
          .clickOnAddBtn({force:true})
          .errorTextCheck(messages.spacesAndunderscores)
          .clickOnCloseBtn()
          cy.log('Validate if the prompt node is not added')
          DiagramUtils.findDiagram()
          DiagramUtils.findChildNodeByLinkText('Start', 'default').then(($el) => {
            expect($el.data.key).not.equals('morethan32moreth_a')
          })
        })
    
    
    it("[ CMTS-10447 ] Two flows with the same name cannot be added to one prompt node ", () => {
      AddNewPromptNodePopUp.setLabel('Prompt1')
      .clickOnAddBtn({force:true})
      DiagramUtils.findDiagram()
      DiagramUtils.findNodeByKey('Prompt1')
      DiagramUtils.addNodeInDiagram(PalleteBox.promptNodeIdSelector, 'Prompt1', false)
      AddNewPromptNodePopUp.setLabel('Prompt2')
      .setLink('yes')
      .clickOnAddBtn({force:true})
      DiagramUtils.findDiagram()
      DiagramUtils.findNodeByKey('Prompt2')
      DiagramUtils.addNodeInDiagram(PalleteBox.promptNodeIdSelector, 'Prompt1', false)
      AddNewPromptNodePopUp.setLabel('Prompt3')
      .setLink('yes')
      .clickOnAddBtn({force:true})
      .errorTextCheck(messages.existingLinkLabel)
      .clickOnCloseBtn()
      cy.log('Validate if the prompt node is not added')
              DiagramUtils.findDiagram()
              DiagramUtils.findChildNodeByLinkText('Prompt1', 'yes').then(($el) => {
                expect($el.data.key).not.equals('Prompt3')
              })
        })


    it("[ CMTS-10426 ] Prompt node can be added without a default prompt", () => {
      AddNewPromptNodePopUp.setLabel('Prompt1')
      .clickOnAddBtn({force:true})
        DiagramUtils.findDiagram()
        DiagramUtils.findNodeByKey('Prompt1')
        DiagramUtils.click('Prompt1')
        TabPropertiesSideBar.isTitleSet('Prompt1')
        .isNodeLabelSet('Prompt1') 
        .selectTabByName('Output')
        TabOutputSideBar.clickNodeLabelEditBtn()
        TabOutputSideBar.isContent('Default','')
      })

      it("[ CMTS-10455 ] A new prompt node is added after clicking the Add button in the Add New Node pop-up", () => {
        AddNewPromptNodePopUp.setLabel('Prompt1')
        .setDescription('This is the first prompt')
        .setLink('yes')
        .setLinkDescription('This a positive flow')
        .setDefaultPrompt('Welcome to the Best Club!')
        .clickOnAddBtn({force:true})
            cy.log('Validate if the prompt node is added')
            DiagramUtils.findDiagram()
            DiagramUtils.findNodeByKey('Prompt1')
            DiagramUtils.click('Prompt1')
            TabPropertiesSideBar.isTitleSet('Prompt1')
            TabPropertiesSideBar.isNodeLabelSet('Prompt1') 
            DiagramUtils.findChildNodeByLinkText('Start', 'yes').then(($el) => {
            expect($el.data.key).equals('Prompt1')
          })
          TabPropertiesSideBar.selectTabByName('Output')
          TabOutputSideBar.clickNodeLabelEditBtn()
          TabOutputSideBar.isContent('Default','Welcome to the Best Club!')
          })
        })