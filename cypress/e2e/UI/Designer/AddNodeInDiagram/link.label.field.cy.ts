import { ProjectInfo } from '../../../../types/response/ProjectInfo'
import DiagramUtils from '../../../../support/DiagramUtils'
import AddNewNodePopUp from '../../../../classes/popups/AddNewNodePopUp'
import PaletteBox from '../../../../classes/PaletteBox'
import AddNewGlobalFallbackOutcomePopUp from '../../../../classes/popups/AddNewGlobalFallbackOutcomePopUp'

describe('Add New Node modal -> [Link Label] filed', () => {
    beforeEach(() => {
    cy.restoreLocalStorage()

    cy.fixture('projects-templates/qa_autotest.json').as('projects')

    cy.get<ProjectInfo>('@projects').then((json: ProjectInfo) => {
      cy.loginAPI()
      cy.createNewProject(json).then((projectInfo: ProjectInfo) => {
        cy.loginUI()
        cy.selectProjectAndPattern(projectInfo.label, 'subflow-prompt')
        DiagramUtils.findDiagram()
        DiagramUtils.click('SubFlow1')
      })
    })
  })

  afterEach(() => {
    cy.get<ProjectInfo>('@projects').then((json: ProjectInfo) => {
      cy.removeProject(json.id)
    })
  })

  let nodesInfo: {
    nodeSelector: string
    nodeName: string
    dropTarget: string
    isPlus: boolean
    linkLabelValue: string
  }[] = [
    {
      nodeSelector: PaletteBox.subflowNodeIdSelector,
      nodeName: 'Sub-flow Node',
      dropTarget: 'plus icon',
      isPlus: true,
      linkLabelValue: 'default',
    },
    {
      nodeSelector: PaletteBox.subflowNodeIdSelector,
      nodeName: 'Sub-flow Node',
      dropTarget: 'another node',
      isPlus: false,
      linkLabelValue: '',
    },
    {
      nodeSelector: PaletteBox.skillRouterNodeIdSelector,
      nodeName: 'Skill Router',
      dropTarget: 'plus icon',
      isPlus: true,
      linkLabelValue: 'default',
    },
    {
      nodeSelector: PaletteBox.skillRouterNodeIdSelector,
      nodeName: 'Skill Router',
      dropTarget: 'another node',
      isPlus: false,
      linkLabelValue: '',
    },
    {
      nodeSelector: PaletteBox.tagNodeIdSelector,
      nodeName: 'Tag Node',
      dropTarget: 'plus icon',
      isPlus: true,
      linkLabelValue: 'default',
    },
    {
      nodeSelector: PaletteBox.tagNodeIdSelector,
      nodeName: 'Tag Node',
      dropTarget: 'another node',
      isPlus: false,
      linkLabelValue: '',
    },
    {
      nodeSelector: PaletteBox.goToNodeIdSelector,
      nodeName: 'GoTo',
      dropTarget: 'plus icon',
      isPlus: true,
      linkLabelValue: 'default',
    },
    {
      nodeSelector: PaletteBox.goToNodeIdSelector,
      nodeName: 'GoTo',
      dropTarget: 'another node',
      isPlus: false,
      linkLabelValue: '',
    },
  ]

  let outcomesInfo: {
    nodeSelector: string
    nodeName: string
    dropTarget: string
    isPlus: boolean
    linkLabelValue: string
  }[] = [
    {
      nodeSelector: PaletteBox.patternAbandonSelector,
      nodeName: 'Abandon',
      dropTarget: 'plus icon',
      isPlus: true,
      linkLabelValue: 'default',
    },
    {
      nodeSelector: PaletteBox.patternAbandonSelector,
      nodeName: 'Abandon',
      dropTarget: 'another node',
      isPlus: false,
      linkLabelValue: '',
    },
    {
      nodeSelector: PaletteBox.patternHITLSelector,
      nodeName: 'HITL',
      dropTarget: 'plus icon',
      isPlus: true,
      linkLabelValue: 'default',
    },
    {
      nodeSelector: PaletteBox.patternHITLSelector,
      nodeName: 'HITL',
      dropTarget: 'another node',
      isPlus: false,
      linkLabelValue: '',
    },
    {
      nodeSelector: PaletteBox.patternContainedSelector,
      nodeName: 'Contained',
      dropTarget: 'plus icon',
      isPlus: true,
      linkLabelValue: 'default',
    },
    {
      nodeSelector: PaletteBox.patternContainedSelector,
      nodeName: 'Contained',
      dropTarget: 'another node',
      isPlus: false,
      linkLabelValue: '',
    },
    {
      nodeSelector: PaletteBox.globalEscalateSelector,
      nodeName: 'Escalate',
      dropTarget: 'plus icon',
      isPlus: true,
      linkLabelValue: 'default',
    },
    {
      nodeSelector: PaletteBox.globalEscalateSelector,
      nodeName: 'Escalate',
      dropTarget: 'another node',
      isPlus: false,
      linkLabelValue: '',
    },
    {
      nodeSelector: PaletteBox.globalFallbackSelector,
      nodeName: 'Fallback',
      dropTarget: 'plus icon',
      isPlus: true,
      linkLabelValue: 'default',
    },
    {
      nodeSelector: PaletteBox.globalFallbackSelector,
      nodeName: 'Fallback',
      dropTarget: 'another node',
      isPlus: false,
      linkLabelValue: '',
    },
  ]
  nodesInfo.forEach((info) => {
    it(`Check [Link label] field value if [${info.nodeName}] node is dropped into ${info.dropTarget}`, () => {
      DiagramUtils.findDiagram()
      DiagramUtils.addNodeInDiagram(info.nodeSelector, 'SubFlow1', info.isPlus)
      AddNewNodePopUp.isLinkSet(info.linkLabelValue)
    })
  })

  outcomesInfo.forEach((info) => {
    it(`Check [Link label] field value if [${info.nodeName}] outcome is dropped into ${info.dropTarget}`, () => {
      DiagramUtils.findDiagram()
      PaletteBox.expandCollapseExits().click()
      DiagramUtils.addNodeInDiagram(info.nodeSelector, 'SubFlow1', info.isPlus)
      AddNewGlobalFallbackOutcomePopUp.isEndpointLinkSet(info.linkLabelValue)
    })
  })
})
