import { dataObject } from '../../../fixtures/data/data-spec-projects'
import { ProjectInfo } from '../../../types/response/ProjectInfo'
import DiagramUtils from '../../../support/DiagramUtils'
import NodePropertiesSideBar from '../../../classes/bars/NodePropertiesSideBar'
import AddNewOutputCondition from '../../../classes/popovers/AddNewOutputCondition'

describe.skip('work with flow diagram on display - prompt node', () => { // need to add test suite for skill router
  beforeEach(() => {
    cy.restoreLocalStorage()

    cy.fixture('projects-templates/qa_autotest.json').as('project')

    cy.get<ProjectInfo>('@project').then((json: ProjectInfo) => {
      cy.loginAPI()
      cy.createNewProject(json).as('current-project')
      cy.loginUI()
    })

    cy.get<ProjectInfo>('@current-project').then((projectInfo: ProjectInfo) => {
      cy.selectProjectAndPattern(projectInfo.label, 'subflow-prompt')
    })

    DiagramUtils.findDiagram()
    DiagramUtils.doubleClick(dataObject.pattern_diagram.subflow1.nodeLabel)
    DiagramUtils.findDiagram()
  })

  afterEach(() => {
    cy.get<ProjectInfo>('@current-project').then((json: ProjectInfo) => {
      cy.removeProject(json.id)
    })
  })

  it('[CMTS-9966] Output content text and Node description are displayed on the Prompt node when the Both radio button is selected', () => {
    DiagramUtils.click(dataObject.flow_diagram.prompt1.nodeLabel)

    NodePropertiesSideBar.openProperties().setDescription('This is a prompt node description')

    NodePropertiesSideBar.openOutput().clickOnAddNewOutputConditionBtn()
    AddNewOutputCondition.setLabel('OutputCondition1').setContent('Content for OutputCondition1').clickOnAddBtn()

    NodePropertiesSideBar.openProperties().clickOnBothBtn().isBothBtnFocused()

    NodePropertiesSideBar.openOutput().isContentSet('Content for OutputCondition1')

    NodePropertiesSideBar.openProperties().isDescriptionSet('This is a prompt node description')
  })

  it('[CMTS-9967] Only the Node description is displayed on the Prompt node when the Both radio button is selected and the Content field of the Output tab is empty', () => {
    DiagramUtils.click(dataObject.flow_diagram.prompt1.nodeLabel)

    NodePropertiesSideBar.openProperties().setDescription('This is a prompt node description')

    NodePropertiesSideBar.openOutput().clickOnAddNewOutputConditionBtn()
    AddNewOutputCondition.setLabel('OutputCondition1').setContent('').clickOnAddBtn()

    NodePropertiesSideBar.openProperties().clickOnBothBtn().isBothBtnFocused()

    NodePropertiesSideBar.openOutput().isContentSet('')

    NodePropertiesSideBar.openProperties().isDescriptionSet('This is a prompt node description')
  })

  it('[CMTS-9968] Only the Output content text is displayed on the Prompt node when the Both radio button is selected and the Node description of the Properties tab is empty', () => {
    DiagramUtils.click(dataObject.flow_diagram.prompt1.nodeLabel)

    NodePropertiesSideBar.openProperties().setDescription('')

    NodePropertiesSideBar.openOutput().clickOnAddNewOutputConditionBtn()
    AddNewOutputCondition.setLabel('OutputCondition1').setContent('Content for OutputCondition1').clickOnAddBtn()

    NodePropertiesSideBar.openProperties().clickOnBothBtn().isBothBtnFocused()

    NodePropertiesSideBar.openOutput().isContentSet('Content for OutputCondition1')

    NodePropertiesSideBar.openProperties().isDescriptionSet('')
  })

  it('[CMTS-10008] Output content text and Node description are not displayed on the Prompt node when the Both radio button is selected and these fields are left empty', () => {
    DiagramUtils.click(dataObject.flow_diagram.prompt1.nodeLabel)

    NodePropertiesSideBar.openProperties().setDescription('')

    NodePropertiesSideBar.openOutput().clickOnAddNewOutputConditionBtn()
    AddNewOutputCondition.setLabel('OutputCondition1').setContent('').clickOnAddBtn()

    NodePropertiesSideBar.openProperties().clickOnBothBtn().isBothBtnFocused()

    NodePropertiesSideBar.openOutput().isContentSet('')

    NodePropertiesSideBar.openProperties().isDescriptionSet('')
  })

  it('[CMTS-10923] Both radio button selected: output and description are not displayed when Content and Node Description fields are empty', () => {
    DiagramUtils.click(dataObject.flow_diagram.prompt1.nodeLabel)

    NodePropertiesSideBar.openProperties().setDescription('')

    NodePropertiesSideBar.openOutput().clickOnAddNewOutputConditionBtn()
    AddNewOutputCondition.setLabel('OutputCondition1').setContent('').clickOnAddBtn()

    NodePropertiesSideBar.openProperties().clickOnBothBtn().isBothBtnFocused()

    NodePropertiesSideBar.openOutput().isContentSet('')

    NodePropertiesSideBar.openProperties().isDescriptionSet('')
  })

  it('[CMTS-10927] Node description is displayed on the Prompt node when "Description" radio button is selected', () => {
    DiagramUtils.click(dataObject.flow_diagram.prompt1.nodeLabel)

    NodePropertiesSideBar.openProperties().setDescription('This is a prompt node description')

    NodePropertiesSideBar.openOutput().clickOnAddNewOutputConditionBtn()
    AddNewOutputCondition.setLabel('OutputCondition1').setContent('Content for OutputCondition1').clickOnAddBtn()

    NodePropertiesSideBar.openProperties().clickOnDescriptionBtn().isDescriptionBtnFocused()

    NodePropertiesSideBar.openOutput().isContentSet('Content for OutputCondition1')

    NodePropertiesSideBar.openProperties().isDescriptionSet('This is a prompt node description')
  })

  it('[CMTS-10928] Output content text displayed on the Prompt node when Output radio button is selected ', () => {
    DiagramUtils.click(dataObject.flow_diagram.prompt1.nodeLabel)

    NodePropertiesSideBar.openProperties().setDescription('This is a prompt node description')

    NodePropertiesSideBar.openOutput().clickOnAddNewOutputConditionBtn()
    AddNewOutputCondition.setLabel('OutputCondition1').setContent('Content for OutputCondition1').clickOnAddBtn()

    NodePropertiesSideBar.openProperties().clickOnOutputBtn().isOutputBtnFocused()

    NodePropertiesSideBar.openOutput().isContentSet('Content for OutputCondition1')

    NodePropertiesSideBar.openProperties().isDescriptionSet('This is a prompt node description')
  })
})
