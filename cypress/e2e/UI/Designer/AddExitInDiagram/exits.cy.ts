import { dataObject } from '../../../../fixtures/data/data-spec-projects'
import DialogModal from '../../../../classes/popups/DialogPopUp'
import PaletteBox from '../../../../classes/PaletteBox'
import { ProjectInfo } from '../../../../types/response/ProjectInfo'
import DiagramUtils from '../../../../support/DiagramUtils'
import AddNewNodePopUp from '../../../../classes/popups/AddNewNodePopUp'
import AddNewSubFlowNodePopUp from '../../../../classes/popups/AddNewSubFlowNodePopUp'
import AddNewGlobalFallbackOutcomePopUp from '../../../../classes/popups/AddNewGlobalFallbackOutcomePopUp'
import AddNewSkillRouterNodePopUp from '../../../../classes/popups/AddNewSkillRouterNodePopUp'
import TabPropertiesSideBar from '../../../../classes/bars/TabPropertiesSideBar'
import TabFlowSideBar from '../../../../classes/bars/TabFlowSideBar'
import NavigationBar from '../../../../classes/navigation/NavigationBar'
import FilterBar from '../../../../classes/bars/FilterBar'
import LeftSubflowBars from '../../../../classes/bars/LeftSubflowBars'
import DiagramBar from '../../../../classes/bars/DiagramBar'
import AddNewOutcome from '../../../../classes/popups/AddNewOutcome'
import { DesignerTopBarHrefs } from '../../../../fixtures/navigation-data/designer-top-bar-hrefs'
import NodePropertiesSideBar from '../../../../classes/bars/NodePropertiesSideBar'
import { warnings } from '../../../../fixtures/data/warning'
import LabelWithError from '../../../../fixtures/provider/LabelWithError'
import LinkSubflowCharacter from '../../../../fixtures/provider/LinkSubflowCharacter'

describe('adding exits', () => {
  beforeEach(() => {
    cy.restoreLocalStorage()
  })

  afterEach(() => {
    cy.get<ProjectInfo>('@current-project').then((json: ProjectInfo) => {
      cy.removeProject(json.id)
    })
  })

  describe('different scenars adding node in diagram', { tags: '@smoke' }, () => {
    it('[CMTS-10274] Tag node can be added to the Start node via Drag&Drop to the plus sign', () => {
      cy.precondition('empty')

      DiagramUtils.findDiagram()
      DiagramUtils.addNodeInDiagram(PaletteBox.tagNodeIdSelector, 'Start', true)
      cy.fillTagPopUp(dataObject.pattern_diagram.tag1)
      DialogModal.clickOnAddBtn()

      DiagramUtils.findChildNodeByLinkText('Start', dataObject.pattern_diagram.tag1.linkLabel).then(($it) => {
        expect($it.data.key).equals(dataObject.pattern_diagram.tag1.nodeLabel)
      })
    })

    it('[CMTS-10275] Tag node can be added to the another node via Drag&Drop to the plus sign of this node', () => {
      cy.precondition('empty')

      DiagramUtils.findDiagram()

      DiagramUtils.addNodeInDiagram(PaletteBox.subflowNodeIdSelector, 'Start', true)
      cy.fillTagPopUp(dataObject.pattern_diagram.subflow1)
      DialogModal.clickOnAddBtn()

      DiagramUtils.addNodeInDiagram(PaletteBox.tagNodeIdSelector, dataObject.pattern_diagram.subflow1.nodeLabel, true)
      cy.fillTagPopUp(dataObject.pattern_diagram.tag1)
      DialogModal.clickOnAddBtn()

      DiagramUtils.findChildNodeByLinkText('Start', dataObject.pattern_diagram.subflow1.linkLabel).then(($it) => {
        expect($it.data.key).equals(dataObject.pattern_diagram.subflow1.nodeLabel)
      })

      DiagramUtils.findChildNodeByLinkText(
        dataObject.pattern_diagram.subflow1.nodeLabel,
        dataObject.pattern_diagram.tag1.linkLabel,
      ).then(($it) => {
        expect($it.data.key).equals(dataObject.pattern_diagram.tag1.nodeLabel)
      })
    })

    it('[CMTS-10276] Tag node can be added to the another node via Drag&Drop to this node', () => {
      cy.precondition('empty')

      DiagramUtils.findDiagram()

      DiagramUtils.addNodeInDiagram(PaletteBox.subflowNodeIdSelector, 'Start', true)
      cy.fillTagPopUp(dataObject.pattern_diagram.subflow1)
      DialogModal.clickOnAddBtn()

      DiagramUtils.addNodeInDiagram(PaletteBox.tagNodeIdSelector, dataObject.pattern_diagram.subflow1.nodeLabel)
      cy.fillTagPopUp(dataObject.pattern_diagram.tag1)
      DialogModal.clickOnAddBtn()

      DiagramUtils.findChildNodeByLinkText('Start', dataObject.pattern_diagram.subflow1.linkLabel).then(($it) => {
        expect($it.data.key).equals(dataObject.pattern_diagram.subflow1.nodeLabel)
      })

      DiagramUtils.findChildNodeByLinkText(
        dataObject.pattern_diagram.subflow1.nodeLabel,
        dataObject.pattern_diagram.tag1.linkLabel,
      ).then(($it) => {
        expect($it.data.key).equals(dataObject.pattern_diagram.tag1.nodeLabel)
      })
    })
  })

  describe('reset fields', { tags: '@reset' }, () => {
    it('[CMTS-10269] All input data inside Add New Node pop-up are removed after Clicking Reset button (tag node)', () => {
      cy.precondition('10269')

      DiagramUtils.findDiagram()
      DiagramUtils.addNodeInDiagram(PaletteBox.tagNodeIdSelector, 'Start', true)
      cy.fillTagPopUp(dataObject.pattern_diagram.tag1)
      AddNewNodePopUp.clickOnResetBtn()
        .isLabelSet('')
        .isDescriptionSet('')
        .isNodeLabelErrorMessageNotDisplayed()
        .isLinkSet('default')
        .isLinkLabelErrorMessageNotDisplayed()
        .isLinkDescriptionSet('')
      DialogModal.clickOnCloseBtn()
      DiagramUtils.findChildNodeByLinkText('Start', 'default').then(($el) => {
        expect($el.data.key).equals('AddNode1')
      })
    })

    it('[CMTS-10106] All input data inside Add New Node pop-up are removed after Clicking Reset button (subflow node)', () => {
      cy.precondition('10106')

      DiagramUtils.findDiagram()
      DiagramUtils.addNodeInDiagram(PaletteBox.subflowNodeIdSelector, 'Start', true)
      cy.fillTagPopUp(dataObject.pattern_diagram.subflow1)
      AddNewSubFlowNodePopUp.clickOnRemoveVoipBtn().isWarningMessageDisplayed(
        'Warning: Deleting channels from a subflow may cause automatic bot deployments to misbehave. ',
      )
      AddNewSubFlowNodePopUp.clickOnResetBtn()
        .isLabelSet('')
        .isNodeLabelErrorMessageNotDisplayed()
        .isDescriptionSet('')
        .isLinkSet('default')
        .isLinkLabelErrorMessageNotDisplayed()
        .isLinkDescriptionSet('')
        .isVoipBtnDisplayed()
      DialogModal.clickOnCloseBtn()
      DiagramUtils.findChildNodeByLinkText('Start', 'default').then(($el) => {
        expect($el.data.key).equals('AddNode1')
      })
    })

    it('[CMTS-12914] To validate the reset button removes the data on the Outcpme Add Outcome pop up', () => {
      cy.precondition('12914')

      DiagramUtils.findDiagram()
      PaletteBox.expandCollapseExits().click()
      DiagramUtils.addNodeInDiagram(
        PaletteBox.globalFallbackSelector,
        dataObject.pattern_diagram.subflow1.nodeLabel,
        true,
      )
      AddNewGlobalFallbackOutcomePopUp.setEndpointLink('globalFallback')
        .selectLinkType('Normal (down)')
        .setReason('This is the reason')
      AddNewGlobalFallbackOutcomePopUp.clickOnResetBtn()
        .isEndpointLinkSet('default')
        .isLinkLabelErrorMessageNotDisplayed()
        .isLinkTypeSet(' Abandonment (left)')
        .isReasonEmptySet('')
    })

    it('[CMTS-10000] To validate the reset button removes the data on the Skill Router node Add New Node pop up', () => {
      cy.precondition('10000')

      DiagramUtils.findDiagram()
      DiagramUtils.addNodeInDiagram(PaletteBox.skillRouterNodeIdSelector, 'Start', true)
      AddNewSkillRouterNodePopUp.setLabel(dataObject.pattern_diagram.skillsrouter1.nodeLabel)
        .setDescription(dataObject.pattern_diagram.skillsrouter1.nodeDescription)
        .setSkills()
        .setLink(dataObject.pattern_diagram.skillsrouter1.linkLabel)
        .setLinkDescription(dataObject.pattern_diagram.skillsrouter1.linkDescription)

        .clickOnResetBtn()
        .isLabelSet('')
        .isNodeLabelErrorMessageNotDisplayed()
        .isDescriptionSet('')
        .isLinkSet('default')
        .isLinkLabelErrorMessageNotDisplayed()
        .isLinkDescriptionSet('')
        .isSkillSet('')
      DialogModal.clickOnCloseBtn()
      DiagramUtils.findChildNodeByLinkText('Start', 'default').then(($el) => {
        expect($el.data.key).equals('AddNode1')
      })
    })
  })

  describe('adding exits ', { tags: '@exit' }, () => {
    it('[CMTS-14553] Exit can be added with Normal (down) link type', () => {
      cy.precondition('add-endpoint', `14533`)

      DiagramUtils.findDiagram()
      DiagramUtils.doubleClick(dataObject.pattern_diagram.subflow2.nodeLabel)
      NavigationBar.clickFlowBtn()
      DiagramUtils.findDiagram()
      PaletteBox.expandCollapseExits().click()
      DiagramUtils.addNodeInDiagram(PaletteBox.subflowEscalateIdSelector, 'AddNode1')
      AddNewOutcome.clickOnAddBtn()
      DiagramUtils.click(dataObject.flow_diagram.closedq1.nodeLabel)
      TabFlowSideBar.openFlow().isSideNode('default', 'down')
    })

    it('[CMTS-14588] Exit can be added with Escalate to HITL (right) link type', () => {
      cy.precondition('add-endpoint', `14533`)

      DiagramUtils.findDiagram()
      DiagramUtils.doubleClick(dataObject.pattern_diagram.subflow2.nodeLabel)
      NavigationBar.clickFlowBtn()
      DiagramUtils.findDiagram()
      DiagramUtils.click('AddNode1')
      PaletteBox.expandCollapseExits().click()
      DiagramUtils.addNodeInDiagram(PaletteBox.subflowEscalateIdSelector, 'AddNode1')
      AddNewOutcome.setLinkLabel('right').clickOnLinkType(' Escalate to HitL (right) ').clickOnAddBtn()
      DiagramUtils.click(dataObject.flow_diagram.closedq1.nodeLabel)
      TabFlowSideBar.openFlow().isSideNode('right', 'right').isSideNodeConditionSel('right', 'escalate')
    })

    it('[CMTS-14590] Exit can be added with Abandonment (left) link type', () => {
      cy.precondition('add-endpoint', `14533`)

      DiagramUtils.findDiagram()
      DiagramUtils.doubleClick(dataObject.pattern_diagram.subflow2.nodeLabel)
      NavigationBar.clickFlowBtn()
      DiagramUtils.findDiagram()
      DiagramUtils.click('AddNode1')
      PaletteBox.expandCollapseExits().click()
      DiagramUtils.addNodeInDiagram(PaletteBox.subflowAbandonmentIdSelecor, 'AddNode1')
      AddNewOutcome.setLinkLabel('left').clickOnLinkType(' Escalate to HitL (right) ')
      AddNewNodePopUp.clickOnAddBtn()
      DiagramUtils.click(dataObject.flow_diagram.closedq1.nodeLabel)
      TabFlowSideBar.openFlow().isSideNode('left', 'left').isSideNodeConditionSel('left', 'abandonment')
    })

    it('[CMTS-14600] All input data in Add Endpoint pop-up are set to default ones after clicking Reset button', () => {
      cy.precondition('add-endpoint', `14533`)

      DiagramUtils.findDiagram()
      DiagramUtils.doubleClick(dataObject.pattern_diagram.subflow2.nodeLabel)
      NavigationBar.clickFlowBtn()
      DiagramUtils.findDiagram()
      DiagramUtils.click('AddNode1')
      PaletteBox.expandCollapseExits().click()
      DiagramUtils.addNodeInDiagram(PaletteBox.subflowDefaultIdSelecor, 'AddNode1')
      AddNewOutcome.setLinkLabel('custom')
        .clickOnLinkType(' Escalate to HitL (right) ')
        .setReasonField(' Reason for escalation')
        .clickOnResetBtn()
      AddNewOutcome.isLinkTypeSet('Normal (down)')
      AddNewOutcome.checkReasonField('Describe the reason.')
    })

    it('[CMTS-14604] Exit can be added to the middle of the node tree', () => {
      cy.precondition('add-endpoint', '14533')

      DiagramUtils.findDiagram()
      DiagramUtils.doubleClick(dataObject.pattern_diagram.subflow2.nodeLabel)
      NavigationBar.clickFlowBtn()
      DiagramUtils.findDiagram()
      PaletteBox.expandCollapseExits().click()
      DiagramUtils.click(dataObject.flow_diagram.prompt1.nodeLabel)
      DiagramUtils.addNodeInDiagram(PaletteBox.subflowAbandonmentIdSelecor, 'Prompt1')
      AddNewOutcome.setLinkLabel('default').isLinkTypeSet(' Normal (down) ').clickOnAddBtn()
      DiagramUtils.click(dataObject.flow_diagram.prompt1.nodeLabel)
      TabFlowSideBar.openFlow().isSideNode('default', 'down').isSideNodeConditionSel('default', 'abandonment')
    })

    it('[CMTS-14606] Exits cannot be added with the same link labels to one parent node', () => {
      cy.precondition('add-endpoint', `14533`)

      DiagramUtils.findDiagram()
      DiagramUtils.doubleClick(dataObject.pattern_diagram.subflow2.nodeLabel)
      NavigationBar.clickFlowBtn()
      DiagramUtils.findDiagram()
      DiagramUtils.click(dataObject.flow_diagram.prompt1.nodeLabel)
      PaletteBox.expandCollapseExits().click()
      DiagramUtils.addNodeInDiagram(PaletteBox.subflowAbandonmentIdSelecor, 'Prompt1')
      AddNewOutcome.setLinkLabel('custom').isLinkTypeSet(' Normal (down) ').clickOnAddBtn()
      DiagramUtils.click(dataObject.flow_diagram.prompt1.nodeLabel)
      TabFlowSideBar.openFlow().isSideNode('custom', 'down').isSideNodeConditionSel('custom', 'abandonment')
      DiagramUtils.findDiagram()
      DiagramUtils.click(dataObject.flow_diagram.prompt1.nodeLabel)
      DiagramUtils.addNodeInDiagram(PaletteBox.subflowAbandonmentIdSelecor, 'Prompt1')
      AddNewOutcome.setLinkLabel('custom')
        .isLinkTypeSet(' Normal (down) ')
        .clickOnAddBtn()
        .checkErrorMessage('Link label is not unique!')
    })

    it('[CMTS-14634] Exit cannot be added without a link label', () => {
      cy.precondition('add-endpoint', `14533`)

      DiagramUtils.findDiagram()
      DiagramUtils.doubleClick(dataObject.pattern_diagram.subflow2.nodeLabel)
      NavigationBar.clickFlowBtn()
      DiagramUtils.findDiagram()
      PaletteBox.expandCollapseExits().click()
      DiagramUtils.click(dataObject.flow_diagram.closedq1.nodeLabel)
      DiagramUtils.addNodeInDiagram(PaletteBox.subflowAbandonmentIdSelecor, 'ClosedQuestion1')
      AddNewOutcome.setLinkLabel('').isLinkTypeSet(' Normal (down) ').clickOnAddBtn()
      AddNewOutcome.checkErrorMessage('Each Link Label must be uniquely named.')
    })

    LinkSubflowCharacter.forEach((input) => {
      it(input.title, () => {
        cy.precondition(input.preconditionId, input.pattern)
        DiagramUtils.findDiagram()
        DiagramUtils.doubleClick(dataObject.pattern_diagram.subflow2.nodeLabel)
        NavigationBar.clickFlowBtn()
        DiagramUtils.findDiagram()
        DiagramUtils.click('AddNode1')
        PaletteBox.expandCollapseExits().click()
        DiagramUtils.addNodeInDiagram(PaletteBox.subflowEscalateIdSelector, 'AddNode1')
        AddNewOutcome.setLinkLabel(input.linkLabel).isLinkTypeSet(input.linkType).clickOnAddBtn()
        DiagramUtils.click(dataObject.flow_diagram.closedq1.nodeLabel)
        TabFlowSideBar.openFlow()
          .isSideNode(input.linkLabel, input.sideText)
          .isSideNodeConditionSel(input.linkLabel, input.conditionText)
      })
    })

    it('[CMTS-14630] Identical exits added to one parent node can be deleted from the node tree', () => {
      cy.precondition('add-endpoint', `14630`)

      DiagramUtils.findDiagram()
      DiagramUtils.doubleClick(dataObject.pattern_diagram.subflow2.nodeLabel)
      NavigationBar.clickFlowBtn()
      DiagramUtils.findDiagram()
      DiagramUtils.click('endpointNode1')
      DiagramBar.clickOnDeleteNodeBtn().clickOnYesBtn()
      DiagramUtils.findDiagram()
      DiagramUtils.click('endpointNode2')
      DiagramBar.clickOnDeleteNodeBtn().clickOnYesBtn()
      DiagramUtils.click(dataObject.flow_diagram.closedq1.nodeLabel)
      DiagramUtils.click(dataObject.flow_diagram.prompt1.nodeLabel)
      DiagramUtils.click(dataObject.flow_diagram.closedq1.nodeLabel)
      TabFlowSideBar.openFlow()
        .isSideNode('Custom', 'down')
        .isSideNodeConditionSel('Custom', 'AddPoint')
        .isSideNode('Agent', 'down')
        .isSideNodeConditionSel('Agent', 'AddPoint')
    })

    it('[CMTS-14639] Exit can be added when link label contains letters and digits', () => {
      cy.precondition('add-endpoint', `14533`)

      DiagramUtils.findDiagram()
      DiagramUtils.doubleClick(dataObject.pattern_diagram.subflow2.nodeLabel)
      NavigationBar.clickFlowBtn()
      DiagramUtils.findDiagram()
      DiagramUtils.click('AddNode1')
      PaletteBox.expandCollapseExits().click()
      DiagramUtils.addNodeInDiagram(PaletteBox.subflowDefaultIdSelecor, 'AddNode1')
      AddNewOutcome.setLinkLabel('A1B2csd3').isLinkTypeSet(' Normal (down) ').clickOnAddBtn()
      DiagramUtils.click(dataObject.flow_diagram.closedq1.nodeLabel)
      TabFlowSideBar.openFlow().isSideNode('A1B2csd3', 'down').isSideNodeConditionSel('A1B2csd3', 'default')
    })

    it('[CMTS-14643] Exit can be added without a reason', () => {
      cy.precondition('add-endpoint', `14533`)

      DiagramUtils.findDiagram()
      DiagramUtils.doubleClick(dataObject.pattern_diagram.subflow2.nodeLabel)
      NavigationBar.clickFlowBtn()
      DiagramUtils.findDiagram()
      DiagramUtils.click('AddNode1')
      PaletteBox.expandCollapseExits().click()
      DiagramUtils.addNodeInDiagram(PaletteBox.subflowAbandonmentIdSelecor, 'AddNode1')
      AddNewOutcome.setLinkLabel('abandonment').isLinkTypeSet(' Normal (down) ').setReasonField('').clickOnAddBtn()
      DiagramUtils.click(dataObject.flow_diagram.closedq1.nodeLabel)
      TabFlowSideBar.openFlow()
        .isSideNode('abandonment', 'down')
        .isSideNodeConditionSel('abandonment', 'abandonment')
        .isSideNodeReasonSel('')
    })

    LabelWithError.forEach((input) => {
      it(input.title, () => {
        cy.precondition(input.preconditionId, input.pattern)

        DiagramUtils.findDiagram()
        DiagramUtils.doubleClick(dataObject.pattern_diagram.subflow2.nodeLabel)
        NavigationBar.clickFlowBtn()
        DiagramUtils.findDiagram()
        DiagramUtils.click('AddNode1')
        PaletteBox.expandCollapseExits().click()
        DiagramUtils.addNodeInDiagram(PaletteBox.subflowAbandonmentIdSelecor, 'AddNode1')
        AddNewOutcome.setLinkLabel(input.linkLabel)
          .isLinkTypeSet(input.linkType)
          .clickOnAddBtn()
          .checkErrorMessage(input.error)
          .checkReasonField('Describe the reason.')
      })
    })

    it('[CMTS-14640] Exit reason can be added via the Add Endpoint pop-up', () => {
      cy.precondition('add-endpoint', `14533`)

      DiagramUtils.findDiagram()
      DiagramUtils.doubleClick(dataObject.pattern_diagram.subflow2.nodeLabel)
      NavigationBar.clickFlowBtn()
      DiagramUtils.findDiagram()
      DiagramUtils.click('AddNode1')
      PaletteBox.expandCollapseExits().click()
      DiagramUtils.addNodeInDiagram(PaletteBox.subflowDefaultIdSelecor, 'AddNode1')
      AddNewOutcome.setLinkLabel('default').isLinkTypeSet(' Normal (down) ').setReasonField('Reason').clickOnAddBtn()
      DiagramUtils.click(dataObject.flow_diagram.closedq1.nodeLabel)
      TabFlowSideBar.openFlow()
        .isSideNode('default', 'down')
        .isSideNodeConditionSel('default', 'default')
        .isSideNodeReasonSel('Reason')
    })

    it('[CMTS-14689] Several exits can be added to one parent node with different link types', () => {
      cy.precondition('add-endpoint', `14533`)

      DiagramUtils.findDiagram()
      DiagramUtils.doubleClick(dataObject.pattern_diagram.subflow2.nodeLabel)
      NavigationBar.clickFlowBtn()
      DiagramUtils.findDiagram()
      DiagramUtils.click('AddNode1')
      DiagramBar.clickOnDeleteNodeBtn().clickOnYesBtn()
      DiagramUtils.findDiagram()
      DiagramUtils.click(dataObject.flow_diagram.closedq1.nodeLabel)
      PaletteBox.expandCollapseExits().click()
      DiagramUtils.addNodeInDiagram(PaletteBox.subflowDefaultIdSelecor, 'ClosedQuestion1')
      AddNewOutcome.setLinkLabel('default').isLinkTypeSet(' Normal (down) ').clickOnAddBtn()
      DiagramUtils.click(dataObject.flow_diagram.closedq1.nodeLabel)
      DiagramUtils.addNodeInDiagram(PaletteBox.subflowEscalateIdSelector, 'ClosedQuestion1')
      AddNewOutcome.setLinkLabel('agent').clickOnLinkType('Escalate to HitL (right)').clickOnAddBtn()
      DiagramUtils.click(dataObject.flow_diagram.closedq1.nodeLabel)
      DiagramUtils.addNodeInDiagram(PaletteBox.subflowAbandonmentIdSelecor, 'ClosedQuestion1')
      AddNewOutcome.setLinkLabel('custom').clickOnLinkType('Abandonment (left)').clickOnAddBtn()
      TabFlowSideBar.openFlow()
        .isSideNode('default', 'down')
        .isSideNode('agent', 'right')
        .isSideNode('custom', 'left')
        .isSideNodeConditionSel('default', 'default')
        .isSideNodeConditionSel('agent', 'escalate')
        .isSideNodeConditionSel('custom', 'abandonment')
    })

    it('[CMTS-14708] Exit color is changed to green when all exits are added to the nodes tree on the Flows tab', () => {
      cy.precondition('add-endpoint', '14533')

      DiagramUtils.findDiagram()
      DiagramUtils.doubleClick(dataObject.pattern_diagram.subflow2.nodeLabel)
      NavigationBar.clickFlowBtn()
      DiagramUtils.findDiagram()
      DiagramUtils.click('AddNode1')
      DiagramBar.clickOnDeleteNodeBtn().clickOnYesBtn()
      DiagramUtils.findDiagram()
      DiagramUtils.click(dataObject.flow_diagram.closedq1.nodeLabel)
      PaletteBox.expandCollapseExits().click()
      DiagramUtils.addNodeInDiagram(PaletteBox.subflowDefaultIdSelecor, 'ClosedQuestion1')
      AddNewOutcome.setLinkLabel('default').isLinkTypeSet(' Normal (down) ').clickOnAddBtn()
      DiagramUtils.click(dataObject.flow_diagram.closedq1.nodeLabel)
      DiagramUtils.addNodeInDiagram(PaletteBox.subflowEscalateIdSelector, 'ClosedQuestion1')
      AddNewOutcome.setLinkLabel('agent').clickOnLinkType('Normal (down)').clickOnAddBtn()
      DiagramUtils.click(dataObject.flow_diagram.closedq1.nodeLabel)
      DiagramUtils.addNodeInDiagram(PaletteBox.subflowAbandonmentIdSelecor, 'ClosedQuestion1')
      AddNewOutcome.setLinkLabel('custom').clickOnLinkType('Normal (down)').clickOnAddBtn()
      DiagramUtils.findDiagram()

      it('[CMTS-14716] The flow added as an exit to the nodes tree on the Flows tab cannot be deleted from the Patterns tab', () => {
        cy.precondition('add-endpoint', '14533')

        DiagramUtils.findDiagram()
        DiagramUtils.doubleClick(dataObject.pattern_diagram.subflow2.nodeLabel)
        DiagramUtils.findDiagram()
        DiagramUtils.click('AddNode1')
        PaletteBox.expandCollapseExits().click()
        DiagramUtils.addNodeInDiagram(PaletteBox.subflowDefaultIdSelecor, 'AddNode1')
        AddNewOutcome.setLinkLabel('custom').clickOnLinkType(' Normal (down) ').clickOnAddBtn()
        DiagramUtils.click(dataObject.flow_diagram.closedq1.nodeLabel)
        TabFlowSideBar.openFlow().isSideNode('custom', 'down').isSideNodeConditionSel('custom', 'default')
        NavigationBar.clickPatternBtn()
        DiagramUtils.findDiagram()
        DiagramUtils.click('outcomeNode1')
        DiagramBar.clickOnDeleteNodeBtn().clickOnYesBtn()
        DiagramUtils.click('AddNode1')
        DiagramBar.clickOnDeleteNodeBtn().clickOnYesBtn()
        NavigationBar.clickFlowBtn().clickPatternBtn()
        DiagramUtils.findDiagram()
        DiagramUtils.click(dataObject.pattern_diagram.subflow2.nodeLabel)
        TabFlowSideBar.openFlow()
      })

      it('[CMTS-14691] Two identical exits can be added to one parent node', () => {
        cy.precondition('add-endpoint', '14533')

        DiagramUtils.findDiagram()
        DiagramUtils.doubleClick(dataObject.pattern_diagram.subflow2.nodeLabel)
        DiagramUtils.findDiagram()
        DiagramUtils.click('AddNode1')
        DiagramBar.clickOnDeleteNodeBtn().clickOnYesBtn()
        DiagramUtils.click(dataObject.flow_diagram.closedq1.nodeLabel)
        PaletteBox.expandCollapseExits().click()
        DiagramUtils.addNodeInDiagram(PaletteBox.subflowDefaultIdSelecor, 'ClosedQuestion1')
        AddNewOutcome.setLinkLabel('default').isLinkTypeSet(' Normal (down) ').clickOnAddBtn()
        DiagramUtils.click(dataObject.flow_diagram.closedq1.nodeLabel)
        PaletteBox.expandCollapseExits().click()
        DiagramUtils.addNodeInDiagram(PaletteBox.subflowDefaultIdSelecor, 'ClosedQuestion1')
        AddNewOutcome.setLinkLabel('custom').isLinkTypeSet(' Normal (down) ').clickOnAddBtn()
        DiagramUtils.click(dataObject.flow_diagram.closedq1.nodeLabel)
        TabFlowSideBar.openFlow()
          .isSideNode('default', 'down')
          .isSideNodeConditionSel('default', 'default')
          .isSideNode('custom', 'down')
          .isSideNodeConditionSel('custom', 'default')
      })

      it('[CMTS-14729] Identical exits can be added to different parent nodes', () => {
        cy.precondition('add-endpoint', '14533')

        DiagramUtils.findDiagram()
        DiagramUtils.doubleClick(dataObject.pattern_diagram.subflow2.nodeLabel)
        DiagramUtils.findDiagram()
        PaletteBox.expandCollapseExits().click()
        DiagramUtils.addNodeInDiagram(PaletteBox.subflowDefaultIdSelecor, 'AddNode1')
        AddNewOutcome.setLinkLabel('default').isLinkTypeSet(' Normal (down) ').clickOnAddBtn()
        DiagramUtils.click(dataObject.flow_diagram.closedq1.nodeLabel)
        TabFlowSideBar.openFlow().isSideNode('default', 'down').isSideNodeConditionSel('default', 'default')
        PaletteBox.expandCollapseExits().click()
        DiagramUtils.addNodeInDiagram(PaletteBox.subflowDefaultIdSelecor, 'Prompt1')
        AddNewOutcome.setLinkLabel('custom').isLinkTypeSet(' Normal (down) ').clickOnAddBtn()
        DiagramUtils.click(dataObject.flow_diagram.prompt1.nodeLabel)
        TabFlowSideBar.openFlow().isSideNode('custom', 'down').isSideNodeConditionSel('custom', 'default')
      })

      it('[CMTS-14767] When the flow used for exit is deleted on the Patterns tab, this exit is also deleted from the exit list on the Flows tab', () => {
        cy.precondition('add-endpoint', '14533')

        NavigationBar.clickFlowBtn()
        FilterBar.selectFlow('SubFlow2')
        LeftSubflowBars.isCheckExitByText('default').isCheckExitByText('escalate').isCheckExitByText('abandonment')
        NavigationBar.clickPatternBtn()
        DiagramUtils.findDiagram()
        DiagramUtils.click(dataObject.pattern_diagram.subflow2.nodeLabel)
        TabFlowSideBar.openFlow().clickOnEditBtn()
        TabFlowSideBar.clickDeleteFlowBtn()
        DiagramUtils.findDiagram()
        DiagramUtils.doubleClick(dataObject.pattern_diagram.subflow2.nodeLabel)
        LeftSubflowBars.isCheckExitByText('escalate').isCheckExitByText('abandonment')
      })

      it('[CMTS-14747] Sub-flow label changed on the Patterns tab is also changed in the list of exits and Flow field on the Flows tab', () => {
        cy.precondition('add-endpoint', '14533')

        NavigationBar.clickFlowBtn()
        FilterBar.checkSelectedFlow('SubFlow1')
        FilterBar.selectFlow('SubFlow2')
        LeftSubflowBars.checkTitleSubflow('SubFlow2 (3) Exits')
        NavigationBar.clickPatternBtn()
        DiagramUtils.findDiagram()
        DiagramUtils.click(dataObject.pattern_diagram.subflow2.nodeLabel)
        TabPropertiesSideBar.setNodeLabel('NewName')
        DiagramUtils.findDiagram()
        DiagramUtils.doubleClick('NewName')
        FilterBar.checkListFlow('SubFlow1')
        FilterBar.checkListFlow('NewName')
        LeftSubflowBars.checkTitleSubflow('NewName (3) Exits')
      })

      it('[CMTS-14731] Default exit is displayed in the list of exits on the Flows tab when sub-flow node is added to the nodes tree on the Patterns tab', () => {
        cy.precondition('add-endpoint', '14731')
        DiagramUtils.findDiagram()
        PaletteBox.expandCollapseExits().click()
        DiagramUtils.addNodeInDiagram(PaletteBox.subflowNodeIdSelector, 'AddNode1')
        AddNewSubFlowNodePopUp.setLabel('SubFlow1')
          .setLink('subflow1')
          .setDescription('This is first SubFlow description')
          .setLinkDescription('This is first SubFlow link description')
          .clickOnAddBtn()
        DiagramUtils.click(dataObject.pattern_diagram.subflow1.nodeLabel)
        TabFlowSideBar.openFlow().isSideNode('default', 'down').isSideNodeConditionSel('default', 'addPoint')
        NavigationBar.clickFlowBtn()
        LeftSubflowBars.checkTitleSubflow('SubFlow1 (1) Exits')
      })

      it("[ CMTS-14094 ] On Pattern Tab ,'All' is displayed at the top center of the tree diagram area when properties panel is opened", () => {
        cy.precondition('empty')
        NavigationBar.clickTopBarButton(DesignerTopBarHrefs.patterns)
        DiagramUtils.findDiagram()
        cy.log('Validate if the properties panel is open')
        NodePropertiesSideBar.checkPropertiesPanelVisibility('be.visible')
        cy.log('Validate the text and header color')
        DiagramUtils.checkTextHeaderColor('All', 255, 255, 255, 0.8)
        DiagramUtils.checkBackgroundHeaderColor(30, 30, 30, 0.3)
      })
    })
  })
})

describe('Header on the Patterns Tab', () => {
  beforeEach(() => {
    cy.fixture('projects-templates/empty-project.json').as('projects')
    cy.get<ProjectInfo>('@projects').then((json: ProjectInfo) => {
      cy.loginAPI()
      cy.createNewProject(json).then((projectInfo: ProjectInfo) => {
        cy.loginUI()
        NavigationBar.selectProject(projectInfo.label)
        cy.restoreLocalStorage()
      })
    })
  })

  afterEach(() => {
    cy.get<ProjectInfo>('@projects').then((json: ProjectInfo) => {
      cy.removeProject(json.id)
    })
  })

  it("[ CMTS-14028 ] On Pattern Tab , 'All' is displayed on the top of the page when no patterns are added to the project", () => {
    NavigationBar.clickTopBarButton(DesignerTopBarHrefs.patterns)
    cy.log('Validate when no pattern is aligned , there should be no pattern selected in the dropdown')
    FilterBar.checkSelectedPattern('NO PATTERN SELECTED')
    cy.log('Validate the header and text color')
    DiagramUtils.checkChannelText('All')
    DiagramUtils.checkBackgroundHeaderColor(30, 30, 30, 0.3)
    cy.log('Validate if the warning message is displayed')
    DiagramUtils.checkAlertMessage('Could not find any selected flow.')
  })

  it("[ CMTS-14094 ] On Pattern Tab ,'All' is displayed at the top center of the tree diagram area when properties panel is closed", () => {
    NavigationBar.clickTopBarButton(DesignerTopBarHrefs.patterns)
    cy.log('Validate the text and header color')
    cy.checkWarning(warnings.flowNotFound)
    DiagramUtils.checkTextHeaderColor('All', 255, 255, 255, 0.8)
    DiagramUtils.checkBackgroundHeaderColor(30, 30, 30, 0.3)
    FilterBar.checkSelectedPattern('NO PATTERN SELECTED')
    // Failing due to issue: https://jira.tools.deloitteinnovation.us/browse/CMTS-14070
    cy.log('Validate when no pattern is present the properties panel is closed')
    NodePropertiesSideBar.checkPropertiesPanelVisibility('be.not.visible')
  })
})
