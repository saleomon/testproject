import { dataObject } from '../../../../fixtures/data/data-spec-projects'
import PaletteBox from '../../../../classes/PaletteBox'
import DiagramUtils from '../../../../support/DiagramUtils'
import AddNewGlobalFallbackOutcomePopUp from '../../../../classes/popups/AddNewGlobalFallbackOutcomePopUp'
import NodePropertiesSideBar from '../../../../classes/bars/NodePropertiesSideBar'
import DiagramBar from '../../../../classes/bars/DiagramBar'
import NavigationBar from '../../../../classes/navigation/NavigationBar'
import { ProjectInfo } from '../../../../types/response/ProjectInfo'
import LinkNodeSideEscalate from '../../../../fixtures/provider/LinkNodeSideEscalate'
import LabelWithCharactersEscalate from '../../../../fixtures/provider/LabelWithCharactersEscalate'
import LabelWithUnderscoreEscalate from '../../../../fixtures/provider/LabelWithUnderscoreEscalate'
import LabelWithErrorEscalate from '../../../../fixtures/provider/LabelWithErrorEscalate'
import LabelWithSymbolEscalate from '../../../../fixtures/provider/LabelWithSymbolEscalate'
import LabelWithReasonEscalate from '../../../../fixtures/provider/LabelWithReasonEscalate'

describe('work with global escalate', {tags: '@exit'}, () => {
  afterEach(() => {
    cy.get<ProjectInfo>('@current-project').then((json: ProjectInfo) => {
      cy.removeProject(json.id)
    })
  })

  LinkNodeSideEscalate.forEach((input) => {
    it(input.title, () => {
      cy.precondition(input.preconditionId)

      DiagramUtils.findDiagram()
      DiagramUtils.click(dataObject.pattern_diagram.subflow1.nodeLabel)
      PaletteBox.expandCollapseExits().click()
      DiagramUtils.addNodeInDiagram(PaletteBox.globalEscalateSelector, dataObject.pattern_diagram.subflow1.nodeLabel)
      AddNewGlobalFallbackOutcomePopUp.setEndpointLink(input.linkLabel).selectLinkType(input.linkType).clickOnAddBtn()
      DiagramUtils.click(dataObject.pattern_diagram.subflow1.nodeLabel)
      NodePropertiesSideBar.openFlow()
        .isSideNode(input.linkLabel, input.sideText)
        .isSideNodeConditionSel(input.linkLabel, input.conditionText)
    })
  })

  it('[CMTS-13891] Global Escalate outcomes cannot be added with the same link labels', () => {
    cy.precondition('13891')

    DiagramUtils.findDiagram()
    DiagramUtils.click(dataObject.pattern_diagram.subflow1.nodeLabel)
    PaletteBox.expandCollapseExits().click()
    DiagramUtils.addNodeInDiagram(PaletteBox.globalEscalateSelector, 'SubFlow1')
    AddNewGlobalFallbackOutcomePopUp.setEndpointLink('Custom').selectLinkType(' Normal (down) ').clickOnAddBtn()
    DiagramUtils.click(dataObject.pattern_diagram.subflow1.nodeLabel)
    NodePropertiesSideBar.openFlow().isSideNode('Custom', 'down')
    DiagramUtils.click(dataObject.pattern_diagram.subflow1.nodeLabel)
    DiagramUtils.addNodeInDiagram(PaletteBox.globalEscalateSelector, 'SubFlow1')
    AddNewGlobalFallbackOutcomePopUp.setEndpointLink('Custom')
      .selectLinkType(' Normal (down) ')
      .clickOnAddBtn()
      .checkErrorMessage('Link label is not unique!')
  })

  it('[CMTS-13879] Global Escalate outcomes added to one parent node can be deleted from the node tree', () => {
    cy.precondition('13879')

    DiagramUtils.findDiagram()
    DiagramUtils.click('outcomeNode1')
    DiagramBar.clickOnDeleteNodeBtn().clickOnYesBtn()
    DiagramUtils.click('outcomeNode2')
    DiagramBar.clickOnDeleteNodeBtn().clickOnYesBtn()
    NavigationBar.clickFlowBtn().clickPatternBtn()
    DiagramUtils.findDiagram()
    DiagramUtils.click(dataObject.pattern_diagram.subflow1.nodeLabel)
    NodePropertiesSideBar.openFlow()
      .isSideNode('Flow10', 'true')
      .isSideNodeConditionSel('Flow10', 'addPoint')
      .isSideNode('Flow11', 'true')
      .isSideNodeConditionSel('Flow11', 'addPoint')
  })

  LabelWithCharactersEscalate.forEach((input) => {
    it(input.title, () => {
      cy.precondition(input.preconditionId)

      DiagramUtils.findDiagram()
      PaletteBox.expandCollapseExits().click()
      DiagramUtils.addNodeInDiagram(PaletteBox.globalEscalateSelector, 'AddNode1')
      AddNewGlobalFallbackOutcomePopUp.setEndpointLink(input.linkLabel).selectLinkType(input.linkType).clickOnAddBtn()
      DiagramUtils.click(dataObject.pattern_diagram.subflow1.nodeLabel)
      NodePropertiesSideBar.openFlow()
        .isSideNode(input.linkLabel, input.sideText)
        .isSideNodeConditionSel(input.linkLabel, input.conditionText)
    })
  })

  LabelWithReasonEscalate.forEach((input) => {
    it(input.title, () => {
      cy.precondition(input.preconditionId)

      DiagramUtils.findDiagram()
      PaletteBox.expandCollapseExits().click()
      DiagramUtils.addNodeInDiagram(PaletteBox.globalEscalateSelector, 'AddNode1')
      AddNewGlobalFallbackOutcomePopUp.setEndpointLink(input.linkLabel)
        .selectLinkType(input.linkType)
        .setReason(input.reason)
        .clickOnAddBtn()

      DiagramUtils.click(dataObject.pattern_diagram.subflow1.nodeLabel)
      NodePropertiesSideBar.openFlow()
        .isSideNode(input.linkLabel, input.sideText)
        .isSideNodeConditionSel(input.linkLabel, input.conditionText)
        .isSideNodeReasonSel(input.reason)
    })
  })

  it('[CMTS-13941] Global Escalate outcomes with different link types are attached to one parent node according to the selected link type', () => {
    cy.precondition('13941')

    DiagramUtils.findDiagram()
    PaletteBox.expandCollapseExits().click()
    DiagramUtils.addNodeInDiagram(PaletteBox.globalEscalateSelector, 'AddNode1')
    AddNewGlobalFallbackOutcomePopUp.setEndpointLink('down').clickOnAddBtn()
    DiagramUtils.click(dataObject.pattern_diagram.subflow1.nodeLabel)
    NodePropertiesSideBar.openFlow().isSideNode('down', 'down')
  })

  LabelWithUnderscoreEscalate.forEach((input) => {
    it(input.title, () => {
      cy.precondition(input.preconditionId)

      DiagramUtils.findDiagram()
      PaletteBox.expandCollapseExits().click()
      DiagramUtils.addNodeInDiagram(PaletteBox.globalEscalateSelector, 'AddNode1')
      AddNewGlobalFallbackOutcomePopUp.setEndpointLink(input.linkLabel).selectLinkType(input.linkType).clickOnAddBtn()
    })
  })

  LabelWithErrorEscalate.forEach((input) => {
    it(input.title, () => {
      cy.precondition(input.preconditionId)

      DiagramUtils.findDiagram()
      PaletteBox.expandCollapseExits().click()
      DiagramUtils.addNodeInDiagram(PaletteBox.globalEscalateSelector, 'AddNode1')
      AddNewGlobalFallbackOutcomePopUp.setEndpointLink(input.linkLabel)
        .selectLinkType(input.linkType)
        .clickOnAddBtn()
        .checkErrorMessage(input.error)
    })
  })

  LabelWithSymbolEscalate.forEach((input) => {
    it(input.title, () => {
      cy.precondition(input.preconditionId)

      DiagramUtils.findDiagram()
      PaletteBox.expandCollapseExits().click()
      DiagramUtils.addNodeInDiagram(PaletteBox.globalEscalateSelector, 'AddNode1')
      AddNewGlobalFallbackOutcomePopUp.setEndpointLink(input.linkLabel).selectLinkType(input.linkType).clickOnAddBtn()
    })
  })
})
