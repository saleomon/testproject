import FilterBar from '../../../classes/bars/FilterBar'
import NavigationBar from '../../../classes/navigation/NavigationBar'
import { ProjectInfo } from '../../../types/response/ProjectInfo'
import DiagramUtils from '../../../support/DiagramUtils'

describe('Save Layout test', () => {
  before(() => {
    cy.fixture('projects-templates/reattach-spec-project.json').as('projects')

    cy.get<ProjectInfo>('@projects').then((json: ProjectInfo) => {
      cy.loginAPI()
      cy.createNewProject(json).then((projectInfo: ProjectInfo) => {
        Cypress.env('projectId', projectInfo.id)
        cy.loginUI()
        NavigationBar.selectProject(projectInfo.label)
        NavigationBar.openPatternsPage()
        FilterBar.selectPattern('reattach')
        DiagramUtils.findDiagram()
      })
    })
  })

  it('Reattach nodes', { retries: { runMode: 3, openMode: 3 } }, () => {
    cy.linkNodes('Start', 'subflowNode')
    cy.linkNodes('subflowNode', 'skillNode')
    cy.linkNodes('skillNode', 'tagNode')
    cy.linkNodes('tagNode', 'subflowNode')

    // assertions
    cy.findNodesOutOf('Start').then(($array) => {
      expect($array.length).equals(1)
      expect($array[0].key).equals('subflowNode')
    })
    cy.findNodesOutOf('subflowNode').then(($array) => {
      expect($array.length).equals(1)
      expect($array[0].key).equals('skillNode')
    })
    cy.findNodesOutOf('skillNode').then(($array) => {
      expect($array.length).equals(1)
      expect($array[0].key).equals('tagNode')
    })
    cy.findNodesOutOf('tagNode').then(($array) => {
      expect($array.length).equals(1)
      expect($array[0].type).equals('gotoLink')
      expect($array[0].nodeLabel).equals('subflowNode')
    })
  })

  afterEach(() => {
    cy.removeProject(Cypress.env('projectId'))
  })
})
