import { dataObject } from '../../../fixtures/data/data-spec-node-details'
import FilterBar from '../../../classes/bars/FilterBar'
import DiagramToolbar from '../../../classes/DiagramToolbar'
import NodeDetailsPanel from '../../../classes/NodeDetailsPanel'
import { DesignerTopBarHrefs } from '../../../fixtures/navigation-data/designer-top-bar-hrefs'
import NavigationBar from '../../../classes/navigation/NavigationBar'
import { ProjectInfo } from '../../../types/response/ProjectInfo'

describe('Save Layout test', () => {
  beforeEach(() => {
    cy.fixture('projects-templates/save-layout-spec-project.json').as('projects')

    cy.get<ProjectInfo>('@projects').then((json: ProjectInfo) => {
      cy.loginAPI()
      cy.createNewProject(json).then((projectInfo: ProjectInfo) => {
        Cypress.env('projectId', projectInfo.id)
        cy.loginUI()
        NavigationBar.selectProject(projectInfo.label)
        NavigationBar.openPatternsPage()
        FilterBar.selectPattern('general')
      })

    })
  })

  afterEach(() => {
    cy.removeProject(Cypress.env('projectId'))
  })

  it('Save custom layout', () => {
    let tools = new DiagramToolbar()
    let nodeDetails = new NodeDetailsPanel()
    let subflowLoc
    let skillLoc
    let tagLoc

    cy.moveNode(dataObject.pattern_diagram.tag.nodeLabel, -250, 0)
    cy.moveNode(dataObject.pattern_diagram.skillsrouter.nodeLabel, +250, 0)
    cy.moveNode(dataObject.pattern_diagram.subflow.nodeLabel, -125, 0)

    tools
      .saveLayoutButton()
      .click()
      .then(() => {
        cy.get('.ant-message-notice').find('span').contains('Layout saved successfully.').should('be.visible') // toast message visible

        // get coordinates before leaving
        cy.getNodeLocation(dataObject.pattern_diagram.subflow.nodeLabel).then(($loc) => {
          subflowLoc = $loc
        })
        cy.getNodeLocation(dataObject.pattern_diagram.skillsrouter.nodeLabel).then(($loc) => {
          skillLoc = $loc
        })
        cy.getNodeLocation(dataObject.pattern_diagram.tag.nodeLabel).then(($loc) => {
          tagLoc = $loc
        })

        // reload
        NavigationBar.clickTopBarButton(DesignerTopBarHrefs.outcomes)
        NavigationBar.clickTopBarButton(DesignerTopBarHrefs.patterns)
        cy.window().should('have.property', 'go')
        if (nodeDetails.element) {
          nodeDetails.close()
        }
        // assert coordinates
        cy.getNodeLocation(dataObject.pattern_diagram.subflow.nodeLabel).then(($loc) => {
          expect($loc.x).equals(subflowLoc.x)
        })
        cy.getNodeLocation(dataObject.pattern_diagram.skillsrouter.nodeLabel).then(($loc) => {
          expect($loc.x).equals(skillLoc.x)
        })
        cy.getNodeLocation(dataObject.pattern_diagram.tag.nodeLabel).then(($loc) => {
          expect($loc.x).equals(tagLoc.x)
        })
      })
  })

  it('Revert layout test', () => {
    let tools = new DiagramToolbar()
    let nodeDetails = new NodeDetailsPanel()
    let subflowLoc
    let skillLoc
    let tagLoc
    tools.revertToAutoLayoutButton().click()
    cy.moveNode(dataObject.pattern_diagram.tag.nodeLabel, -250, 0)
    cy.moveNode(dataObject.pattern_diagram.skillsrouter.nodeLabel, +250, 0)
    cy.moveNode(dataObject.pattern_diagram.subflow.nodeLabel, -125, 0)

    tools
      .saveLayoutButton()
      .click()
      .then(() => {
        cy.get('.ant-message-notice').find('span').contains('Layout saved successfully.').should('be.visible') // toast message visible
        cy.wait(500)
        // get coordinates before leaving
        cy.getNodeLocation(dataObject.pattern_diagram.subflow.nodeLabel).then(($loc) => {
          subflowLoc = $loc
        })
        cy.getNodeLocation(dataObject.pattern_diagram.skillsrouter.nodeLabel).then(($loc) => {
          skillLoc = $loc
        })
        cy.getNodeLocation(dataObject.pattern_diagram.tag.nodeLabel).then(($loc) => {
          tagLoc = $loc
        })
      })

    // reload
    NavigationBar.clickTopBarButton(DesignerTopBarHrefs.outcomes)
    NavigationBar.clickTopBarButton(DesignerTopBarHrefs.patterns)
    cy.window().should('have.property', 'go')
    if (nodeDetails.element) {
      nodeDetails.close()
    }

    tools
      .revertToAutoLayoutButton()
      .click()
      .then(() => {
        // assert coordinates
        cy.getNodeLocation(dataObject.pattern_diagram.subflow.nodeLabel).then(($loc) => {
          expect($loc.x).not.equal(subflowLoc.x)
        })
        cy.getNodeLocation(dataObject.pattern_diagram.skillsrouter.nodeLabel).then(($loc) => {
          expect($loc.x).not.equal(skillLoc.x)
        })
        cy.getNodeLocation(dataObject.pattern_diagram.tag.nodeLabel).then(($loc) => {
          expect($loc.x).not.equal(tagLoc.x)
        })
      })
  })
})
