import { entityEditorObj } from '../../../classes/entityEditorObject'
import { contentsEntityEditor } from '../../../fixtures/data/entityEditorData'
import NavigationBar from '../../../classes/navigation/NavigationBar'
import FilterBar from '../../../classes/bars/FilterBar'
import AddEntityPopOver from '../../../classes/popovers/AddEntityPopOver'
import AddEntityGroupPopOver from '../../../classes/popovers/AddEntityGroupPopOver'
import entityTable from '../../../classes/entityTable'
import DiagramBar from '../../../classes/bars/DiagramBar'

let createdProjectInfo = {}

describe('Enitity Editor', () => {
  beforeEach(() => {
    cy.getToken()
    cy.fixture('entity-templates/sample-entity.json').then(($json) => {
      cy.createProjectNew($json).then(($projectInfo) => {
        cy.loginUI()
        NavigationBar.selectProject($projectInfo.label)
        NavigationBar.openPatternsPage()
        FilterBar.selectPattern('TestPattern EntityEditor')
        createdProjectInfo = $projectInfo
        Cypress.env('projectNameForEntity', $projectInfo.label)
      })
      DiagramBar.clickOnEntitiesViewerBtn()
    })
  })

  afterEach(() => {
    // clean up project
    cy.deleteProject(Cypress.env('baseUrlAPI'), Cypress.env('username'), Cypress.env('password'), createdProjectInfo.id)
    cy.window().then(($win) => {
      $win.localStorage.clear()
    })
  })

  describe('entity', () => {
    it('CMTS-40-add-new-entity', { tags: '@smoke' }, () => {
      entityTable.chooseTab('Entity') // open tab Entity
      entityTable.openAddEntityPopOver() // open modal Add New Entity

      // check correctness format data for chosen type data
      entityEditorObj.checkFormatData('Date', contentsEntityEditor.dateFormatArray)
      entityEditorObj.checkFormatData('String', contentsEntityEditor.stringFormatArray)
      entityEditorObj.checkFormatData('Number', contentsEntityEditor.numberFormatArray)

      AddEntityPopOver.fillEntity(contentsEntityEditor.entity, true, true) // fill all fields on modal Add New Entity
      AddEntityPopOver.clickOnAddBtn()
      entityTable.closePopOver() // close modal Add New Entity
      cy.checkToastMessage('Saved New Entity')

      // entityTable.checkTableAG(contentsEntityEditor.entityRow) - AG Grid component allows to check all table - not particular row
      entityTable.checkEntityRowData(contentsEntityEditor.entityRow)
      entityTable.chooseTab('Entity Group') // open tab Entity
      entityTable.checkEntityPresence('defaultEntityGroup') // check if new entity group is added
      entityTable.getEditEntityGroupBtn('defaultEntityGroup').click() // open Edit Entity Group modal

      // check if newly added entity is present in selects Primary Key and Depends On for Enity Group
      cy.selectCustom(AddEntityGroupPopOver.primaryKey, contentsEntityEditor.entity.entityLabel)
      cy.selectCustom(AddEntityGroupPopOver.dependsOn, contentsEntityEditor.entity.entityLabel)
    })
    it('CMTS-41-edit-entity', { tags: '@smoke' }, () => {
      entityTable.chooseTab('Entity') // open tab Entity
      entityTable.getEditEntityBtn('EntityToEdit').click() // open modal for editing entity
      AddEntityPopOver.fillEntity(contentsEntityEditor.entityUpdated) // fill entity
      AddEntityPopOver.clickOnSaveBtn()

      cy.checkToastMessage('Saved Entity') // check toast message
      entityTable.checkEntityRowData(contentsEntityEditor.entityRowUpdated)
    })

    it('CMTS-42-delete-entity', { tags: '@smoke' }, () => {
      entityTable.chooseTab('Entity') // open tab Entity
      entityTable.getDeleteEntityButton('EntityToDelete').click()
      entityTable.confirmDeletingEntity()
      cy.checkToastMessage('Deleted Entity')
      entityTable.checkEntityAbsence('EntityToDelete') // check if entity is deleted
    })
  })

  describe('entity group', () => {
    it('CMTS-43-add-new-entity-group', { tags: '@smoke' }, () => {
      entityTable.chooseTab('Entity Group') // open tab Entity Group
      cy.get('.labelDiv').contains('Group Editor').should('be.visible')
      entityTable.openAddEntityPopOver()
      AddEntityGroupPopOver.fillEnitityGroup(contentsEntityEditor.entityGroupNew, true)
      AddEntityGroupPopOver.clickOnAddBtn()
      cy.checkToastMessage('Saved New Entity Group')

      entityTable.closePopOver() // close modal
      entityTable.checkEntityGroupRowData(contentsEntityEditor.entityGroupRow)

      entityTable.chooseTab('Entity') // open tab Entity
      entityTable.getEditEntityBtn(contentsEntityEditor.entityGroupNew.entityGroupPrimaryKey).click()
      cy.selectCustom(AddEntityPopOver.entityGroup, contentsEntityEditor.entityGroupLabel) // check if new entity group appears in select Group for entity
    })
    it('CMTS-44-edit-entity-group', { tags: '@smoke' }, () => {
      entityTable.chooseTab('Entity Group') // open tab Entity Group
      entityTable.closePopOver() // close modal
      entityTable.openEditEnityGroupPopOver('EntityGroupToEdit')
      AddEntityGroupPopOver.fillEnitityGroup(contentsEntityEditor.entityGroupUpdated, false)
      AddEntityGroupPopOver.clickOnSaveBtn()
      cy.checkToastMessage('Saved Entity Group')
      entityTable.closePopOver()

      // check table after editing entity group
      entityTable.checkEntityGroupRowData(contentsEntityEditor.entityGroupRowUpdated)
    })

    it('CMTS-45-delete-entity-group', { tags: '@smoke' }, () => {
      entityTable.chooseTab('Entity Group') // open tab Entity Group
      entityTable.getDeleteEntityGroupButton('EntityGroupToDelete').click()
      entityTable.confirmDeletingEntityGroup()
      cy.checkToastMessage('Deleted Entity Group')
      entityTable.checkEntityAbsence('EntityGroupToDelete')
    })
  })

  describe('validation', { tags: ['@validation'] }, () => {
    it('CMTS-9505-test-pack-for-entity-validations', { tags: ['@validation'] }, () => {
      entityTable.chooseTab('Entity') // open tab Entity
      entityTable.openAddEntityPopOver() // open modal Add New Entity
      AddEntityPopOver.checkValidationError(
        entityEditorObj.entityLabel,
        entityEditorObj.lengthLimitErrMsg,
        entityEditorObj.entityLabelField,
        'a',
        true,
      )
      AddEntityPopOver.checkValidationError(
        entityEditorObj.entityLabel,
        entityEditorObj.lengthLimitErrMsg,
        entityEditorObj.entityLabelField,
        'ab',
        false,
      )
      AddEntityPopOver.checkValidationError(
        entityEditorObj.entityLabel,
        entityEditorObj.lengthLimitErrMsg,
        entityEditorObj.entityLabelField,
        'asssdddddddddddddddddddddddddddda',
        true,
      )
      AddEntityPopOver.checkValidationError(
        entityEditorObj.entityLabel,
        entityEditorObj.containSpacesErrMsg,
        entityEditorObj.entityLabelField,
        '  ',
        true,
      )
      AddEntityPopOver.checkValidationError(
        entityEditorObj.entityLabel,
        entityEditorObj.notUniqueErrMsg,
        entityEditorObj.entityLabelField,
        'entityValidation',
        true,
      )
      AddEntityPopOver.checkValidationError(
        entityEditorObj.entityLabel,
        entityEditorObj.emptyFieldErrMsg,
        entityEditorObj.entityLabelField,
        '',
        true,
      )
    })

    it('CMTS-9506-test-pack-for-entity-group-validations', { tags: ['@validation'] }, () => {
      entityTable.chooseTab('Entity Group') // open tab Entity Group
      entityTable.openAddEntityPopOver() // open modal Add New Entity Group
      AddEntityPopOver.checkValidationError(
        entityEditorObj.entityLabel,
        entityEditorObj.lengthLimitErrMsg,
        entityEditorObj.entityLabelField,
        'a',
        true,
      )
      AddEntityPopOver.checkValidationError(
        entityEditorObj.entityLabel,
        entityEditorObj.lengthLimitErrMsg,
        entityEditorObj.entityLabelField,
        'ab',
        false,
      )
      AddEntityPopOver.checkValidationError(
        entityEditorObj.entityLabel,
        entityEditorObj.lengthLimitErrMsg,
        entityEditorObj.entityLabelField,
        'asssdddddddddddddddddddddddddddda',
        true,
      )
      AddEntityPopOver.checkValidationError(
        entityEditorObj.entityLabel,
        entityEditorObj.containSpacesErrMsg,
        entityEditorObj.entityLabelField,
        '  ',
        true,
      )
      AddEntityPopOver.checkValidationError(
        entityEditorObj.entityLabel,
        entityEditorObj.notUniqueErrMsg,
        entityEditorObj.entityLabelField,
        'defaultEntityGroup',
        true,
      )
      AddEntityPopOver.checkValidationError(
        entityEditorObj.entityLabel,
        entityEditorObj.emptyFieldErrMsg,
        entityEditorObj.entityLabelField,
        '',
        true,
      )
    })
  })
})
