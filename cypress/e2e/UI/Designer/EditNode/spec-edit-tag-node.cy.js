import FilterBar from '../../../../classes/bars/FilterBar.ts'
import NodeDetailsPanel from '../../../../classes/NodeDetailsPanel'
import NavigationBar from '../../../../classes/navigation/NavigationBar.ts'
import DiagramUtils from '../../../../support/DiagramUtils.ts'

let createdProjectInfo = {}
let detailsPanel = new NodeDetailsPanel()
let nodeName = 'tag'

before(function () {
  cy.fixture('projects-templates/edit-node-project.json').then(($json) => {
    cy.createProject(Cypress.env('baseUrlAPI'), Cypress.env('username'), Cypress.env('password'), $json).then(
      ($projectInfo) => {
        cy.UILogin(Cypress.env('username'), Cypress.env('password'))
        NavigationBar.selectProject($projectInfo.label)
        cy.get('div').contains($projectInfo.label).should('be.visible')
        NavigationBar.openPatternsPage()
        FilterBar.selectPattern('tag')
        createdProjectInfo = $projectInfo
      },
    )
  })
})

beforeEach(function () {
  // restore localstorage
  cy.restoreLocalStorage()
})

describe.skip('Edit tag node using Details panel', () => {
  describe('Properties Tab', () => {
    it('Edit Description', () => {
      let assertionText = 'edited description'
      DiagramUtils.findDiagram()
      DiagramUtils.click(nodeName)
      detailsPanel.selectTab('Properties')
      cy.get('[placeholder="Enter Node Description"]')
        .parent()
        .find('.contentEditButton')
        .click()
        .then(() => {
          cy.get('div').contains('Node Description').find('textarea').click().clear().type(assertionText)
          cy.get('div').contains('Node Description').find('.editControls .saveItEditButton').click()
          detailsPanel.nodeDescription().then(($textarea) => {
            expect($textarea.val()).contains(assertionText)
          })
        })
      cy.findNodeForKey(nodeName).then(($node) => {
        expect($node.data.description).equals(assertionText)
      })
    })
  })

  describe('Flows Tab', () => {
    let flowName = 'named'
    let flowNewName = 'renamed'
    let flowType = 'Normal'
    let flowDescription = 'flow descr'
    let flowNewDescription = 'new flow descr'

    it('Flow - Add - (positive)', () => {
      DiagramUtils.findDiagram()
      DiagramUtils.click(nodeName)
      detailsPanel.selectTab('Flow')
      cy.intercept('PUT', 'api/patterns/*/*').as('addFlow')
      detailsPanel.addFlow(flowName, flowType, flowDescription)
      cy.wait('@addFlow')
        .its('response')
        .then((response) => {
          expect(response).to.have.property('statusCode', 201)
        })
      cy.get('.itemHeader').should('contain', flowName)
      // cy.get('.itemConditionType').should('contain', flowType) //disabled due to CMTS-9178
      cy.get('.descriptionSpan').should('contain', flowDescription)
      // assertions on diagram object
      cy.findLinksOutOf(nodeName).then(($links) => {
        expect($links.length).equals(2)
        expect($links[1].text).equals(flowName)
      })
    })

    it('Flow - Edit - CMTS-9236', () => {
      detailsPanel.selectTab('Flow')
      cy.get('.itemHeader').contains(flowName).find('button').click()
      cy.get('.ant-dropdown-menu-item').contains('Edit Properties').click()
      cy.intercept('PUT', 'api/patterns/*/*').as('editFlow')
      cy.get('.ant-popover').contains('Edit Existing Flow Condition').should('be.visible')
      cy.get('.ant-popover-placement-left').find('input').clear().type(flowNewName)

      cy.get('.ant-popover-placement-left').find('textarea').clear().type(flowNewDescription)
      cy.get('.ant-popover-placement-left').find('.ant-btn-primary').click()
      cy.wait('@editFlow')
        .its('response')
        .then((response) => {
          expect(response).to.have.property('statusCode', 201)
        })

      // aseertion on front
      cy.get('.itemHeader').should('contain', flowNewName)
      cy.get('.itemHeader').should('contain', flowNewDescription)
      // assertions on diagram object
      cy.findLinksOutOf(nodeName).then(($links) => {
        expect($links.length).equals(2)
        expect($links[1].text).equals(flowNewName)
      })
    })

    it('Flow - Delete created flow - (CMTS-9243)', () => {
      cy.intercept('PUT', 'api/patterns/*/*').as('deleteFlow')
      cy.get('.itemHeader').contains(flowName).find('button').click()
      cy.get('.ant-dropdown-menu-item').contains('Delete Flow').click()
      cy.wait('@deleteFlow')
        .its('response')
        .then((response) => {
          expect(response).to.have.property('statusCode', 201)
        })
      // assertions on front
      cy.get('.itemHeader').should('not.contain', flowNewName)
      cy.get('.itemHeader').should('not.contain', flowNewDescription)
      // assertions on diagram
      cy.findLinksOutOf(nodeName).then(($links) => {
        expect($links.length).equals(1)
        expect($links[0].text).equals('default')
      })
    })

    it('Flow - Delete default flow (CMTS-9244)', () => {
      cy.intercept('PUT', 'api/patterns/*/*').as('deleteFlow')
      cy.get('.itemHeader').contains('default').find('button').click()
      cy.get('.ant-dropdown-menu-item').contains('Delete Flow').click()
      cy.wait('@deleteFlow')
        .its('response')
        .then((response) => {
          expect(response).to.have.property('statusCode', 201)
        })
      // assertions on front
      cy.get('.itemHeader').should('not.contain', 'default')
      // assertions on diagram
      cy.findLinksOutOf(nodeName).then(($links) => {
        expect($links.length).equals(0)
      })
    })

    it('Flow - Input validation (CMTS-9227, CMTS-9228, CMTS-9229, CMTS-9230, CMTS-9231, CMTS-9232, CMTS-9233)',
      { tags: '@validation' }, () => {
        detailsPanel.selectTab('Flow')
        detailsPanel.addFlowButton().click() // to show popover

        detailsPanel.flowNameField().type('a') // one characher
        detailsPanel.flowNameField().blur() // blur triggers validation
        detailsPanel.flowValidationMessage().should('contain', 'Length be 2 to 32 chars')
        detailsPanel.flowNameField().clear()

        detailsPanel.flowNameField().type('morethan32morethan32morethan32mor') // 33 charactes
        detailsPanel.flowNameField().blur() // blur triggers validation
        detailsPanel.flowValidationMessage().should('contain', 'Length be 2 to 32 chars')
        detailsPanel.flowNameField().clear()

        detailsPanel.flowNameField().type('space in it') // space inside label
        detailsPanel.flowNameField().blur() // blur triggers validation
        detailsPanel.flowValidationMessage().should('contain', 'Label must not contain spaces or underscores.')
        detailsPanel.flowNameField().clear()

        detailsPanel.flowNameField().type('  ') // two spaces only
        detailsPanel.flowNameField().blur() // blur triggers validation
        detailsPanel.flowValidationMessage().should('contain', 'Label must not contain spaces or underscores.')
        detailsPanel.flowNameField().clear()

        detailsPanel.flowNameField().type('underscore_label') // underscore in label
        detailsPanel.flowNameField().blur() // blur triggers validation
        detailsPanel.flowValidationMessage().should('contain', 'Label must not contain spaces or underscores.')
        detailsPanel.flowNameField().clear()

        detailsPanel.flowNameField().type('aa') // two charachers
        detailsPanel.flowNameField().blur() // blur triggers validation
        detailsPanel.flowValidationMessage().should('not.exist')
        detailsPanel.flowNameField().clear()

        detailsPanel.flowNameField().type('32chars32chars32chars32chars32ch') // 32 charachers
        detailsPanel.flowNameField().blur() // blur triggers validation
        detailsPanel.flowValidationMessage().should('not.exist')
        detailsPanel.flowNameField().clear()
        detailsPanel.addFlowButton().click() // to hide popover
      },
    )

    it('Flow - Reset validation (CMTS-9237)', () => {
      detailsPanel.selectTab('Flow')
      detailsPanel.fillFlowForm('reset', 'Normal', 'reset')
      cy.get('.ant-popover').contains('Add New Flow Condition').next().find('span').contains('Reset').click()
      detailsPanel.flowNameField().should('contain', '')
      cy.get('.ant-popover')
        .contains('Add New Flow Condition')
        .next()
        .find('[role="combobox"]')
        .then(($combobox) => {
          cy.wrap($combobox)
            .attribute('aria-controls')
            .then(($id) => {
              cy.get('#' + $id)
                .find('ul>li')
                .not('[aria-selected="true"]')
                .should('have.length', 7)
            })
        })
      cy.get('.ant-popover').contains('Add New Flow Condition').next().find('textarea').should('contain', '')
    })
  })
})

afterEach(function () {
  // preserve local storage after each hook
  cy.saveLocalStorage()
})

after(function () {
  cy.deleteProject(Cypress.env('baseUrlAPI'), Cypress.env('username'), Cypress.env('password'), createdProjectInfo.id)
  cy.window().then(($win) => {
    $win.localStorage.clear()
  })
})
