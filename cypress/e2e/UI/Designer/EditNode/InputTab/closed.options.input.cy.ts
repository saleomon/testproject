import { ProjectInfo } from '../../../../../types/response/ProjectInfo'
import DiagramUtils from '../../../../../support/DiagramUtils'
import NodeDetails from '../../../../../classes/NodeDetails'
import NavigationBar from '../../../../../classes/navigation/NavigationBar'
import { DesignerTopBarHrefs } from '../../../../../fixtures/navigation-data/designer-top-bar-hrefs'

const addClosedInputForm: string = 'Add Closed Input'
const editInput: string = 'Edit Input'
const grammarSetData: object = {
  fieldName: 'Grammar Set',
  fieldType: 'textarea',
  value: 'Yes, Yeah, Ya',
}
const sectionLabelData: object = {
  fieldName: 'Selection Label',
  fieldType: 'input',
  value: 'Login',
}

const descriptionData: object = {
  fieldName: 'Description',
  fieldType: 'textarea',
  value: 'Describe this for developers',
}

describe('[CMTS-10943] Open QuestionNode - Properties - Input tab - Closed input', () => {
  beforeEach(() => {
    cy.restoreLocalStorage()

    cy.fixture('projects-templates/edit-node-project.json').as('projects')

    cy.get<ProjectInfo>('@projects').then((json: ProjectInfo) => {
      cy.loginAPI()
      cy.createNewProject(json).then((projectInfo: ProjectInfo) => {
        cy.loginUI()
        cy.selectProjectAndPattern(projectInfo.label, 'flowNodes')
        NavigationBar.clickTopBarButton(DesignerTopBarHrefs.flows)
        DiagramUtils.findDiagram()
        DiagramUtils.doubleClick('openq')
        NodeDetails.selectTabByName('Input')
      })
    })
  })

  afterEach(() => {
    cy.get<ProjectInfo>('@projects').then((json: ProjectInfo) => {
      cy.removeProject(json.id)
    })
  })

  it('[CMTS-12231] Check whether closed input parser can be created', { tags: '@smoke' }, () => {
    NodeDetails.selectInputParserOption('Closed Options')
    NodeDetails.clickAddOptionButton('Closed Options')
      .fillAddClosedInputField(addClosedInputForm, sectionLabelData)
      .fillAddClosedInputField(addClosedInputForm, grammarSetData)
      .fillAddClosedInputField(addClosedInputForm, descriptionData)
      .clickFormButtonByText('Add')
    NodeDetails.checkCreatedOption('Login ', ' Yes, Yeah, Ya ', ' Describe this for developers ')
    NodeDetails.checkInputParsersCount('1')
  })

  it('[CMTS-12264] Check whether the checkbox can be checked while creating closed input parser', () => {
    NodeDetails.selectInputParserOption('Closed Options')
    NodeDetails.addClosedOption('Login', ' Yes, Yeah, Ya ', ' Describe this for developers ')
    NodeDetails.clickApplyAllToFlowCheckbox()
    // @ts-ignore
    // eslint-disable-next-line @typescript-eslint/no-unsafe-call
    cy.findNodeForKey('openq').then(($node) => {
      expect($node.data.inputCount).equals(1)
      // expect($node.data.flowCount).equals(3)
    })
    // @ts-ignore
    // eslint-disable-next-line @typescript-eslint/no-unsafe-call
    // commented because found array of 2 elements. Login is third element array[2]
    /*  cy.findLinksOutOf('openq').then(($links) => { 
      expect($links[2].text).to.equals('Login') 
    }) */
    NodeDetails.selectTabByName('Flow')
    NodeDetails.checkEditNodeModalItemState('Login ', 'exist')
  })

  it('[CMTS-12266] Check whether Selection Label and Description fields can be edited', { tags: '@smoke' }, () => {
    const editSectionLabelData: object = {
      fieldName: 'Selection Label',
      fieldType: 'input',
      value: 'One',
    }

    const editDescriptionData: object = {
      fieldName: 'Description',
      fieldType: 'textarea',
      value: ' ',
    }
    NodeDetails.selectInputParserOption('Closed Options')
    NodeDetails.addClosedOption('Login', ' Yes, Yeah, Ya ', ' Describe this for developers ')
    NodeDetails.clickMoreIcon()
      .selectMenuItemByName('Edit Properties')
      .fillAddClosedInputField(editInput, editSectionLabelData)
      .fillAddClosedInputField(editInput, editDescriptionData)
      .clickFormButtonByText('Save')
    NodeDetails.checkCreatedOption('One ', '  Yes, Yeah, Ya  ', '   ')
  })

  it('[CMTS-12271] Check whether closed input parser can be deleted', { tags: '@smoke' }, () => {
    NodeDetails.selectInputParserOption('Closed Options')
    NodeDetails.addClosedOption('Login', ' Yes, Yeah, Ya', ' Describe this for developers ')
    NodeDetails.clickMoreIcon().selectMenuItemByName('Delete Input')
    NodeDetails.checkEditNodeModalItemState('Login ', 'not.exist')
  })

  it('[CMTS-12273] Check whether Grammar Set field can be edited in the closed input parser', () => {
    NodeDetails.selectInputParserOption('Closed Options')
    NodeDetails.addClosedOption('Login', ' Yes, Yeah, Ya ', 'Describe this for developers')
    NodeDetails.clickEditContentButton().fillContentEditorTextarea('No, no, no').clickSaveEditContentButton()
    NodeDetails.checkCreatedOption('Login ', ' No, no, no ', ' Describe this for developers ')
  })

  it('[CMTS-12279] Check whether it is possible to create 6 closed input parsers', () => {
    NodeDetails.selectInputParserOption('Closed Options')
    NodeDetails.addClosedOption('Login1', ' Yes, Yeah, Ya  1', 'Describe this for developers 1')
    NodeDetails.checkCreatedOption('Login1 ', '  Yes, Yeah, Ya  1 ', ' Describe this for developers 1 ')
    NodeDetails.addClosedOption('Login2', ' Yes, Yeah, Ya  2', 'Describe this for developers 2')
    NodeDetails.checkCreatedOption('Login2 ', '  Yes, Yeah, Ya  2 ', ' Describe this for developers 2 ')
    NodeDetails.addClosedOption('Login3', ' Yes, Yeah, Ya  3', 'Describe this for developers 3')
    NodeDetails.checkCreatedOption('Login3 ', '  Yes, Yeah, Ya  3 ', ' Describe this for developers 3 ')
    NodeDetails.addClosedOption('Login4', ' Yes, Yeah, Ya  4', 'Describe this for developers 4')
    NodeDetails.checkCreatedOption('Login4 ', '  Yes, Yeah, Ya  4 ', ' Describe this for developers 4 ')
    NodeDetails.addClosedOption('Login5', ' Yes, Yeah, Ya  5', 'Describe this for developers 5')
    NodeDetails.checkCreatedOption('Login5 ', '  Yes, Yeah, Ya  5 ', ' Describe this for developers 5 ')
    NodeDetails.addClosedOption('Login6', ' Yes, Yeah, Ya  6', 'Describe this for developers 6')
    NodeDetails.checkCreatedOption('Login6 ', '  Yes, Yeah, Ya  6 ', ' Describe this for developers 6 ')
    NodeDetails.checkInputParsersCount('6')
    // @ts-ignore
    // eslint-disable-next-line @typescript-eslint/no-unsafe-call
    cy.findNodeForKey('openq').then(($node) => {
      expect($node.data.inputCount).equals(6)
    })
  })

  it('[CMTS-12314] Check whether closed input parser can be created with empty values', { tags: '@validation' }, () => {
    NodeDetails.selectInputParserOption('Closed Options')
    NodeDetails.clickAddOptionButton('Closed Options').clickFormButtonByText('Add')
    NodeDetails.checkAddClosedInputFieldError(
      addClosedInputForm,
      sectionLabelData.fieldName,
      'Please provide a unique parser label',
    )
    NodeDetails.checkAddClosedInputFieldErrorState(addClosedInputForm, grammarSetData.fieldName, 'not.exist')
    NodeDetails.checkAddClosedInputFieldErrorState(addClosedInputForm, descriptionData.fieldName, 'not.exist')
  })

  it('[CMTS-12317] Check whether closed input parser can be created with N maximum characters', { tags: '@validation' }, () => {
    const sectionLabelDataMaxLength: object = {
      fieldName: 'Selection Label',
      fieldType: 'input',
      value: 'qwertyuiopasdfghjklzxcvbnmqwertyu',
    }
    NodeDetails.selectInputParserOption('Closed Options')
    NodeDetails.clickAddOptionButton('Closed Options')
      .fillAddClosedInputField(addClosedInputForm, sectionLabelDataMaxLength)
      .clickFormButtonByText('Add')
    NodeDetails.checkAddClosedInputFieldError(
      addClosedInputForm,
      sectionLabelData.fieldName,
      'Length should be 2 to 32 chars',
    )
    NodeDetails.checkAddClosedInputFieldErrorState(addClosedInputForm, grammarSetData.fieldName, 'not.exist')
    NodeDetails.checkAddClosedInputFieldErrorState(addClosedInputForm, descriptionData.fieldName, 'not.exist')
  })

  it('[CMTS-12322] Check whether closed input parser can be created with N minimum characters', { tags: '@validation' }, () => {
    const sectionLabelDataMaxLength: object = {
      fieldName: 'Selection Label',
      fieldType: 'input',
      value: 'q',
    }
    NodeDetails.selectInputParserOption('Closed Options')
    NodeDetails.clickAddOptionButton('Closed Options')
      .fillAddClosedInputField(addClosedInputForm, sectionLabelDataMaxLength)
      .clickFormButtonByText('Add')
    NodeDetails.checkAddClosedInputFieldError(
      addClosedInputForm,
      sectionLabelData.fieldName,
      'Length should be 2 to 32 chars',
    )
    NodeDetails.checkAddClosedInputFieldErrorState(addClosedInputForm, grammarSetData.fieldName, 'not.exist')
    NodeDetails.checkAddClosedInputFieldErrorState(addClosedInputForm, descriptionData.fieldName, 'not.exist')
  })

  it('[CMTS-12305] Check whether Description field can be edited in the closed input parser', () => {
    const editDescriptionData: object = {
      fieldName: 'Description',
      fieldType: 'textarea',
      value: 'Edit Description',
    }
    NodeDetails.selectInputParserOption('Closed Options')
    NodeDetails.addClosedOption('Login', ' Yes, Yeah, Ya ', ' Describe this for developers ')
    NodeDetails.clickMoreIcon()
      .selectMenuItemByName('Edit Properties')
      .fillAddClosedInputField(editInput, editDescriptionData)
      .clickFormButtonByText('Save')
    NodeDetails.checkCreatedOption('Login ', '  Yes, Yeah, Ya  ', ' Edit Description ')
  })

  it('[CMTS-12328] Check whether closed input parser could be created with letters and numbers', { tags: '@validation' }, () => {
    NodeDetails.selectInputParserOption('Closed Options')
    NodeDetails.addClosedOption('test12340', ' ', ' ')
    NodeDetails.checkCreatedOption('test12340 ', '   ', '   ')
  })

  it('[CMTS-12332] Check whether closed input parser can be created with spaces', () => {
    const sectionLabelDataMaxLength: object = {
      fieldName: 'Selection Label',
      fieldType: 'input',
      value: 'space test',
    }
    NodeDetails.selectInputParserOption('Closed Options')
    NodeDetails.clickAddOptionButton('Closed Options')
      .fillAddClosedInputField(addClosedInputForm, sectionLabelDataMaxLength)
      .clickFormButtonByText('Add')
    NodeDetails.checkAddClosedInputFieldError(
      addClosedInputForm,
      // @ts-ignore
      sectionLabelData.fieldName,
      'Label must not contain spaces or underscores.',
    )
    NodeDetails.checkAddClosedInputFieldErrorState(addClosedInputForm, grammarSetData.fieldName, 'not.exist')
    NodeDetails.checkAddClosedInputFieldErrorState(addClosedInputForm, descriptionData.fieldName, 'not.exist')
  })

  it('[CMTS-12342] Check whether closed input parser can be created while Apply All To Flow checkbox is checked', { tags: '@smoke' }, () => {
    NodeDetails.selectInputParserOption('Closed Options')
    NodeDetails.addClosedOption('Login', 'Yes, Yeah, Ya', 'Describe this for developers')
    NodeDetails.clickApplyAllToFlowCheckbox()
    NodeDetails.checkCreatedOption('Login ', ' Yes, Yeah, Ya ', ' Describe this for developers ')
    // @ts-ignore
    // eslint-disable-next-line @typescript-eslint/no-unsafe-call
    cy.findNodeForKey('openq').then(($node) => {
      expect($node.data.inputCount).equals(1)
    })
    // @ts-ignore
    // eslint-disable-next-line @typescript-eslint/no-unsafe-call
    cy.findLinksOutOf('openq').then(($links) => {
      expect($links.length).equals(3)
    })
  })
})
