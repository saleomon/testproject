import FilterBar from '../../../../classes/bars/FilterBar.ts'
import NodeDetailsPanel from '../../../../classes/NodeDetailsPanel'
import NavigationBar from '../../../../classes/navigation/NavigationBar.ts'
import DiagramUtils from '../../../../support/DiagramUtils.ts'

let createdProjectInfo = {}
let detailsPanel = new NodeDetailsPanel()

before(function () {
  cy.fixture('projects-templates/edit-subflow-spec.json').then(($json) => {
    cy.createProject(Cypress.env('baseUrlAPI'), Cypress.env('username'), Cypress.env('password'), $json).then(
      ($projectInfo) => {
        cy.UILogin(Cypress.env('username'), Cypress.env('password'))
        NavigationBar.selectProject($projectInfo.label)
        cy.get('div').contains($projectInfo.label).should('be.visible')
        NavigationBar.openPatternsPage()
        FilterBar.selectPattern('general')
        createdProjectInfo = $projectInfo
      },
    )
  })
})

beforeEach(function () {
  // restore localstorage
  cy.restoreLocalStorage()
})

describe('Edit subflow node using Details panel', () => {
  describe.skip('Properties Tab', () => {
    it('Edit Description - CMTS-9157', () => {
      let assertionText = 'edited description'
      DiagramUtils.findDiagram()
      DiagramUtils.click('subflow')
      detailsPanel.selectTab('Properties')
      cy.get('[placeholder="Enter Node Description"]')
        .parent()
        .find('.contentEditButton')
        .click()
        .then(() => {
          cy.get('div').contains('Node Description').find('textarea').click().type(assertionText)
          cy.get('div').contains('Node Description').find('.editControls .saveItEditButton').click()
          detailsPanel.nodeDescription().then(($textarea) => {
            expect($textarea.val()).contains(assertionText)
          })
        })
      cy.findNodeForKey('subflow').then(($subflow) => {
        expect($subflow.data.description).equals(assertionText)
      })
    })
  })

  describe('Channel Tab', () => {
    let newChannel = 'SMS'
    it('Channel - Add - CMTS-9239', () => {
      cy.clickNode('subflow')
      detailsPanel.selectTab('Channels')
      detailsPanel.addChannel(newChannel)
      cy.get('.itemHeader').should('contain', newChannel)
    })

    it('Channel - Impossible to add same channel - CMTS-9240', () => {
      detailsPanel.addChannel(newChannel)
      cy.get('.ant-message-warning').contains('Subflow already contains this channel').should('be.visible') // warning toast message appears
      cy.get('.itemHeader')
        .contains(newChannel)
        .then(($elements) => {
          expect($elements.length).equals(1)
        })
    })

    it('Channel - Remove channel added with subflow - CMTS-9274', () => {
      cy.get('.itemHeader').contains(newChannel).find('button').click()
      cy.get('#delete-button').contains('Remove Channel').click()
      cy.get('.itemHeader').should('not.contain', newChannel)
    })

    it('Channel - Remove channel added through Details Panel - CMTS-9242', () => {
      cy.get('.itemHeader').contains('RWC').find('button').click()
      cy.get('#delete-button').contains('Remove Channel').click()
      cy.get('.itemHeader').should('not.contain', 'RWC')
    })

    it('Channel - Reseted form validation - CMTS-9241', () => {
      detailsPanel.fillChannelForm(newChannel)
      cy.get('.ant-popover').contains('Add Channel').next().find('span').contains('Reset').click() // reset
      cy.get('.ant-popover')
        .contains('Add Channel')
        .next()
        .find('[role="combobox"]')
        .should('be.visible')
        .then(($combobox) => {
          cy.wrap($combobox)
            .attribute('aria-controls')
            .then(($id) => {
              cy.get('#' + $id)
                .find('ul>li')
                .not('[aria-selected="true"]')
                .should('have.length', 3) // verify that anyone of 3 channels  are not selected
            })
        })
      detailsPanel.addChannelButton().click() // to hide popover
    })
  })

  describe.skip('Flows Tab - CMTS-9107', () => {
    let flowName = 'named'
    let flowNewName = 'renamed'
    let flowType = 'Normal'
    let flowDescription = 'flow descr'
    let flowNewDescription = 'new flow descr'

    it('Flow - Add - (positive)', () => {
      cy.waitForDiagram('goDiagram')
      cy.clickNode('subflow')
      detailsPanel.selectTab('Flow')
      detailsPanel.addFlow(flowName, flowType, flowDescription)
      cy.get('.itemHeader').should('contain', flowName)
      // cy.get('.itemConditionType').should('contain', flowType) //disabled due to CMTS-9178
      cy.get('.descriptionSpan').should('contain', flowDescription)
      // assertions on diagram object
      cy.findLinksOutOf('subflow').then(($links) => {
        expect($links.length).equals(2)
        expect($links[1].text).equals(flowName)
      })
    })

    it('Flow - Edit - CMTS-9236', () => {
      detailsPanel.selectTab('Flow')
      cy.get('.itemHeader').contains(flowName).find('button').click()
      cy.get('.ant-dropdown-menu-item').contains('Edit Properties').click()
      cy.get('.ant-popover').contains('Edit Existing Flow Condition').should('be.visible')
      cy.get('.ant-popover-placement-left').find('input').clear().type(flowNewName)

      cy.get('.ant-popover-placement-left').find('textarea').clear().type(flowNewDescription)
      cy.get('.ant-popover-placement-left').find('.ant-btn-primary').click()

      // aseertion on front
      cy.get('.itemHeader').should('contain', flowNewName)
      cy.get('.itemHeader').should('contain', flowNewDescription)
      // assertions on diagram object
      cy.findLinksOutOf('subflow').then(($links) => {
        expect($links.length).equals(2)
        expect($links[1].text).equals(flowNewName)
      })
    })

    it('Flow - Delete created flow (CMTS-9243)', () => {
      cy.get('.itemHeader').contains(flowName).find('button').click()
      cy.get('.ant-dropdown-menu-item').contains('Delete Flow').click()
      // assertions on front
      cy.get('.itemHeader').should('not.contain', flowNewName)
      cy.get('.itemHeader').should('not.contain', flowNewDescription)
      // assertions on diagram
      cy.findLinksOutOf('subflow').then(($links) => {
        expect($links.length).equals(1)
        expect($links[0].text).equals('default')
      })
    })

    it('Flow - Delete default flow (CMTS-9244)', () => {
      cy.get('.itemHeader').contains('default').find('button').click()
      cy.get('.ant-dropdown-menu-item').contains('Delete Flow').click()
      // assertions on front
      cy.get('.itemHeader').should('not.contain', 'default')
      // assertions on diagram
      cy.findLinksOutOf('subflow').then(($links) => {
        expect($links.length).equals(0)
      })
    })

    it('Flow - Input validation (CMTS-9227, CMTS-9228, CMTS-9229, CMTS-9230, CMTS-9231, CMTS-9232, CMTS-9233)',
      { tags: '@validation' },
      () => {
        detailsPanel.selectTab('Flow')
        detailsPanel.addFlowButton().click() // to show popover

        detailsPanel.flowNameField().type('a') // one characher
        detailsPanel.flowNameField().blur() // blur triggers validation
        cy.get('.ant-popover')
          .contains('Add New Flow Condition')
          .next()
          .find('.ant-form-explain')
          .should('contain', 'Length be 2 to 32 chars')
        detailsPanel.flowNameField().clear()

        detailsPanel.flowNameField().type('morethan32morethan32morethan32mor') // 33 charactes
        detailsPanel.flowNameField().blur() // blur triggers validation
        cy.get('.ant-popover')
          .contains('Add New Flow Condition')
          .next()
          .find('.ant-form-explain')
          .should('contain', 'Length be 2 to 32 chars')
        detailsPanel.flowNameField().clear()

        detailsPanel.flowNameField().type('space in it') // space inside label
        detailsPanel.flowNameField().blur() // blur triggers validation
        cy.get('.ant-popover')
          .contains('Add New Flow Condition')
          .next()
          .find('.ant-form-explain')
          .should('contain', 'Label must not contain spaces or underscores.')
        detailsPanel.flowNameField().clear()

        detailsPanel.flowNameField().type('  ') // two spaces only
        detailsPanel.flowNameField().blur() // blur triggers validation
        cy.get('.ant-popover')
          .contains('Add New Flow Condition')
          .next()
          .find('.ant-form-explain')
          .should('contain', 'Label must not contain spaces or underscores.')
        detailsPanel.flowNameField().clear()

        detailsPanel.flowNameField().type('underscore_label') // underscore in label
        detailsPanel.flowNameField().blur() // blur triggers validation
        cy.get('.ant-popover')
          .contains('Add New Flow Condition')
          .next()
          .find('.ant-form-explain')
          .should('contain', 'Label must not contain spaces or underscores.')
        detailsPanel.flowNameField().clear()

        detailsPanel.flowNameField().type('aa') // two charachers
        detailsPanel.flowNameField().blur() // blur triggers validation
        cy.get('.ant-popover').contains('Add New Flow Condition').next().find('.ant-form-explain').should('not.exist')
        detailsPanel.flowNameField().clear()

        detailsPanel.flowNameField().type('32chars32chars32chars32chars32ch') // 32 charachers
        detailsPanel.flowNameField().blur() // blur triggers validation
        cy.get('.ant-popover').contains('Add New Flow Condition').next().find('.ant-form-explain').should('not.exist')
        detailsPanel.flowNameField().clear()
        detailsPanel.addFlowButton().click() // to hide popover
      },
    )

    it('Flow - Reset validation (CMTS-9237)', () => {
      detailsPanel.selectTab('Flow')
      detailsPanel.fillFlowForm('reset', 'Normal', 'reset')
      cy.get('.ant-popover').contains('Add New Flow Condition').next().find('span').contains('Reset').click()
      detailsPanel.flowNameField().should('contain', '')
      cy.get('.ant-popover')
        .contains('Add New Flow Condition')
        .next()
        .find('[role="combobox"]')
        .then(($combobox) => {
          cy.wrap($combobox)
            .attribute('aria-controls')
            .then(($id) => {
              cy.get('#' + $id)
                .find('ul>li')
                .not('[aria-selected="true"]')
                .should('have.length', 7)
            })
        })
      cy.get('.ant-popover').contains('Add New Flow Condition').next().find('textarea').should('contain', '')
    })
  })
})

afterEach(function () {
  // preserve local storage after each hook
  cy.saveLocalStorage()
})

after(function () {
  cy.deleteProject(Cypress.env('baseUrlAPI'), Cypress.env('username'), Cypress.env('password'), createdProjectInfo.id)
  cy.window().then(($win) => {
    $win.localStorage.clear()
  })
})
