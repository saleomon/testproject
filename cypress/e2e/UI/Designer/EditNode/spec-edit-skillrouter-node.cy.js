import FilterBar from '../../../../classes/bars/FilterBar.ts'
import NodeDetailsPanel from '../../../../classes/NodeDetailsPanel'
import NavigationBar from '../../../../classes/navigation/NavigationBar.ts'
import DiagramUtils from '../../../../support/DiagramUtils.ts'

let createdProjectInfo = {}
let detailsPanel = new NodeDetailsPanel()

before(function () {
  cy.fixture('projects-templates/edit-node-project.json').then(($json) => {
    cy.createProject(Cypress.env('baseUrlAPI'), Cypress.env('username'), Cypress.env('password'), $json).then(
      ($projectInfo) => {
        cy.UILogin(Cypress.env('username'), Cypress.env('password'))
        NavigationBar.selectProject($projectInfo.label)
        cy.get('div').contains($projectInfo.label).should('be.visible')
        NavigationBar.openPatternsPage()
        FilterBar.selectPattern('skillrouter')
        createdProjectInfo = $projectInfo
      },
    )
  })
})

beforeEach(function () {
  // restore localstorage
  cy.restoreLocalStorage()
})

describe('Edit skillrouter node using Details panel', () => {
  describe.skip('Properties Tab', () => {
    it('Edit Description', () => {
      let assertionText = 'edited description'
      DiagramUtils.findDiagram()
      DiagramUtils.click('skillrouter')
      detailsPanel.selectTab('Properties')
      detailsPanel
        .nodeDescriptionEditBtn()
        .click()
        .then(() => {
          cy.get('div').contains('Node Description').find('textarea').click().clear().type(assertionText)
          detailsPanel.nodeDescriptionSaveBtn().click()
          detailsPanel.nodeDescription().then(($textarea) => {
            expect($textarea.val()).contains(assertionText)
          })
        })
      cy.findNodeForKey('skillrouter').then(($node) => {
        expect($node.data.description).equals(assertionText)
      })
    })
  })

  describe('Skills Tab', () => {
    let newSkill = 'skill2'
    it('Skills - Add', () => {
      cy.clickNode('skillrouter')
      detailsPanel.selectTab('Skills')
      cy.intercept('PUT', 'api/patterns/*/*').as('addSkill')
      detailsPanel.addSkill(newSkill)
      cy.wait('@addSkill')
        .its('response')
        .then((response) => {
          expect(response).to.have.property('statusCode', 201)
        })
      cy.get('.ant-message-success').contains('OK').should('be.visible') // warning toast message appears
      cy.get('.itemHeader').should('contain', newSkill)
    })

    it('Skills - Impossible to add same skill', () => {
      detailsPanel.addSkill(newSkill)
      cy.get('.ant-message-error').contains('This skill has already been added to this Router.').should('be.visible') // warning toast message appears
      cy.get('.itemHeader')
        .contains(newSkill)
        .then(($elements) => {
          expect($elements.length).equals(1)
        })
    })

    it('Skills - Remove skill added while node creation', () => {
      cy.intercept('PUT', 'api/patterns/*/*').as('removeSkill')
      cy.get('.itemHeader').filter(':visible').contains('skill1').find('button').click()
      cy.get('[role="menuitem"]').contains('Remove Skill').click()
      cy.get('.itemHeader').should('not.contain', 'skill1')
      cy.wait('@removeSkill')
        .its('response')
        .then((response) => {
          expect(response).to.have.property('statusCode', 201)
        })
    })

    it('Skills - Remove skill added through Details Panel', () => {
      cy.intercept('PUT', 'api/patterns/*/*').as('removeSkill')
      cy.get('.itemHeader').contains(newSkill).find('button').click()
      cy.get('[role="menuitem"]').contains('Remove Skill').click()
      cy.get('.itemHeader').should('have.length', 0)
      cy.wait('@removeSkill')
        .its('response')
        .then((response) => {
          expect(response).to.have.property('statusCode', 201)
        })
      cy.get('.itemHeader').should('have.length', 0)
    })

    it('Skills - Reseted form validation', () => {
      detailsPanel.fillSkillForm(newSkill)
      cy.get('.ant-popover').contains('Add Existing Skill').next().find('span').contains('Reset').click() // reset
      cy.get('.ant-popover')
        .contains('Add Existing Skill')
        .next()
        .find('[role="combobox"]')
        .should('be.visible')
        .then(($combobox) => {
          cy.wrap($combobox)
            .attribute('aria-controls')
            .then(($id) => {
              cy.get('#' + $id)
                .find('ul>li')
                .not('[aria-selected="true"]')
                .should('have.length', 2) // verify that anyone of 3 channels  are not selected
            })
        })
      detailsPanel.addSkillButton().click() // to hide popover
    })
  })

  describe.skip('Flows Tab', () => {
    let flowName = 'named'
    let flowNewName = 'renamed'
    let flowType = 'Normal'
    let flowDescription = 'flow descr'
    let flowNewDescription = 'new flow descr'

    it('Flow - Add - (positive)', () => {
      DiagramUtils.findDiagram()
      DiagramUtils.click('skillrouter')
      detailsPanel.selectTab('Flow')
      cy.intercept('PUT', 'api/patterns/*/*').as('addFlow')
      detailsPanel.addFlow(flowName, flowType, flowDescription)
      cy.wait('@addFlow')
        .its('response')
        .then((response) => {
          expect(response).to.have.property('statusCode', 201)
        })
      cy.get('.itemHeader').should('contain', flowName)
      // cy.get('.itemConditionType').should('contain', flowType) //disabled due to CMTS-9178
      cy.get('.descriptionSpan').should('contain', flowDescription)
      // assertions on diagram object
      cy.findLinksOutOf('skillrouter').then(($links) => {
        expect($links.length).equals(2)
        expect($links[1].text).equals(flowName)
      })
    })

    it('Flow - Edit - CMTS-9236', () => {
      detailsPanel.selectTab('Flow')
      cy.get('.itemHeader').contains(flowName).find('button').click()
      cy.get('.ant-dropdown-menu-item').contains('Edit Properties').click()
      cy.intercept('PUT', 'api/patterns/*/*').as('editFlow')
      cy.get('.ant-popover').contains('Edit Existing Flow Condition').should('be.visible')
      cy.get('.ant-popover-placement-left').find('input').clear().type(flowNewName)

      cy.get('.ant-popover-placement-left').find('textarea').clear().type(flowNewDescription)
      cy.get('.ant-popover-placement-left').find('.ant-btn-primary').click()
      cy.wait('@editFlow')
        .its('response')
        .then((response) => {
          expect(response).to.have.property('statusCode', 201)
        })

      // aseertion on front
      cy.get('.itemHeader').should('contain', flowNewName)
      cy.get('.itemHeader').should('contain', flowNewDescription)
      // assertions on diagram object
      cy.findLinksOutOf('skillrouter').then(($links) => {
        expect($links.length).equals(2)
        expect($links[1].text).equals(flowNewName)
      })
    })

    it('Flow - Delete created flow - (CMTS-9243)', () => {
      cy.intercept('PUT', 'api/patterns/*/*').as('deleteFlow')
      cy.get('.itemHeader').contains(flowName).find('button').click()
      cy.get('.ant-dropdown-menu-item').contains('Delete Flow').click()
      cy.wait('@deleteFlow')
        .its('response')
        .then((response) => {
          expect(response).to.have.property('statusCode', 201)
        })
      // assertions on front
      cy.get('.itemHeader').should('not.contain', flowNewName)
      cy.get('.itemHeader').should('not.contain', flowNewDescription)
      // assertions on diagram
      cy.findLinksOutOf('skillrouter').then(($links) => {
        expect($links.length).equals(1)
        expect($links[0].text).equals('default')
      })
    })

    it('Flow - Delete default flow (CMTS-9244)', () => {
      cy.intercept('PUT', 'api/patterns/*/*').as('deleteFlow')
      cy.get('.itemHeader').contains('default').find('button').click()
      cy.get('.ant-dropdown-menu-item').contains('Delete Flow').click()
      cy.wait('@deleteFlow')
        .its('response')
        .then((response) => {
          expect(response).to.have.property('statusCode', 201)
        })
      // assertions on front
      cy.get('.itemHeader').should('not.contain', 'default')
      // assertions on diagram
      cy.findLinksOutOf('skillrouter').then(($links) => {
        expect($links.length).equals(0)
      })
    })

    it('Flow - Input validation (CMTS-9227, CMTS-9228, CMTS-9229, CMTS-9230, CMTS-9231, CMTS-9232, CMTS-9233)',
     { tags: '@validation' }, () => {
      detailsPanel.selectTab('Flow')
      detailsPanel.addFlowButton().click() // to show popover

      detailsPanel.flowNameField().type('a') // one characher
      detailsPanel.flowNameField().blur() // blur triggers validation
      detailsPanel.flowValidationMessage().should('contain', 'Length be 2 to 32 chars')
      detailsPanel.flowNameField().clear()

      detailsPanel.flowNameField().type('morethan32morethan32morethan32mor') // 33 charactes
      detailsPanel.flowNameField().blur() // blur triggers validation
      detailsPanel.flowValidationMessage().should('contain', 'Length be 2 to 32 chars')
      detailsPanel.flowNameField().clear()

      detailsPanel.flowNameField().type('space in it') // space inside label
      detailsPanel.flowNameField().blur() // blur triggers validation
      detailsPanel.flowValidationMessage().should('contain', 'Label must not contain spaces or underscores.')
      detailsPanel.flowNameField().clear()

      detailsPanel.flowNameField().type('  ') // two spaces only
      detailsPanel.flowNameField().blur() // blur triggers validation
      detailsPanel.flowValidationMessage().should('contain', 'Label must not contain spaces or underscores.')
      detailsPanel.flowNameField().clear()

      detailsPanel.flowNameField().type('underscore_label') // underscore in label
      detailsPanel.flowNameField().blur() // blur triggers validation
      detailsPanel.flowValidationMessage().should('contain', 'Label must not contain spaces or underscores.')
      detailsPanel.flowNameField().clear()

      detailsPanel.flowNameField().type('aa') // two charachers
      detailsPanel.flowNameField().blur() // blur triggers validation
      detailsPanel.flowValidationMessage().should('not.exist')
      detailsPanel.flowNameField().clear()

      detailsPanel.flowNameField().type('32chars32chars32chars32chars32ch') // 32 charachers
      detailsPanel.flowNameField().blur() // blur triggers validation
      detailsPanel.flowValidationMessage().should('not.exist')
      detailsPanel.flowNameField().clear()
      detailsPanel.addFlowButton().click() // to hide popover
    })

    it('Flow - Reset validation (CMTS-9237)', () => {
      detailsPanel.selectTab('Flow')
      detailsPanel.fillFlowForm('reset', 'Normal', 'reset')
      cy.get('.ant-popover').contains('Add New Flow Condition').next().find('span').contains('Reset').click()
      detailsPanel.flowNameField().should('contain', '')
      cy.get('.ant-popover')
        .contains('Add New Flow Condition')
        .next()
        .find('[role="combobox"]')
        .then(($combobox) => {
          cy.wrap($combobox)
            .attribute('aria-controls')
            .then(($id) => {
              cy.get('#' + $id)
                .find('ul>li')
                .not('[aria-selected="true"]')
                .should('have.length', 7)
            })
        })
      cy.get('.ant-popover').contains('Add New Flow Condition').next().find('textarea').should('contain', '')
    })
  })
})

afterEach(function () {
  // preserve local storage after each hook
  cy.saveLocalStorage()
})

after(function () {
  cy.deleteProject(Cypress.env('baseUrlAPI'), Cypress.env('username'), Cypress.env('password'), createdProjectInfo.id)
  cy.window().then(($win) => {
    $win.localStorage.clear()
  })
})
