import { dataObject } from '../../../fixtures/data/data-spec-projects'
import { ProjectInfo } from '../../../types/response/ProjectInfo'
import DiagramUtils from '../../../support/DiagramUtils'
import NodePropertiesSideBar from '../../../classes/bars/NodePropertiesSideBar'
import DiagramBar from '../../../classes/bars/DiagramBar'

describe('delete nodes', () => {
  beforeEach(() => {
    cy.restoreLocalStorage()
  })

  afterEach(() => {
    cy.get<ProjectInfo>('@current-project').then((json: ProjectInfo) => {
      cy.removeProject(json.id)
    })
  })

  it('[CMTS-11029] Sub-flow node can be deleted from the node tree via Delete Node icon pop-up in the left-side toolbar',  { tags: '@smoke' }, () => {
    cy.precondition('11139')

    DiagramUtils.findDiagram()

    DiagramUtils.click(dataObject.pattern_diagram.subflow2.nodeLabel)
    DiagramBar.clickOnDeleteNodeBtn().clickOnYesBtn()
    DiagramUtils.click(dataObject.pattern_diagram.subflow1.nodeLabel)
    NodePropertiesSideBar.openFlow()
    DiagramUtils.findChildNodeByLinkText('Start', dataObject.pattern_diagram.subflow1.linkLabel).then(($el) => {
      expect($el.data.key).equals(dataObject.pattern_diagram.subflow1.nodeLabel)
    })
    DiagramUtils.findChildNodeByLinkText(
      dataObject.pattern_diagram.subflow1.nodeLabel,
      dataObject.pattern_diagram.subflow2.linkLabel,
    ).then(($el) => {
      expect($el.data.key).equals('AddNode1')
    })
  })

  it('[CMTS-11055] The subflow node in the middle of the nodes tree can be removed without its child', () => {
    cy.precondition('11055')

    DiagramUtils.findDiagram()

    DiagramUtils.click(dataObject.pattern_diagram.subflow1.nodeLabel)
    DiagramBar.clickOnDeleteNodeBtn().clickOnYesBtn()
    DiagramUtils.click('Start')
    NodePropertiesSideBar.openFlow()
    DiagramUtils.findChildNodeByLinkText('Start', dataObject.pattern_diagram.subflow1.linkLabel).then(($el) => {
      expect($el.data.key).equals('AddNode1')
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.pattern_diagram.subflow2.nodeLabel, 'default').then(($el) => {
      expect($el.data.key).equals('AddNode2')
    })
  })

  it('[CMTS-11063] Sub-flow node is not removed from the node tree after clicking "No" button in the Delete Node icon pop-up', () => {
    cy.precondition('11063')

    DiagramUtils.findDiagram()

    DiagramUtils.click(dataObject.pattern_diagram.subflow2.nodeLabel)
    DiagramBar.clickOnDeleteNodeBtn().clickOnNoBtn()
    DiagramUtils.click(dataObject.pattern_diagram.subflow1.nodeLabel)
    NodePropertiesSideBar.openFlow()
    DiagramUtils.findChildNodeByLinkText('Start', dataObject.pattern_diagram.subflow1.linkLabel).then(($el) => {
      expect($el.data.key).equals(dataObject.pattern_diagram.subflow1.nodeLabel)
    })
    DiagramUtils.findChildNodeByLinkText(
      dataObject.pattern_diagram.subflow1.nodeLabel,
      dataObject.pattern_diagram.subflow2.linkLabel,
    ).then(($el) => {
      expect($el.data.key).equals(dataObject.pattern_diagram.subflow2.nodeLabel)
    })
  })

  it('[CMTS-11067] Skill router node can be deleted from the node via Delete Node icon pop-up in the left-side toolbar', () => {
    cy.precondition('11067')

    DiagramUtils.findDiagram()

    DiagramUtils.click(dataObject.pattern_diagram.skillsrouter1.nodeLabel)
    DiagramBar.clickOnDeleteNodeBtn().clickOnYesBtn()
    DiagramUtils.click(dataObject.pattern_diagram.subflow2.nodeLabel)
    NodePropertiesSideBar.openFlow()
    DiagramUtils.findChildNodeByLinkText('Start', dataObject.pattern_diagram.subflow1.linkLabel).then(($el) => {
      expect($el.data.key).equals(dataObject.pattern_diagram.subflow1.nodeLabel)
    })
    DiagramUtils.findChildNodeByLinkText(
      dataObject.pattern_diagram.subflow1.nodeLabel,
      dataObject.pattern_diagram.subflow2.linkLabel,
    ).then(($el) => {
      expect($el.data.key).equals(dataObject.pattern_diagram.subflow2.nodeLabel)
    })
    DiagramUtils.findChildNodeByLinkText(
      dataObject.pattern_diagram.subflow2.nodeLabel,
      dataObject.pattern_diagram.skillsrouter1.linkLabel,
    ).then(($el) => {
      expect($el.data.key).equals('AddNode1')
    })
  })

  it('[CMTS-11248] The default flow below the Start node cannot be removed from the node tree via Delete Node icon pop-up in the left-side toolbar', { tags: '@smoke' }, () => {
    cy.fixture('projects-templates/qa_autotest.json').as('project')

    cy.get<ProjectInfo>('@project').then((json: ProjectInfo) => {
      cy.loginAPI()
      cy.createNewProject(json).as('current-project')
      cy.loginUI()
    })

    cy.get<ProjectInfo>('@current-project').then((projectInfo: ProjectInfo) => {
      cy.selectProjectAndPattern(projectInfo.label, 'two-subflow')
    })

    DiagramUtils.findDiagram()

    DiagramUtils.click('AddNode1')
    DiagramBar.clickOnDeleteNodeBtn().clickOnYesBtn()
    cy.get('.ant-message-warning>span').should('have.text', 'ALL nodes must have at least one exit flow.')
  })

  it('[CMTS-11250] The flow can be removed from the node tree via Delete Node icon pop-up in the left-side toolbar', { tags: '@smoke' }, () => {
    cy.fixture('projects-templates/qa_autotest.json').as('project')

    cy.get<ProjectInfo>('@project').then((json: ProjectInfo) => {
      cy.loginAPI()
      cy.createNewProject(json).as('current-project')
      cy.loginUI()
    })

    cy.get<ProjectInfo>('@current-project').then((projectInfo: ProjectInfo) => {
      cy.selectProjectAndPattern(projectInfo.label, 'two-subflow')
    })

    DiagramUtils.findDiagram()
    DiagramUtils.click(dataObject.pattern_diagram.subflow1.nodeLabel)
    DiagramBar.clickOnDeleteNodeBtn().clickOnYesBtn()
     DiagramUtils.findChildNodeByLinkText('Start', dataObject.pattern_diagram.subflow1.linkLabel).then(($it) => {
      expect($it.data.key).equals('AddNode1')
    })
  })

  it('[CMTS-11317] The default exit can be deleted from the node tree via Delete Node icon pip-up in the left-side toolbar', { tags: '@smoke' }, () => {
    cy.precondition('11317')

    DiagramUtils.findDiagram()

    DiagramUtils.doubleClick(dataObject.pattern_diagram.subflow1.nodeLabel)

    DiagramUtils.findDiagram()
    DiagramUtils.click('endpointNode1')
    DiagramBar.clickOnDeleteNodeBtn().clickOnYesBtn()
    DiagramUtils.click(dataObject.flow_diagram.tag1.nodeLabel)
    NodePropertiesSideBar.openFlow()
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.tag1.nodeLabel, 'default').then(($el) => {
      expect($el.data.key).equals('AddNode1')
    })
  })

  it('[CMTS-11332] Patterns tab: outcomes can be deleted from the node tree with their parent node', () => {
    cy.precondition('11332')

    DiagramUtils.findDiagram()

    DiagramUtils.click(dataObject.pattern_diagram.tag1.nodeLabel)
    DiagramBar.clickOnDeleteNodeBtn().clickOnYesBtn()
    DiagramUtils.click(dataObject.pattern_diagram.subflow2.nodeLabel)
    NodePropertiesSideBar.openFlow()
    DiagramUtils.findChildNodeByLinkText('Start', dataObject.pattern_diagram.subflow1.linkLabel).then(($el) => {
      expect($el.data.key).equals(dataObject.pattern_diagram.subflow1.nodeLabel)
    })
    DiagramUtils.findChildNodeByLinkText(
      dataObject.pattern_diagram.skillsrouter1.nodeLabel,
      dataObject.pattern_diagram.subflow2.linkLabel,
    ).then(($el) => {
      expect($el.data.key).equals(dataObject.pattern_diagram.subflow2.nodeLabel)
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.pattern_diagram.subflow2.nodeLabel, 'tagnode1').then(($el) => {
      expect($el.data.key).equals('AddNode1')
    })
  })

  it('[CMTS-11075] Tag node can be deleted from the node tree via Delete Node icon pop-up in the left-side toolbar', () => {
    cy.precondition('11075')

    DiagramUtils.findDiagram()

    DiagramUtils.click(dataObject.pattern_diagram.tag1.nodeLabel)
    DiagramBar.clickOnDeleteNodeBtn().clickOnYesBtn()
    DiagramUtils.click(dataObject.pattern_diagram.skillsrouter1.nodeLabel)
    NodePropertiesSideBar.openFlow()
    DiagramUtils.findChildNodeByLinkText('Start', dataObject.pattern_diagram.subflow1.linkLabel).then(($el) => {
      expect($el.data.key).equals(dataObject.pattern_diagram.subflow1.nodeLabel)
    })
    DiagramUtils.findChildNodeByLinkText(
      dataObject.pattern_diagram.subflow1.nodeLabel,
      dataObject.pattern_diagram.skillsrouter1.linkLabel,
    ).then(($el) => {
      expect($el.data.key).equals(dataObject.pattern_diagram.skillsrouter1.nodeLabel)
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.pattern_diagram.skillsrouter1.nodeLabel, 'tagnode1').then(($el) => {
      expect($el.data.key).equals('AddNode1')
    })
  })

  it('[CMTS-11079] GoTo link node can be deleted from the node tree via Delete Node icon pop-up in the left-side toolbar', { tags: '@smoke' }, () => {
    cy.precondition('11079')

    DiagramUtils.findDiagram()

    DiagramUtils.click('gotoNode1')
    DiagramBar.clickOnDeleteNodeBtn().clickOnYesBtn()
    DiagramUtils.click(dataObject.pattern_diagram.skillsrouter2.nodeLabel)
    NodePropertiesSideBar.openFlow()
    DiagramUtils.findChildNodeByLinkText('Start', dataObject.pattern_diagram.subflow1.linkLabel).then(($el) => {
      expect($el.data.key).equals(dataObject.pattern_diagram.subflow1.nodeLabel)
    })
    DiagramUtils.findChildNodeByLinkText(
      dataObject.pattern_diagram.subflow1.nodeLabel,
      dataObject.pattern_diagram.skillsrouter1.linkLabel,
    ).then(($el) => {
      expect($el.data.key).equals(dataObject.pattern_diagram.skillsrouter1.nodeLabel)
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.pattern_diagram.subflow1.nodeLabel, 'default').then(($el) => {
      expect($el.data.key).equals('AddNode1')
    })
    DiagramUtils.findChildNodeByLinkText(
      dataObject.pattern_diagram.subflow1.nodeLabel,
      dataObject.pattern_diagram.skillsrouter2.linkLabel,
    ).then(($el) => {
      expect($el.data.key).equals(dataObject.pattern_diagram.skillsrouter2.nodeLabel)
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.pattern_diagram.skillsrouter1.nodeLabel, 'default').then(($el) => {
      expect($el.data.key).equals('AddNode2')
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.pattern_diagram.skillsrouter2.nodeLabel, 'gotolink').then(($el) => {
      expect($el.data.key).equals('AddNode3')
    })
  })

  it('[CMTS-11139] Skill outcome can be deleted from the node tree via Delete Node icon pop-up in the left-side toolbar', () => {
    cy.precondition('11139')

    DiagramUtils.findDiagram()

    DiagramUtils.click('outcomeNode1')
    DiagramBar.clickOnDeleteNodeBtn().clickOnYesBtn()
    DiagramUtils.click(dataObject.pattern_diagram.tag1.nodeLabel)
    NodePropertiesSideBar.openFlow()
    DiagramUtils.findChildNodeByLinkText('Start', dataObject.pattern_diagram.subflow1.linkLabel).then(($el) => {
      expect($el.data.key).equals(dataObject.pattern_diagram.subflow1.nodeLabel)
    })
    DiagramUtils.findChildNodeByLinkText(
      dataObject.pattern_diagram.subflow1.nodeLabel,
      dataObject.pattern_diagram.subflow2.linkLabel,
    ).then(($el) => {
      expect($el.data.key).equals(dataObject.pattern_diagram.subflow2.nodeLabel)
    })
    DiagramUtils.findChildNodeByLinkText(
      dataObject.pattern_diagram.subflow2.nodeLabel,
      dataObject.pattern_diagram.tag1.linkLabel,
    ).then(($el) => {
      expect($el.data.key).equals(dataObject.pattern_diagram.tag1.nodeLabel)
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.pattern_diagram.tag1.nodeLabel, 'testOutcome').then(($el) => {
      expect($el.data.key).equals('AddNode1')
    })
  })

  it('[CMTS-11074] The skill router node in the middle of the nodes tree can be removed without its child', () => {
    cy.precondition('11074')

    DiagramUtils.findDiagram()

    DiagramUtils.click(dataObject.pattern_diagram.skillsrouter1.nodeLabel)
    DiagramBar.clickOnDeleteNodeBtn().clickOnYesBtn()
    DiagramUtils.click(dataObject.pattern_diagram.subflow1.nodeLabel)
    NodePropertiesSideBar.openFlow()
    DiagramUtils.findChildNodeByLinkText('Start', dataObject.pattern_diagram.subflow1.linkLabel).then(($it) => {
      expect($it.data.key).equals(dataObject.pattern_diagram.subflow1.nodeLabel)
    })
    DiagramUtils.findChildNodeByLinkText(
      dataObject.pattern_diagram.subflow1.nodeLabel,
      dataObject.pattern_diagram.skillsrouter1.linkLabel,
    ).then(($it) => {
      expect($it.data.key).equals('AddNode1')
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.pattern_diagram.skillsrouter2.nodeLabel, 'default').then(($it) => {
      expect($it.data.key).equals('AddNode2')
    })
  })

  it('[CMTS-11077] The tag node in the middle of the nodes tree can be removed without its child', () => {
    cy.precondition('11077')

    DiagramUtils.findDiagram()

    DiagramUtils.click(dataObject.pattern_diagram.tag1.nodeLabel)
    DiagramBar.clickOnDeleteNodeBtn().clickOnYesBtn()
    DiagramUtils.click(dataObject.pattern_diagram.subflow1.nodeLabel)
    NodePropertiesSideBar.openFlow()
    DiagramUtils.findChildNodeByLinkText('Start', dataObject.pattern_diagram.subflow1.linkLabel).then(($it) => {
      expect($it.data.key).equals(dataObject.pattern_diagram.subflow1.nodeLabel)
    })
    DiagramUtils.findChildNodeByLinkText(
      dataObject.pattern_diagram.subflow1.nodeLabel,
      dataObject.pattern_diagram.tag1.linkLabel,
    ).then(($it) => {
      expect($it.data.key).equals('AddNode1')
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.pattern_diagram.skillsrouter1.nodeLabel, 'default').then(($it) => {
      expect($it.data.key).equals('AddNode2')
    })
  })

  it('[CMTS-11124] The Start node cannot be removed from the node tree via Delete Node icon pop-up in the left-side toolbar', () => {
    cy.precondition('11124')

    DiagramUtils.findDiagram()

    DiagramUtils.click('Start')
    DiagramBar.clickOnDeleteNodeBtn().clickOnYesBtn()
    DiagramUtils.click('Start')
    NodePropertiesSideBar.openFlow()
    DiagramUtils.findChildNodeByLinkText('Start', 'default').then(($it) => {
      expect($it.data.key).equals('AddNode1')
    })
  })

  it('[CMTS-11131] Abandon outcome can be deleted from the node tree via Delete Node icon pop-up in the left-side toolbar', () => {
    cy.precondition('11131')

    DiagramUtils.findDiagram()

    DiagramUtils.click('outcomeNode1')
    DiagramBar.clickOnDeleteNodeBtn().clickOnYesBtn()
    DiagramUtils.click(dataObject.pattern_diagram.tag1.nodeLabel)
    NodePropertiesSideBar.openFlow()
    DiagramUtils.findChildNodeByLinkText(dataObject.pattern_diagram.tag1.nodeLabel, 'default').then(($it) => {
      expect($it.data.key).equals('AddNode1')
    })
  })

  it('[CMTS-11237] Global Escalate outcome can be deleted from the node tree via Delete Node icon pop-up in the left-side toolbar', () => {
    cy.precondition('11237')

    DiagramUtils.findDiagram()

    DiagramUtils.click('outcomeNode1')
    DiagramBar.clickOnDeleteNodeBtn().clickOnYesBtn()
    DiagramUtils.click(dataObject.pattern_diagram.tag1.nodeLabel)
    NodePropertiesSideBar.openFlow()
    DiagramUtils.findChildNodeByLinkText(dataObject.pattern_diagram.tag1.nodeLabel, 'default').then(($it) => {
      expect($it.data.key).equals('AddNode1')
    })
  })

  it('[CMTS-11241] Global Fallback outcome can be deleted from the node tree via Delete Node icon pop-up in the left-side toolbar', () => {
    cy.precondition('11241')

    DiagramUtils.findDiagram()

    DiagramUtils.click('outcomeNode1')
    DiagramBar.clickOnDeleteNodeBtn().clickOnYesBtn()
    DiagramUtils.click(dataObject.pattern_diagram.tag1.nodeLabel)
    NodePropertiesSideBar.openFlow()
    DiagramUtils.findChildNodeByLinkText(dataObject.pattern_diagram.tag1.nodeLabel, 'default').then(($it) => {
      expect($it.data.key).equals('AddNode1')
    })
    DiagramUtils.findChildNodeByLinkText(
      dataObject.pattern_diagram.skillsrouter1.nodeLabel,
      dataObject.pattern_diagram.tag1.linkLabel,
    ).then(($it) => {
      expect($it.data.key).equals(dataObject.pattern_diagram.tag1.nodeLabel)
    })
  })

  it('[CMTS-11248] The default flow below the Start node cannot be removed from the node tree via Delete Node  icon pop-up in the left-side toolbar ', () => {
    cy.precondition('empty')

    DiagramUtils.findDiagram()

    DiagramUtils.click('AddNode1')
    DiagramBar.clickOnDeleteNodeBtn().clickOnYesBtn()
    DiagramUtils.click('Start')
    NodePropertiesSideBar.openFlow()
    DiagramUtils.findChildNodeByLinkText('Start', 'AddNode1').then(($it) => {
      expect($it.data.key).equals('AddNode1')
    })
  })
 
  it('[CMTS-11642] GoTo link node can be deleted from the node tree with its parent node', { tags: '@smoke' }, () => {
    cy.precondition('11642')

    DiagramUtils.findDiagram()

    DiagramUtils.click(dataObject.pattern_diagram.skillsrouter2.nodeLabel)
    DiagramBar.clickOnDeleteNodeBtn().clickOnYesBtn()
    DiagramUtils.click(dataObject.pattern_diagram.skillsrouter1.nodeLabel)
    NodePropertiesSideBar.openFlow()

    DiagramUtils.findChildNodeByLinkText('Start', dataObject.pattern_diagram.subflow1.linkLabel).then(($el) => {
      expect($el.data.key).equals(dataObject.pattern_diagram.subflow1.nodeLabel)
    })
    DiagramUtils.findChildNodeByLinkText(
      dataObject.pattern_diagram.subflow1.nodeLabel,
      dataObject.pattern_diagram.skillsrouter1.linkLabel,
    ).then(($el) => {
      expect($el.data.key).equals(dataObject.pattern_diagram.skillsrouter1.nodeLabel)
    })
    DiagramUtils.findChildNodeByLinkText(
      dataObject.pattern_diagram.skillsrouter1.nodeLabel,
      dataObject.pattern_diagram.tag1.linkLabel,
    ).then(($el) => {
      expect($el.data.key).equals(dataObject.pattern_diagram.tag1.nodeLabel)
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.pattern_diagram.tag1.nodeLabel, 'default').then(($el) => {
      expect($el.data.key).equals('AddNode2')
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.pattern_diagram.skillsrouter1.nodeLabel, 'routermenu2').then(
      ($el) => {
        expect($el.data.key).equals('AddNode1')
      },
    )
  })
})
