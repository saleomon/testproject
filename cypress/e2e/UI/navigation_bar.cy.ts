import NavigationBar from '../../classes/navigation/NavigationBar'
import { LeftSidebarHrefs } from '../../fixtures/navigation-data/left-sidebar-hrefs'
import { DiscoveryTopBarHrefs } from '../../fixtures/navigation-data/discovery-top-bar-hrefs'
import { DesignerTopBarHrefs } from '../../fixtures/navigation-data/designer-top-bar-hrefs'
import { ModelsTopBarHrefs } from '../../fixtures/navigation-data/models-top-bar-hrefs'
import { DeployTopBarHrefs } from '../../fixtures/navigation-data/deploy-top-bar-hrefs'
import { AnalyticsTopBarHrefs } from '../../fixtures/navigation-data/analytics-top-bar-hrefs'
import { ControlPanelTopBarHrefs } from '../../fixtures/navigation-data/control-panel-top-bar-hrefs'

import { ProjectInfo } from '../../types/response/ProjectInfo'

describe('Navigation test', () => {
  beforeEach(() => {
    cy.restoreLocalStorage()

    cy.fixture('projects-templates/qa_autotest.json').as('projects')

    cy.get<ProjectInfo>('@projects').then((json: ProjectInfo) => {
      cy.loginAPI()
      cy.createNewProject(json).then((projectInfo: ProjectInfo) => {
        cy.loginUI()
        NavigationBar.selectProject(projectInfo.label)
      })
    })
  })

  afterEach(() => {
    cy.get<ProjectInfo>('@projects').then((json: ProjectInfo) => {
      cy.removeProject(json.id)
    })
  })

  it('Discovery Tab + top links', () => {
    NavigationBar.clickLeftSidebarButton(LeftSidebarHrefs.discovery)
    NavigationBar.topBarButton(DiscoveryTopBarHrefs.corpus).parent().should('not.be.undefined')
    NavigationBar.topBarButton(DiscoveryTopBarHrefs.corpus).parent().should('be.visible')
    cy.url().should('include', DiscoveryTopBarHrefs.corpus)

    NavigationBar.clickTopBarButton(DiscoveryTopBarHrefs.topics)
    NavigationBar.topBarButton(DiscoveryTopBarHrefs.topics).parent().should('not.be.undefined')
    NavigationBar.topBarButton(DiscoveryTopBarHrefs.topics).parent().should('be.visible')
    cy.url().should('include', DiscoveryTopBarHrefs.topics)

    NavigationBar.clickTopBarButton(DiscoveryTopBarHrefs.corpus)
    NavigationBar.topBarButton(DiscoveryTopBarHrefs.corpus).parent().should('not.be.undefined')
    NavigationBar.topBarButton(DiscoveryTopBarHrefs.corpus).parent().should('be.visible')
    cy.url().should('include', DiscoveryTopBarHrefs.corpus)
  })

  it('Flows Tab + top links', () => {
    NavigationBar.clickLeftSidebarButton(LeftSidebarHrefs.designer)
    cy.url().should('include', DesignerTopBarHrefs.outcomes)

    NavigationBar.clickTopBarButton(DesignerTopBarHrefs.patterns)
    NavigationBar.topBarButton(DesignerTopBarHrefs.patterns).parent().should('not.be.undefined')
    NavigationBar.topBarButton(DesignerTopBarHrefs.patterns).parent().should('be.visible')
    cy.url().should('include', DesignerTopBarHrefs.patterns)

    NavigationBar.clickTopBarButton(DesignerTopBarHrefs.flows)
    NavigationBar.topBarButton(DesignerTopBarHrefs.flows).parent().should('not.be.undefined')
    NavigationBar.topBarButton(DesignerTopBarHrefs.flows).parent().should('be.visible')
    cy.url().should('include', DesignerTopBarHrefs.flows)

    NavigationBar.clickTopBarButton(DesignerTopBarHrefs.content)
    NavigationBar.topBarButton(DesignerTopBarHrefs.content).parent().should('not.be.undefined')
    NavigationBar.topBarButton(DesignerTopBarHrefs.content).parent().should('be.visible')
    cy.url().should('include', DesignerTopBarHrefs.content)

    NavigationBar.clickTopBarButton(DesignerTopBarHrefs.outcomes)
    NavigationBar.topBarButton(DesignerTopBarHrefs.outcomes).parent().should('not.be.undefined')
    NavigationBar.topBarButton(DesignerTopBarHrefs.outcomes).parent().should('be.visible')
    cy.url().should('include', DesignerTopBarHrefs.outcomes)
  })

  it('Models Tab + top links', () => {
    NavigationBar.clickLeftSidebarButton(LeftSidebarHrefs.models)

    NavigationBar.topBarButton(ModelsTopBarHrefs.models).parent().should('not.be.undefined')
    NavigationBar.topBarButton(ModelsTopBarHrefs.models).parent().should('be.visible')
    cy.url().should('include', ModelsTopBarHrefs.models)
  })

  it('Deploy Tab + top links', () => {
    NavigationBar.clickLeftSidebarButton(LeftSidebarHrefs.deploy)
    NavigationBar.topBarButton(DeployTopBarHrefs.mapper).parent().should('not.be.undefined')
    NavigationBar.topBarButton(DeployTopBarHrefs.mapper).parent().should('be.visible')
    cy.url().should('include', DeployTopBarHrefs.mapper)

    NavigationBar.clickTopBarButton(DeployTopBarHrefs.requirements)
    NavigationBar.topBarButton(DeployTopBarHrefs.requirements).parent().should('not.be.undefined')
    NavigationBar.topBarButton(DeployTopBarHrefs.requirements).parent().should('be.visible')
    cy.url().should('include', DeployTopBarHrefs.requirements)
  })

  it('Analytics Tab + top links', () => {
    NavigationBar.clickLeftSidebarButton(LeftSidebarHrefs.analytics)
    NavigationBar.topBarButton(AnalyticsTopBarHrefs.performance).parent().should('not.be.undefined')
    NavigationBar.topBarButton(AnalyticsTopBarHrefs.performance).parent().should('be.visible')
    cy.url().should('include', AnalyticsTopBarHrefs.performance)
  })

  it('Control Panel Tab + top links', () => {
    NavigationBar.clickLeftSidebarButton(LeftSidebarHrefs.controlPanel)
    NavigationBar.topBarButton(ControlPanelTopBarHrefs.users).parent().should('not.be.undefined')
    NavigationBar.topBarButton(ControlPanelTopBarHrefs.users).parent().should('be.visible')
    cy.url().should('include', ControlPanelTopBarHrefs.projects)

    NavigationBar.clickTopBarButton(ControlPanelTopBarHrefs.users)
    NavigationBar.topBarButton(ControlPanelTopBarHrefs.projects).parent().should('not.be.undefined')
    NavigationBar.topBarButton(ControlPanelTopBarHrefs.projects).parent().should('be.visible')
    cy.url().should('include', ControlPanelTopBarHrefs.users)
  })
})
