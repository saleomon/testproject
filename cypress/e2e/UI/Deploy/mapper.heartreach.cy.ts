import NavigationBar from '../../../classes/navigation/NavigationBar'
import Mapper from '../../../classes/deploy-pages/Mapper'
import FilterBar from '../../../classes/bars/FilterBar'
import GenerateFlowsPopUp from '../../../classes/popups/GenerateFlowsPopUp'
import ConfigureHeartReachPopUp from '../../../classes/popups/ConfigureHeartReachPopUp'
import { ProjectInfo } from '../../../types/response/ProjectInfo'

describe('HeartReach', () => {
  beforeEach(() => {
    cy.restoreLocalStorage()

    cy.newProject('empty')
  })

  afterEach(() => {
    cy.get<ProjectInfo>('@current-project').then((json: ProjectInfo) => {
      cy.removeProject(json.id)
    })
  })

  it('[CMTS-15396] To validate the toast message is displayed after addingh new valid Vendor Environment Account', () => {
    NavigationBar.clickOnRocketBtn()
    FilterBar.clickGenerateFlow()
    GenerateFlowsPopUp.clickOnSettingsIconBtn()
    ConfigureHeartReachPopUp.setPdeUrlField('https://discovery.deloitte-corp.api.onereach.ai')
      .setAccountField('Templatestautomation')
      .setTokenField(
        'AAABogECAQB45rbZDpSPnVfntrh+EjeuF0mdDw45yrNMoRticvz3tloB5NQnNG7ZjFNITiyI0fxF8QAAAWgwggFkBgkqhkiG9w0BBwagggFVMIIBUQIBADCCAUoGCSqGSIb3DQEHATAeBglghkgBZQMEAS4wEQQM2DNw7DpC8tAYsW0tAgEQgIIBG1DhA4ExDjMYTN1f76OmNHfxDIXvIRepYrLq40iehUyGNqUE3Nz7jMJseuZbdMUqfzKUQpxAmp4SDGZNRkaWy+n/70e8H/lyOkImZBQaD3soX2/GNu1zJt6++t+O44uZLaJQYe24Grx1Z4ObYtDKZ1nuggue0fOQBOSDYP9FsYYSh5YjVdm2sO4bQpnRbsJ//FkUIL87FpOunZF2e6bDfZ41mVSQd5V/t+jiF4BjDzMDZgfiXD58m2GlYLfbPQWaedUJjSwJEXK/f3TNa9PafwN/flQnffbUuD/naNjmObu3FERCoKAp6it+7gk6SRv+AxZM1UR3SOIEAa2N4kpHb+tM2zaqwU/wd2aCHrj/BPItlQeHcH2377wsj00AAAFEZXlKaGJHY2lPaUpJVXpJMU5pSXNJblI1Y0NJNklrcFhWQ0o5LmV5SmhZMk52ZFc1MFNXUWlPaUl3Tnpaa00yRTJZUzAwTXpBeExUUTRZV0l0WWpBNE5TMDJPVFpqTnpneFlUQXhNV0lpTENKMWMyVnlTV1FpT2lJM01HTmpObVE0TWkwMk5qTTRMVFJtTmpVdE9EUmxZaTAxTVdWaU1UYzNNMlF5WkRRaUxDSnliMnhsSWpvaVZWTkZVaUlzSW1GMWRHaFViMnRsYmlJNklqYzBaV1JpTm1WbExXVTVOV0V0TkRCbVlpMWhaalkxTFdRMk56WXlNbUppWmpnd1lpSXNJbWxoZENJNk1UWTNNems0TlRJeU5uMC5YbFRJT2M4V0VmQTBwQzloSEsyVkZneDBEajVFb1dJeUVHWWNqNlVJckJz',
      )
      .clickAddBtn()
      .checkMessageInvalidToken('Invalid Token')
  })

  it('[CMTS-15397] To validate it is possible to Deploy the flow with new added valid Account', () => {
    NavigationBar.clickOnRocketBtn()
    FilterBar.clickGenerateFlow()
    GenerateFlowsPopUp.clickOnSettingsIconBtn()
    ConfigureHeartReachPopUp.setPdeUrlField('https://discovery.deloitte-corp.api.onereach.ai')
      .setAccountField('Templatestautomation')
      .setTokenField(
        'AAABogECAQB45rbZDpSPnVfntrh+EjeuF0mdDw45yrNMoRticvz3tloB5NQnNG7ZjFNITiyI0fxF8QAAAWgwggFkBgkqhkiG9w0BBwagggFVMIIBUQIBADCCAUoGCSqGSIb3DQEHATAeBglghkgBZQMEAS4wEQQM2DNw7DpC8tAYsW0tAgEQgIIBG1DhA4ExDjMYTN1f76OmNHfxDIXvIRepYrLq40iehUyGNqUE3Nz7jMJseuZbdMUqfzKUQpxAmp4SDGZNRkaWy+n/70e8H/lyOkImZBQaD3soX2/GNu1zJt6++t+O44uZLaJQYe24Grx1Z4ObYtDKZ1nuggue0fOQBOSDYP9FsYYSh5YjVdm2sO4bQpnRbsJ//FkUIL87FpOunZF2e6bDfZ41mVSQd5V/t+jiF4BjDzMDZgfiXD58m2GlYLfbPQWaedUJjSwJEXK/f3TNa9PafwN/flQnffbUuD/naNjmObu3FERCoKAp6it+7gk6SRv+AxZM1UR3SOIEAa2N4kpHb+tM2zaqwU/wd2aCHrj/BPItlQeHcH2377wsj00AAAFEZXlKaGJHY2lPaUpJVXpJMU5pSXNJblI1Y0NJNklrcFhWQ0o5LmV5SmhZMk52ZFc1MFNXUWlPaUl3Tnpaa00yRTJZUzAwTXpBeExUUTRZV0l0WWpBNE5TMDJPVFpqTnpneFlUQXhNV0lpTENKMWMyVnlTV1FpT2lJM01HTmpObVE0TWkwMk5qTTRMVFJtTmpVdE9EUmxZaTAxTVdWaU1UYzNNMlF5WkRRaUxDSnliMnhsSWpvaVZWTkZVaUlzSW1GMWRHaFViMnRsYmlJNklqYzBaV1JpTm1WbExXVTVOV0V0TkRCbVlpMWhaalkxTFdRMk56WXlNbUppWmpnd1lpSXNJbWxoZENJNk1UWTNNems0TlRJeU5uMC5YbFRJT2M4V0VmQTBwQzloSEsyVkZneDBEajVFb1dJeUVHWWNqNlVJckJz',
      )
      .clickAddBtn()
      .checkMessageInvalidToken('Invalid Token')
  })

  it('CMTS-15398 To validate all the fields are mandatory inside the Configure HeartReach with vendor pop up', () => {
    NavigationBar.clickOnRocketBtn()
    FilterBar.clickGenerateFlow()
    GenerateFlowsPopUp.clickOnSettingsIconBtn()
    ConfigureHeartReachPopUp.checkPdeUrlField()
      .checkAccountField()
      .checkTokenField()
      .checkAddBtn()
      .checkCloseBtn()
      .clickAddBtn()
      .checkErrorMessage('Please provide PDE URL')
    ConfigureHeartReachPopUp.checkErrorMessage('Please input a value for Token')
    ConfigureHeartReachPopUp.checkErrorMessage('Please input a value for Account')
  })

  it('[CMTS-15399] Configure HeartReach with vendor pop up: To validate all the highlighted mandatory fields becomes not highligted after entering the valid data', () => {
    NavigationBar.clickOnRocketBtn()
    FilterBar.clickGenerateFlow()
    GenerateFlowsPopUp.clickOnSettingsIconBtn()
    ConfigureHeartReachPopUp.checkPdeUrlField()
      .checkAccountField()
      .checkTokenField()
      .checkAddBtn()
      .checkCloseBtn()
      .clickAddBtn()
      .checkColorAccount(`rgb(245, 34, 45)`)
      .checkColorPde(`rgb(245, 34, 45)`)
      .checkColorToken(`rgb(245, 34, 45)`)
      .setPdeUrlField('https://discovery.deloitte-corp.api.onereach.ai')
      .setTokenField(
        'AAABogECAQB45rbZDpSPnVfntrh+EjeuF0mdDw45yrNMoRticvz3tloB5NQnNG7ZjFNITiyI0fxF8QAAAWgwggFkBgkqhkiG9w0BBwagggFVMIIBUQIBADCCAUoGCSqGSIb3DQEHATAeBglghkgBZQMEAS4wEQQM2DNw7DpC8tAYsW0tAgEQgIIBG1DhA4ExDjMYTN1f76OmNHfxDIXvIRepYrLq40iehUyGNqUE3Nz7jMJseuZbdMUqfzKUQpxAmp4SDGZNRkaWy+n/70e8H/lyOkImZBQaD3soX2/GNu1zJt6++t+O44uZLaJQYe24Grx1Z4ObYtDKZ1nuggue0fOQBOSDYP9FsYYSh5YjVdm2sO4bQpnRbsJ//FkUIL87FpOunZF2e6bDfZ41mVSQd5V/t+jiF4BjDzMDZgfiXD58m2GlYLfbPQWaedUJjSwJEXK/f3TNa9PafwN/flQnffbUuD/naNjmObu3FERCoKAp6it+7gk6SRv+AxZM1UR3SOIEAa2N4kpHb+tM2zaqwU/wd2aCHrj/BPItlQeHcH2377wsj00AAAFEZXlKaGJHY2lPaUpJVXpJMU5pSXNJblI1Y0NJNklrcFhWQ0o5LmV5SmhZMk52ZFc1MFNXUWlPaUl3Tnpaa00yRTJZUzAwTXpBeExUUTRZV0l0WWpBNE5TMDJPVFpqTnpneFlUQXhNV0lpTENKMWMyVnlTV1FpT2lJM01HTmpObVE0TWkwMk5qTTRMVFJtTmpVdE9EUmxZaTAxTVdWaU1UYzNNMlF5WkRRaUxDSnliMnhsSWpvaVZWTkZVaUlzSW1GMWRHaFViMnRsYmlJNklqYzBaV1JpTm1WbExXVTVOV0V0TkRCbVlpMWhaalkxTFdRMk56WXlNbUppWmpnd1lpSXNJbWxoZENJNk1UWTNNems0TlRJeU5uMC5YbFRJT2M4V0VmQTBwQzloSEsyVkZneDBEajVFb1dJeUVHWWNqNlVJckJz',
      )
      .setAccountField('Templatestautomationone')
      .checkColorPde(`rgb(217, 217, 217)`)
      .checkColorToken(`rgb(217, 217, 217)`)
      .checkColorAccount(`rgb(217, 217, 217)`)
  })

  it('[CMTS-15400] Configure HeartReach with vendor pop up: To validate it is not possible to add new Account with already exist account name', () => {
    NavigationBar.clickOnRocketBtn()
    FilterBar.clickGenerateFlow()
    GenerateFlowsPopUp.clickOnSettingsIconBtn()
    ConfigureHeartReachPopUp.setPdeUrlField('https://discovery.deloitte-corp.api.onereach.ai')
      .setAccountField('Templatestautomation')
      .setTokenField(
        'AAABogECAQB45rbZDpSPnVfntrh+EjeuF0mdDw45yrNMoRticvz3tloB5NQnNG7ZjFNITiyI0fxF8QAAAWgwggFkBgkqhkiG9w0BBwagggFVMIIBUQIBADCCAUoGCSqGSIb3DQEHATAeBglghkgBZQMEAS4wEQQM2DNw7DpC8tAYsW0tAgEQgIIBG1DhA4ExDjMYTN1f76OmNHfxDIXvIRepYrLq40iehUyGNqUE3Nz7jMJseuZbdMUqfzKUQpxAmp4SDGZNRkaWy+n/70e8H/lyOkImZBQaD3soX2/GNu1zJt6++t+O44uZLaJQYe24Grx1Z4ObYtDKZ1nuggue0fOQBOSDYP9FsYYSh5YjVdm2sO4bQpnRbsJ//FkUIL87FpOunZF2e6bDfZ41mVSQd5V/t+jiF4BjDzMDZgfiXD58m2GlYLfbPQWaedUJjSwJEXK/f3TNa9PafwN/flQnffbUuD/naNjmObu3FERCoKAp6it+7gk6SRv+AxZM1UR3SOIEAa2N4kpHb+tM2zaqwU/wd2aCHrj/BPItlQeHcH2377wsj00AAAFEZXlKaGJHY2lPaUpJVXpJMU5pSXNJblI1Y0NJNklrcFhWQ0o5LmV5SmhZMk52ZFc1MFNXUWlPaUl3Tnpaa00yRTJZUzAwTXpBeExUUTRZV0l0WWpBNE5TMDJPVFpqTnpneFlUQXhNV0lpTENKMWMyVnlTV1FpT2lJM01HTmpObVE0TWkwMk5qTTRMVFJtTmpVdE9EUmxZaTAxTVdWaU1UYzNNMlF5WkRRaUxDSnliMnhsSWpvaVZWTkZVaUlzSW1GMWRHaFViMnRsYmlJNklqYzBaV1JpTm1WbExXVTVOV0V0TkRCbVlpMWhaalkxTFdRMk56WXlNbUppWmpnd1lpSXNJbWxoZENJNk1UWTNNems0TlRJeU5uMC5YbFRJT2M4V0VmQTBwQzloSEsyVkZneDBEajVFb1dJeUVHWWNqNlVJckJz',
      )
      .clickAddBtn()
      .checkMessageInvalidToken('Invalid Token')
  })

  it('[CMTS-15402] To validate the toast message is displayed after adding new Account', () => {
    NavigationBar.clickOnRocketBtn()
    FilterBar.clickGenerateFlow()
    GenerateFlowsPopUp.clickOnSettingsIconBtn()
    ConfigureHeartReachPopUp.setPdeUrlField('https://discovery.deloitte-corp.api.onereach.ai')
      .setAccountField('TrueServe Templatestautomation')
      .setTokenField(
        'AAABogECAQB45rbZDpSPnVfntrh+EjeuF0mdDw45yrNMoRticvz3tloB5NQnNG7ZjFNITiyI0fxF8QAAAWgwggFkBgkqhkiG9w0BBwagggFVMIIBUQIBADCCAUoGCSqGSIb3DQEHATAeBglghkgBZQMEAS4wEQQM2DNw7DpC8tAYsW0tAgEQgIIBG1DhA4ExDjMYTN1f76OmNHfxDIXvIRepYrLq40iehUyGNqUE3Nz7jMJseuZbdMUqfzKUQpxAmp4SDGZNRkaWy+n/70e8H/lyOkImZBQaD3soX2/GNu1zJt6++t+O44uZLaJQYe24Grx1Z4ObYtDKZ1nuggue0fOQBOSDYP9FsYYSh5YjVdm2sO4bQpnRbsJ//FkUIL87FpOunZF2e6bDfZ41mVSQd5V/t+jiF4BjDzMDZgfiXD58m2GlYLfbPQWaedUJjSwJEXK/f3TNa9PafwN/flQnffbUuD/naNjmObu3FERCoKAp6it+7gk6SRv+AxZM1UR3SOIEAa2N4kpHb+tM2zaqwU/wd2aCHrj/BPItlQeHcH2377wsj00AAAFEZXlKaGJHY2lPaUpJVXpJMU5pSXNJblI1Y0NJNklrcFhWQ0o5LmV5SmhZMk52ZFc1MFNXUWlPaUl3Tnpaa00yRTJZUzAwTXpBeExUUTRZV0l0WWpBNE5TMDJPVFpqTnpneFlUQXhNV0lpTENKMWMyVnlTV1FpT2lJM01HTmpObVE0TWkwMk5qTTRMVFJtTmpVdE9EUmxZaTAxTVdWaU1UYzNNMlF5WkRRaUxDSnliMnhsSWpvaVZWTkZVaUlzSW1GMWRHaFViMnRsYmlJNklqYzBaV1JpTm1WbExXVTVOV0V0TkRCbVlpMWhaalkxTFdRMk56WXlNbUppWmpnd1lpSXNJbWxoZENJNk1UWTNNems0TlRJeU5uMC5YbFRJT2M4V0VmQTBwQzloSEsyVkZneDBEajVFb1dJeUVHWWNqNlVJckJz',
      )
      .clickAddBtn()
      .checkMessageInvalidToken('Invalid Token')
  })

  it('[CMTS-15401] Configure HeartReach with vendor pop up: To validate it is possible to close pop up after click on the any screen area', () => {
    NavigationBar.clickOnRocketBtn()
    FilterBar.clickGenerateFlow()
    GenerateFlowsPopUp.clickOnSettingsIconBtn()
    ConfigureHeartReachPopUp.checkAccountField().checkTokenField().checkPdeUrlField().checkAddBtn().checkCloseBtn()
    Mapper.clickOnOutput()
  })
})
