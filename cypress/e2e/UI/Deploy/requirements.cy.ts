import { ProjectInfo } from '../../../types/response/ProjectInfo'
import NavigationBar from '../../../classes/navigation/NavigationBar'
import Requirements from '../../../classes/deploy-pages/Requirements'
import { LeftSidebarHrefs } from '../../../fixtures/navigation-data/left-sidebar-hrefs'
import { DeployTopBarHrefs } from '../../../fixtures/navigation-data/deploy-top-bar-hrefs'
import flowsOnRequirementsTab from '../../../fixtures/data/deploy-requirements-tab/table-data-from-node-details-prj'
import flowsForQaAutotestsPrj from '../../../fixtures/data/deploy-requirements-tab/table-data-qa-autotest-prj'
import Table from '../../../classes/tables/Table'
import FilterBar from '../../../classes/bars/FilterBar'

let flowInfo: object
let qaAutotestInfo: object
let qaAutotestProjectName: string

describe('[CMTS-11654] - Requirements page', () => {
  let tabs = ['Output', 'Input', 'Flow', 'Flow Starts', 'Endpoints']
  beforeEach(() => {
    cy.restoreLocalStorage()

    cy.fixture('projects-templates/node-details-spec-project.json').as('projects')
    cy.fixture('projects-templates/qa_autotest.json').as('qaAutotestProject')

    cy.get<ProjectInfo>('@qaAutotestProject').then((json: ProjectInfo) => {
      cy.loginAPI()
      cy.createNewProject(json).then(() => {
        cy.reload()
        qaAutotestProjectName = json.label
        qaAutotestInfo = flowsForQaAutotestsPrj(json.label.replace(/-/g, ''))
      })
    })

    cy.get<ProjectInfo>('@projects').then((json: ProjectInfo) => {
      cy.createNewProject(json).then((projectInfo: ProjectInfo) => {
        cy.loginUI()
        NavigationBar.selectProject(projectInfo.label)
        flowInfo = flowsOnRequirementsTab(json.label.replace(/-/g, ''))
      })
      NavigationBar.clickLeftSidebarButton(LeftSidebarHrefs.deploy)
      NavigationBar.clickTopBarButton(DeployTopBarHrefs.requirements)
    })
  })

  afterEach(() => {
    cy.get<ProjectInfo>('@projects').then((json: ProjectInfo) => {
      cy.removeProject(json.id)
    })
    cy.get<ProjectInfo>('@qaAutotestProject').then((json: ProjectInfo) => {
      cy.removeProject(json.id)
    })
  })

  tabs.forEach(function (tab: string) {
    it(`User should be able to open [${tab}] tab and check table information`, () => {
      Requirements.openTabByName(tab)
      Table.expandAllTableRows()
      // @ts-ignore
      Table.checkTableInfo(Requirements.requirementsTableSelector, flowInfo[tab])
    })
  })

  it('User should be able to drop column name to filter and table would be updated', () => {
    Requirements.openTabByName('Output')
    Requirements.dragColumnAsFilter(Table.columnHeaderByName('Element Type'))
    Table.expandAllTableRows()
    Table.expandAllTableRows()
    // @ts-ignore
    Table.checkTableInfo(Requirements.requirementsTableSelector, flowInfo['Output Filtered'])
  })

  it('User should be able to remove group from drop zone', () => {
    Requirements.removeGroupFromDropArea('Node')
    Requirements.dropZoneEmptyMessageIsVisible()
  })

  it('Table should be updated after pattern changing', () => {
    NavigationBar.selectProject(qaAutotestProjectName)
    // @ts-ignore
    Table.checkTableInfo(Requirements.requirementsTableSelector, qaAutotestInfo['Pattern - empty, All Flows'])
    FilterBar.selectPattern('subflow-prompt')
    // @ts-ignore
    Table.checkTableInfo(Requirements.requirementsTableSelector, qaAutotestInfo['Pattern - subflow-prompt, SubFlow1'])
  })

  it('Table should be updated after flow changing', () => {
    NavigationBar.selectProject(qaAutotestProjectName)
    FilterBar.selectPattern('subflow-prompt')
    // @ts-ignore
    Table.checkTableInfo(Requirements.requirementsTableSelector, qaAutotestInfo['Pattern - subflow-prompt, SubFlow1'])
    FilterBar.selectFlow('All Flows')
    // @ts-ignore
    Table.checkTableInfo(Requirements.requirementsTableSelector, qaAutotestInfo['Pattern - subflow-prompt, All Flows'])
  })

  it('Table should reflect data from all flows if [All Flows] is selected', () => {
    NavigationBar.selectProject(qaAutotestProjectName)
    FilterBar.selectPattern('subflow-prompt')
    FilterBar.selectFlow('All Flows')
    Table.expandAllTableRows()
    Table.checkTableInfo(
      Requirements.requirementsTableSelector,
      // @ts-ignore
      qaAutotestInfo['Pattern - subflow-prompt, All Flows (expanded)'],
    )
  })
})
