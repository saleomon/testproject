import { dataObject } from '../../fixtures/data/data-spec-filterbar-dropdowns'
import FilterBar from '../../classes/bars/FilterBar'
import NavigationBar from '../../classes/navigation/NavigationBar'
import { LeftSidebarHrefs } from '../../fixtures/navigation-data/left-sidebar-hrefs'
import { ProjectInfo } from '../../types/response/ProjectInfo'

let createdProjectInfo = {}

describe('FilterBar dropdowns test (Flows Page)', () => {
  before(() => {
    cy.restoreLocalStorage()

    cy.fixture('projects-templates/filter-bar-spec-project.json').as('project')

    cy.get<ProjectInfo>('@project').then((json: ProjectInfo) => {
      cy.loginAPI()
      cy.createNewProject(json).then((projectInfo: ProjectInfo) => {
        cy.loginUI()
        Cypress.env('projectName', projectInfo.label) // to input project name as the part of variable 'tag description'
        NavigationBar.selectProject(projectInfo.label)
        NavigationBar.openFlowsPage()
        FilterBar.selectPattern('TestPatternName')
      })
    })

  })

  after(() => {
    cy.get<ProjectInfo>('@project').then((json: ProjectInfo) => {
      cy.removeProject(json.id)
    })
  })

  it('Verify Flows and Channels dropdowns', () => {
    let subflow1Channels = dataObject.subflow1.channels
    let subflow2Channels = dataObject.subflow2.channels

    // subflow1 assertions
    FilterBar.selectFlow(dataObject.subflow1.nodeLabel)
    FilterBar.openChannelDropdown()
    cy.get('.ant-dropdown.ant-dropdown-placement-bottomLeft').then(() => {
      for (let i = 0; i < subflow1Channels.length; i++) {
        cy.get('[role="menuitem"]')
          .contains(subflow1Channels[i])
          .then(($item) => {
            cy.wrap($item).should('not.be.undefined')
          })
      }
    })

    // subflow2 assertions
    FilterBar.selectFlow(dataObject.subflow2.nodeLabel)
    FilterBar.openChannelDropdown()
    cy.get('.ant-dropdown.ant-dropdown-placement-bottomLeft').then(() => {
      for (let i = 0; i < subflow2Channels.length; i++) {
        cy.get('[role="menuitem"]')
          .contains(subflow2Channels[i])
          .then(($item) => {
            cy.wrap($item).should('not.be.undefined')
          })
      }
    })
  })
})

describe('[CMTS-60] Automate search by flow', () => {
  beforeEach(function () {
    cy.fixture('projects-templates/test-flow-project.json').then(($json) => {
      cy.createProject(Cypress.env('baseUrlAPI'), Cypress.env('username'), Cypress.env('password'), $json).then(
        ($projectInfo) => {
          createdProjectInfo = $projectInfo
          cy.UILogin(Cypress.env('username'), Cypress.env('password'))
          cy.selectProject(createdProjectInfo.label)
          cy.get('div').contains(createdProjectInfo.label).should('be.visible')
          NavigationBar.clickLeftSidebarButton(LeftSidebarHrefs.analytics)
        },
      )
    })
  })


  it('Check search field with Lower/Upper case letter', () => {
    FilterBar.selectFlow('First')
    FilterBar.searchFlow('TEst')
    FilterBar.checkFlowSearchResult('test1')
  })

  it('Flow search field available with current value', () => {
    FilterBar.selectFlow('Second')
    FilterBar.searchFlow('test1')
    FilterBar.checkFlowSearchResult('test1')
  })

  it('Check if all sub-flow items are highlighted in bold ', () => {
    FilterBar.searchFlow('test1')
    cy.get('.ant-select-tree-title').should(($labels) => {
      expect($labels).to.have.css('font-weight', '400')
    })
  })

  it('Message No Data', () => {
    FilterBar.selectFlow('First')
    FilterBar.searchFlow('invalid')
    cy.get('.ant-empty-description').contains('No Data').should('be.visible')
  })

  it('Check All Flows search', () => {
    FilterBar.searchFlow('All Flows')
    FilterBar.checkFlowSearchResult('All Flows')
  })

  afterEach(function () {
    cy.deleteProject(Cypress.env('baseUrlAPI'), Cypress.env('username'), Cypress.env('password'), createdProjectInfo.id)
  })
})
