/* //------------CONTENTS------------------
//This spec contains follwinf specs: 
// - build pattern diagram 
// - build flow diagram
// - detach a labeled node
import DialogModal from '../../classes/DialogModal'
import PaletteBox from '../../classes/PaletteBox'
import { dataObject } from '../../fixtures/data/diagramOperationsSpecData'

before(function () {
  cy.UILogin(Cypress.env('username'), Cypress.env('password'))
  cy.get('.topLink').contains('Patterns').should('be.visible').click()
  cy.url().should('contain', '/patterns')
  Cypress.LocalStorage.clear = function (keys, ls, rs) {
    if (keys) {
    }
  }
})

beforeEach(function () {
   // restore localstorage
  cy.restoreLocalStorage()
})

describe(' Build pattern diagram by drag&drop', () => {

      it("Select project", () => {
            let project = 'test-project'
            cy.selectProject(project)
            cy.get('div').contains(project).should('be.visible')
      })
      
      it("Select a pattern", () => {
            let pattern = 'general'
            cy.selectPattern(pattern)
            cy.get('.ant-select-selection-selected-value').first().should('contain', pattern)
      })

      it('Build a pattern tree ', () => {
            let palleteBox = new PaletteBox()
            let dialogModal = new DialogModal()
            //add Subflow node
            cy.addNodeIntoDiagram(palleteBox.subflowNodeIdSelector, 'AddNode1')
            cy.fillSubflowNode(dataObject.subflow)
            dialogModal.addButton().click()

            //add SkillRouter node
            cy.addNodeIntoDiagram(palleteBox.skillRouterNodeIdLSelector, dataObject.subflow.nodeLabel)
            cy.fillSkillsRouterNode(dataObject.skillsrouter)
            dialogModal.addButton().click()

            //add Tag node
            cy.addNodeIntoDiagram(palleteBox.tagNodeIdSelector, dataObject.skillsrouter.nodeLabel)
            cy.fillTagNode(dataObject.tag)
            dialogModal.addButton().click()

            //add Goto node
            cy.addNodeIntoDiagram(palleteBox.goToNodeIdSelector, dataObject.tag.nodeLabel)
            cy.fillGotoNode(dataObject.goto)
            dialogModal.addButton().click()
            cy.wait(3000)

            //add Outcome node
            cy.getOutcomeNodeByText("Fallback").then($fallback => {
                  cy.addNodeIntoDiagram($fallback, dataObject.subflow.nodeLabel)
                  cy.fillOutcomeNode(dataObject.outcome)
                  dialogModal.addButton().click()
            })

            //assertions on Start node
            cy.getChildNodeByLinkText("Start", dataObject.subflow.linkLabel).then($node => {
                  expect($node.data.key).equals(dataObject.subflow.nodeLabel)
            })
            cy.findLinksOutOf("Start").then($links => {
                  expect($links.length).equals(1)
            })
            //assertions on Subflow node
            cy.getChildNodeByLinkText(dataObject.subflow.nodeLabel, dataObject.skillsrouter.linkLabel).then($node => {
                  expect($node.data.key).equals(dataObject.skillsrouter.nodeLabel)
            })
            cy.findLinksOutOf(dataObject.subflow.nodeLabel).then($links => {
                  expect($links.length).equals(2)
            })
            //assertions on SkillRouter node
            cy.getChildNodeByLinkText(dataObject.skillsrouter.nodeLabel, dataObject.tag.linkLabel).then($node => {
                  expect($node.data.key).equals(dataObject.tag.nodeLabel)
            })
            cy.findLinksOutOf(dataObject.skillsrouter.nodeLabel).then($links => {
                  expect($links.length).equals(2)
            })
            //assertions on NewTag node
            cy.getChildNodeByLinkText(dataObject.tag.nodeLabel, dataObject.goto.linkLabel).then($node => {
                  expect($node.data.type).equals("gotoLink")
            })
            cy.findLinksOutOf(dataObject.tag.nodeLabel).then($links => {
                  expect($links.length).equals(2)
            })

            //assertions on Fallback global outcome
            cy.getChildNodeByLinkText(dataObject.subflow.nodeLabel, dataObject.outcome.endpointLabel).then($node => {
                  expect($node.data.type).equals("Global")
                  expect($node.data.nodeLabel).equals("Fallback")

            })
            
            cy.findLinksOutOf(dataObject.tag.nodeLabel).then($links => {
                  expect($links.length).equals(2)
            })

    // it is mandatorty to add verification of an AddPoint node -> should be length === 0
  })

  // it('Open Subflow node flow ', () => {
  //       cy.dblClickNode("NewSubflow")
  //             .dblClickNode("NewSubflow")
  //             .url().should('contain', '/nodes')
  //             .closePropertiesRail()
  // })

  // it('Build a flow tree', () => {
  //       //     add Prompt node
  //       cy.addNodeIntoDiagram(PageElements.promptNodeIdSelector, '+')
  //       cy.fixture('BuildPatternDiagram/promptNodeData.json').then($data => {
  //             cy.fillByData($data, "Add");
  //       })

  //       //     add openq node
  //       cy.addNodeIntoDiagram(PageElements.openQNodeIdSelector, 'newPrompt')
  //       cy.fixture('BuildPatternDiagram/openQNodeData.json').then($data => {
  //             cy.fillByData($data, "Add");
  //       })

  //       //     add closedq node
  //       cy.addNodeIntoDiagram(PageElements.closedQNodeIdSelector, 'NewOpenQuestion')
  //       cy.fixture('BuildPatternDiagram/closedQNodeData.json').then($data => {
  //             cy.fillByData($data, "Add");
  //       })

  //       //     exit
  //       cy.getOutcomeNodeByText("NewTag").then($exitOutcome => {
  //             cy.addNodeIntoDiagram($exitOutcome, 'NewClosedQuestion')
  //             cy.fixture('BuildPatternDiagram/outcomeNodeData.json').then($data => {
  //                   cy.fillByData($data, "Add");
  //             })
  //       })
  // })

  it('detach the node', () => {
    cy.clickNode(dataObject.tag.nodeLabel)
    cy.linkOffSelectedNode()

    cy.findLinksInto(dataObject.tag.nodeLabel).then(($linksInto) => {
      expect($linksInto.length).equals(0)
    })
    cy.findLinksOutOf(dataObject.tag.nodeLabel).then(($linksOutOf) => {
      expect($linksOutOf.length).equals(2)
    })
    cy.findNodesOutOf(dataObject.tag.nodeLabel).then(($nodesOutOf) => {
      expect($nodesOutOf.length).equals(2)
    })
  })

 
afterEach(function () {
      //preserve local storage after each hook
      cy.saveLocalStorage()
}) */