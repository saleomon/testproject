import { OutcomesPageElements } from '../../../classes/outcomesPage'
import { contentsOutcomes } from '../../../fixtures/data/outcomes_data/outcomesTestData'

// this autotests WIP - wait for clarification and fixing defects
describe.skip('table Skill Patterns', function () {
  before(() => {
    cy.getToken()
    // creating new project
    cy.fixture('metrics-templates/skill-patterns.json').then(($json) => {
      cy.createProjectNew($json).then(($projectInfo) => {
        cy.UILogin(Cypress.env('username'), Cypress.env('password'))
        cy.selectProject($projectInfo.label)
        cy.get('div').contains($projectInfo.label).should('be.visible')
        Cypress.env('projectName', $projectInfo.label)
        Cypress.env('projectId', $projectInfo.id)
      })
    })
  })

  after(() => {
    cy.deleteProjectNew()
    cy.window().then(($win) => {
      $win.localStorage.clear()
    })
  })

  it('CMTS-18-create-new-skill-pattern', { tags: ['@smoke', '@regression'] }, () => {
    cy.get(OutcomesPageElements.openCreateSkillPatternBtn).click()
    cy.get(OutcomesPageElements.skillPatternNameField).clear().type(contentsOutcomes.skillPatternName)
    cy.get(OutcomesPageElements.skillPatternDescriptionField).clear().type(contentsOutcomes.skillPatternDescription)
    cy.multiselect('[title="Channels"]', contentsOutcomes.skillPatternChannels)
    cy.get(OutcomesPageElements.skillPatternAddBtn).click()
    cy.get('.ant-popover-title').contains('Create Skill Pattern').should('not.be.visible')
    cy.checkSkillPatternTable(
      contentsOutcomes.skillPatternName,
      contentsOutcomes.skillPatternChannels,
      contentsOutcomes.skillPatternDescription,
    )
    cy.get(OutcomesPageElements.openCreateSkillOutcomeBtn).should('not.have.attr', 'disabled')
  })

  it.skip('CMTS-22-create-new-skill-outcome', { tags: ['@smoke', '@regression'] }, () => {
    cy.get(OutcomesPageElements.skillPatternTable).contains(contentsOutcomes.skillPatternName).click()
    cy.get(OutcomesPageElements.openCreateSkillOutcomeBtn).click()
    // cy.get(OutcomesPageElements.increaseOutcomePercentage).click()
    cy.fillOutcome(
      contentsOutcomes.skillOutcomeName,
      contentsOutcomes.skillOutcomeDescription,
      contentsOutcomes.skillOutcomePercentage,
      contentsOutcomes.skillOutcomeValue,
      contentsOutcomes.skillOutcomeColor,
    )
    cy.get(OutcomesPageElements.skillOutcomeAddBtn).click()

    // check if skill outcome name is added to table Skill Library
    OutcomesPageElements.checkSkillOutcomesNames(contentsOutcomes.skillOutcomeName)

    // next assertion is skipped due to defect
    // cy.checkSkillOutcomeFields(contentsOutcomes.skillOutcomeName, contentsOutcomes.skillOutcomeDescription, contentsOutcomes.skillOutcomeValue, contentsOutcomes.skillOutcomeColor)
  })

  it.skip('CMTS-23-edit-skill-outcome', () => {
    OutcomesPageElements.getEditBtn(OutcomesPageElements.skillOutcomeTable, contentsOutcomes.skillOutcomeName).click()
    cy.fillOutcome(
      contentsOutcomes.skillOutcomeNameUpd,
      contentsOutcomes.skillOutcomeDescriptionUpd,
      contentsOutcomes.skillOutcomePercentageUpd,
      contentsOutcomes.skillOutcomeValueUpd,
      contentsOutcomes.skillOutcomeColorUpd,
    )
    cy.get(OutcomesPageElements.skillOutcomeAddBtn).click()
  })

  it.skip('CMTS-24-delete-skill-outcome', () => {
    // can not delete skill outcome - defect error 'Failed to delete skill outcome: Pattern does not exist, or it is not a skill!' appears
    OutcomesPageElements.getDeleteBtn(OutcomesPageElements.skillOutcomeTable, contentsOutcomes.skillOutcomeName).click()
    // check if message Deleted outcome appears
    OutcomesPageElements.checkToastMsg(contentsOutcomes.skillOutcomeDeletedMsg)
  })

  it('CMTS-19-edit-skill-pattern', { tags: ['@smoke', '@regression'] }, () => {
    // open modal Edit Skill
    OutcomesPageElements.getEditBtn(OutcomesPageElements.skillPatternTable, 'Skill Pattern to Update').click()
    cy.get(OutcomesPageElements.skillPatternNameField)
      .filter(':visible')
      .clear()
      .type(contentsOutcomes.skillPatternNameUpd)
    cy.get(OutcomesPageElements.skillPatternDescriptionField)
      .filter(':visible')
      .clear()
      .type(contentsOutcomes.skillPatternDescriptionUpd)
    cy.multiselect('[title="Channels"]', contentsOutcomes.skillPatternChannelsUpd)
    cy.get(OutcomesPageElements.skillPatternAddBtn).filter(':visible').click()
    cy.checkSkillPatternTable(
      contentsOutcomes.skillPatternNameUpd,
      contentsOutcomes.skillPatternChannelsUpd,
      contentsOutcomes.skillPatternDescriptionUpd,
    )
    cy.checkToastMsg('Successfully edited skill')
  })

  it('CMTS-25-delete-skill-pattern', { tags: ['@smoke', '@regression'] }, () => {
    OutcomesPageElements.getDeleteBtn(OutcomesPageElements.skillPatternTable, 'Skill Pattern to Delete').click()

    // check if message Deleted outcome appears
    cy.checkToastMsg('Successfully deleted skill')

    // check if outcome is deleted
    cy.get(OutcomesPageElements.skillPatternTable)
      .find(OutcomesPageElements.outcomeRow)
      .contains('Skill Pattern to Delete')
      .should('not.exist')

    /*  // check if button add skill outcome inactive
    cy.get(OutcomesPageElements.openCreateSkillOutcomeBtn).should('have.attr', 'disabled') */

    /* // check if message No Skills ... appears
    cy.get(OutcomesPageElements.skillPatternTable).find('.ag-overlay-no-rows-center').should('exist')
      .and('have.text', contentsOutcomes.noSkillPatternMsg) */
    /*  cy.get(OutcomesPageElements.skillOutcomeTable).find('.ag-overlay-no-rows-center').should('exist')
    .and('have.text', contentsOutcomes.noSkillOutcomesMsg)    */
  })

  it(
    'CMTS-9782-new-channel-should-appears-on-the-page-flows-for-a-particular-sub-flow',
    { tags: ['@regression'] },
    () => {
      // open modal Edit Skill
      OutcomesPageElements.getEditBtn(OutcomesPageElements.skillPatternTable, 'Test Skill Name 9782').click()
      // add new channel for Test Skill Name 9782
      cy.multiselect('[title="Channels"]', contentsOutcomes.skillPatternChannels9782)
      cy.get(OutcomesPageElements.skillPatternAddBtn).filter(':visible').click()
      // open page Patterns
      cy.get('.topLink').contains('Patterns').should('be.visible').click()
      // choose required pattern
      cy.selectPattern('Test Skill Name 9782')
      cy.wait(1000)
      // open content panel
      cy.clickNode('SubFlow')
      // add new channel for subflow
      cy.get('[aria-controls="channelTab"]').should('be.visible').click()
      cy.get('#channelTab .addButton').should('be.visible').click()
      cy.get('.ant-popover')
        .contains('Add Channel')
        .next()
        .find('[role="combobox"]')
        .should('be.visible')
        .click()
        .then(($combobox) => {
          cy.wrap($combobox)
            .attribute('aria-controls')
            .then(($id) => {
              cy.get('#' + $id)
                .find('li')
                .contains('RWC')
                .click()
            })
        })
      cy.get('.ant-popover').contains('Add Channel').next().find('.ant-btn-primary').click()
      // open page Flows
      cy.get('.topLink').contains('Flows').should('be.visible').click()
      // Check if newly added channel for Subflow appears in select Channel on page Flow
      cy.get('#channelFilter .filterButton').should('be.visible').click()
      cy.get('.ant-dropdown.ant-dropdown-placement-bottomLeft').then(($dropdown) => {
        for (let i = 0; i < contentsOutcomes.skillPatternChannels9782.length; i++) {
          cy.get('[role="menuitem"]')
            .contains(contentsOutcomes.skillPatternChannels9782[i])
            .then(($item) => {
              cy.wrap($item).should('not.be.undefined')
            })
        }
      })
    },
  )

  describe('CMTS-141-test-pack-for-skill-library-validations', { tags: '@regression' }, () => {
    before(() => {
      // open page Outcomes
      cy.get('.topLink').contains('Outcomes').should('be.visible').click()
    })

    it('check if all required validation messages appear after clicking on the button Add', () => {
      // open modal Create Skill Pattern
      cy.get(OutcomesPageElements.openCreateSkillPatternBtn).click()
      // check if all required validation messages appear after clicking on the button 'Add'
      cy.get(OutcomesPageElements.addGlobalPatternBtn).click()
      OutcomesPageElements.checkValidationError(
        OutcomesPageElements.skillPatternNameLabel,
        contentsOutcomes.emptyFieldErrMsg,
        OutcomesPageElements.skillPatternNameField,
        '',
        true,
      )
      OutcomesPageElements.checkValidationErrorSelect(
        OutcomesPageElements.slillPatternchannelsLbl,
        contentsOutcomes.channelsRequiredErrMsg,
      )
      // close modal Create Skill Pattern
      cy.get(OutcomesPageElements.openCreateSkillPatternBtn).click()
    })

    it('check if message Please dont use slashes. appears', () => {
      // open modal Create Skill Pattern
      cy.get(OutcomesPageElements.openCreateSkillPatternBtn).click()
      // check if message Please don't use slashes. appears
      OutcomesPageElements.checkValidationError(
        OutcomesPageElements.skillPatternNameLabel,
        contentsOutcomes.slashesPresentErrMsg,
        OutcomesPageElements.skillPatternNameField,
        contentsOutcomes.twoSlashes,
        true,
      )
      // close modal Create Skill Pattern
      cy.get(OutcomesPageElements.openCreateSkillPatternBtn).click()
    })

    it('check if message Length should be 2 to 32 chars appears if input one symbol', () => {
      // open modal Create Skill Pattern
      cy.get(OutcomesPageElements.openCreateSkillPatternBtn).click()
      // check if message Length should be 2 to 32 chars appears
      OutcomesPageElements.checkValidationError(
        OutcomesPageElements.skillPatternNameLabel,
        contentsOutcomes.lengthLimitErrMsg,
        OutcomesPageElements.skillPatternNameField,
        contentsOutcomes.oneLetter,
        true,
      )
      // close modal Create Skill Pattern
      cy.get(OutcomesPageElements.openCreateSkillPatternBtn).click()
    })

    it('CMTS-8435-to-validate-the-name-with-33-characters-including-the-space-in-the-end-is-not-valid-skill', () => {
      // open modal Create Skill Pattern
      cy.get(OutcomesPageElements.openCreateSkillPatternBtn).click()
      // check if message Length should be 2 to 32 chars appears
      OutcomesPageElements.checkValidationError(
        OutcomesPageElements.skillPatternNameLabel,
        contentsOutcomes.lengthLimitErrMsg,
        OutcomesPageElements.skillPatternNameField,
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut aliqu ',
        true,
      )
      // close modal Create Skill Pattern
      cy.get(OutcomesPageElements.openCreateSkillPatternBtn).click()
    })

    it('check if message Please dont use slashes. appears if input backslashes', () => {
      // open modal Create Skill Pattern
      cy.get(OutcomesPageElements.openCreateSkillPatternBtn).click()
      // check if message Please don't use slashes. appears
      OutcomesPageElements.checkValidationError(
        OutcomesPageElements.skillPatternNameLabel,
        contentsOutcomes.slashesPresentErrMsg,
        OutcomesPageElements.skillPatternNameField,
        contentsOutcomes.twoBackslashes,
        true,
      )
      // close modal Create Skill Pattern
      cy.get(OutcomesPageElements.openCreateSkillPatternBtn).click()
    })

    it('check if search by channels works', () => {
      // open modal Create Skill Pattern
      cy.get(OutcomesPageElements.openCreateSkillPatternBtn).click()
      // check if search by channels works
      OutcomesPageElements.checkSearchMultiselect(
        OutcomesPageElements.globalPatternChannelsSelect,
        'r',
        contentsOutcomes.channelsContainsR,
      )
      // close modal Create Skill Pattern
      cy.get(OutcomesPageElements.openCreateSkillPatternBtn).click()
    })

    it('check if all validation messages disappear after clicking on the button Reset', () => {
      // open modal Create Skill Pattern
      cy.get(OutcomesPageElements.openCreateSkillPatternBtn).click()
      // check if all validation messages disappear after clicking on the button 'Reset'
      Cypress._.times(2, () => {
        cy.contains('Reset').should('be.visible').dblclick()
      })
      cy.get(OutcomesPageElements.validationMsg).should('not.exist')
      // close modal Create Skill Pattern
      cy.get(OutcomesPageElements.openCreateSkillPatternBtn).click()
    })

    it('check if image No Data appears if input data does not fit any channel', () => {
      // open modal Create Skill Pattern
      cy.get(OutcomesPageElements.openCreateSkillPatternBtn).click()
      // check if image No Data appears if input data does not fit any channel
      OutcomesPageElements.checksearchNoData(OutcomesPageElements.globalPatternChannelsSelect, 'abc')
      // close modal Create Skill Pattern
      cy.get(OutcomesPageElements.openCreateSkillPatternBtn).click()
    })

    it('check if can not create skill pattern with existing skill pattern name', () => {
      // open modal Create Skill Pattern
      cy.get(OutcomesPageElements.openCreateSkillPatternBtn).click()
      // check if message Label is not unique! appears
      OutcomesPageElements.checkValidationError(
        OutcomesPageElements.skillPatternNameLabel,
        contentsOutcomes.notUniqueErrMsg,
        OutcomesPageElements.skillPatternNameField,
        contentsOutcomes.skillPatternNameUnique,
        true,
      )
      // close modal Create Skill Pattern
      cy.get(OutcomesPageElements.openCreateSkillPatternBtn).click()
    })
  })
})
