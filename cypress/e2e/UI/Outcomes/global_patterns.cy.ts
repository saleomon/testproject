import PaletteBox from '../../../classes/PaletteBox'
import AddNewSubFlowNodePopUp from '../../../classes/popups/AddNewSubFlowNodePopUp'
import DiagramBar from '../../../classes/bars/DiagramBar'
import NavigationBar from '../../../classes/navigation/NavigationBar'
import FilterBar from '../../../classes/bars/FilterBar'
import AddNewGlobalPatternPopover from '../../../classes/popovers/outcomes/AddGlobalPatternPopOver'
import GlobalInteractionPatternTable from '../../../classes/outcomes/GlobalInteractionPatternTable'
import SkillPatternTable from '../../../classes/outcomes/SkillPatternTable'
import DiagramUtils from '../../../support/DiagramUtils'
import { OutcomesPageElements } from '../../../classes/outcomesPage'
import { dataObject } from '../../../fixtures/data/diagramOperationsSpecData'
import { contentsOutcomes } from '../../../fixtures/data/outcomes_data/outcomesTestData'
import { globalPatternData } from '../../../fixtures/data/outcomes_data/globalPatternData'
import { skillTableData } from '../../../fixtures/data/outcomes_data/skillsData'
import { ProjectInfo } from '../../../types/response/ProjectInfo'

describe('table Global Patterns', function () {
  beforeEach(() => {
    cy.fixture('metrics-templates/sample-patterns.json').as('project')

    cy.get<ProjectInfo>('@project').then((json: ProjectInfo) => {
      cy.loginAPI()
      cy.createNewProject(json).then((projectInfo: ProjectInfo) => {
        cy.loginUI()
        Cypress.env('projectName', projectInfo.label)
        Cypress.env('projectId', projectInfo.id)
        NavigationBar.selectProject(projectInfo.label)
        // NavigationBar.openOutcomesPage()
      })
    })
  })

  afterEach(() => {
    cy.get<ProjectInfo>('@project').then((json: ProjectInfo) => {
      cy.removeProject(json.id)
    })
  })

  it('CMTS-16-create-new-global-interaction-pattern', { tags: '@smoke' }, () => {
    // get list of projected outcome from table and saved as array
    let itemsList = []
    cy.get('.metricsDataGrids')
      .find(OutcomesPageElements.leftContainer)
      .within(($list) => {
        cy.wrap($list)
          .get('[role="row"]')
          .find('b')
          .each(($item) => {
            itemsList.push($item.text())
          })
        cy.log(itemsList)
      })

    GlobalInteractionPatternTable.openCreateNewGlobalPattern()
    AddNewGlobalPatternPopover.fillGlobalPatternObj(globalPatternData.globalPattern)
    AddNewGlobalPatternPopover.clickOnAddBtn()
    GlobalInteractionPatternTable.checkGlobalPatternRow(globalPatternData.globalPattern)
    GlobalInteractionPatternTable.deleteRow(globalPatternData.globalPattern.name)
  })

  it('CMTS-9522-changing-nodes-count-on-table-global-pattern-if-add-delete-node', { retries: 0 }, () => {
    NavigationBar.openPatternsPage()
    FilterBar.selectPattern(globalPatternData.pattern9522)
    DiagramUtils.findDiagram()
    DiagramUtils.addNodeInDiagram(PaletteBox.subflowNodeIdSelector, 'Start', true)
    AddNewSubFlowNodePopUp.setLabel(dataObject.subflow.nodeLabel)
    AddNewSubFlowNodePopUp.clickOnAddBtn()
    NavigationBar.openOutcomesPage()
    OutcomesPageElements.checkNodeCount(OutcomesPageElements.patternTable, globalPatternData.pattern9522, '1') // check node count for particular pattern
    OutcomesPageElements.getDeleteBtn(OutcomesPageElements.patternTable, globalPatternData.pattern9522).should(
      'have.class',
      'deleteButtonDisabled',
    )
    NavigationBar.openPatternsPage()
    FilterBar.selectPattern(globalPatternData.pattern9522)
    DiagramUtils.findDiagram()
    DiagramUtils.click(dataObject.subflow.nodeLabel)
    DiagramBar.clickOnDeleteNodeBtn().clickOnYesBtn()
    NavigationBar.openOutcomesPage()
    OutcomesPageElements.checkNodeCount(OutcomesPageElements.patternTable, globalPatternData.pattern9522, '0') // check node count for particular pattern after deleting node
    OutcomesPageElements.getDeleteBtn(OutcomesPageElements.patternTable, globalPatternData.pattern9522).should(
      'have.class',
      'deleteButton',
    )
  })

  it('CMTS-17-edit-global-pattern', { tags: '@smoke' }, () => {
    NavigationBar.openOutcomesPage()
    GlobalInteractionPatternTable.editRow(globalPatternData.patternToEdit)
    AddNewGlobalPatternPopover.fillGlobalPatternObj(globalPatternData.PatternUpdated)
    AddNewGlobalPatternPopover.clickOnSaveBtn()
    cy.checkToastMessage(globalPatternData.patternUpdatedMsg) // check if toast message appears
    GlobalInteractionPatternTable.checkGlobalPatternRow(globalPatternData.PatternUpdated)
    GlobalInteractionPatternTable.deleteRow(globalPatternData.patternNameUpd) // delete edited  global pattern
  })

  it('CMTS-26-delete-global-pattern', { tags: '@smoke' }, () => {
    GlobalInteractionPatternTable.deleteRow(globalPatternData.patternToDelete) // delete pattern
    cy.checkToastMessage(globalPatternData.patternDeletedMsg) // check if message Deleted outcome appears
    GlobalInteractionPatternTable.checkPatternAbsence(globalPatternData.patternToDelete) // check if outcome is deleted
    /* cy.get(OutcomesPageElements.patternTable).find('.ag-overlay-no-rows-center').should('exist')
    .and('have.text', globalPatternData.noPatternMsg)
    */
  })

  describe('CMTS-13918 change pattern types', () => {
    it('[CMTS-11144] change  Global pattern to Fallback Pattern', { tags: '@smoke' }, () => {
      NavigationBar.openOutcomesPage()
      GlobalInteractionPatternTable.editRow(globalPatternData.GlobalToFallback.name) // open window Edit Pattern
      AddNewGlobalPatternPopover.setType('Fallback') // change pattern type
      AddNewGlobalPatternPopover.clickOnSaveBtn()
      cy.checkToastMessage(globalPatternData.patternUpdatedMsg) // check if toast message appears
      GlobalInteractionPatternTable.checkPatternAbsence(globalPatternData.GlobalToFallback.name)
      GlobalInteractionPatternTable.chooseTab('Escalate-Fallback') // switch to tab 'Escalate-Fallback'
      GlobalInteractionPatternTable.checkGlobalPatternRow(globalPatternData.GlobalToFallback) // check data
      NavigationBar.openPatternsPage()
      FilterBar.checkPatternDropdownSection(
        globalPatternData.escalateFallbackSection,
        globalPatternData.GlobalToFallback.name,
      ) // check appearance pattern name in section Escalate-Fallback in dropdown Pattern
    })

    it('[CMTS-11140] change Global pattern to Escalate Pattern', { tags: '@smoke' }, () => {
      NavigationBar.openOutcomesPage()
      GlobalInteractionPatternTable.editRow(globalPatternData.GlobalPatternToEscalate.name) // open window Edit Pattern
      AddNewGlobalPatternPopover.setType('Escalate') // change pattern type
      AddNewGlobalPatternPopover.clickOnSaveBtn()
      cy.checkToastMessage(globalPatternData.patternUpdatedMsg) // check if toast message appears
      GlobalInteractionPatternTable.checkPatternAbsence(globalPatternData.GlobalPatternToEscalate.name)
      GlobalInteractionPatternTable.chooseTab('Escalate-Fallback') // switch to tab 'Escalate-Fallback'
      GlobalInteractionPatternTable.checkGlobalPatternRow(globalPatternData.GlobalPatternToEscalate) // check data
      NavigationBar.openPatternsPage()
      FilterBar.checkPatternDropdownSection(
        globalPatternData.escalateFallbackSection,
        globalPatternData.GlobalPatternToEscalate.name,
      ) // check appearance pattern name in section Escalate-Fallback in dropdown Pattern
    })

    it('[CMTS-11190] change Fallback type of pattern to Outbound', { tags: '@smoke' }, () => {
      NavigationBar.openOutcomesPage()
      GlobalInteractionPatternTable.chooseTab(globalPatternData.escalateFallbackTab) // switch to tab 'Escalate-Fallback'
      GlobalInteractionPatternTable.editRow(globalPatternData.FallbackToGlobalPattern.name) // open window Edit Pattern
      AddNewGlobalPatternPopover.setType('Outbound') // change pattern type
      AddNewGlobalPatternPopover.clickOnSaveBtn()
      cy.checkToastMessage(globalPatternData.patternUpdatedMsg) // check if toast message appears
      GlobalInteractionPatternTable.checkPatternAbsence(globalPatternData.FallbackToGlobalPattern.name)
      GlobalInteractionPatternTable.chooseTab(globalPatternData.globalPatternTab) // switch to tab 'Global Patterns'
      GlobalInteractionPatternTable.checkGlobalPatternRow(globalPatternData.FallbackToGlobalPattern) // check data
      NavigationBar.openPatternsPage()
      FilterBar.checkPatternDropdownSection(
        globalPatternData.globalPatternSection,
        globalPatternData.FallbackToGlobalPattern.name,
      ) // check appearance pattern name in section global patterns in dropdown Pattern
    })

    it('[CMTS-11189] change Escalate type of pattern to Mobile App', { tags: '@smoke' }, () => {
      NavigationBar.openOutcomesPage()
      GlobalInteractionPatternTable.chooseTab(globalPatternData.escalateFallbackTab) // switch to tab 'Escalate-Fallback'
      GlobalInteractionPatternTable.editRow(globalPatternData.EscalateToGlobalPattern.name) // open window Edit Pattern
      AddNewGlobalPatternPopover.setType('Mobile App') // change pattern type
      AddNewGlobalPatternPopover.clickOnSaveBtn()
      cy.checkToastMessage(globalPatternData.patternUpdatedMsg) // check if toast message appears
      GlobalInteractionPatternTable.checkPatternAbsence(globalPatternData.EscalateToGlobalPattern.name)
      GlobalInteractionPatternTable.chooseTab(globalPatternData.globalPatternTab) // switch to tab 'Global Patterns'
      GlobalInteractionPatternTable.checkGlobalPatternRow(globalPatternData.EscalateToGlobalPattern) // check data
      NavigationBar.openPatternsPage()
      FilterBar.checkPatternDropdownSection(
        globalPatternData.globalPatternSection,
        globalPatternData.EscalateToGlobalPattern.name,
      ) // check appearance pattern name in section global patterns in dropdown Pattern
    })

    it('[CMTS-] change Global type of pattern to Skill Pattern', () => {
      NavigationBar.openOutcomesPage()
      GlobalInteractionPatternTable.chooseTab(globalPatternData.globalPatternTab)
      GlobalInteractionPatternTable.editRow(skillTableData.GlobalPatternToSkillPattern.skillPatternName) // open window Edit Pattern
      AddNewGlobalPatternPopover.setType(globalPatternData.skillPattern) // change pattern type
      AddNewGlobalPatternPopover.clickOnSaveBtn()
      cy.checkToastMessage(globalPatternData.patternUpdatedMsg) // check if toast message appears
      GlobalInteractionPatternTable.checkPatternAbsence(skillTableData.GlobalPatternToSkillPattern.skillPatternName)
      GlobalInteractionPatternTable.chooseTab(globalPatternData.escalateFallbackTab) // switch to tab 'Escalate-Fallback'
      GlobalInteractionPatternTable.checkPatternAbsence(skillTableData.GlobalPatternToSkillPattern.skillPatternName)
      SkillPatternTable.checkSkillPatternTable(skillTableData.GlobalPatternToSkillPattern) // check data
      NavigationBar.openPatternsPage()
      FilterBar.checkPatternDropdownSection(
        globalPatternData.skillPatternsection,
        skillTableData.GlobalPatternToSkillPattern.skillPatternName,
      ) // check appearance pattern name in section global patterns in dropdown Pattern
    })

    // WIP -  perhaps the option 'Skill Pattern' should not be available for global pattern
    it.skip('[CMTS-11080] change Escalate type of pattern to Skill Pattern', { tags: '@smoke' }, () => {
      NavigationBar.openOutcomesPage()
      GlobalInteractionPatternTable.chooseTab(globalPatternData.escalateFallbackTab) // switch to tab 'Escalate-Fallback'
      GlobalInteractionPatternTable.editRow(skillTableData.EscalateToSkillPattern.name) // open window Edit Pattern
      AddNewGlobalPatternPopover.setType(globalPatternData.skillPattern) // change pattern type
      AddNewGlobalPatternPopover.clickOnSaveBtn()
      cy.checkToastMessage(globalPatternData.patternUpdatedMsg) // check if toast message appears
      GlobalInteractionPatternTable.checkPatternAbsence(skillTableData.EscalateToSkillPattern.name)
      GlobalInteractionPatternTable.chooseTab(globalPatternData.globalPatternTab) // switch to tab Global Patterns
      GlobalInteractionPatternTable.checkPatternAbsence(skillTableData.EscalateToSkillPattern.name)
      SkillPatternTable.checkSkillPatternTable(skillTableData.EscalateToSkillPattern) // check data
      NavigationBar.openPatternsPage()
      FilterBar.checkPatternDropdownSection(
        globalPatternData.skillPatternsection,
        skillTableData.EscalateToSkillPattern.name,
      ) // check appearance pattern name in section global patterns in dropdown Pattern
    })
  })

  it('CMTS-138-add-test-for-reset-for-projected-global-interaction-pattern-add-edit', () => {
    // fill pop-up Create New Global Pattern
    GlobalInteractionPatternTable.openCreateNewGlobalPattern()
    AddNewGlobalPatternPopover.fillGlobalPatternObj(globalPatternData.globalPattern)
    AddNewGlobalPatternPopover.clickOnResetBtn() // reset fields on pop-up add projected outcome
    cy.get(OutcomesPageElements.globalPatternNameField).should('have.value', '')
    OutcomesPageElements.checkDefaultValue(OutcomesPageElements.typeSelect, ' Inbound ')
    cy.get(OutcomesPageElements.globalPatternDescriptionField).should('have.value', '')
    cy.get(OutcomesPageElements.globalPatternOutcomesSelect).should('have.value', '')
    cy.get(OutcomesPageElements.globalPatternChannelsSelect).should('have.value', '')
  })

  // eslint-disable-next-line prettier/prettier
  describe('CMTS-140-test-pack-for-global-interaction-patterns-validations',  { tags: '@validation' }, () => {
    it('CMTS-8448-to-validate-the-name-with-1-character-is-not-valid-pattern-s-name', () => {
      GlobalInteractionPatternTable.openCreateNewGlobalPattern() // open modal Create Global Pattern
      AddNewGlobalPatternPopover.checkValidationError(
        OutcomesPageElements.globalPatternNameLabel,
        contentsOutcomes.lengthLimitErrMsg,
        OutcomesPageElements.globalPatternNameField,
        contentsOutcomes.oneLetter,
        true,
      )
      GlobalInteractionPatternTable.openCreateNewGlobalPattern() // close modal Create Global Pattern
    })

    it('CMTS-8449-to-validate-the-name-with-33-character-is-not-valid-pattern-s-name', () => {
      GlobalInteractionPatternTable.openCreateNewGlobalPattern() // open modal Create Global Pattern
      AddNewGlobalPatternPopover.checkValidationError(
        OutcomesPageElements.globalPatternNameLabel,
        contentsOutcomes.lengthLimitErrMsg,
        OutcomesPageElements.globalPatternNameField,
        contentsOutcomes.oneLetter,
        true,
      )
      GlobalInteractionPatternTable.openCreateNewGlobalPattern() // close modal Create Global Pattern
    })

    it('check if all required validation messages appear after clicking on the button Add', () => {
      GlobalInteractionPatternTable.openCreateNewGlobalPattern() // open modal Create Global Pattern
      AddNewGlobalPatternPopover.clickOnAddBtn()
      AddNewGlobalPatternPopover.checkValidationError(
        OutcomesPageElements.globalPatternNameLabel,
        contentsOutcomes.emptyFieldErrMsg,
        OutcomesPageElements.globalPatternNameField,
        '',
        true,
      )
      OutcomesPageElements.checkValidationErrorSelect(
        OutcomesPageElements.patternChannelsLbl,
        contentsOutcomes.channelsRequiredErrMsg,
      )
      OutcomesPageElements.checkValidationErrorSelect(
        OutcomesPageElements.patternOutcomesLbl,
        contentsOutcomes.outcomesRequiredErrMsg,
      )
      GlobalInteractionPatternTable.openCreateNewGlobalPattern() // close modal Create Global Pattern
    })

    it('check if search by channels works', () => {
      GlobalInteractionPatternTable.openCreateNewGlobalPattern() // open modal Create Global Pattern
      OutcomesPageElements.checkSearchMultiselect(
        OutcomesPageElements.globalPatternChannelsSelect,
        'r',
        globalPatternData.channelsContainsR,
      )
      cy.get('.ant-select-dropdown-placement-bottomLeft').should('not.be.visible')
      GlobalInteractionPatternTable.openCreateNewGlobalPattern() // close modal Create Global Pattern
    })

    it('check if search by outcomes works', () => {
      GlobalInteractionPatternTable.openCreateNewGlobalPattern() // open modal Create Global Pattern
      OutcomesPageElements.checkSearchMultiselect(
        OutcomesPageElements.globalPatternOutcomesSelect,
        'A',
        globalPatternData.outcomesContainsA,
      )
      GlobalInteractionPatternTable.openCreateNewGlobalPattern() // close modal Create Global Pattern
    })

    it('check if image No Data appears if input data does not fit any channel', () => {
      GlobalInteractionPatternTable.openCreateNewGlobalPattern() // open modal Create Global Pattern
      cy.get(OutcomesPageElements.globalPatternChannelsSelect).type('aaa')
      cy.get('.ant-empty-image').should('exist')
      cy.get('.ant-popover-title').click()
      cy.get('.ant-empty-image').should('not.be.visible')
      GlobalInteractionPatternTable.openCreateNewGlobalPattern() // close modal Create Global Pattern
    })

    it('check if image No Data appears if input data does not fit any channel', () => {
      // open modal Create Global Pattern
      cy.get(OutcomesPageElements.openCreateNewGlobalPatternBtn).click()
      cy.get(OutcomesPageElements.globalPatternOutcomesSelect).type('aaa')
      cy.get('.ant-empty-image').should('exist')
      cy.get('.ant-popover-title').click()
      cy.get('.ant-empty-image').should('not.be.visible')
      GlobalInteractionPatternTable.openCreateNewGlobalPattern() // close modal Create Global Pattern
    })

    it('check if all validation messages disappear after clicking on the button Reset', () => {
      GlobalInteractionPatternTable.openCreateNewGlobalPattern() // open modal Create Global Pattern
      AddNewGlobalPatternPopover.clickOnAddBtn()
      AddNewGlobalPatternPopover.clickOnResetBtn()
      cy.get(OutcomesPageElements.validationMsg).should('not.exist')
      GlobalInteractionPatternTable.openCreateNewGlobalPattern() // close modal Create Global Pattern
    })

    it('CMTS-8450-to-validate-it-is-not-possible-to-create-the-pattern-with-pattern-s-name-which-is-already', () => {
      GlobalInteractionPatternTable.openCreateNewGlobalPattern() // open modal Create Global Pattern
      AddNewGlobalPatternPopover.checkValidationError(
        OutcomesPageElements.globalPatternNameLabel,
        globalPatternData.notUniqueErrMsg,
        OutcomesPageElements.globalPatternNameField,
        'TestUniquePattern',
        true,
      )
      GlobalInteractionPatternTable.openCreateNewGlobalPattern() // close modal Create Global Pattern
    })
  })
})
