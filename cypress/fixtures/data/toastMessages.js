// eslint-disable-next-line import/prefer-default-export
export const toastMessages = {
  projectedOutcome: {
    deleted: 'Deleted outcome',
  },

  globalPattern: {
    added: 'Successfully added pattern', // - not added yet
    updated: 'Successfully updated pattern',
    deleted: 'Successfully deleted pattern',
  },

  projectToast: {
    projectCreated: 'Project created',
    projectUpdated: 'Updated',
    projectDeleted: 'Deleted',
  },

  entityToast: {
    entityAdded: 'Saved New Entity',
    entityGroupAdded: 'Saved New Entity Group',
    entitySaved: 'Saved Entity',
    entityGroupSaved: 'Saved Entity Group',
    entityDeleted: 'Deleted Entity',
    entityGroupDeleted: 'Deleted Entity Group',
  },

  node: {
    deleted: 'Delete successful', // Node delete successful
  },

  flowTab: {
    added: 'OK',
  },

  outputTab: {
    edit: 'Saved!',
  },

  errors: {
    validationFailed: 'Validation Failed',
    failedToDelete: 'Failed to delete: Error: Request failed with status code 400',
    failedUpdateEntityGroup: 'Failed to save edited entity group:Error: Request failed with status code 400',
    failedUpdatePoject: 'Failed to update project from server!',
    failSavedPattern: 'Failed to save new pattern to database, please save before continuing! Error: Request failed with status code 400',
    canNotReadData: 'TypeError: Cannot read properties of undefined (reading \'data\')',
    userExits: 'Failed to create new user: Error while creating user User with email ID already exists.',
  },
}
