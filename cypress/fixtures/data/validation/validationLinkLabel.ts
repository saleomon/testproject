import { messages } from '../messagesErrorText'

// eslint-disable-next-line import/prefer-default-export
const LinkLabel: { title: string; input: string; message?: string; error: boolean }[] = [
  {
    title: 'Link Label - exisiting label',
    input: 'default',
    message: messages.existingLinkLabel,
    error: true,
  },
  {
    title: 'Link Label - 1 char',
    input: 'a',
    message: messages.labelLength,
    error: true,
  },
  {
    title: 'Link Label - 33 characters',
    input: 'morethan32morethan32morethan32mor',
    message: messages.labelLength,
    error: true,
  },
  {
    title: 'Link Label - space in name',
    input: 'space in it',
    message: messages.spacesAndUnderscores,
    error: true,
  },
  {
    title: 'Link Label - undersore_in_label',
    input: 'undersore_in_label',
    message: messages.spacesAndUnderscores,
    error: true,
  },
  {
    title: 'Link Label - 2 chars',
    input: 'aa',
    error: false,
  },
  {
    title: 'Link Label - 32 chars',
    input: 'longnamelongnamelongnamelongname',
    error: false,
  },
  {
    title: 'Link Label - special symbols',
    input: 'label"with&special//chars',
    message: messages.startWithLetter,
    error: true,
  },
  {
    title: 'Link Label - digits',
    input: '12345',
    error: false,
  },
  {
    title: 'Link Label - empty',
    input: '',
    message: messages.uniqueLinkLabel,
    error: true,
  },
]

export default LinkLabel
