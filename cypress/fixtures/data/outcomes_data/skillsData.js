/* eslint-disable import/prefer-default-export */
export const skillTableData = {
  GlobalPatternToSkillPattern: {
    skillPatternName: 'GlobalPatternToSkillPattern',
    outcomes: ['Escalate'],
    type: 'Mobile App',
    nodeCount: '0',
    skillPatternChannels: ['IVR', ' RWC'],
    skillPatternDescription: 'GlobalPatternToSkillPattern',
  },
  EscalateToSkillPattern: {
    name: 'EscalateToSkillPattern',
    outcomes: ['Escalate'],
    nodeCount: '0',
    channels: 'IVR',
    description: '',
  },
}
