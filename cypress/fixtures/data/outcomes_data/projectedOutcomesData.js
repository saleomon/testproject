/* eslint-disable import/prefer-default-export */
export const projectedOutcomesData = {
  projectedOutcomeNew: {
    projectedOutcomeLabel: 'TestProjectedOutcome',
    outcomeDescription: 'Description',
    outcomePercentage: '50',
    outcomeValue: 'Negative',
    outcomeColor: '#005E00',
  },
  projectedOutcomeUpdated: {
    projectedOutcomeLabel: 'NewProjectedOutcome',
    outcomeDescription: 'NewDescription',
    outcomePercentage: '35',
    outcomeValue: 'Positive',
    outcomeColor: '#FFDF66',
  },
  projectedOutcomeNoDescription: {
    projectedOutcomeLabel: 'ProjectedOutcomeNoDescription',
    outcomeDescription: '',
    outcomePercentage: '35',
    outcomeValue: 'Positive',
    outcomeColor: '#FFDF66',
  },
  colorSet: [
    ' #FFED8F ',
    ' #FFDF66 ',
    ' #005E00 ',
    ' #3ADC49 ',
    ' #FFBFCB ',
    ' #B22929 ',
    ' #EB4627 ',
    ' #F2A787 ',
    ' #8FBCFF ',
    ' #0095C9 ',
    ' #70B9E8 ',
    ' #003378 ',
    ' #C4C4C4 ',
  ],
}
