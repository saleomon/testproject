const contentsEntityEditor = {
  entity: {
    entityLabel: 'UserName',
    enitityDescription: 'Description',
    type: 'Date',
    formatType: 'Short Date',
    mockValue: 'John White',
  },

  entityUpdated: {
    entityLabel: 'EditedEntity',
    enitityDescription: 'Description Updated',
    type: ' String ',
    formatType: 'Formal Name',
    mockValue: 'Jack Black',
  },

  entityGroupNew: {
    entityGroupLabel: 'PII',
    enitityGroupDescription: 'Personal Identity Information',
    entityGroupPrimaryKey: 'EntityForEntityGroup',
    dependsOn: ['EntityForEntityGroup'],
  },

  entityGroupPIIDisabled: {
    entityGroupLabel: 'PII',
    enitityGroupDescription: 'Personal Identity Information',
    dependsOn: '',
  },

  entityGroupUpdated: {
    entityGroupLabel: 'Location',
    enitityGroupDescription: 'Location',
    entityGroupPrimaryKey: 'EntityUpdForEntityGroup',
    dependsOn: ['EntityForEntityGroup'],
  },

  dateFormatArray: [' Short Date ', ' Long Date ', ' Hour '],
  numberFormatArray: [' None ', ' Currency ', ' SSN ', ' Telephone '],
  stringFormatArray: [' None ', ' Formal Name '],

  // data objects to check tables info
  entityRow: {
    entityLabel: 'UserName',
    type: 'date',
    formatType: 'Short Date',
    description: 'Description',
    entityGroup: 'defaultEntityGroup',
    PII: 'true',
    Writable: 'true',
    mockValue: 'John White',
  },

  entityRowUpdated: {
    entityLabel: 'EditedEntity',
    type: 'string',
    formatType: 'Formal Name',
    description: 'Description Updated',
    entityGroup: 'defaultEntityGroup',
    PII: 'false',
    Writable: 'false',
    mockValue: 'Jack Black',
  },

  entityGroupRow: {
    entityGroupLabel: 'PII',
    entityList: '',
    primaryKey: 'EntityForEntityGroup',
    dependsOn: 'EntityForEntityGroup',
    sharedEndpoint: 'true',
    // namespace:
    enitityGroupDescription: 'Personal Identity Information',
  },

  entityGroupRowUpdated: {
    entityGroupLabel: 'Location',
    entityList: '',
    primaryKey: 'EntityUpdForEntityGroup',
    dependsOn: 'EntityForEntityGroup',
    sharedEndpoint: 'false',
    // namespace:
    enitityGroupDescription: 'Location',
  },


  /*  entityGroupLabel: 'PII',
  enitityGroupDescription: 'Personal Identity Information',
  entityGroupLabelUpd: 'Location',
  enitityGroupDescriptionUpd: 'Location',
  entityGroupModal: 'Edit Outcome',
  entityGroupPrimaryKey: 'EntityForEntityGroup',
  entityGroupPrimaryKeyUpd: 'EntityUpdForEntityGroup',
  dependsOn: ['EntityForEntityGroup'], */
}

// eslint-disable-next-line import/prefer-default-export
export { contentsEntityEditor }
