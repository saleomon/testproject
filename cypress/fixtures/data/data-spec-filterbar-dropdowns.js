// eslint-disable-next-line import/prefer-default-export
export const dataObject = {
  subflow1: {
    nodeType: 'subFlow',
    nodeLabel: 'First',
    nodeDescription: 'RWC channel only',
    linkLabel: 'default',
    linkDescription: ' ',
    channels: ['RWC'],
  },
  subflow2: {
    nodeType: 'subFlow',
    nodeLabel: 'Second',
    nodeDescription: 'IVR channel only',
    linkLabel: 'default',
    linkDescription: ' ',
    channels: ['IVR'],
  },
}
