let linkSubflowCharacter: {
  title: string
  linkLabel: string
  linkType: string
  sideText: string
  conditionText: string
  preconditionId: string
  pattern: string
}[] = [
  {
    title: '[CMTS-14637] Exit can be added when link label contains 2 characters',
    preconditionId: 'add-endpoint',
    pattern: '14533',
    linkLabel: 'ab',
    linkType: ' Normal (down) ',
    sideText: 'down',
    conditionText: 'escalate',
  },
  {
    title: '[CMTS-14638] Exit can be added when link label contains 32 characters',
    preconditionId: 'add-endpoint',
    pattern: `14533`,
    linkLabel: 'AbcdefghijklMNopqrstuVwXyzabcdeF',
    linkType: ' Normal (down) ',
    sideText: 'down',
    conditionText: 'escalate',
  },
  {
    title: '[CMTS-14639] Exit can be added when link label contains letters and digits',
    preconditionId: 'add-endpoint',
    pattern: `14533`,
    linkLabel: 'A1B2csd3',
    linkType: ' Normal (down) ',
    sideText: 'down',
    conditionText: 'escalate',
  },
]

export default linkSubflowCharacter
