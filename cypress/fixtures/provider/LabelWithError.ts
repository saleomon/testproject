let labelWithError: {
  title: string
  linkLabel: string
  linkType: string
  preconditionId: string
  pattern: string
  error: string
}[] = [
  {
    title: '[CMTS-14645] Exit cannot be added when a link label contains 1 character',
    preconditionId: 'add-endpoint',
    pattern: `14533`,
    linkLabel: 'a',
    linkType: ' Normal (down) ',
    error: 'Length should be 2 to 32 chars',
  },
  {
    title: '[CMTS-14647] Exit cannot be added when a link label contains 33 characters',
    preconditionId: 'add-endpoint',
    pattern: `14533`,
    linkLabel: 'qqqgsadasdasdasdasgsadasdasdasdas',
    linkType: ' Normal (down) ',
    error: 'Length should be 2 to 32 chars',
  },
  {
    title: '[CMTS-14653] Exit cannot be added when a link label contains a space at the end',
    preconditionId: 'add-endpoint',
    pattern: `14533`,
    linkLabel: 'ab ',
    linkType: ' Normal (down) ',
    error: 'Label must not contain spaces.',
  },
  {
    title: '[CMTS-14650] Exit cannot be added when a link label contains special characters',
    preconditionId: 'add-endpoint',
    pattern: `14533`,
    linkLabel: 'ab@#%^&',
    linkType: ' Normal (down) ',
    error: 'Label must start with a letter and must not contain special characters.',
  },
  {
    title: '[CMTS-14651] Exit cannot be added when a link label starts with a digit',
    preconditionId: 'add-endpoint',
    pattern: `14533`,
    linkLabel: '123ab',
    linkType: ' Normal (down) ',
    error: 'Label must start with a letter and must not contain special characters.',
  },
  {
    title: '[CMTS-14652] Exit cannot be added when a link label contains a space in the middle',
    preconditionId: 'add-endpoint',
    pattern: `14533`,
    linkLabel: 'ab ab',
    linkType: ' Normal (down) ',
    error: 'Label must not contain spaces.',
  },
  {
    title: '[CMTS-14684] Exit cannot be added when a link label contains an underscore at the end',
    preconditionId: 'add-endpoint',
    pattern: `14533`,
    linkLabel: 'ab_',
    linkType: ' Normal (down) ',
    error: 'Label must not contain spaces.',
  },
  {
    title: '[CMTS-14681] Exit cannot be added when a link label contains an underscore in the middle',
    preconditionId: 'add-endpoint',
    pattern: `14533`,
    linkLabel: 'ab_cde',
    linkType: ' Normal (down) ',
    error: 'Label must not contain spaces or underscores.',
  },
  {
    title: '[CMTS-14648] Exit cannot be added when a link label contains symbols',
    preconditionId: 'add-endpoint',
    pattern: `14533`,
    linkLabel: 'ab€£¥©™',
    linkType: ' Normal (down) ',
    error: 'Label must start with a letter and must not contain special characters.',
  },
]

export default labelWithError
