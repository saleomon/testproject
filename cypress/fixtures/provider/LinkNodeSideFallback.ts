let linkNodeSideFallback: {
  title: string
  linkLabel: string
  linkType: string
  sideText: string
  conditionText: string
  preconditionId: string
}[] = [
  {
    title: '[CMTS-13964] Global Fallback outcome can be added with Normal (down) link type',
    preconditionId: '13964',
    linkLabel: 'Down',
    linkType: ' Normal (down) ',
    sideText: 'down',
    conditionText: 'Fallback',
  },
  {
    title: '[CMTS-13966] Global Fallback outcome can be added with Abandonment (left) link type',
    preconditionId: '13966',
    linkLabel: 'Left',
    linkType: 'Abandonment (left)',
    sideText: 'left',
    conditionText: 'Fallback',
  },
  {
    title: '[CMTS-13967] Global Fallback outcome can be added with Escalate to HITL (right) link type',
    preconditionId: '13967',
    linkLabel: 'Right',
    linkType: 'Escalate to HitL (right)',
    sideText: 'right',
    conditionText: 'Fallback',
  },
]

export default linkNodeSideFallback
