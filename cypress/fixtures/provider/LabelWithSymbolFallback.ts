let labelWithSymbolFallback: {
  title: string
  linkLabel: string
  linkType: string
  preconditionId: string
}[] = [
  {
    title: '[CMTS-13987] Global Fallback outcome cannot be added when a link label contains symbols',
    preconditionId: '13987',
    linkLabel: 'ab€£¥©™',
    linkType: ' Normal (down) ',
  },
  {
    title: '[CMTS-13989] Global Fallback outcome cannot be added when a link label contains special characters',
    preconditionId: '13989',
    linkLabel: 'ab_@#%^&',
    linkType: ' Normal (down) ',
  },
]

export default labelWithSymbolFallback
