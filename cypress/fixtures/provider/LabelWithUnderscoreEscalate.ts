let labelWithUnderscoreEscalate: {
  title: string
  linkLabel: string
  linkType: string
  preconditionId: string
}[] = [
  {
    title: '[CMTS-13939] Global Escalate outcome cannot be added when a link label contains an underscore at the end',
    preconditionId: '13939',
    linkLabel: 'ab_',
    linkType: ' Normal (down) ',
  },
  {
    title:
      '[CMTS-13938] Global Escalate outcome cannot be added when a link label contains an underscore in the middle',
    preconditionId: '13938',
    linkLabel: 'abSS_D123',
    linkType: ' Normal (down) ',
  },
]

export default labelWithUnderscoreEscalate
