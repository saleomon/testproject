const linkNodeSideEscalate: {
  title: string
  linkLabel: string
  linkType: string
  sideText: string
  conditionText: string
  preconditionId: string
}[] = [
  {
    title: '[CMTS-13812] Global Escalate outcome can be added with Normal (down) link type',
    preconditionId: '13812',
    linkLabel: 'Down',
    linkType: ' Normal (down) ',
    sideText: 'down',
    conditionText: 'Escalate',
  },
  {
    title: '[CMTS-13840] Global Escalate outcome can be added with Abandonment (left) link type',
    preconditionId: '13840',
    linkLabel: 'Left',
    linkType: 'Abandonment (left)',
    sideText: 'left',
    conditionText: 'Escalate',
  },
  {
    title: '[CMTS-13933] Global Escalate outcome can be added with Escalate to HitL (right) link type',
    preconditionId: '13933',
    linkLabel: 'Right',
    linkType: 'Escalate to HitL (right)',
    sideText: 'right',
    conditionText: 'Escalate',
  },
]

export default linkNodeSideEscalate
