let labelWithErrorFallback: {
  title: string
  linkLabel: string
  linkType: string
  preconditionId: string
  error: string
}[] = [
  {
    title: '[CMTS-13992] Global Fallback outcome cannot be added when a link label contains a space at the end',
    preconditionId: '13992',
    linkLabel: 'ab ',
    linkType: ' Normal (down) ',
    error: 'Label must not contain spaces.',
  },
  {
    title: '[CMTS-13977] Global Fallback outcome cannot be added without a link label',
    preconditionId: '13977',
    linkLabel: '',
    linkType: ' Normal (down) ',
    error: 'Each Link Label must be uniquely named.',
  },
  {
    title: '[CMTS-13990] Global Fallback outcome cannot be added when a link label starts with a digit',
    preconditionId: '13990',
    linkLabel: '123ab',
    linkType: ' Normal (down) ',
    error: 'Label must not contain spaces.',
  },
  {
    title: '[CMTS-13991] Global Fallback outcome cannot be added when a link label contains a space in the middle',
    preconditionId: '13991',
    linkLabel: 'ab ab',
    linkType: ' Normal (down) ',
    error: 'Label must not contain spaces.',
  },
  {
    title: '[CMTS-13985] Global Fallback outcome cannot be added when a link label contains 1 character',
    preconditionId: '13985',
    linkLabel: 'a',
    linkType: ' Normal (down) ',
    error: 'Length should be 2 to 32 chars',
  },
  {
    title: '[CMTS-13986] Global Fallback outcome cannot be added when a link label contains 33 characters',
    preconditionId: '13986',
    linkLabel: 'qqqgsadasdasdasdasgsadasdasdasdas',
    linkType: ' Normal (down) ',
    error: 'Length should be 2 to 32 chars',
  },
]

export default labelWithErrorFallback
