let labelWithSymbolEscalate: {
  title: string
  linkLabel: string
  linkType: string
  preconditionId: string
}[] = [
  {
    title: '[CMTS-13932] Global Escalate outcome cannot be added when a link label contains symbols',
    preconditionId: '13932',
    linkLabel: 'ab€£¥©™',
    linkType: ' Normal (down) ',
  },
  {
    title: '[CMTS-13933] Global Escalate outcome cannot be added when a link label contains special characters',
    preconditionId: '13933',
    linkLabel: 'ab_@#%^&',
    linkType: ' Normal (down) ',
  },
]

export default labelWithSymbolEscalate
