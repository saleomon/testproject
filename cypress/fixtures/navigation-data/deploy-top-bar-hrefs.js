// eslint-disable-next-line import/prefer-default-export
export const DeployTopBarHrefs = {
  mapper: '/deploy/mapper',
  requirements: '/deploy/requirements',
  designRequirements: '/deploy/designrequirements',
}
