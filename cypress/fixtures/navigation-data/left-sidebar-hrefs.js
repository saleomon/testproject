// eslint-disable-next-line import/prefer-default-export
export const LeftSidebarHrefs = {
  discovery: '/discovery/corpus',
  designer: '/designer/metrics',
  models: '/models/',
  deploy: '/deploy/mapper',
  analytics: '/analytics/performance',
  controlPanel: '/control-panel/projects',
}
