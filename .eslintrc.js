let sharedConfig = {
  'no-var': ['error'],
  'no-plusplus': ['off'],
  'func-names': ['off'],
  'no-else-return': ['off'],
  'prefer-const': ['off'],
  'spaced-comment': ['error', 'always'],
  'prefer-template': ['off'],
  'no-param-reassign': ['off'],
  'import/no-named-as-default': ['off'],
  '@typescript-eslint/no-unsafe-argument': ['off'],
  '@typescript-eslint/no-unsafe-assignment': ['off'],
  '@typescript-eslint/restrict-plus-operands': ['off'],
  '@typescript-eslint/no-unsafe-member-access': ['off'],
  '@typescript-eslint/no-namespace': ['off'],
  '@typescript-eslint/ban-ts-comment': ['off', { 'ts-expect-error': 'allow-with-description' }],
  '@typescript-eslint/no-inferrable-types': ['off'],
  'import/no-unresolved': 'off',
}

module.exports = {
  root: true,
  env: {
    node: true,
    es6: true,
    'cypress/globals': true,
  },
  globals: {
    cy: true,
    Cypress: true,
  },
  parser: '@babel/eslint-parser',
  parserOptions: {
    sourceType: 'module',
  },
  plugins: ['cypress'],
  extends: [
    'airbnb-base',
    'eslint:recommended',
    'plugin:import/recommended',
    'plugin:prettier/recommended',
    'plugin:cypress/recommended',
  ],
  rules: {
    ...sharedConfig,
  },
  overrides: [
    {
      files: ['**/*.ts', '**/*.tsx'],
      env: {
        node: true,
        es6: true,
        'cypress/globals': true,
      },
      globals: {
        cy: true,
        Cypress: true,
      },
      parser: '@typescript-eslint/parser',
      parserOptions: {
        sourceType: 'module',
        tsconfigRootDir: __dirname,
        project: './tsconfig.json',
      },
      plugins: ['cypress'],
      extends: [
        'airbnb-base',
        'airbnb-typescript/base',
        'eslint:recommended',
        'plugin:import/recommended',
        'plugin:import/typescript',
        'plugin:prettier/recommended',
        'plugin:cypress/recommended',
        'plugin:@typescript-eslint/recommended',
        'plugin:@typescript-eslint/recommended-requiring-type-checking',
        'plugin:import/recommended',
        'plugin:import/typescript',
      ],
      rules: {
        ...sharedConfig,
        'cypress/no-unnecessary-waiting': 'error',
      },
    },
  ],
}
